#include <stdint.h>

static void putuintA(uint32_t i) {
	*((volatile uint32_t *)0x0FFFFFF0) = i;
}//N1
static void putuintB(uint32_t k) {
	*((volatile uint32_t *)0x0FFFFFF4) = k;
}//N2


static void putuint(uint32_t i) {
	*((volatile uint32_t *)0x10000000) = i;
}

//Funcion factorial para ejercicio 1
int factorial(int n)
{
  if (n == 0)
    return 1;
  else
    return(n * factorial(n-1));
}



void main() {
	uint32_t a= 5;		//Valores para ejercicio 2
	uint32_t b = 4;		//Valores para ejercicio 2

	const uint32_t limit = 1000000;
	
	
	while (1) {

		putuintA(a);	//Valores se guardan en registros para ejercicio 2
		putuintB(b);	//Valores se guardan en registros para ejercicio 2


		//putuint(factorial(3));		//Se utiliza la funcion factorial y se envia el dato al registro correspondiente para ejercicio1

		//putuintA(a);					//NUmero al cual se le quiere calcular el factorial para ejercicio 5
		//putuintB(1);					//Registro de control ej5

		
		
		
	}

}

 
