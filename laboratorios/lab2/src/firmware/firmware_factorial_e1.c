#include <stdint.h>

static void putuint(uint32_t i) {
	*((volatile uint32_t *)0x10000000) = i;
}
int factorial(int n)
{
  if (n == 0)
    return 1;
  else
    return(n * factorial(n-1));
}

void main() {
	uint32_t number_to_display = 0;
	uint32_t counter = 0;
	const uint32_t limit = 1000000;
	
	
	while (1) {
		putuint(factorial(3));
		counter = 0;

		while (counter < limit) {
			counter++;
		}
		
		putuint(factorial(4));
		counter = 0;

		while (counter < limit) {
			counter++;
		}
		putuint(factorial(5));
		counter = 0;

		while (counter < limit) {
			counter++;
		}
	}

}

 
