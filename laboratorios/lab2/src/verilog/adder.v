
module adder #(
	parameter WIDTH = 16
)(
	input  	   [WIDTH-1:0]   A,
	input  	   [WIDTH-1:0]   B,
	output reg [WIDTH:0]   sum
);

   always@(*) begin
      sum = A + B;
   end

endmodule // adder_c