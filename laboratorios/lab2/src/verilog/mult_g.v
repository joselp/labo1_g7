

module mult_g #(
	parameter WIDTH = 8
)(
	input [WIDTH-1:0] 	   A,
	input [WIDTH-1:0] 	   B,
	output [(2*WIDTH)-1:0] mult 
);

   wire [WIDTH-1:0] 	aux1       [WIDTH-2:0];
   wire [WIDTH:0] 	aux2       [WIDTH-1:0];
   //      COLS ,                  ROWS

   genvar 				   i;
   generate
      for (i=0; i<(WIDTH-1); i=i+1) begin :  genmult
         if(i == 0) begin
            assign aux1[0]    = (B[0]) ? (A>>1):0; 
            assign aux2[0]    = (B[1]) ? A:0;
            assign mult[0]    =  A[0] & B[0];
            adder #(
               .WIDTH (WIDTH)
            ) add (
               .A(aux1[0]),
               .B(aux2[0]),
               .sum(aux2[1])
            );
         end else begin
            assign aux1[i]    = (B[i+1]) ? A:0; 
            assign mult[i]    = aux2[i][0];
            adder #(
               .WIDTH (WIDTH)
            ) add (
               .A(aux1[i]),
               .B(aux2[i][WIDTH:1]),
               .sum(aux2[i+1])
            );
         end
      end
      assign mult[(2*WIDTH)-1:WIDTH-1] = aux2[WIDTH-1];   
   endgenerate
   
   
endmodule
