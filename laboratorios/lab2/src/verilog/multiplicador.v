module multiplicador(
    input 		     [3:0]	        A,
    input            [3:0]          B,
    output           [7:0]   		Out
);
    wire wcarry1;
    wire wcarry2;
    wire wcarry3;
    wire wcarry4;
    wire wcarry5;
    wire wcarry6;
    
    
    assign Out[0]=A[0] & B[0];
    assign {wcarry1, Out[1]}=(A[1] & B[0]) + (A[0] & B[1]);
    assign {wcarry2, Out[2]}= wcarry1 + (A[2] & B[0]) + (A[1] & B[1]) + (A[0] & B[2]);
    assign {wcarry3, Out[3]}= wcarry2 + (A[3] & B[0]) + (A[2] & B[1]) + (A[1] & B[2]) + (A[0] & B[3]);
    assign {wcarry4, Out[4]}= wcarry3 + (A[3] & B[1]) + (A[2] & B[2]) + (A[1] & B[3]);
    assign {wcarry5, Out[5]}= wcarry4 + (A[3] & B[2]) + (A[2] & B[3]);
    assign {wcarry6, Out[6]}= wcarry5 + (A[3] & B[3]);
    assign Out[7]=wcarry6;
    
    

endmodule


