module system (
	input            clk,
	input            resetn,
	output           trap,
	output reg [7:0] out_byte,
	output reg       out_byte_en,
	output reg [7:0] Anode_Activate,
	output reg [6:0] LED_out
);
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 1;

	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 4096;

    reg [16:0] A;
    reg [16:0] B;
    wire [31:0] res;
    
	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;

	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;
	
    reg [6:0] LED_0;
    reg [6:0] LED_1;
    reg [6:0] LED_2;
    reg [7:0] active_leds;
    reg [7:0] res_Sec;
    
    reg [15:0]ResultadoFactorial_reg;
    wire [15:0]ResultadoFactorial_wire;
    reg Control;
    reg Status_reg;
    wire Status_wire;
    reg [3:0] NumeroFactorial;
    
	picorv32 picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);

	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("/home/cris2697/inicio-ie424/src/firmware/firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			mem_ready <= 1;
			out_byte_en <= 0;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			/*
			if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
				out_byte_en <= 1;
				out_byte <= mem_la_wdata;
			end
			
			if (mem_la_write && mem_la_addr == 32'h0FFF_FFF0) begin
				A <= mem_la_wdata;                                              //Para ejercicio 2
			end
			if (mem_la_write && mem_la_addr == 32'h0FFF_FFF4) begin
				B <= mem_la_wdata;
			end
			
			if (mem_la_read && mem_la_addr == 32'h0FFF_FFF8) begin
				mem_rdata <= res;
			end
			*/
			/*
			
			if (mem_la_write && mem_la_addr == 32'h0FFF_FFF0) begin  //Para erecicio 5//Se lee desde nuestra perspectiva el valor del numero a calcularle el factorial
				A <= mem_la_wdata;
			end
			
			if (mem_la_write && mem_la_addr == 32'h0FFF_FFF4) begin  //Para erecicio 5//Se lee desde nuestra perspectiva el registro de control
				Control <= mem_la_wdata;
			end
			
			
			if (mem_la_read && mem_la_addr == 32'h0FFF_FFF8) begin//Para ejercicio 5//Se escribe desde nuestra perspectiva el resultado de factorial
				mem_rdata <= ResultadoFactorial_reg;
			end
			
			if (mem_la_read && mem_la_addr == 32'h0FFF_FFFC) begin//Para ejercicio 5//Se escribe desde nuestra perspectiva el status
				mem_rdata <= Status_reg;
			end
			*/
	/*wire ResultadoFactorial;
    wire Control;
    wire Status;
    wire NumeroFactorial;*/		
			
			
		      end
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			out_byte_en <= 0;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
					out_byte_en <= 1;
					out_byte <= mem_wdata;
					
					mem_ready <= 1;
				end
			endcase
			
		end
	end endgenerate
	
	
	wire [7:0] anode;
	wire [6:0] led;
   /*
    mult_g #(
        .WIDTH(16)
    )mult_g1(
        .A      (A),
        .B      (B),
        .mult   (res)
    );
    */
    // Ejercicio 4
    
    mult_lut_16b #(
        .WIDTH   (16)
    ) mult_lut (
        .A    (A),
        .B    (B),
        .mult (res)
    );
    

    //Ejercicio 2
    /*
    multiplicador multiplicador(
    .A      (A),
    .B      (B),
    .Out    (res)
    );
    */
    
    /**
    //Ejercicio5
    factorial factorial(
    .numero      (A),
    .clk            (clk),
    .reset          (resetn),
    .begin_         (Control),
    .resultado      (ResultadoFactorial_wire),
    .finish          (Status_wire)
    );
	*/
    
    
    reg [15:0] Resultado_Listo;
	seven_segment_controller seven_controller (
	    .reset              (~resetn),
        .clk                (clk),
        .displayed_number   (res),        
        .Anode_Activate     (anode),
        .LED_out            (led)
	);
	
	always @(posedge clk) begin
	   if (~resetn) begin
	       Anode_Activate         <= 0;
	       LED_out                <= 0;
	       A                      <= 450;
	       B                      <= 86;   
	       Resultado_Listo        <= 0;   
	       ResultadoFactorial_reg <=0;
	       Status_reg <=0;
	   end else begin;
	  	   Anode_Activate <= anode;
	       LED_out        <= led;
	       A                      <= 45;
	       B                      <= 72;
	       ResultadoFactorial_reg<=ResultadoFactorial_wire;
	       Status_reg<=Status_wire;
	       if(Status_reg)
	       begin
	           Resultado_Listo       <= ResultadoFactorial_reg;   
            end
	   end  
	end
	
endmodule
