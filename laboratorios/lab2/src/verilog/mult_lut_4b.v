
`ifndef MULT_LUT_4B_C
 `define MULT_LUT_4B_C


module mult_lut_4b #(
    parameter WIDTH = 16
)(
	input      [WIDTH-1:0] 	   A,
	input      [3:0] 		   B,
	output reg [(2*WIDTH)-1:0] mult
);
    wire [(2*WIDTH)-1:0] res_0, res_1;

    lut #(
        .WIDTH          (WIDTH)
	) lut_0(
		.A              (A),
		.B              (B[1:0]),
		.mult           (res_0)
	);
   
    lut #(
        .WIDTH          (WIDTH)
	) lut_1(
		.A              (A),
		.B              (B[3:2]),
		.mult           (res_1)
	);

    always @(*) begin
        mult = (res_1 << 2) + res_0;
    end
   
endmodule
`endif // MULT_LUT_16B_C
// Local Variables:
// verilog-library-directories:("." "../tester" "../synth" "../testbench"):
// End:
