`ifndef LUT_C
 `define LUT_C
module lut #(
	parameter WIDTH = 16
)(
	input      [WIDTH-1:0]       A,
	input      [1:0] 	         B,
	output reg [(2*WIDTH)-1:0]   mult
);

   always @(*) begin
      case (B)
        2'b00  :   mult = 0;
        2'b01  :   mult = A;
        2'b10  :   mult = A << 1;
        2'b11  :   mult = (A<< 1) + A;
        default:   mult = 0;
      endcase
   end
   
endmodule
`endif // ADDER
// Local Variables:
// verilog-library-directories:("." "../tester" "../synth" "../testbench"):
// End:
