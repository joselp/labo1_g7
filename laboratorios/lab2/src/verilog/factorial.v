
module factorial(
    input 		     [3:0]          numero, 			
    input                           clk,
    input                           reset,
    input                           begin_,
    output     reg  [15:0]		    resultado,
    output     reg                  finish
);


    reg [7:0] count;
    reg [7:0] realimentacion;
    reg [7:0] Multiplicando_A;
    wire [15:0] res;
    
    
    always@(numero)
    begin
        count=1;
        Multiplicando_A=numero;
    end
    always@(realimentacion)
    begin
        Multiplicando_A=realimentacion;
    end
    always@(posedge clk or posedge reset )
    begin
        if(!reset)
        begin
            count<=1;
            resultado<=1;
            finish<=0;
            realimentacion<=1;
        end
        else if(!finish)
        begin
            if(begin_)
            begin
                realimentacion<=res;
                resultado<=res;
                count<=count+1;
                
                if(count==numero-1)
                begin
                    finish<=1;
                end
                else
                begin
                    finish<=0;
                end
            end
            else
            begin
                count<=1;
                resultado<=1;
                finish<=0;
                realimentacion<=1;
            end
        end
        else
        begin
            count<=count;
            resultado<=resultado;
            finish<=1;
            realimentacion<=1;
        end
       
    end




    mult_g multiplicador_16b(
    .A      (Multiplicando_A),
    .B      (count),
    .mult    (res)
    );
    

    
  
endmodule