`ifndef MULT_LUT_16B_C
 `define MULT_LUT_16B_C
 
module mult_lut_16b #(
	parameter WIDTH = 16
)(
	input      [WIDTH-1:0] 	   A,
	input      [WIDTH-1:0] 	   B,
	output reg [(2*WIDTH)-1:0] mult
);
   wire [(2*WIDTH)-1:0] res_0, res_1, res_2, res_3;

    mult_lut_4b #(
		.WIDTH   (WIDTH)
	) mult_0(
		.A       (A),
		.B       (B[3:0]),
		.mult    (res_0)
    );

    mult_lut_4b #(
		.WIDTH   (WIDTH)
	) mult_1(
		.A       (A),
		.B       (B[7:4]),
		.mult    (res_1)
	);

    mult_lut_4b #(
		.WIDTH   (WIDTH)
    ) mult_2(
	    .A       (A),
	    .B       (B[11:8]),
	    .mult    (res_2)
	);

    mult_lut_4b #(
		.WIDTH   (WIDTH)
	) mult_3(
	    .A       (A),
	    .B       (B[15:12]),
	    .mult    (res_3)
	);

    always @(*) begin
        mult = (((res_3 << 4) + res_2) << 8) + ( res_1 << 4) + res_0 ;
    end
   
endmodule
`endif // zMULT_LUT_16B_C
// Local Variables:
// verilog-library-directories:("." "../tester" "../synth" "../testbench"):
// End:
