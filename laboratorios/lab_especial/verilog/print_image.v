module print_image (
    input      [15:0] column,
    input      [15:0] row,
    input      [11:0] data_mem,
    input             visible,
    input             tik,
    input             resetn,
    output reg [9:0]  addr_mem,
    output reg [4:0]  offset,
    output reg [3:0]  red,
    output reg [3:0]  green,
    output reg [3:0]  blue
);
    parameter SIZE = 8;
    reg [4:0] column_out;
    reg [4:0] row_out;
    reg [4:0] suma;
    always @(posedge tik or negedge resetn) begin
        if(!resetn)
        begin
            offset<=0;
        end
        else 
        begin
            offset<=offset-1;
        end
    end
    always @(*) begin
        column_out = column/20;
        row_out    = row/15;  
        suma=row_out+offset;

        
        addr_mem   = {suma,column_out};

        if(!visible)
        begin
            red   =     0;
            green =     0;
            blue  =     0;
        end
        else
        begin
            red   = data_mem[11:8];
            green = data_mem[7:4];
            blue  = data_mem[3:0];
        end
    end    

endmodule