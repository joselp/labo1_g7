module cuadro_color(
    input 		               clk, 	
    input 		               reset,
   // input                      tik,
    input     [4:0]            columna_in,
    input     [4:0 ]           fila_in,
    input                        tik,
    input     [11:0]           color_in,
    input                      visible,
    input 		               up, 	
    input      [4:0]                offset,
    input 		               down, 	
    input 		               left, 	
    input 		               rigth,
    input                      w0,
    input                      w1,
    input                      w2,		
    output     reg   [11:0]    color_out,
    output reg fin
);
    wire [3:0] bus_botones;
    assign bus_botones={up,down,left,rigth};
    
    //Cuadro RojoAAAAAAAAAAAAAAAaaaaaaaa
    //contadores fila columna donde se ubica el cuadro rojo
    reg [4:0] fila;
    reg [4:0] columna;
    reg [4:0] filaw0;
    reg [4:0] columnaw0;
    reg [4:0] filaw1;
    reg [4:0] columnaw1;
    reg [4:0] filaw2;
    reg [4:0] columnaw2;
    reg [4:0] aux_fila;
    reg [4:0] aux_columna;
    reg [11:0] color_predeterminado;
    reg cambio_hecho;
    
    reg w0_ant;
    reg w1_ant;
    reg w2_ant;
    reg w0_tik;
    reg w1_tik;
    reg w2_tik;
    

    reg [4:0] filam1;
    reg [4:0] filam2;
    reg [4:0] filam3;
    reg [4:0] filam4;
    reg [4:0] filaw0m1;
    reg [4:0] filaw0m2;
    reg [4:0] filaw0m3;
    reg [4:0] filaw0m4;
    reg [4:0] filaw1m1;
    reg [4:0] filaw1m2;
    reg [4:0] filaw1m3;
    reg [4:0] filaw1m4;
    reg [4:0] filaw2m1;
    reg [4:0] filaw2m2;
    reg [4:0] filaw2m3;
    reg [4:0] filaw2m4;
    always@(*)begin
        filam1=fila+1;
        filam2=fila+2;
        filam3=fila+3;
        filam4=fila+4;
        
        filaw0m1=filaw0+1;
        filaw0m2=filaw0+2;
        filaw0m3=filaw0+3;
        filaw0m4=filaw0+4;
        
        filaw1m1=filaw1+1;
        filaw1m2=filaw1+2;
        filaw1m3=filaw1+3;
        filaw1m4=filaw1+4;
        
        filaw2m1=filaw2+1;
        filaw2m2=filaw2+2;
        filaw2m3=filaw2+3;
        filaw2m4=filaw2+4;
    end
    
    always@(posedge clk)
    begin
        if(!reset )
        begin
            fin<=0;
            fila<= 25;
            columna   <= 14;
            w0_tik <= 0;
            w1_tik <= 0;
            w2_tik <= 0;
            w0_ant <= 0;
            w1_ant <= 0;
            w2_ant <= 0;
            color_predeterminado<=12'h8a0;
            color_out<=0;
            cambio_hecho<=0;
        end
        else
        begin
           w0_ant <= w0;
           if(~w0_ant && w0) begin
                w0_tik <= 1;
           end else begin
                w0_tik <= 0;
           end

           w1_ant <= w1;
           if(~w1_ant && w1) begin
                w1_tik <= 1;
           end else begin
                w1_tik <= 0;
           end

           w2_ant <= w2;
           if(~w2_ant && w2) begin
                w2_tik <= 1;
           end else begin
                w2_tik <= 0;
           end
        
            if(bus_botones==0)
                cambio_hecho<=0;
            if (bus_botones[0] && !cambio_hecho) begin
                aux_columna=columna+10;
                columna = (columna == 24)? columna:aux_columna;
                cambio_hecho<=1;
            end
            if (bus_botones[1] && !cambio_hecho) begin
                aux_columna=columna-10;
                columna = (columna == 4)? columna:aux_columna;
                cambio_hecho<=1;
            end
           
            if (w0_tik || !reset) begin
                filaw0    = fila+7;
                columnaw0 = 4;
            end
            if (w1_tik|| !reset) begin 
                filaw1    = fila+7;
                columnaw1 = 14;  
              
            end
            if (w2_tik|| !reset) begin
                filaw2    = fila+7;
                columnaw2 = 24;

            end 
             if (tik) begin
                if(w0)
                    filaw0= filaw0+1;
                if(w1)
                    filaw1= filaw1+1;
                if(w2)
                    filaw2= filaw2+1;
            end
            
            if(tik) begin
                fila=fila-1;
            end
            if(!fin) begin
                // carro usuario
                if(  (  (fila_in==fila)     &&  (columna_in==columna) )  
                    ||( (fila_in==filam1)   &&  (columna_in==columna) )
                    ||( (fila_in==filam2)   &&  (columna_in==columna) )
                    ||( (fila_in==filam3)   &&  (columna_in==columna) )
                    ||( (fila_in==filam4)   &&  (columna_in==columna) )
                    
                    ||( (fila_in==fila)     &&  (columna_in==columna+1) )
                    ||( (fila_in==filam1)   &&  (columna_in==columna+1) )
                    ||( (fila_in==filam2)   &&  (columna_in==columna+1) )
                    ||( (fila_in==filam3)   &&  (columna_in==columna+1) )
                    ||( (fila_in==filam4)   &&  (columna_in==columna+1) )
                    
                    ||( (fila_in==fila)     &&  (columna_in==columna+2) )
                    ||( (fila_in==filam1)   &&  (columna_in==columna+2) )
                    ||( (fila_in==filam2)   &&  (columna_in==columna+2) )
                    ||( (fila_in==filam3)   &&  (columna_in==columna+2) )
                    ||( (fila_in==filam4)   &&  (columna_in==columna+2) )              
                    )
                    if(visible)
                        color_out<=color_predeterminado;
                    else
                        color_out<=0;
                else if( ((fila_in==filam1) && (columna_in==columna-1)) 
                    || (  (fila_in==filam1) && (columna_in==columna+3) ) 
                    || (  (fila_in==filam3) && (columna_in==columna-1) ) 
                    || (  (fila_in==filam3) && (columna_in==columna+3) ) 
                    ) begin
                    if(visible)
                        color_out<='hF00;
                    else
                        color_out<= 0;
                
                end else if(w0 &&(( (fila_in==filaw0) &&  (columna_in==columnaw0) )  
                        ||( (fila_in==filaw0m1)    &&  (columna_in==columnaw0) )
                        ||( (fila_in==filaw0m2)    &&  (columna_in==columnaw0) )
                        ||( (fila_in==filaw0m3)    &&  (columna_in==columnaw0) )
                        ||( (fila_in==filaw0m4)    &&  (columna_in==columnaw0) )
                    
                        ||( (fila_in==filaw0)     &&  (columna_in==columnaw0+1) )
                        ||( (fila_in==filaw0m1)   &&  (columna_in==columnaw0+1) )
                        ||( (fila_in==filaw0m2)   &&  (columna_in==columnaw0+1) )
                        ||( (fila_in==filaw0m3)   &&  (columna_in==columnaw0+1) )
                        ||( (fila_in==filaw0m4)   &&  (columna_in==columnaw0+1) )
                    
                        ||( (fila_in==filaw0)     &&  (columna_in==columnaw0+2) )
                        ||( (fila_in==filaw0m1)   &&  (columna_in==columnaw0+2) )
                        ||( (fila_in==filaw0m2)   &&  (columna_in==columnaw0+2) )
                        ||( (fila_in==filaw0m3)   &&  (columna_in==columnaw0+2) )
                        ||( (fila_in==filaw0m4)   &&  (columna_in==columnaw0+2) )              
                        ))
                        if(visible)
                            color_out<='hF1B;
                        else
                            color_out<=0;
                    else if(w0 &&  (((fila_in==filaw0m1) && (columna_in==columnaw0-1)) 
                        || (  (fila_in==filaw0m1) && (columna_in==columnaw0+3) ) 
                        || (  (fila_in==filaw0m3) && (columna_in==columnaw0-1) ) 
                        || (  (fila_in==filaw0m3) && (columna_in==columnaw0+3) ) 
                        )) begin
                            if(visible)
                                color_out<='h696;
                            else
                                color_out<= 0;
                    end     
                else if(w1 && ( ((fila_in==filaw1) &&  (columna_in==columnaw1))  
                        ||( (fila_in==filaw1m1)   &&  (columna_in==columnaw1) )
                        ||( (fila_in==filaw1m2)   &&  (columna_in==columnaw1) )
                        ||( (fila_in==filaw1m3)   &&  (columna_in==columnaw1) )
                        ||( (fila_in==filaw1m4)   &&  (columna_in==columnaw1) )
                    
                        ||( (fila_in==filaw1)     &&  (columna_in==columnaw1+1) )
                        ||( (fila_in==filaw1m1)   &&  (columna_in==columnaw1+1) )
                        ||( (fila_in==filaw1m2)   &&  (columna_in==columnaw1+1) )
                        ||( (fila_in==filaw1m3)   &&  (columna_in==columnaw1+1) )
                        ||( (fila_in==filaw1m4)   &&  (columna_in==columnaw1+1) )
                    
                        ||( (fila_in==filaw1)     &&  (columna_in==columnaw1+2) )
                        ||( (fila_in==filaw1m1)   &&  (columna_in==columnaw1+2) )
                        ||( (fila_in==filaw1m2)   &&  (columna_in==columnaw1+2) )
                        ||( (fila_in==filaw1m3)   &&  (columna_in==columnaw1+2) )
                        ||( (fila_in==filaw1m4)   &&  (columna_in==columnaw1+2) )              
                        ))
                        if(visible)
                            color_out<='h00F;
                        else
                            color_out<=0;
                    else if(w1 &&  (((fila_in==filaw1m1) && (columna_in==columnaw1-1)) 
                        || (  (fila_in==filaw1m1) && (columna_in==columnaw1+3) ) 
                        || (  (fila_in==filaw1m3) && (columna_in==columnaw1-1) ) 
                        || (  (fila_in==filaw1m3) && (columna_in==columnaw1+3) ) 
                        )) begin
                            if(visible)
                                color_out<='hFF0;
                            else
                                color_out<= 0;
                    end     
                 else if(w2 && (( (fila_in==filaw2) && (columna_in==columnaw2) )  
                        ||( (fila_in==filaw2m1)   &&  (columna_in==columnaw2) )
                        ||( (fila_in==filaw2m2)   &&  (columna_in==columnaw2) )
                        ||( (fila_in==filaw2m3)   &&  (columna_in==columnaw2) )
                        ||( (fila_in==filaw2m4)   &&  (columna_in==columnaw2) )
                    
                        ||( (fila_in==filaw2)     &&  (columna_in==columnaw2+1) )
                        ||( (fila_in==filaw2m1)   &&  (columna_in==columnaw2+1) )
                        ||( (fila_in==filaw2m2)   &&  (columna_in==columnaw2+1) )
                        ||( (fila_in==filaw2m3)   &&  (columna_in==columnaw2+1) )
                        ||( (fila_in==filaw2m4)   &&  (columna_in==columnaw2+1) )
                    
                        ||( (fila_in==filaw2)     &&  (columna_in==columnaw2+2) )
                        ||( (fila_in==filaw2m1)   &&  (columna_in==columnaw2+2) )
                        ||( (fila_in==filaw2m2)   &&  (columna_in==columnaw2+2) )
                        ||( (fila_in==filaw2m3)   &&  (columna_in==columnaw2+2) )
                        ||( (fila_in==filaw2m4)   &&  (columna_in==columnaw2+2) )              
                        ))
                        if(visible)
                            color_out<='hF0F;
                        else
                            color_out<=0;
                    else if(w2 && ( ((fila_in==filaw2m1) && (columna_in==columnaw2-1)) 
                        || (  (fila_in==filaw2m1) && (columna_in==columnaw2+3) ) 
                        || (  (fila_in==filaw2m3) && (columna_in==columnaw2-1) ) 
                        || (  (fila_in==filaw2m3) && (columna_in==columnaw2+3) ) 
                        )) begin
                            if(visible)
                                color_out<='h00F;
                            else
                                 color_out<= 0;
                            end 
                    
                else 
                    
                    if(visible)
                        color_out<=color_in;
                    else
                        color_out<=0;
                        
            end
            else begin
                    if(visible)
                        if(((fila_in==10 )&& (columna_in==10))
                         || ((fila_in==11 )&& (columna_in==10))
                         || ((fila_in==12 )&& (columna_in==10))
                         || ((fila_in==13 )&& (columna_in==10))
                         || ((fila_in==14 )&& (columna_in==10))
                         || ((fila_in==15 )&& (columna_in==10))
                         || ((fila_in==16 )&& (columna_in==10))
                         || ((fila_in==17 )&& (columna_in==10))
                         || ((fila_in==18 )&& (columna_in==10))
                         || ((fila_in==19 )&& (columna_in==10))
                         || ((fila_in==10 )&& (columna_in==11))   
                         || ((fila_in==10 )&& (columna_in==12))
                         || ((fila_in==15 )&& (columna_in==11)
                         || ((fila_in==15 )&& (columna_in==12)))
                         
                         
                         || ((fila_in==10 )&& (columna_in==15))
                         || ((fila_in==11 )&& (columna_in==15))
                         || ((fila_in==12 )&& (columna_in==15))
                         || ((fila_in==13 )&& (columna_in==15))
                         || ((fila_in==14 )&& (columna_in==15))
                         || ((fila_in==15 )&& (columna_in==15))
                         || ((fila_in==16 )&& (columna_in==15))
                         || ((fila_in==17 )&& (columna_in==15))
                         || ((fila_in==18 )&& (columna_in==15))
                         || ((fila_in==19 )&& (columna_in==15))
                         
                         
                         || ((fila_in==10 )&& (columna_in==20-3))
                         || ((fila_in==11 )&& (columna_in==20-3))
                         || ((fila_in==12 )&& (columna_in==20-3))
                         || ((fila_in==13 )&& (columna_in==20-3))
                         || ((fila_in==14 )&& (columna_in==20-3))
                         || ((fila_in==15 )&& (columna_in==20-3))
                         || ((fila_in==16 )&& (columna_in==20-3))
                         || ((fila_in==17 )&& (columna_in==20-3))
                         || ((fila_in==18 )&& (columna_in==20-3))
                         || ((fila_in==19 )&& (columna_in==20-3))
                         
                         || ((fila_in==11 )&& (columna_in==21-3))
                         || ((fila_in==12 )&& (columna_in==21-3))
                         || ((fila_in==13 )&& (columna_in==22-3))
                         || ((fila_in==14 )&& (columna_in==22-3))
                         || ((fila_in==15 )&& (columna_in==23-3))
                         || ((fila_in==16 )&& (columna_in==23-3))
                         || ((fila_in==17 )&& (columna_in==24-3))
                         || ((fila_in==18 )&& (columna_in==24-3))
                         || ((fila_in==18 )&& (columna_in==25-3))
                         
                         || ((fila_in==10 )&& (columna_in==25-3))
                         || ((fila_in==11 )&& (columna_in==25-3))
                         || ((fila_in==12 )&& (columna_in==25-3))
                         || ((fila_in==13 )&& (columna_in==25-3))
                         || ((fila_in==14 )&& (columna_in==25-3))
                         || ((fila_in==15 )&& (columna_in==25-3))
                         || ((fila_in==16 )&& (columna_in==25-3))
                         || ((fila_in==17 )&& (columna_in==25-3))
                         || ((fila_in==18 )&& (columna_in==25-3))
                         || ((fila_in==19 )&& (columna_in==25-3))
                         )
                        color_out<='hF00;
                    else
                        color_out<=0;
            end        
                    
                    
            if (
                (w0_ant&&w0&&
                ( 
                ( (fila == filaw0)
                ||    (fila == filaw0m1)
                ||    (fila == filaw0m2)
                ||    (fila == filaw0m3)
                ||    (fila == filaw0m4)  ) 
                 &&  (columna == columnaw0) 
                )
                ) 
                 
                ||
                
                (w1_ant&&w1&&
                (
                ((fila == filaw1)
                || (fila == filaw1m1)
                || (fila == filaw1m2)
                || (fila == filaw1m3)
                || (fila == filaw1m4)) 
                && (columna == columnaw1)
                )
                )
                
                ||
                
                
                (w2_ant&&w2&&
                (
                ((fila ==filaw2)
                || (fila == filaw2m1)
                || (fila == filaw2m2)
                || (fila == filaw2m3)
                || (fila == filaw2m4)) 
                && (columna == columnaw2)
                )
                )
                
                
                )
                  
                fin<=1;
                  
            else
                fin<=fin;
                    
            
            
            
                    
        end
    end
  
   
endmodule