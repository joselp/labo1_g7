`timescale 1 ns / 1 ps

















module ejercicio3_tb;
	reg clk = 1;
	always #5 clk = ~clk;

	reg resetn = 0;
	initial begin
		if ($test$plusargs("vcd")) begin
			$dumpfile("ejercicio3.vcd");
			$dumpvars(0, ejercicio3);
		end
		repeat (10000) @(posedge clk);
		resetn <= 1;
		repeat (10000) @(posedge clk);
		resetn <= 0;
		repeat (10000) @(posedge clk);
		resetn <= 1;
	end


	wire trap;
    wire up;
    wire down;
    wire left;
    wire right;
    wire center;
    wire kclk;
    wire kdata;
    wire prueba;
    wire [3:0]red;
    wire [3:0]green;
    wire [3:0]blue;
    wire hsync;
    wire vsync;

	ejercicio3 uut (
		.clk          (clk        ),
		.resetn       (resetn     ),
		.up           (up         ),
		.down         (down       ),
		.right        (right      ),
        .left         (left       ),
		.center       (center     ),
		.kclk         (kclk       ),
		.kdata        (kdata      ),
		.trap         (trap       ),
		.prueba       (prueba     ),
		.red          (red        ),
		.green        (green      ),
		.blue         (blue       ),
		.hsync        (hsync      ),
		.vsync        (vsync      )

	);

endmodule
