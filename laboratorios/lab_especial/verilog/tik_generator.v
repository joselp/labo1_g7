module tik_generator (
    input wire  clk,                                
    input wire  resetn, 
    input       tik_flag,                           
    output reg  tik
);
    reg flag_ant;
        
    always @(posedge clk) begin
       if(~resetn) begin
        flag_ant <= 0;
        tik      <= 0;
       end else begin 
           flag_ant <= tik_flag;
           if(~flag_ant && tik_flag) begin
                tik <= 1;
           end else begin
                tik <= 0;
           end
       end
    end
   
endmodule