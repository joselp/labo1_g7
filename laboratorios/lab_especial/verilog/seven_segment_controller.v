
module seven_segment_controller(
    input            reset,
    input            clk,
    input      [26:0] displayed_number,        
    output reg [7:0] Anode_Activate, // anode signals of the 7-segment LED display
    output reg [6:0] LED_out
);
    reg  [3:0]  LED_BCD;
    reg  [19:0] refresh_counter;
    wire [2:0]  LED_activating_counter; 

    always @(posedge clk or posedge reset)
    begin 
        if(reset==1)
            refresh_counter <= 0;
        else
            refresh_counter <= refresh_counter + 1;
    end 
    assign LED_activating_counter = refresh_counter[19:17];
    
    always @(*)
    begin
        case(LED_activating_counter)
        3'b000: begin // 8
            Anode_Activate = 8'b01111111; 
            LED_BCD = displayed_number/10000000;
              end
        3'b001: begin // 7
            Anode_Activate = 8'b10111111; 
            LED_BCD = (displayed_number%10000000)/1000000;
              end
        3'b010: begin // 6
            Anode_Activate = 8'b11011111; 
            LED_BCD = (displayed_number%1000000)/100000;
            end
        3'b011: begin // 5
            Anode_Activate = 8'b11101111; 
            LED_BCD = (displayed_number%100000)/10000;    
               end
        3'b100: begin // 4
            Anode_Activate = 8'b11110111; 
            LED_BCD = (displayed_number%10000)/1000;    
               end
        3'b101: begin // 3
            Anode_Activate = 8'b11111011; 
            LED_BCD = (displayed_number%1000)/100;    
               end 
        3'b110: begin // 2
            Anode_Activate = 8'b11111101; 
            LED_BCD = (displayed_number%100)/10;    
               end
        3'b111: begin // 1
            Anode_Activate = 8'b11111110; 
            LED_BCD = displayed_number%10;    
               end
        default: begin 
            Anode_Activate = 8'b00000000;
            LED_BCD        = 7'b11111111;
            end
        endcase
    end

    always @(*)
    begin
        case(LED_BCD)
        4'b0000: LED_out = 7'b0000001; // "0"     
        4'b0001: LED_out = 7'b1001111; // "1" 
        4'b0010: LED_out = 7'b0010010; // "2" 
        4'b0011: LED_out = 7'b0000110; // "3" 
        4'b0100: LED_out = 7'b1001100; // "4" 
        4'b0101: LED_out = 7'b0100100; // "5" 
        4'b0110: LED_out = 7'b0100000; // "6" 
        4'b0111: LED_out = 7'b0001111; // "7" 
        4'b1000: LED_out = 7'b0000000; // "8"     
        4'b1001: LED_out = 7'b0000100; // "9" 
        default: LED_out = 7'b0000001; // "0"
        endcase
    end
endmodule
