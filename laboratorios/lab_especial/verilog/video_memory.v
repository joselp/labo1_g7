module video_memory # ( 
    parameter DATA_WIDTH= 16,
    parameter ADDR_WIDTH=8,
    parameter MEM_SIZE=8 
)(
    input wire Clock,                                       //CLK
    input wire iWriteEnable,                                //Ctrl
    input wire[ADDR_WIDTH-1:0] iReadAddress,
    input wire[ADDR_WIDTH-1:0] iWriteAddress,               //Direccion donde escribir en la memoria
    input wire[DATA_WIDTH-1:0] iDataIn,                     //Color
    output reg [DATA_WIDTH-1:0] oDataOut
);
    reg [DATA_WIDTH-1:0] Ram [MEM_SIZE-1:0];
    
    always @(posedge Clock) begin
        if (iWriteEnable) begin
            Ram[iWriteAddress] <= iDataIn;
        end
        oDataOut <= Ram[iReadAddress];
    end
endmodule