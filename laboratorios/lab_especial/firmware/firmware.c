#include <stdint.h>
#include <stdio.h> 
#include <stdlib.h>
#define LIMIT_ROW 2097152	//VALOR MAXIMO PARA FILAS CON COLUMNAS =0 {16b para filas, 16 bits para columnas}
static void putuintAddr(uint32_t i) {
	*((volatile uint32_t *)0x10000000) = i;
}
static void putuintColor(uint32_t i) {
	*((volatile uint32_t *)0x10000004) = i;
}
static void putuintCtrl(uint32_t i) {
	*((volatile uint32_t *)0x10000008) = i;
}

static void putuintFlagTik(uint32_t i) {
	*((volatile uint32_t *)0x1000000C) = i;
}

static void putuintWayFree0(uint32_t i) {
	*((volatile uint32_t *)0x10000010) = i;
}

static void putuintWayFree1(uint32_t i) {
	*((volatile uint32_t *)0x10000014) = i;
}

static void putuintWayFree2(uint32_t i) {
	*((volatile uint32_t *)0x10000018) = i;
}

/*static uint32_t resultado() {
	return *((volatile uint32_t *)0x0FFFFFF8);
}*/

void main() {


	uint32_t ROW_SIZE = 24;		//Valores para ejercicio 1
	uint32_t COL_SIZE = 32;		//Valores para ejercicio 1
	uint32_t rojo     = 0xF00;	//Valores para ejercicio 1
	uint32_t verde    = 0x0F0;	//Valores para ejercicio 1
	uint32_t fucsia   = 0xF0F;	//Valores para ejercicio 1
	uint32_t azul     = 0x00F;	//Valores para ejercicio 1
	uint32_t negro    = 0x000;	//Valores para ejercicio 2
	uint32_t blanco   = 0xFFF;	//Valores para ejercicio 2
	uint32_t gris     = 0x223;	//Valores para ejercicio 2
	uint32_t amarillo = 0xEE1;	//Valores para ejercicio 2
	uint32_t Addres   = 0;		//Valores para ejercicio 1
	uint32_t Fila     = 65536;	//Valores para ejercicio 1
	uint32_t Columna  = 1;		//Valores para ejercicio 1
	uint32_t contador = 0;		//Valores para ejercicio 1
	uint32_t contador_velocidad = 0;		//Valores para ejercicio 1
	uint32_t velocidad_inv = 800000;		//Valores para ejercicio 
	//uint32_t velocidad_inv = 1000;
	uint32_t contador_filas = 0;		//Valores para ejercicio 2
	int contador_auxiliar =0;
	uint32_t contador_ticks = 20;


	uint32_t way_free[3] = {0,0,0};
	uint32_t cont_car[3] = {0,0,0};
	uint32_t ptr_car = 0;
	uint32_t cars [20]= {0,2,1,0,1,2,0,2,0,1,2,0,1,0,1,0,2,0,1,0}; 
	//uint32_t pos0[2];
	//pos0[0] = 26;
	//pos0[1] = 4;
	//uint32_t pos1[2];
	//pos1[0] = 26;
	//pos1[1] = 14;
	//uint32_t pos2[2];
	//pos2[0] = 26;
	//pos2[1] = 24;
	//uint32_t pos_car[2];
	//pos_car[0] = 18;
	/*
	0 rojo
	1 verde
	2 morado
	3 azul
	*/

	while (1) {
		putuintCtrl(0);
		putuintAddr(Addres);

		// generador tick
		if(contador_velocidad == velocidad_inv){

			contador_ticks++;
			contador_velocidad=0;

			// Actualización posiciones de carros
			//pos0[0]++;
			//pos1[0]++;
			//pos2[0]++;
			cont_car[0]++;
			cont_car[1]++;
			cont_car[2]++;
			
			//if(pos0[0] == 32){
			//	pos0[0] = 0;	
			//}
			//if(pos1[0] == 32){
			//	pos1[0] = 0;	
			//}
			//if(pos2[0] == 32){
			//	pos2[0] = 0;	
			//}


			// aumento de velocidad
			if(contador_ticks >= 25){
				contador_ticks = 0;
				velocidad_inv *=0.86;
				if(velocidad_inv <= 90420){
					velocidad_inv = 90420;
				}
				
				// Nuevo carro
				switch (cars[ptr_car])
				{
				case 0:
					way_free[0] = 1;
					cont_car[0] = 0;
					//pos0[0] = 26;
					//pos0[1] = 4;
					break;
				case 1:
					way_free[1] = 1;
					cont_car[1] = 0;
					//pos0[0] = 26;
					//pos0[1] = 14;
					break;
				case 2:
					way_free[2] = 1;
					cont_car[2] = 0;
					//pos0[0] = 26;
					//pos0[1] = 24;
					break;
				default:
					break;
				}
				// actualización de ptr_car
				ptr_car++;
				if(ptr_car == 19){
					ptr_car = 0;
				}
			}
			// Comprobación coliciones
			
			
			// elminación de carros
			if(cont_car[0] == 29){
				way_free[0] = 0;
			}
			if(cont_car[1] == 29){
				way_free[1] = 0;
			}
			if(cont_car[2] == 29){
				way_free[2] = 0;
			}
			// Se escriben en los registros las ways ocupadas
			putuintWayFree0(way_free[0]);
			putuintWayFree1(way_free[1]);
			putuintWayFree2(way_free[2]);

			putuintFlagTik(1);
		}		
		else{
			contador_velocidad++;
			putuintFlagTik(0);
		}


		//EJERCICIO 2
		if((Addres&65535)==10||(Addres&65535)==20){
			if(contador_auxiliar==6)
			{
				putuintColor(gris);
				contador_auxiliar++;
			}
			else if(contador_auxiliar==7)
			{
				putuintColor(gris);
				contador_auxiliar=0;
			}
			else{
				putuintColor(amarillo);
				contador_auxiliar++;

			}
		}
		else
			putuintColor(gris);

		if(Addres < LIMIT_ROW){				
			if((Addres&65535)!=COL_SIZE-1){
				Addres=Addres+Columna;
				contador_filas++;				//EJERCICIO2
			}

			else{
				Addres=Addres-(COL_SIZE-1);
				Addres=Addres+Fila;
				//contador++;				//EJERCICIO1
			}
			//printf("addr: 0x%.8X\n",Addres);
			//PONER ADRES EN 0 Y CONTADOR EN 0/
			putuintCtrl(1);
		}
	}
}
