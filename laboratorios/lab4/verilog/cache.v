
module cache#(
    parameter ASOC       = 2,
    parameter BLOCK_SIZE = 8,                // bytes
    parameter WORD_SIZE  = 32,               // bits
    parameter CACHE_SIZE = 2048,             // bytes
    parameter MEM_SIZE   = 65536,            // bytes
    parameter ADDR_SIZE  = 32
)(
    input                       clk,
    input                       reset,
    
    input                       proc_valid,      // valid               [proc  -> cache]
    input                       proc_instr,      // lectura o escritura [proc  -> cache]
    output reg                  proc_ready,      // listo??             [cache -> proc]
    input      [ADDR_SIZE-1:0]  proc_addr,       // adress              [proc  -> cache]
    input      [WORD_SIZE-1:0]  proc_wdata,      // dato a escribir     [proc  -> cache]
    input                       proc_wstrb,      // escritura o lectura [proc  -> cache]
    output reg [WORD_SIZE-1:0]  proc_rdata,      // dato leído          [cache -> proc]
     
    output reg                  mem_valid,       // validador de cache  [cache -> mem]
    output reg                  mem_instr,       // lectura o escritura [cache -> mem]
    input                       mem_ready,       // listo??             [cache -> proc]
    output reg [ADDR_SIZE-1:0]  mem_addr,        // adress              [cache -> mem]
    output reg [WORD_SIZE-1:0]  mem_wdata,       // dato a escribir     [cache -> mem]
    output reg                  mem_wstrb,       // escritura o lectura [cache -> mem]
    input  [WORD_SIZE-1:0]  mem_rdata        // dato leído          [mem   -> cache]
);
    localparam OFFSET_SIZE_B = $clog2(BLOCK_SIZE);
    localparam OFFSET_SIZE   = BLOCK_SIZE/4;
    localparam IDX_SIZE      = CACHE_SIZE/(BLOCK_SIZE*ASOC);
    localparam IDX_SIZE_B    = $clog2(IDX_SIZE);
    localparam TAG_SIZE_B    = ADDR_SIZE -(OFFSET_SIZE_B + IDX_SIZE_B);
    localparam RP_SIZE_B     = $clog2(ASOC);  
    localparam ENTRY_SIZE_B  = 2 + RP_SIZE_B + TAG_SIZE_B;

    reg  [WORD_SIZE-1:0] mem [IDX_SIZE-1:0][ASOC-1:0][OFFSET_SIZE_B-1:0];
    reg  [ENTRY_SIZE_B-1:0] entries[IDX_SIZE-1:0][ASOC-1:0];
    
    wire [IDX_SIZE_B-1:0]    proc_idx;
    wire [TAG_SIZE_B-1:0]    proc_tag;
    wire [OFFSET_SIZE_B-1:0] proc_offset;
    integer i, j;  
    reg [4:0] state, next_state;
    localparam WAITING_ACCESS   = 5'b00001;
    localparam VICTIMIZING      = 5'b00010;
    localparam LOADING_BLOCK    = 5'b00100;
    localparam UPDATING_ENTRIES = 5'b00100;
    localparam SENDING_DATA     = 5'b1000;
    always @(posedge clk) begin
        if(~reset) begin
            proc_ready <= 0;
            state <= WAITING_ACCESS;
            for(i=0;i<IDX_SIZE;i=i+1) begin
                for(j=0;j<ASOC;j=j+1) begin  
                    entries[i][j][ENTRY_SIZE_B-1] <= 0;
                end
            end
        end else begin
            state <= next_state;
        end
    end
    
    reg [OFFSET_SIZE_B-1:0] offset_cont;
    reg                     hit_way_0;
    reg                     hit_way_1;
    reg                     cand_vic;
    reg [2:0]               way;
    reg [2:0]               way_free;
    always @(*) begin
        // hay hit
        if(entries [proc_idx][0][ENTRY_SIZE_B-1] == 1) begin 
            if(entries [proc_idx][0][TAG_SIZE_B-1:0] == proc_tag) begin
                hit_way_0  = 1;
            end else hit_way_0 = 0;
        end else hit_way_0 = 0;
        if(entries [proc_idx][1][ENTRY_SIZE_B-1] == 1) begin 
            if(entries [proc_idx][1][TAG_SIZE_B-1:0] == proc_tag) begin
                hit_way_1 = 1;
            end else hit_way_1 = 0;
        end else hit_way_1 = 0;
        // El set está lleno
        if(entries [proc_idx][0][1:0] == 1)begin 
            if(entries [proc_idx][0][1:0] == 1)begin
                way_free = 2;
            end else begin
                way_free = 1;
            end
        end else begin
            way_free = 0; 
        end
        // candidato a victimizar
        if(entries [proc_idx][0][ENTRY_SIZE_B-2] == 1) begin
            cand_vic = 0;
        end else begin
            cand_vic = 1;
        end
    end
  
    always @(state)begin
        case (state)
            WAITING_ACCESS:begin 
                proc_ready <= 0;
                if(proc_valid == 1) begin
                    if(hit_way_0 || hit_way_1) begin
                        next_state = UPDATING_ENTRIES;
                    end else begin
                        if(way_free !=2) begin
                            next_state = LOADING_BLOCK;
                        end else begin
                            next_state = VICTIMIZING;
                        end
                    end
                end else begin
                    next_state = WAITING_ACCESS;
                end
            end
            VICTIMIZING:begin 
                mem_valid = 1;
                mem_instr = 1;
                mem_addr  = {entries[proc_idx][cand_vic][TAG_SIZE_B-1:0],proc_idx,offset_cont};
                mem_wdata = mem[proc_idx][cand_vic][offset_cont];
                if(mem_ready) begin
                    if(offset_cont < BLOCK_SIZE)begin
                        offset_cont = offset_cont + 1;
                    end else begin 
                        offset_cont = 0;
                        mem_valid   = 0;
                        next_state  = LOADING_BLOCK;
                    end
                end
            end
            LOADING_BLOCK:begin 
                mem_valid = 1;
                mem_instr = 0;
                mem_addr  = {proc_tag,proc_idx,offset_cont};
                if(mem_ready) begin
                    if(offset_cont < BLOCK_SIZE)begin
                        offset_cont = offset_cont + 1;
                    end else begin 
                        offset_cont = 0;
                        mem_valid   = 0;
                        next_state  = UPDATING_ENTRIES;
                    end
                end
            end
            UPDATING_ENTRIES:begin 
                if(hit_way_0 || hit_way_1) begin
                    if(hit_way_0) begin
                        entries [proc_idx][0][ENTRY_SIZE_B-3] = 0;
                        entries [proc_idx][1][ENTRY_SIZE_B-3] = 1;
                    end else begin
                        entries [proc_idx][1][ENTRY_SIZE_B-3] = 0;
                        entries [proc_idx][0][ENTRY_SIZE_B-3] = 1;                    
                    end
                    // Update dirty bit
                    if(proc_instr == 1) begin
                        entries [proc_idx][way_free][ENTRY_SIZE_B-2]  = 1;
                    end else begin
                        entries [proc_idx][way_free][ENTRY_SIZE_B-2]  = entries [proc_idx][way_free][ENTRY_SIZE_B-2];
                    end
                end else begin
                    if(way_free != 2) begin
                        entries [proc_idx][way_free][ENTRY_SIZE_B-3]  = 0;
                        entries [proc_idx][~way_free][ENTRY_SIZE_B-3] = 1;  
                    end else begin
                        entries [proc_idx][cand_vic][ENTRY_SIZE_B-3]  = 0;
                        entries [proc_idx][~cand_vic][ENTRY_SIZE_B-3] = 1;  
                    end
                end  
            end
            SENDING_DATA:begin 
                if(hit_way_0 || hit_way_1) begin
                    if(hit_way_0) begin
                        proc_rdata = mem[proc_idx][hit_way_0][proc_offset];
                    end else begin
                        proc_rdata = mem[proc_idx][hit_way_1][proc_offset];
                    end
                end else begin
                    if(way_free != 2) begin
                        proc_rdata = mem[proc_idx][way_free][proc_offset];
                    end else begin
                        proc_rdata = mem[proc_idx][cand_vic][proc_offset];
                    end
                end
                proc_ready = 1;
                next_state = WAITING_ACCESS; 
            end
            default: begin
                next_state = WAITING_ACCESS;
            end
        endcase
    end
                       
    assign proc_idx    = proc_addr[(OFFSET_SIZE_B+IDX_SIZE_B)-1:OFFSET_SIZE_B];
    assign proc_tag    = proc_addr[ENTRY_SIZE_B-1:(OFFSET_SIZE_B+IDX_SIZE_B)];
    assign proc_offset = proc_addr[OFFSET_SIZE_B-1:0];
    
endmodule
