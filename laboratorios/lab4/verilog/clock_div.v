
module clock_div #(
    parameter DIV = 2
)(
    input      clk,
    input      reset,
    output reg clk_div
);

reg [8:0] counter;

always @(posedge clk) begin
    if(~reset) begin
        counter <= 0;
        clk_div <= 1;
    end else begin
        counter = counter + 1;
        if(counter == DIV/2) begin
            clk_div <= ~clk_div;
            counter <= 0;
        end
    end
end
    
endmodule