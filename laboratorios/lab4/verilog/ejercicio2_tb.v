`timescale 1 ns / 1 ps

module ejercicio2_tb;
	reg clk = 1;
	always #5 clk = ~clk;

	reg resetn = 0;
	initial begin
		if ($test$plusargs("vcd")) begin
			$dumpfile("ejercicio2.vcd");
			$dumpvars(0, ejercicio2_tb);
		end
		repeat (100) @(posedge clk);
		resetn <= 1;
	end

	wire trap;
	wire [7:0] out_byte;
	wire out_byte_en;

	ejercicio2 uut (
		.clk        (clk        ),
		.resetn     (resetn     ),
		.trap       (trap       )
	);

	always @(posedge clk) begin
		if (resetn && trap) begin
			$finish;
		end
	end
endmodule
