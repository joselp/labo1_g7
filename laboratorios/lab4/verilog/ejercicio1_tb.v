`timescale 1 ns / 1 ps

module ejercicio1_tb;
	reg clk = 1;
	always #5 clk = ~clk;

	reg resetn = 0;
	initial begin
		if ($test$plusargs("vcd")) begin
			$dumpfile("ejercicio1_tb.vcd");
			$dumpvars(0, ejercicio1_tb);
		end
		repeat (100) @(posedge clk);
		#1000 resetn <= 1;
	end

	wire trap;
	wire [15:0] LED;
	wire [7:0] AN; 
	wire [6:0]LED_out;
	wire out_byte_en;
	wire prueba;
	wire DP;
	wire [6:0] SEG;
	ejercicio1 uut (
		.clk        (clk        ),
		.resetn     (resetn     ),
		.SEG( SEG),
		.AN(AN),
		.DP(DP)
	);
	
	
	
endmodule
