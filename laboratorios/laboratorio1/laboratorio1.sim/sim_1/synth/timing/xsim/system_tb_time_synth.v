// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1.2 (lin64) Build 2615518 Fri Aug  9 15:53:29 MDT 2019
// Date        : Wed Aug 21 09:57:27 2019
// Host        : jose-dell running 64-bit Ubuntu 18.04.3 LTS
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/joselp/Escritorio/repos/lab_digitales/laboratorio1/laboratorio1.sim/sim_1/synth/timing/xsim/system_tb_time_synth.v
// Design      : system
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module RAM32M_UNIQ_BASE_
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD23
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD24
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD25
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD26
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD27
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD28
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD29
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD30
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD31
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD32
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD33
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module picorv32
   (trap_OBUF,
    Q,
    out_byte_en01_out,
    E,
    ADDRARDADDR,
    p_0_in,
    p_2_in,
    clk_IBUF_BUFG,
    resetn_IBUF,
    mem_rdata);
  output trap_OBUF;
  output [7:0]Q;
  output out_byte_en01_out;
  output [0:0]E;
  output [11:0]ADDRARDADDR;
  output [3:0]p_0_in;
  output [23:0]p_2_in;
  input clk_IBUF_BUFG;
  input resetn_IBUF;
  input [31:0]mem_rdata;

  wire [11:0]ADDRARDADDR;
  wire [0:0]E;
  wire [7:0]Q;
  wire [31:0]alu_out;
  wire \alu_out0_inferred__2/i___29_carry__0_n_0 ;
  wire \alu_out0_inferred__2/i___29_carry__0_n_1 ;
  wire \alu_out0_inferred__2/i___29_carry__0_n_2 ;
  wire \alu_out0_inferred__2/i___29_carry__0_n_3 ;
  wire \alu_out0_inferred__2/i___29_carry__0_n_4 ;
  wire \alu_out0_inferred__2/i___29_carry__0_n_5 ;
  wire \alu_out0_inferred__2/i___29_carry__0_n_6 ;
  wire \alu_out0_inferred__2/i___29_carry__0_n_7 ;
  wire \alu_out0_inferred__2/i___29_carry__1_n_0 ;
  wire \alu_out0_inferred__2/i___29_carry__1_n_1 ;
  wire \alu_out0_inferred__2/i___29_carry__1_n_2 ;
  wire \alu_out0_inferred__2/i___29_carry__1_n_3 ;
  wire \alu_out0_inferred__2/i___29_carry__1_n_4 ;
  wire \alu_out0_inferred__2/i___29_carry__1_n_5 ;
  wire \alu_out0_inferred__2/i___29_carry__1_n_6 ;
  wire \alu_out0_inferred__2/i___29_carry__1_n_7 ;
  wire \alu_out0_inferred__2/i___29_carry__2_n_0 ;
  wire \alu_out0_inferred__2/i___29_carry__2_n_1 ;
  wire \alu_out0_inferred__2/i___29_carry__2_n_2 ;
  wire \alu_out0_inferred__2/i___29_carry__2_n_3 ;
  wire \alu_out0_inferred__2/i___29_carry__2_n_4 ;
  wire \alu_out0_inferred__2/i___29_carry__2_n_5 ;
  wire \alu_out0_inferred__2/i___29_carry__2_n_6 ;
  wire \alu_out0_inferred__2/i___29_carry__2_n_7 ;
  wire \alu_out0_inferred__2/i___29_carry__3_n_0 ;
  wire \alu_out0_inferred__2/i___29_carry__3_n_1 ;
  wire \alu_out0_inferred__2/i___29_carry__3_n_2 ;
  wire \alu_out0_inferred__2/i___29_carry__3_n_3 ;
  wire \alu_out0_inferred__2/i___29_carry__3_n_4 ;
  wire \alu_out0_inferred__2/i___29_carry__3_n_5 ;
  wire \alu_out0_inferred__2/i___29_carry__3_n_6 ;
  wire \alu_out0_inferred__2/i___29_carry__3_n_7 ;
  wire \alu_out0_inferred__2/i___29_carry__4_n_0 ;
  wire \alu_out0_inferred__2/i___29_carry__4_n_1 ;
  wire \alu_out0_inferred__2/i___29_carry__4_n_2 ;
  wire \alu_out0_inferred__2/i___29_carry__4_n_3 ;
  wire \alu_out0_inferred__2/i___29_carry__4_n_4 ;
  wire \alu_out0_inferred__2/i___29_carry__4_n_5 ;
  wire \alu_out0_inferred__2/i___29_carry__4_n_6 ;
  wire \alu_out0_inferred__2/i___29_carry__4_n_7 ;
  wire \alu_out0_inferred__2/i___29_carry__5_n_0 ;
  wire \alu_out0_inferred__2/i___29_carry__5_n_1 ;
  wire \alu_out0_inferred__2/i___29_carry__5_n_2 ;
  wire \alu_out0_inferred__2/i___29_carry__5_n_3 ;
  wire \alu_out0_inferred__2/i___29_carry__5_n_4 ;
  wire \alu_out0_inferred__2/i___29_carry__5_n_5 ;
  wire \alu_out0_inferred__2/i___29_carry__5_n_6 ;
  wire \alu_out0_inferred__2/i___29_carry__5_n_7 ;
  wire \alu_out0_inferred__2/i___29_carry__6_n_1 ;
  wire \alu_out0_inferred__2/i___29_carry__6_n_2 ;
  wire \alu_out0_inferred__2/i___29_carry__6_n_3 ;
  wire \alu_out0_inferred__2/i___29_carry__6_n_4 ;
  wire \alu_out0_inferred__2/i___29_carry__6_n_5 ;
  wire \alu_out0_inferred__2/i___29_carry__6_n_6 ;
  wire \alu_out0_inferred__2/i___29_carry__6_n_7 ;
  wire \alu_out0_inferred__2/i___29_carry_n_0 ;
  wire \alu_out0_inferred__2/i___29_carry_n_1 ;
  wire \alu_out0_inferred__2/i___29_carry_n_2 ;
  wire \alu_out0_inferred__2/i___29_carry_n_3 ;
  wire \alu_out0_inferred__2/i___29_carry_n_4 ;
  wire \alu_out0_inferred__2/i___29_carry_n_5 ;
  wire \alu_out0_inferred__2/i___29_carry_n_6 ;
  wire \alu_out0_inferred__2/i___29_carry_n_7 ;
  wire alu_out_00_carry__0_i_1_n_0;
  wire alu_out_00_carry__0_i_2_n_0;
  wire alu_out_00_carry__0_i_3_n_0;
  wire alu_out_00_carry__0_i_4_n_0;
  wire alu_out_00_carry__0_n_0;
  wire alu_out_00_carry__0_n_1;
  wire alu_out_00_carry__0_n_2;
  wire alu_out_00_carry__0_n_3;
  wire alu_out_00_carry__1_i_1_n_0;
  wire alu_out_00_carry__1_i_2_n_0;
  wire alu_out_00_carry__1_i_3_n_0;
  wire alu_out_00_carry__1_n_1;
  wire alu_out_00_carry__1_n_2;
  wire alu_out_00_carry__1_n_3;
  wire alu_out_00_carry_i_1_n_0;
  wire alu_out_00_carry_i_2_n_0;
  wire alu_out_00_carry_i_3_n_0;
  wire alu_out_00_carry_i_4_n_0;
  wire alu_out_00_carry_n_0;
  wire alu_out_00_carry_n_1;
  wire alu_out_00_carry_n_2;
  wire alu_out_00_carry_n_3;
  wire alu_out_01_carry__0_i_1_n_0;
  wire alu_out_01_carry__0_i_2_n_0;
  wire alu_out_01_carry__0_i_3_n_0;
  wire alu_out_01_carry__0_i_4_n_0;
  wire alu_out_01_carry__0_i_5_n_0;
  wire alu_out_01_carry__0_i_6_n_0;
  wire alu_out_01_carry__0_i_7_n_0;
  wire alu_out_01_carry__0_i_8_n_0;
  wire alu_out_01_carry__0_n_0;
  wire alu_out_01_carry__0_n_1;
  wire alu_out_01_carry__0_n_2;
  wire alu_out_01_carry__0_n_3;
  wire alu_out_01_carry__1_i_1_n_0;
  wire alu_out_01_carry__1_i_2_n_0;
  wire alu_out_01_carry__1_i_3_n_0;
  wire alu_out_01_carry__1_i_4_n_0;
  wire alu_out_01_carry__1_i_5_n_0;
  wire alu_out_01_carry__1_i_6_n_0;
  wire alu_out_01_carry__1_i_7_n_0;
  wire alu_out_01_carry__1_i_8_n_0;
  wire alu_out_01_carry__1_n_0;
  wire alu_out_01_carry__1_n_1;
  wire alu_out_01_carry__1_n_2;
  wire alu_out_01_carry__1_n_3;
  wire alu_out_01_carry__2_i_1_n_0;
  wire alu_out_01_carry__2_i_2_n_0;
  wire alu_out_01_carry__2_i_3_n_0;
  wire alu_out_01_carry__2_i_4_n_0;
  wire alu_out_01_carry__2_i_5_n_0;
  wire alu_out_01_carry__2_i_6_n_0;
  wire alu_out_01_carry__2_i_7_n_0;
  wire alu_out_01_carry__2_i_8_n_0;
  wire alu_out_01_carry__2_n_1;
  wire alu_out_01_carry__2_n_2;
  wire alu_out_01_carry__2_n_3;
  wire alu_out_01_carry_i_1_n_0;
  wire alu_out_01_carry_i_2_n_0;
  wire alu_out_01_carry_i_3_n_0;
  wire alu_out_01_carry_i_4_n_0;
  wire alu_out_01_carry_i_5_n_0;
  wire alu_out_01_carry_i_6_n_0;
  wire alu_out_01_carry_i_7_n_0;
  wire alu_out_01_carry_i_8_n_0;
  wire alu_out_01_carry_n_0;
  wire alu_out_01_carry_n_1;
  wire alu_out_01_carry_n_2;
  wire alu_out_01_carry_n_3;
  wire \alu_out_01_inferred__0/i__carry__0_n_0 ;
  wire \alu_out_01_inferred__0/i__carry__0_n_1 ;
  wire \alu_out_01_inferred__0/i__carry__0_n_2 ;
  wire \alu_out_01_inferred__0/i__carry__0_n_3 ;
  wire \alu_out_01_inferred__0/i__carry__1_n_0 ;
  wire \alu_out_01_inferred__0/i__carry__1_n_1 ;
  wire \alu_out_01_inferred__0/i__carry__1_n_2 ;
  wire \alu_out_01_inferred__0/i__carry__1_n_3 ;
  wire \alu_out_01_inferred__0/i__carry__2_n_1 ;
  wire \alu_out_01_inferred__0/i__carry__2_n_2 ;
  wire \alu_out_01_inferred__0/i__carry__2_n_3 ;
  wire \alu_out_01_inferred__0/i__carry_n_0 ;
  wire \alu_out_01_inferred__0/i__carry_n_1 ;
  wire \alu_out_01_inferred__0/i__carry_n_2 ;
  wire \alu_out_01_inferred__0/i__carry_n_3 ;
  wire [31:0]alu_out_q;
  wire \alu_out_q[0]_i_2_n_0 ;
  wire \alu_out_q[0]_i_3_n_0 ;
  wire \alu_out_q[0]_i_4_n_0 ;
  wire \alu_out_q[0]_i_5_n_0 ;
  wire \alu_out_q[10]_i_2_n_0 ;
  wire \alu_out_q[11]_i_2_n_0 ;
  wire \alu_out_q[12]_i_2_n_0 ;
  wire \alu_out_q[13]_i_2_n_0 ;
  wire \alu_out_q[14]_i_2_n_0 ;
  wire \alu_out_q[15]_i_2_n_0 ;
  wire \alu_out_q[16]_i_2_n_0 ;
  wire \alu_out_q[17]_i_2_n_0 ;
  wire \alu_out_q[18]_i_2_n_0 ;
  wire \alu_out_q[19]_i_2_n_0 ;
  wire \alu_out_q[1]_i_2_n_0 ;
  wire \alu_out_q[20]_i_2_n_0 ;
  wire \alu_out_q[21]_i_2_n_0 ;
  wire \alu_out_q[22]_i_2_n_0 ;
  wire \alu_out_q[23]_i_2_n_0 ;
  wire \alu_out_q[24]_i_2_n_0 ;
  wire \alu_out_q[25]_i_2_n_0 ;
  wire \alu_out_q[26]_i_2_n_0 ;
  wire \alu_out_q[27]_i_2_n_0 ;
  wire \alu_out_q[28]_i_2_n_0 ;
  wire \alu_out_q[29]_i_2_n_0 ;
  wire \alu_out_q[2]_i_2_n_0 ;
  wire \alu_out_q[30]_i_2_n_0 ;
  wire \alu_out_q[30]_i_3_n_0 ;
  wire \alu_out_q[30]_i_4_n_0 ;
  wire \alu_out_q[31]_i_2_n_0 ;
  wire \alu_out_q[31]_i_3_n_0 ;
  wire \alu_out_q[31]_i_4_n_0 ;
  wire \alu_out_q[3]_i_2_n_0 ;
  wire \alu_out_q[4]_i_2_n_0 ;
  wire \alu_out_q[5]_i_2_n_0 ;
  wire \alu_out_q[6]_i_2_n_0 ;
  wire \alu_out_q[7]_i_2_n_0 ;
  wire \alu_out_q[8]_i_2_n_0 ;
  wire \alu_out_q[9]_i_2_n_0 ;
  wire clk_IBUF_BUFG;
  wire \count_cycle[0]_i_2_n_0 ;
  wire [63:0]count_cycle_reg;
  wire \count_cycle_reg[0]_i_1_n_0 ;
  wire \count_cycle_reg[0]_i_1_n_1 ;
  wire \count_cycle_reg[0]_i_1_n_2 ;
  wire \count_cycle_reg[0]_i_1_n_3 ;
  wire \count_cycle_reg[0]_i_1_n_4 ;
  wire \count_cycle_reg[0]_i_1_n_5 ;
  wire \count_cycle_reg[0]_i_1_n_6 ;
  wire \count_cycle_reg[0]_i_1_n_7 ;
  wire \count_cycle_reg[12]_i_1_n_0 ;
  wire \count_cycle_reg[12]_i_1_n_1 ;
  wire \count_cycle_reg[12]_i_1_n_2 ;
  wire \count_cycle_reg[12]_i_1_n_3 ;
  wire \count_cycle_reg[12]_i_1_n_4 ;
  wire \count_cycle_reg[12]_i_1_n_5 ;
  wire \count_cycle_reg[12]_i_1_n_6 ;
  wire \count_cycle_reg[12]_i_1_n_7 ;
  wire \count_cycle_reg[16]_i_1_n_0 ;
  wire \count_cycle_reg[16]_i_1_n_1 ;
  wire \count_cycle_reg[16]_i_1_n_2 ;
  wire \count_cycle_reg[16]_i_1_n_3 ;
  wire \count_cycle_reg[16]_i_1_n_4 ;
  wire \count_cycle_reg[16]_i_1_n_5 ;
  wire \count_cycle_reg[16]_i_1_n_6 ;
  wire \count_cycle_reg[16]_i_1_n_7 ;
  wire \count_cycle_reg[20]_i_1_n_0 ;
  wire \count_cycle_reg[20]_i_1_n_1 ;
  wire \count_cycle_reg[20]_i_1_n_2 ;
  wire \count_cycle_reg[20]_i_1_n_3 ;
  wire \count_cycle_reg[20]_i_1_n_4 ;
  wire \count_cycle_reg[20]_i_1_n_5 ;
  wire \count_cycle_reg[20]_i_1_n_6 ;
  wire \count_cycle_reg[20]_i_1_n_7 ;
  wire \count_cycle_reg[24]_i_1_n_0 ;
  wire \count_cycle_reg[24]_i_1_n_1 ;
  wire \count_cycle_reg[24]_i_1_n_2 ;
  wire \count_cycle_reg[24]_i_1_n_3 ;
  wire \count_cycle_reg[24]_i_1_n_4 ;
  wire \count_cycle_reg[24]_i_1_n_5 ;
  wire \count_cycle_reg[24]_i_1_n_6 ;
  wire \count_cycle_reg[24]_i_1_n_7 ;
  wire \count_cycle_reg[28]_i_1_n_0 ;
  wire \count_cycle_reg[28]_i_1_n_1 ;
  wire \count_cycle_reg[28]_i_1_n_2 ;
  wire \count_cycle_reg[28]_i_1_n_3 ;
  wire \count_cycle_reg[28]_i_1_n_4 ;
  wire \count_cycle_reg[28]_i_1_n_5 ;
  wire \count_cycle_reg[28]_i_1_n_6 ;
  wire \count_cycle_reg[28]_i_1_n_7 ;
  wire \count_cycle_reg[32]_i_1_n_0 ;
  wire \count_cycle_reg[32]_i_1_n_1 ;
  wire \count_cycle_reg[32]_i_1_n_2 ;
  wire \count_cycle_reg[32]_i_1_n_3 ;
  wire \count_cycle_reg[32]_i_1_n_4 ;
  wire \count_cycle_reg[32]_i_1_n_5 ;
  wire \count_cycle_reg[32]_i_1_n_6 ;
  wire \count_cycle_reg[32]_i_1_n_7 ;
  wire \count_cycle_reg[36]_i_1_n_0 ;
  wire \count_cycle_reg[36]_i_1_n_1 ;
  wire \count_cycle_reg[36]_i_1_n_2 ;
  wire \count_cycle_reg[36]_i_1_n_3 ;
  wire \count_cycle_reg[36]_i_1_n_4 ;
  wire \count_cycle_reg[36]_i_1_n_5 ;
  wire \count_cycle_reg[36]_i_1_n_6 ;
  wire \count_cycle_reg[36]_i_1_n_7 ;
  wire \count_cycle_reg[40]_i_1_n_0 ;
  wire \count_cycle_reg[40]_i_1_n_1 ;
  wire \count_cycle_reg[40]_i_1_n_2 ;
  wire \count_cycle_reg[40]_i_1_n_3 ;
  wire \count_cycle_reg[40]_i_1_n_4 ;
  wire \count_cycle_reg[40]_i_1_n_5 ;
  wire \count_cycle_reg[40]_i_1_n_6 ;
  wire \count_cycle_reg[40]_i_1_n_7 ;
  wire \count_cycle_reg[44]_i_1_n_0 ;
  wire \count_cycle_reg[44]_i_1_n_1 ;
  wire \count_cycle_reg[44]_i_1_n_2 ;
  wire \count_cycle_reg[44]_i_1_n_3 ;
  wire \count_cycle_reg[44]_i_1_n_4 ;
  wire \count_cycle_reg[44]_i_1_n_5 ;
  wire \count_cycle_reg[44]_i_1_n_6 ;
  wire \count_cycle_reg[44]_i_1_n_7 ;
  wire \count_cycle_reg[48]_i_1_n_0 ;
  wire \count_cycle_reg[48]_i_1_n_1 ;
  wire \count_cycle_reg[48]_i_1_n_2 ;
  wire \count_cycle_reg[48]_i_1_n_3 ;
  wire \count_cycle_reg[48]_i_1_n_4 ;
  wire \count_cycle_reg[48]_i_1_n_5 ;
  wire \count_cycle_reg[48]_i_1_n_6 ;
  wire \count_cycle_reg[48]_i_1_n_7 ;
  wire \count_cycle_reg[4]_i_1_n_0 ;
  wire \count_cycle_reg[4]_i_1_n_1 ;
  wire \count_cycle_reg[4]_i_1_n_2 ;
  wire \count_cycle_reg[4]_i_1_n_3 ;
  wire \count_cycle_reg[4]_i_1_n_4 ;
  wire \count_cycle_reg[4]_i_1_n_5 ;
  wire \count_cycle_reg[4]_i_1_n_6 ;
  wire \count_cycle_reg[4]_i_1_n_7 ;
  wire \count_cycle_reg[52]_i_1_n_0 ;
  wire \count_cycle_reg[52]_i_1_n_1 ;
  wire \count_cycle_reg[52]_i_1_n_2 ;
  wire \count_cycle_reg[52]_i_1_n_3 ;
  wire \count_cycle_reg[52]_i_1_n_4 ;
  wire \count_cycle_reg[52]_i_1_n_5 ;
  wire \count_cycle_reg[52]_i_1_n_6 ;
  wire \count_cycle_reg[52]_i_1_n_7 ;
  wire \count_cycle_reg[56]_i_1_n_0 ;
  wire \count_cycle_reg[56]_i_1_n_1 ;
  wire \count_cycle_reg[56]_i_1_n_2 ;
  wire \count_cycle_reg[56]_i_1_n_3 ;
  wire \count_cycle_reg[56]_i_1_n_4 ;
  wire \count_cycle_reg[56]_i_1_n_5 ;
  wire \count_cycle_reg[56]_i_1_n_6 ;
  wire \count_cycle_reg[56]_i_1_n_7 ;
  wire \count_cycle_reg[60]_i_1_n_1 ;
  wire \count_cycle_reg[60]_i_1_n_2 ;
  wire \count_cycle_reg[60]_i_1_n_3 ;
  wire \count_cycle_reg[60]_i_1_n_4 ;
  wire \count_cycle_reg[60]_i_1_n_5 ;
  wire \count_cycle_reg[60]_i_1_n_6 ;
  wire \count_cycle_reg[60]_i_1_n_7 ;
  wire \count_cycle_reg[8]_i_1_n_0 ;
  wire \count_cycle_reg[8]_i_1_n_1 ;
  wire \count_cycle_reg[8]_i_1_n_2 ;
  wire \count_cycle_reg[8]_i_1_n_3 ;
  wire \count_cycle_reg[8]_i_1_n_4 ;
  wire \count_cycle_reg[8]_i_1_n_5 ;
  wire \count_cycle_reg[8]_i_1_n_6 ;
  wire \count_cycle_reg[8]_i_1_n_7 ;
  wire count_instr;
  wire \count_instr[0]_i_3_n_0 ;
  wire \count_instr_reg[0]_i_2_n_0 ;
  wire \count_instr_reg[0]_i_2_n_1 ;
  wire \count_instr_reg[0]_i_2_n_2 ;
  wire \count_instr_reg[0]_i_2_n_3 ;
  wire \count_instr_reg[0]_i_2_n_4 ;
  wire \count_instr_reg[0]_i_2_n_5 ;
  wire \count_instr_reg[0]_i_2_n_6 ;
  wire \count_instr_reg[0]_i_2_n_7 ;
  wire \count_instr_reg[12]_i_1_n_0 ;
  wire \count_instr_reg[12]_i_1_n_1 ;
  wire \count_instr_reg[12]_i_1_n_2 ;
  wire \count_instr_reg[12]_i_1_n_3 ;
  wire \count_instr_reg[12]_i_1_n_4 ;
  wire \count_instr_reg[12]_i_1_n_5 ;
  wire \count_instr_reg[12]_i_1_n_6 ;
  wire \count_instr_reg[12]_i_1_n_7 ;
  wire \count_instr_reg[16]_i_1_n_0 ;
  wire \count_instr_reg[16]_i_1_n_1 ;
  wire \count_instr_reg[16]_i_1_n_2 ;
  wire \count_instr_reg[16]_i_1_n_3 ;
  wire \count_instr_reg[16]_i_1_n_4 ;
  wire \count_instr_reg[16]_i_1_n_5 ;
  wire \count_instr_reg[16]_i_1_n_6 ;
  wire \count_instr_reg[16]_i_1_n_7 ;
  wire \count_instr_reg[20]_i_1_n_0 ;
  wire \count_instr_reg[20]_i_1_n_1 ;
  wire \count_instr_reg[20]_i_1_n_2 ;
  wire \count_instr_reg[20]_i_1_n_3 ;
  wire \count_instr_reg[20]_i_1_n_4 ;
  wire \count_instr_reg[20]_i_1_n_5 ;
  wire \count_instr_reg[20]_i_1_n_6 ;
  wire \count_instr_reg[20]_i_1_n_7 ;
  wire \count_instr_reg[24]_i_1_n_0 ;
  wire \count_instr_reg[24]_i_1_n_1 ;
  wire \count_instr_reg[24]_i_1_n_2 ;
  wire \count_instr_reg[24]_i_1_n_3 ;
  wire \count_instr_reg[24]_i_1_n_4 ;
  wire \count_instr_reg[24]_i_1_n_5 ;
  wire \count_instr_reg[24]_i_1_n_6 ;
  wire \count_instr_reg[24]_i_1_n_7 ;
  wire \count_instr_reg[28]_i_1_n_0 ;
  wire \count_instr_reg[28]_i_1_n_1 ;
  wire \count_instr_reg[28]_i_1_n_2 ;
  wire \count_instr_reg[28]_i_1_n_3 ;
  wire \count_instr_reg[28]_i_1_n_4 ;
  wire \count_instr_reg[28]_i_1_n_5 ;
  wire \count_instr_reg[28]_i_1_n_6 ;
  wire \count_instr_reg[28]_i_1_n_7 ;
  wire \count_instr_reg[32]_i_1_n_0 ;
  wire \count_instr_reg[32]_i_1_n_1 ;
  wire \count_instr_reg[32]_i_1_n_2 ;
  wire \count_instr_reg[32]_i_1_n_3 ;
  wire \count_instr_reg[32]_i_1_n_4 ;
  wire \count_instr_reg[32]_i_1_n_5 ;
  wire \count_instr_reg[32]_i_1_n_6 ;
  wire \count_instr_reg[32]_i_1_n_7 ;
  wire \count_instr_reg[36]_i_1_n_0 ;
  wire \count_instr_reg[36]_i_1_n_1 ;
  wire \count_instr_reg[36]_i_1_n_2 ;
  wire \count_instr_reg[36]_i_1_n_3 ;
  wire \count_instr_reg[36]_i_1_n_4 ;
  wire \count_instr_reg[36]_i_1_n_5 ;
  wire \count_instr_reg[36]_i_1_n_6 ;
  wire \count_instr_reg[36]_i_1_n_7 ;
  wire \count_instr_reg[40]_i_1_n_0 ;
  wire \count_instr_reg[40]_i_1_n_1 ;
  wire \count_instr_reg[40]_i_1_n_2 ;
  wire \count_instr_reg[40]_i_1_n_3 ;
  wire \count_instr_reg[40]_i_1_n_4 ;
  wire \count_instr_reg[40]_i_1_n_5 ;
  wire \count_instr_reg[40]_i_1_n_6 ;
  wire \count_instr_reg[40]_i_1_n_7 ;
  wire \count_instr_reg[44]_i_1_n_0 ;
  wire \count_instr_reg[44]_i_1_n_1 ;
  wire \count_instr_reg[44]_i_1_n_2 ;
  wire \count_instr_reg[44]_i_1_n_3 ;
  wire \count_instr_reg[44]_i_1_n_4 ;
  wire \count_instr_reg[44]_i_1_n_5 ;
  wire \count_instr_reg[44]_i_1_n_6 ;
  wire \count_instr_reg[44]_i_1_n_7 ;
  wire \count_instr_reg[48]_i_1_n_0 ;
  wire \count_instr_reg[48]_i_1_n_1 ;
  wire \count_instr_reg[48]_i_1_n_2 ;
  wire \count_instr_reg[48]_i_1_n_3 ;
  wire \count_instr_reg[48]_i_1_n_4 ;
  wire \count_instr_reg[48]_i_1_n_5 ;
  wire \count_instr_reg[48]_i_1_n_6 ;
  wire \count_instr_reg[48]_i_1_n_7 ;
  wire \count_instr_reg[4]_i_1_n_0 ;
  wire \count_instr_reg[4]_i_1_n_1 ;
  wire \count_instr_reg[4]_i_1_n_2 ;
  wire \count_instr_reg[4]_i_1_n_3 ;
  wire \count_instr_reg[4]_i_1_n_4 ;
  wire \count_instr_reg[4]_i_1_n_5 ;
  wire \count_instr_reg[4]_i_1_n_6 ;
  wire \count_instr_reg[4]_i_1_n_7 ;
  wire \count_instr_reg[52]_i_1_n_0 ;
  wire \count_instr_reg[52]_i_1_n_1 ;
  wire \count_instr_reg[52]_i_1_n_2 ;
  wire \count_instr_reg[52]_i_1_n_3 ;
  wire \count_instr_reg[52]_i_1_n_4 ;
  wire \count_instr_reg[52]_i_1_n_5 ;
  wire \count_instr_reg[52]_i_1_n_6 ;
  wire \count_instr_reg[52]_i_1_n_7 ;
  wire \count_instr_reg[56]_i_1_n_0 ;
  wire \count_instr_reg[56]_i_1_n_1 ;
  wire \count_instr_reg[56]_i_1_n_2 ;
  wire \count_instr_reg[56]_i_1_n_3 ;
  wire \count_instr_reg[56]_i_1_n_4 ;
  wire \count_instr_reg[56]_i_1_n_5 ;
  wire \count_instr_reg[56]_i_1_n_6 ;
  wire \count_instr_reg[56]_i_1_n_7 ;
  wire \count_instr_reg[60]_i_1_n_1 ;
  wire \count_instr_reg[60]_i_1_n_2 ;
  wire \count_instr_reg[60]_i_1_n_3 ;
  wire \count_instr_reg[60]_i_1_n_4 ;
  wire \count_instr_reg[60]_i_1_n_5 ;
  wire \count_instr_reg[60]_i_1_n_6 ;
  wire \count_instr_reg[60]_i_1_n_7 ;
  wire \count_instr_reg[8]_i_1_n_0 ;
  wire \count_instr_reg[8]_i_1_n_1 ;
  wire \count_instr_reg[8]_i_1_n_2 ;
  wire \count_instr_reg[8]_i_1_n_3 ;
  wire \count_instr_reg[8]_i_1_n_4 ;
  wire \count_instr_reg[8]_i_1_n_5 ;
  wire \count_instr_reg[8]_i_1_n_6 ;
  wire \count_instr_reg[8]_i_1_n_7 ;
  wire \count_instr_reg_n_0_[0] ;
  wire \count_instr_reg_n_0_[10] ;
  wire \count_instr_reg_n_0_[11] ;
  wire \count_instr_reg_n_0_[12] ;
  wire \count_instr_reg_n_0_[13] ;
  wire \count_instr_reg_n_0_[14] ;
  wire \count_instr_reg_n_0_[15] ;
  wire \count_instr_reg_n_0_[16] ;
  wire \count_instr_reg_n_0_[17] ;
  wire \count_instr_reg_n_0_[18] ;
  wire \count_instr_reg_n_0_[19] ;
  wire \count_instr_reg_n_0_[1] ;
  wire \count_instr_reg_n_0_[20] ;
  wire \count_instr_reg_n_0_[21] ;
  wire \count_instr_reg_n_0_[22] ;
  wire \count_instr_reg_n_0_[23] ;
  wire \count_instr_reg_n_0_[24] ;
  wire \count_instr_reg_n_0_[25] ;
  wire \count_instr_reg_n_0_[26] ;
  wire \count_instr_reg_n_0_[27] ;
  wire \count_instr_reg_n_0_[28] ;
  wire \count_instr_reg_n_0_[29] ;
  wire \count_instr_reg_n_0_[2] ;
  wire \count_instr_reg_n_0_[30] ;
  wire \count_instr_reg_n_0_[31] ;
  wire \count_instr_reg_n_0_[3] ;
  wire \count_instr_reg_n_0_[4] ;
  wire \count_instr_reg_n_0_[5] ;
  wire \count_instr_reg_n_0_[6] ;
  wire \count_instr_reg_n_0_[7] ;
  wire \count_instr_reg_n_0_[8] ;
  wire \count_instr_reg_n_0_[9] ;
  wire [7:0]cpu_state0_out;
  wire \cpu_state[2]_i_2_n_0 ;
  wire \cpu_state[3]_i_2_n_0 ;
  wire \cpu_state[3]_i_3_n_0 ;
  wire \cpu_state[3]_i_4_n_0 ;
  wire \cpu_state[3]_i_5_n_0 ;
  wire \cpu_state[3]_i_6_n_0 ;
  wire \cpu_state[3]_i_7_n_0 ;
  wire \cpu_state[6]_i_2_n_0 ;
  wire \cpu_state[6]_i_3_n_0 ;
  wire \cpu_state[7]_i_10_n_0 ;
  wire \cpu_state[7]_i_11_n_0 ;
  wire \cpu_state[7]_i_12_n_0 ;
  wire \cpu_state[7]_i_13_n_0 ;
  wire \cpu_state[7]_i_14_n_0 ;
  wire \cpu_state[7]_i_15_n_0 ;
  wire \cpu_state[7]_i_16_n_0 ;
  wire \cpu_state[7]_i_1_n_0 ;
  wire \cpu_state[7]_i_2_n_0 ;
  wire \cpu_state[7]_i_4_n_0 ;
  wire \cpu_state[7]_i_5_n_0 ;
  wire \cpu_state[7]_i_6_n_0 ;
  wire \cpu_state[7]_i_7_n_0 ;
  wire \cpu_state[7]_i_8_n_0 ;
  wire \cpu_state[7]_i_9_n_0 ;
  wire \cpu_state_reg_n_0_[0] ;
  wire \cpu_state_reg_n_0_[1] ;
  wire \cpu_state_reg_n_0_[2] ;
  wire \cpu_state_reg_n_0_[3] ;
  wire \cpu_state_reg_n_0_[5] ;
  wire \cpu_state_reg_n_0_[7] ;
  wire cpuregs_reg_r1_0_31_0_5_i_1_n_0;
  wire cpuregs_reg_r1_0_31_0_5_i_2_n_0;
  wire cpuregs_reg_r1_0_31_0_5_i_3_n_0;
  wire cpuregs_reg_r1_0_31_0_5_i_4_n_0;
  wire cpuregs_reg_r1_0_31_0_5_i_5_n_0;
  wire cpuregs_reg_r1_0_31_0_5_i_6_n_0;
  wire cpuregs_reg_r1_0_31_0_5_i_7_n_0;
  wire cpuregs_reg_r1_0_31_0_5_i_8_n_0;
  wire cpuregs_reg_r1_0_31_0_5_i_9_n_0;
  wire cpuregs_reg_r1_0_31_12_17_i_1_n_0;
  wire cpuregs_reg_r1_0_31_12_17_i_2_n_0;
  wire cpuregs_reg_r1_0_31_12_17_i_3_n_0;
  wire cpuregs_reg_r1_0_31_12_17_i_4_n_0;
  wire cpuregs_reg_r1_0_31_12_17_i_5_n_0;
  wire cpuregs_reg_r1_0_31_12_17_i_6_n_0;
  wire cpuregs_reg_r1_0_31_18_23_i_1_n_0;
  wire cpuregs_reg_r1_0_31_18_23_i_2_n_0;
  wire cpuregs_reg_r1_0_31_18_23_i_3_n_0;
  wire cpuregs_reg_r1_0_31_18_23_i_4_n_0;
  wire cpuregs_reg_r1_0_31_18_23_i_5_n_0;
  wire cpuregs_reg_r1_0_31_18_23_i_6_n_0;
  wire cpuregs_reg_r1_0_31_24_29_i_1_n_0;
  wire cpuregs_reg_r1_0_31_24_29_i_2_n_0;
  wire cpuregs_reg_r1_0_31_24_29_i_3_n_0;
  wire cpuregs_reg_r1_0_31_24_29_i_4_n_0;
  wire cpuregs_reg_r1_0_31_24_29_i_5_n_0;
  wire cpuregs_reg_r1_0_31_24_29_i_6_n_0;
  wire cpuregs_reg_r1_0_31_30_31_i_1_n_0;
  wire cpuregs_reg_r1_0_31_30_31_i_2_n_0;
  wire cpuregs_reg_r1_0_31_6_11_i_1_n_0;
  wire cpuregs_reg_r1_0_31_6_11_i_2_n_0;
  wire cpuregs_reg_r1_0_31_6_11_i_3_n_0;
  wire cpuregs_reg_r1_0_31_6_11_i_4_n_0;
  wire cpuregs_reg_r1_0_31_6_11_i_5_n_0;
  wire cpuregs_reg_r1_0_31_6_11_i_6_n_0;
  wire [31:1]current_pc;
  wire [31:0]data3;
  wire data4;
  wire data5;
  wire [19:0]decoded_imm;
  wire \decoded_imm[11]_i_2_n_0 ;
  wire \decoded_imm[11]_i_3_n_0 ;
  wire \decoded_imm[11]_i_4_n_0 ;
  wire \decoded_imm[19]_i_2_n_0 ;
  wire \decoded_imm[20]_i_1_n_0 ;
  wire \decoded_imm[21]_i_1_n_0 ;
  wire \decoded_imm[22]_i_1_n_0 ;
  wire \decoded_imm[23]_i_1_n_0 ;
  wire \decoded_imm[24]_i_1_n_0 ;
  wire \decoded_imm[25]_i_1_n_0 ;
  wire \decoded_imm[26]_i_1_n_0 ;
  wire \decoded_imm[27]_i_1_n_0 ;
  wire \decoded_imm[28]_i_1_n_0 ;
  wire \decoded_imm[29]_i_1_n_0 ;
  wire \decoded_imm[30]_i_1_n_0 ;
  wire \decoded_imm[31]_i_1_n_0 ;
  wire \decoded_imm[31]_i_2_n_0 ;
  wire \decoded_imm_reg_n_0_[0] ;
  wire \decoded_imm_reg_n_0_[10] ;
  wire \decoded_imm_reg_n_0_[11] ;
  wire \decoded_imm_reg_n_0_[12] ;
  wire \decoded_imm_reg_n_0_[13] ;
  wire \decoded_imm_reg_n_0_[14] ;
  wire \decoded_imm_reg_n_0_[15] ;
  wire \decoded_imm_reg_n_0_[16] ;
  wire \decoded_imm_reg_n_0_[17] ;
  wire \decoded_imm_reg_n_0_[18] ;
  wire \decoded_imm_reg_n_0_[19] ;
  wire \decoded_imm_reg_n_0_[1] ;
  wire \decoded_imm_reg_n_0_[20] ;
  wire \decoded_imm_reg_n_0_[21] ;
  wire \decoded_imm_reg_n_0_[22] ;
  wire \decoded_imm_reg_n_0_[23] ;
  wire \decoded_imm_reg_n_0_[24] ;
  wire \decoded_imm_reg_n_0_[25] ;
  wire \decoded_imm_reg_n_0_[26] ;
  wire \decoded_imm_reg_n_0_[27] ;
  wire \decoded_imm_reg_n_0_[28] ;
  wire \decoded_imm_reg_n_0_[29] ;
  wire \decoded_imm_reg_n_0_[2] ;
  wire \decoded_imm_reg_n_0_[30] ;
  wire \decoded_imm_reg_n_0_[31] ;
  wire \decoded_imm_reg_n_0_[3] ;
  wire \decoded_imm_reg_n_0_[4] ;
  wire \decoded_imm_reg_n_0_[5] ;
  wire \decoded_imm_reg_n_0_[6] ;
  wire \decoded_imm_reg_n_0_[7] ;
  wire \decoded_imm_reg_n_0_[8] ;
  wire \decoded_imm_reg_n_0_[9] ;
  wire [31:1]decoded_imm_uj;
  wire \decoded_imm_uj[10]_i_1_n_0 ;
  wire \decoded_imm_uj[12]_i_1_n_0 ;
  wire \decoded_imm_uj[13]_i_1_n_0 ;
  wire \decoded_imm_uj[14]_i_1_n_0 ;
  wire \decoded_imm_uj[5]_i_1_n_0 ;
  wire \decoded_imm_uj[6]_i_1_n_0 ;
  wire \decoded_imm_uj[7]_i_1_n_0 ;
  wire \decoded_imm_uj[8]_i_1_n_0 ;
  wire \decoded_imm_uj[9]_i_1_n_0 ;
  wire [4:0]decoded_rd;
  wire \decoded_rd[0]_i_1_n_0 ;
  wire \decoded_rd[1]_i_1_n_0 ;
  wire \decoded_rd[2]_i_1_n_0 ;
  wire \decoded_rd[3]_i_1_n_0 ;
  wire \decoded_rd[4]_i_1_n_0 ;
  wire [4:0]decoded_rs1;
  wire [4:0]decoded_rs1_0;
  wire \decoded_rs1_rep[0]_i_1_n_0 ;
  wire \decoded_rs1_rep[1]_i_1_n_0 ;
  wire \decoded_rs1_rep[2]_i_1_n_0 ;
  wire \decoded_rs1_rep[3]_i_1_n_0 ;
  wire \decoded_rs1_rep[4]_i_1_n_0 ;
  wire [4:0]decoded_rs2;
  wire decoder_pseudo_trigger;
  wire decoder_pseudo_trigger_i_2_n_0;
  wire decoder_pseudo_trigger_reg_n_0;
  wire decoder_trigger_i_1_n_0;
  wire decoder_trigger_i_2_n_0;
  wire decoder_trigger_i_3_n_0;
  wire decoder_trigger_i_4_n_0;
  wire decoder_trigger_i_5_n_0;
  wire decoder_trigger_reg_n_0;
  wire i___29_carry__0_i_1_n_0;
  wire i___29_carry__0_i_2_n_0;
  wire i___29_carry__0_i_3_n_0;
  wire i___29_carry__0_i_4_n_0;
  wire i___29_carry__1_i_1_n_0;
  wire i___29_carry__1_i_2_n_0;
  wire i___29_carry__1_i_3_n_0;
  wire i___29_carry__1_i_4_n_0;
  wire i___29_carry__2_i_1_n_0;
  wire i___29_carry__2_i_2_n_0;
  wire i___29_carry__2_i_3_n_0;
  wire i___29_carry__2_i_4_n_0;
  wire i___29_carry__3_i_1_n_0;
  wire i___29_carry__3_i_2_n_0;
  wire i___29_carry__3_i_3_n_0;
  wire i___29_carry__3_i_4_n_0;
  wire i___29_carry__4_i_1_n_0;
  wire i___29_carry__4_i_2_n_0;
  wire i___29_carry__4_i_3_n_0;
  wire i___29_carry__4_i_4_n_0;
  wire i___29_carry__5_i_1_n_0;
  wire i___29_carry__5_i_2_n_0;
  wire i___29_carry__5_i_3_n_0;
  wire i___29_carry__5_i_4_n_0;
  wire i___29_carry__6_i_1_n_0;
  wire i___29_carry__6_i_2_n_0;
  wire i___29_carry__6_i_3_n_0;
  wire i___29_carry__6_i_4_n_0;
  wire i___29_carry_i_1_n_0;
  wire i___29_carry_i_2_n_0;
  wire i___29_carry_i_3_n_0;
  wire i___29_carry_i_4_n_0;
  wire i__carry__0_i_1__0_n_0;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2__0_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3__0_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4__0_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__0_i_5_n_0;
  wire i__carry__0_i_6_n_0;
  wire i__carry__0_i_7_n_0;
  wire i__carry__0_i_8_n_0;
  wire i__carry__1_i_1__0_n_0;
  wire i__carry__1_i_1_n_0;
  wire i__carry__1_i_2__0_n_0;
  wire i__carry__1_i_2_n_0;
  wire i__carry__1_i_3__0_n_0;
  wire i__carry__1_i_3_n_0;
  wire i__carry__1_i_4__0_n_0;
  wire i__carry__1_i_4_n_0;
  wire i__carry__1_i_5_n_0;
  wire i__carry__1_i_6_n_0;
  wire i__carry__1_i_7_n_0;
  wire i__carry__1_i_8_n_0;
  wire i__carry__2_i_1__0_n_0;
  wire i__carry__2_i_1_n_0;
  wire i__carry__2_i_2__0_n_0;
  wire i__carry__2_i_2_n_0;
  wire i__carry__2_i_3__0_n_0;
  wire i__carry__2_i_3_n_0;
  wire i__carry__2_i_4__0_n_0;
  wire i__carry__2_i_4_n_0;
  wire i__carry__2_i_5_n_0;
  wire i__carry__2_i_6_n_0;
  wire i__carry__2_i_7_n_0;
  wire i__carry__2_i_8_n_0;
  wire i__carry__3_i_1_n_0;
  wire i__carry__3_i_2_n_0;
  wire i__carry__3_i_3_n_0;
  wire i__carry__3_i_4_n_0;
  wire i__carry__4_i_1_n_0;
  wire i__carry__4_i_2_n_0;
  wire i__carry__4_i_3_n_0;
  wire i__carry__4_i_4_n_0;
  wire i__carry__5_i_1_n_0;
  wire i__carry__5_i_2_n_0;
  wire i__carry__5_i_3_n_0;
  wire i__carry__5_i_4_n_0;
  wire i__carry__6_i_1_n_0;
  wire i__carry__6_i_2_n_0;
  wire i__carry__6_i_3_n_0;
  wire i__carry__6_i_4_n_0;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__0_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3__0_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4__0_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire instr_add;
  wire instr_add0;
  wire instr_addi;
  wire instr_addi0;
  wire instr_and;
  wire instr_and0;
  wire instr_and_i_1_n_0;
  wire instr_and_i_3_n_0;
  wire instr_and_i_4_n_0;
  wire instr_andi;
  wire instr_andi0;
  wire instr_auipc;
  wire instr_auipc_i_1_n_0;
  wire instr_auipc_i_2_n_0;
  wire instr_beq;
  wire instr_beq0;
  wire instr_bge;
  wire instr_bge0;
  wire instr_bgeu;
  wire instr_bgeu0;
  wire instr_blt;
  wire instr_blt0;
  wire instr_bltu;
  wire instr_bltu0;
  wire instr_bne;
  wire instr_bne0;
  wire instr_jal;
  wire instr_jal_i_2_n_0;
  wire instr_jal_i_3_n_0;
  wire instr_jal_i_4_n_0;
  wire instr_jalr;
  wire instr_jalr0;
  wire instr_jalr_i_2_n_0;
  wire instr_lb;
  wire instr_lb_i_1_n_0;
  wire instr_lbu;
  wire instr_lbu_i_1_n_0;
  wire instr_lh;
  wire instr_lh_i_1_n_0;
  wire instr_lhu;
  wire instr_lhu_i_1_n_0;
  wire instr_lhu_i_2_n_0;
  wire instr_lui;
  wire instr_lui0;
  wire instr_lui_i_1_n_0;
  wire instr_lw;
  wire instr_lw_i_1_n_0;
  wire instr_or;
  wire instr_or0;
  wire instr_ori;
  wire instr_ori0;
  wire instr_rdcycle;
  wire instr_rdcycle_i_1_n_0;
  wire instr_rdcycleh;
  wire instr_rdcycleh_i_1_n_0;
  wire instr_rdinstr;
  wire instr_rdinstr_i_1_n_0;
  wire instr_rdinstrh;
  wire instr_rdinstrh_i_1_n_0;
  wire instr_rdinstrh_i_2_n_0;
  wire instr_rdinstrh_i_3_n_0;
  wire instr_rdinstrh_i_4_n_0;
  wire instr_rdinstrh_i_5_n_0;
  wire instr_rdinstrh_i_6_n_0;
  wire instr_rdinstrh_i_7_n_0;
  wire instr_sb;
  wire instr_sb0;
  wire instr_sh;
  wire instr_sh0;
  wire instr_sll;
  wire instr_sll0;
  wire instr_slli;
  wire instr_slli0;
  wire instr_slt;
  wire instr_slt0;
  wire instr_slti;
  wire instr_slti0;
  wire instr_sltiu;
  wire instr_sltiu0;
  wire instr_sltu;
  wire instr_sltu0;
  wire instr_sra;
  wire instr_sra0;
  wire instr_sra_i_2_n_0;
  wire instr_srai;
  wire instr_srai0;
  wire instr_srl;
  wire instr_srl0;
  wire instr_srli;
  wire instr_srli0;
  wire instr_srli_i_2_n_0;
  wire instr_sub;
  wire instr_sub0;
  wire instr_sw;
  wire instr_sw0;
  wire instr_xor;
  wire instr_xor0;
  wire instr_xori;
  wire instr_xori0;
  wire is_alu_reg_imm;
  wire is_alu_reg_imm_i_1_n_0;
  wire is_alu_reg_reg;
  wire is_alu_reg_reg_i_1_n_0;
  wire is_beq_bne_blt_bge_bltu_bgeu;
  wire is_beq_bne_blt_bge_bltu_bgeu_i_1_n_0;
  wire is_compare;
  wire is_compare_i_1_n_0;
  wire is_compare_i_2_n_0;
  wire is_jalr_addi_slti_sltiu_xori_ori_andi;
  wire is_jalr_addi_slti_sltiu_xori_ori_andi0;
  wire is_lb_lh_lw_lbu_lhu;
  wire is_lb_lh_lw_lbu_lhu_i_1_n_0;
  wire is_lbu_lhu_lw;
  wire is_lbu_lhu_lw_i_1_n_0;
  wire is_lui_auipc_jal;
  wire is_lui_auipc_jal_i_1_n_0;
  wire is_lui_auipc_jal_jalr_addi_add_sub;
  wire is_lui_auipc_jal_jalr_addi_add_sub0;
  wire is_lui_auipc_jal_jalr_addi_add_sub_i_1_n_0;
  wire is_sb_sh_sw;
  wire is_sb_sh_sw_i_1_n_0;
  wire is_sb_sh_sw_i_2_n_0;
  wire is_sll_srl_sra;
  wire is_sll_srl_sra0;
  wire is_sll_srl_sra_i_3_n_0;
  wire is_sll_srl_sra_i_4_n_0;
  wire is_slli_srli_srai;
  wire is_slli_srli_srai0;
  wire is_slti_blt_slt;
  wire is_slti_blt_slt_i_1_n_0;
  wire is_sltiu_bltu_sltu;
  wire is_sltiu_bltu_sltu_i_1_n_0;
  wire latched_branch_i_1_n_0;
  wire latched_branch_i_2_n_0;
  wire latched_branch_i_3_n_0;
  wire latched_branch_reg_n_0;
  wire latched_is_lb_i_1_n_0;
  wire latched_is_lb_reg_n_0;
  wire latched_is_lh_i_1_n_0;
  wire latched_is_lh_reg_n_0;
  wire latched_is_lu;
  wire latched_is_lu_i_1_n_0;
  wire latched_is_lu_reg_n_0;
  wire [4:0]latched_rd;
  wire \latched_rd[4]_i_1_n_0 ;
  wire \latched_rd[4]_i_2_n_0 ;
  wire latched_stalu_i_1_n_0;
  wire latched_stalu_reg_n_0;
  wire latched_store;
  wire latched_store_i_1_n_0;
  wire latched_store_i_2_n_0;
  wire latched_store_reg_n_0;
  wire mem_do_prefetch_i_1_n_0;
  wire mem_do_prefetch_i_2_n_0;
  wire mem_do_prefetch_reg_n_0;
  wire mem_do_rdata;
  wire mem_do_rdata_i_1_n_0;
  wire mem_do_rdata_i_2_n_0;
  wire mem_do_rdata_i_3_n_0;
  wire mem_do_rinst0;
  wire mem_do_rinst5_out;
  wire mem_do_rinst_i_10_n_0;
  wire mem_do_rinst_i_11_n_0;
  wire mem_do_rinst_i_1_n_0;
  wire mem_do_rinst_i_3_n_0;
  wire mem_do_rinst_i_4_n_0;
  wire mem_do_rinst_i_5_n_0;
  wire mem_do_rinst_i_6_n_0;
  wire mem_do_rinst_i_7_n_0;
  wire mem_do_rinst_i_8_n_0;
  wire mem_do_rinst_i_9_n_0;
  wire mem_do_rinst_reg_n_0;
  wire mem_do_wdata;
  wire mem_do_wdata_i_1_n_0;
  wire [31:0]mem_rdata;
  wire \mem_rdata_q[0]_i_1_n_0 ;
  wire \mem_rdata_q[1]_i_1_n_0 ;
  wire \mem_rdata_q[2]_i_1_n_0 ;
  wire \mem_rdata_q[3]_i_1_n_0 ;
  wire \mem_rdata_q[4]_i_1_n_0 ;
  wire \mem_rdata_q[5]_i_1_n_0 ;
  wire \mem_rdata_q[6]_i_1_n_0 ;
  wire \mem_rdata_q_reg_n_0_[0] ;
  wire \mem_rdata_q_reg_n_0_[10] ;
  wire \mem_rdata_q_reg_n_0_[11] ;
  wire \mem_rdata_q_reg_n_0_[15] ;
  wire \mem_rdata_q_reg_n_0_[16] ;
  wire \mem_rdata_q_reg_n_0_[17] ;
  wire \mem_rdata_q_reg_n_0_[18] ;
  wire \mem_rdata_q_reg_n_0_[19] ;
  wire \mem_rdata_q_reg_n_0_[1] ;
  wire \mem_rdata_q_reg_n_0_[20] ;
  wire \mem_rdata_q_reg_n_0_[21] ;
  wire \mem_rdata_q_reg_n_0_[22] ;
  wire \mem_rdata_q_reg_n_0_[23] ;
  wire \mem_rdata_q_reg_n_0_[24] ;
  wire \mem_rdata_q_reg_n_0_[25] ;
  wire \mem_rdata_q_reg_n_0_[26] ;
  wire \mem_rdata_q_reg_n_0_[27] ;
  wire \mem_rdata_q_reg_n_0_[28] ;
  wire \mem_rdata_q_reg_n_0_[29] ;
  wire \mem_rdata_q_reg_n_0_[2] ;
  wire \mem_rdata_q_reg_n_0_[30] ;
  wire \mem_rdata_q_reg_n_0_[31] ;
  wire \mem_rdata_q_reg_n_0_[3] ;
  wire \mem_rdata_q_reg_n_0_[4] ;
  wire \mem_rdata_q_reg_n_0_[5] ;
  wire \mem_rdata_q_reg_n_0_[6] ;
  wire \mem_rdata_q_reg_n_0_[7] ;
  wire \mem_rdata_q_reg_n_0_[8] ;
  wire \mem_rdata_q_reg_n_0_[9] ;
  wire mem_state;
  wire \mem_state[0]_i_1_n_0 ;
  wire \mem_state[1]_i_1_n_0 ;
  wire \mem_state_reg_n_0_[0] ;
  wire \mem_state_reg_n_0_[1] ;
  wire mem_valid11_out;
  wire mem_valid_i_1_n_0;
  wire mem_valid_i_2_n_0;
  wire mem_valid_reg_n_0;
  wire [1:0]mem_wordsize;
  wire \mem_wordsize[0]_i_1_n_0 ;
  wire \mem_wordsize[1]_i_1_n_0 ;
  wire \mem_wordsize[1]_i_3_n_0 ;
  wire \mem_wordsize_reg_n_0_[0] ;
  wire \mem_wordsize_reg_n_0_[1] ;
  wire memory_reg_0_i_15_n_0;
  wire memory_reg_0_i_16_n_0;
  wire memory_reg_0_i_17_n_0;
  wire memory_reg_0_i_18_n_0;
  wire memory_reg_0_i_19_n_0;
  wire memory_reg_0_i_20_n_0;
  wire memory_reg_0_i_21_n_0;
  wire memory_reg_0_i_22_n_0;
  wire memory_reg_0_i_23_n_0;
  wire memory_reg_0_i_24_n_0;
  wire memory_reg_0_i_25_n_0;
  wire memory_reg_0_i_26_n_0;
  wire memory_reg_0_i_27_n_0;
  wire memory_reg_0_i_28_n_0;
  wire memory_reg_0_i_29_n_0;
  wire memory_reg_0_i_30_n_0;
  wire memory_reg_0_i_31_n_0;
  wire memory_reg_0_i_32_n_0;
  wire memory_reg_0_i_33_n_0;
  wire memory_reg_3_i_10_n_0;
  wire \out_byte[7]_i_2_n_0 ;
  wire \out_byte[7]_i_3_n_0 ;
  wire \out_byte[7]_i_4_n_0 ;
  wire \out_byte[7]_i_5_n_0 ;
  wire out_byte_en01_out;
  wire [3:0]p_0_in;
  wire p_0_in0;
  wire [2:0]p_0_in_1;
  wire [4:0]p_1_in;
  wire p_1_out_carry__0_n_0;
  wire p_1_out_carry__0_n_1;
  wire p_1_out_carry__0_n_2;
  wire p_1_out_carry__0_n_3;
  wire p_1_out_carry__0_n_4;
  wire p_1_out_carry__0_n_5;
  wire p_1_out_carry__0_n_6;
  wire p_1_out_carry__0_n_7;
  wire p_1_out_carry__1_n_0;
  wire p_1_out_carry__1_n_1;
  wire p_1_out_carry__1_n_2;
  wire p_1_out_carry__1_n_3;
  wire p_1_out_carry__1_n_4;
  wire p_1_out_carry__1_n_5;
  wire p_1_out_carry__1_n_6;
  wire p_1_out_carry__1_n_7;
  wire p_1_out_carry__2_n_0;
  wire p_1_out_carry__2_n_1;
  wire p_1_out_carry__2_n_2;
  wire p_1_out_carry__2_n_3;
  wire p_1_out_carry__2_n_4;
  wire p_1_out_carry__2_n_5;
  wire p_1_out_carry__2_n_6;
  wire p_1_out_carry__2_n_7;
  wire p_1_out_carry__3_n_0;
  wire p_1_out_carry__3_n_1;
  wire p_1_out_carry__3_n_2;
  wire p_1_out_carry__3_n_3;
  wire p_1_out_carry__3_n_4;
  wire p_1_out_carry__3_n_5;
  wire p_1_out_carry__3_n_6;
  wire p_1_out_carry__3_n_7;
  wire p_1_out_carry__4_n_0;
  wire p_1_out_carry__4_n_1;
  wire p_1_out_carry__4_n_2;
  wire p_1_out_carry__4_n_3;
  wire p_1_out_carry__4_n_4;
  wire p_1_out_carry__4_n_5;
  wire p_1_out_carry__4_n_6;
  wire p_1_out_carry__4_n_7;
  wire p_1_out_carry__5_n_0;
  wire p_1_out_carry__5_n_1;
  wire p_1_out_carry__5_n_2;
  wire p_1_out_carry__5_n_3;
  wire p_1_out_carry__5_n_4;
  wire p_1_out_carry__5_n_5;
  wire p_1_out_carry__5_n_6;
  wire p_1_out_carry__5_n_7;
  wire p_1_out_carry__6_n_1;
  wire p_1_out_carry__6_n_2;
  wire p_1_out_carry__6_n_3;
  wire p_1_out_carry__6_n_4;
  wire p_1_out_carry__6_n_5;
  wire p_1_out_carry__6_n_6;
  wire p_1_out_carry__6_n_7;
  wire p_1_out_carry_i_1_n_0;
  wire p_1_out_carry_n_0;
  wire p_1_out_carry_n_1;
  wire p_1_out_carry_n_2;
  wire p_1_out_carry_n_3;
  wire p_1_out_carry_n_4;
  wire p_1_out_carry_n_5;
  wire p_1_out_carry_n_6;
  wire [23:0]p_2_in;
  wire reg_next_pc;
  wire reg_next_pc0_carry__0_i_1_n_0;
  wire reg_next_pc0_carry__0_i_2_n_0;
  wire reg_next_pc0_carry__0_i_3_n_0;
  wire reg_next_pc0_carry__0_i_4_n_0;
  wire reg_next_pc0_carry__0_n_0;
  wire reg_next_pc0_carry__0_n_1;
  wire reg_next_pc0_carry__0_n_2;
  wire reg_next_pc0_carry__0_n_3;
  wire reg_next_pc0_carry__1_i_1_n_0;
  wire reg_next_pc0_carry__1_i_2_n_0;
  wire reg_next_pc0_carry__1_i_3_n_0;
  wire reg_next_pc0_carry__1_i_4_n_0;
  wire reg_next_pc0_carry__1_n_0;
  wire reg_next_pc0_carry__1_n_1;
  wire reg_next_pc0_carry__1_n_2;
  wire reg_next_pc0_carry__1_n_3;
  wire reg_next_pc0_carry__2_i_1_n_0;
  wire reg_next_pc0_carry__2_i_2_n_0;
  wire reg_next_pc0_carry__2_i_3_n_0;
  wire reg_next_pc0_carry__2_i_4_n_0;
  wire reg_next_pc0_carry__2_n_0;
  wire reg_next_pc0_carry__2_n_1;
  wire reg_next_pc0_carry__2_n_2;
  wire reg_next_pc0_carry__2_n_3;
  wire reg_next_pc0_carry__3_i_1_n_0;
  wire reg_next_pc0_carry__3_i_2_n_0;
  wire reg_next_pc0_carry__3_i_3_n_0;
  wire reg_next_pc0_carry__3_i_4_n_0;
  wire reg_next_pc0_carry__3_i_5_n_0;
  wire reg_next_pc0_carry__3_n_0;
  wire reg_next_pc0_carry__3_n_1;
  wire reg_next_pc0_carry__3_n_2;
  wire reg_next_pc0_carry__3_n_3;
  wire reg_next_pc0_carry__4_i_1_n_0;
  wire reg_next_pc0_carry__4_i_2_n_0;
  wire reg_next_pc0_carry__4_i_3_n_0;
  wire reg_next_pc0_carry__4_i_4_n_0;
  wire reg_next_pc0_carry__4_n_0;
  wire reg_next_pc0_carry__4_n_1;
  wire reg_next_pc0_carry__4_n_2;
  wire reg_next_pc0_carry__4_n_3;
  wire reg_next_pc0_carry__5_i_1_n_0;
  wire reg_next_pc0_carry__5_i_2_n_0;
  wire reg_next_pc0_carry__5_i_3_n_0;
  wire reg_next_pc0_carry__5_i_4_n_0;
  wire reg_next_pc0_carry__5_n_0;
  wire reg_next_pc0_carry__5_n_1;
  wire reg_next_pc0_carry__5_n_2;
  wire reg_next_pc0_carry__5_n_3;
  wire reg_next_pc0_carry__6_i_1_n_0;
  wire reg_next_pc0_carry__6_i_2_n_0;
  wire reg_next_pc0_carry__6_i_3_n_0;
  wire reg_next_pc0_carry__6_n_2;
  wire reg_next_pc0_carry__6_n_3;
  wire reg_next_pc0_carry_i_1_n_0;
  wire reg_next_pc0_carry_i_2_n_0;
  wire reg_next_pc0_carry_i_3_n_0;
  wire reg_next_pc0_carry_i_4_n_0;
  wire reg_next_pc0_carry_n_0;
  wire reg_next_pc0_carry_n_1;
  wire reg_next_pc0_carry_n_2;
  wire reg_next_pc0_carry_n_3;
  wire [31:1]reg_next_pc1_in;
  wire \reg_next_pc_reg_n_0_[10] ;
  wire \reg_next_pc_reg_n_0_[11] ;
  wire \reg_next_pc_reg_n_0_[12] ;
  wire \reg_next_pc_reg_n_0_[13] ;
  wire \reg_next_pc_reg_n_0_[14] ;
  wire \reg_next_pc_reg_n_0_[15] ;
  wire \reg_next_pc_reg_n_0_[16] ;
  wire \reg_next_pc_reg_n_0_[17] ;
  wire \reg_next_pc_reg_n_0_[18] ;
  wire \reg_next_pc_reg_n_0_[19] ;
  wire \reg_next_pc_reg_n_0_[1] ;
  wire \reg_next_pc_reg_n_0_[20] ;
  wire \reg_next_pc_reg_n_0_[21] ;
  wire \reg_next_pc_reg_n_0_[22] ;
  wire \reg_next_pc_reg_n_0_[23] ;
  wire \reg_next_pc_reg_n_0_[24] ;
  wire \reg_next_pc_reg_n_0_[25] ;
  wire \reg_next_pc_reg_n_0_[26] ;
  wire \reg_next_pc_reg_n_0_[27] ;
  wire \reg_next_pc_reg_n_0_[28] ;
  wire \reg_next_pc_reg_n_0_[29] ;
  wire \reg_next_pc_reg_n_0_[2] ;
  wire \reg_next_pc_reg_n_0_[30] ;
  wire \reg_next_pc_reg_n_0_[31] ;
  wire \reg_next_pc_reg_n_0_[3] ;
  wire \reg_next_pc_reg_n_0_[4] ;
  wire \reg_next_pc_reg_n_0_[5] ;
  wire \reg_next_pc_reg_n_0_[6] ;
  wire \reg_next_pc_reg_n_0_[7] ;
  wire \reg_next_pc_reg_n_0_[8] ;
  wire \reg_next_pc_reg_n_0_[9] ;
  wire \reg_op10_inferred__0/i__carry__0_n_0 ;
  wire \reg_op10_inferred__0/i__carry__0_n_1 ;
  wire \reg_op10_inferred__0/i__carry__0_n_2 ;
  wire \reg_op10_inferred__0/i__carry__0_n_3 ;
  wire \reg_op10_inferred__0/i__carry__0_n_4 ;
  wire \reg_op10_inferred__0/i__carry__0_n_5 ;
  wire \reg_op10_inferred__0/i__carry__0_n_6 ;
  wire \reg_op10_inferred__0/i__carry__0_n_7 ;
  wire \reg_op10_inferred__0/i__carry__1_n_0 ;
  wire \reg_op10_inferred__0/i__carry__1_n_1 ;
  wire \reg_op10_inferred__0/i__carry__1_n_2 ;
  wire \reg_op10_inferred__0/i__carry__1_n_3 ;
  wire \reg_op10_inferred__0/i__carry__1_n_4 ;
  wire \reg_op10_inferred__0/i__carry__1_n_5 ;
  wire \reg_op10_inferred__0/i__carry__1_n_6 ;
  wire \reg_op10_inferred__0/i__carry__1_n_7 ;
  wire \reg_op10_inferred__0/i__carry__2_n_0 ;
  wire \reg_op10_inferred__0/i__carry__2_n_1 ;
  wire \reg_op10_inferred__0/i__carry__2_n_2 ;
  wire \reg_op10_inferred__0/i__carry__2_n_3 ;
  wire \reg_op10_inferred__0/i__carry__2_n_4 ;
  wire \reg_op10_inferred__0/i__carry__2_n_5 ;
  wire \reg_op10_inferred__0/i__carry__2_n_6 ;
  wire \reg_op10_inferred__0/i__carry__2_n_7 ;
  wire \reg_op10_inferred__0/i__carry__3_n_0 ;
  wire \reg_op10_inferred__0/i__carry__3_n_1 ;
  wire \reg_op10_inferred__0/i__carry__3_n_2 ;
  wire \reg_op10_inferred__0/i__carry__3_n_3 ;
  wire \reg_op10_inferred__0/i__carry__3_n_4 ;
  wire \reg_op10_inferred__0/i__carry__3_n_5 ;
  wire \reg_op10_inferred__0/i__carry__3_n_6 ;
  wire \reg_op10_inferred__0/i__carry__3_n_7 ;
  wire \reg_op10_inferred__0/i__carry__4_n_0 ;
  wire \reg_op10_inferred__0/i__carry__4_n_1 ;
  wire \reg_op10_inferred__0/i__carry__4_n_2 ;
  wire \reg_op10_inferred__0/i__carry__4_n_3 ;
  wire \reg_op10_inferred__0/i__carry__4_n_4 ;
  wire \reg_op10_inferred__0/i__carry__4_n_5 ;
  wire \reg_op10_inferred__0/i__carry__4_n_6 ;
  wire \reg_op10_inferred__0/i__carry__4_n_7 ;
  wire \reg_op10_inferred__0/i__carry__5_n_0 ;
  wire \reg_op10_inferred__0/i__carry__5_n_1 ;
  wire \reg_op10_inferred__0/i__carry__5_n_2 ;
  wire \reg_op10_inferred__0/i__carry__5_n_3 ;
  wire \reg_op10_inferred__0/i__carry__5_n_4 ;
  wire \reg_op10_inferred__0/i__carry__5_n_5 ;
  wire \reg_op10_inferred__0/i__carry__5_n_6 ;
  wire \reg_op10_inferred__0/i__carry__5_n_7 ;
  wire \reg_op10_inferred__0/i__carry__6_n_1 ;
  wire \reg_op10_inferred__0/i__carry__6_n_2 ;
  wire \reg_op10_inferred__0/i__carry__6_n_3 ;
  wire \reg_op10_inferred__0/i__carry__6_n_4 ;
  wire \reg_op10_inferred__0/i__carry__6_n_5 ;
  wire \reg_op10_inferred__0/i__carry__6_n_6 ;
  wire \reg_op10_inferred__0/i__carry__6_n_7 ;
  wire \reg_op10_inferred__0/i__carry_n_0 ;
  wire \reg_op10_inferred__0/i__carry_n_1 ;
  wire \reg_op10_inferred__0/i__carry_n_2 ;
  wire \reg_op10_inferred__0/i__carry_n_3 ;
  wire \reg_op10_inferred__0/i__carry_n_4 ;
  wire \reg_op10_inferred__0/i__carry_n_5 ;
  wire \reg_op10_inferred__0/i__carry_n_6 ;
  wire \reg_op10_inferred__0/i__carry_n_7 ;
  wire \reg_op1[0]_i_1_n_0 ;
  wire \reg_op1[0]_i_2_n_0 ;
  wire \reg_op1[0]_i_3_n_0 ;
  wire \reg_op1[0]_i_4_n_0 ;
  wire \reg_op1[0]_i_5_n_0 ;
  wire \reg_op1[10]_i_1_n_0 ;
  wire \reg_op1[10]_i_2_n_0 ;
  wire \reg_op1[10]_i_3_n_0 ;
  wire \reg_op1[11]_i_1_n_0 ;
  wire \reg_op1[11]_i_2_n_0 ;
  wire \reg_op1[11]_i_3_n_0 ;
  wire \reg_op1[12]_i_1_n_0 ;
  wire \reg_op1[12]_i_2_n_0 ;
  wire \reg_op1[12]_i_3_n_0 ;
  wire \reg_op1[13]_i_1_n_0 ;
  wire \reg_op1[13]_i_2_n_0 ;
  wire \reg_op1[13]_i_3_n_0 ;
  wire \reg_op1[14]_i_1_n_0 ;
  wire \reg_op1[14]_i_2_n_0 ;
  wire \reg_op1[14]_i_3_n_0 ;
  wire \reg_op1[15]_i_1_n_0 ;
  wire \reg_op1[15]_i_2_n_0 ;
  wire \reg_op1[15]_i_3_n_0 ;
  wire \reg_op1[16]_i_1_n_0 ;
  wire \reg_op1[16]_i_2_n_0 ;
  wire \reg_op1[16]_i_3_n_0 ;
  wire \reg_op1[17]_i_1_n_0 ;
  wire \reg_op1[17]_i_2_n_0 ;
  wire \reg_op1[17]_i_3_n_0 ;
  wire \reg_op1[18]_i_1_n_0 ;
  wire \reg_op1[18]_i_2_n_0 ;
  wire \reg_op1[18]_i_3_n_0 ;
  wire \reg_op1[19]_i_1_n_0 ;
  wire \reg_op1[19]_i_2_n_0 ;
  wire \reg_op1[19]_i_3_n_0 ;
  wire \reg_op1[1]_i_1_n_0 ;
  wire \reg_op1[1]_i_2_n_0 ;
  wire \reg_op1[1]_i_3_n_0 ;
  wire \reg_op1[20]_i_1_n_0 ;
  wire \reg_op1[20]_i_2_n_0 ;
  wire \reg_op1[20]_i_3_n_0 ;
  wire \reg_op1[21]_i_1_n_0 ;
  wire \reg_op1[21]_i_2_n_0 ;
  wire \reg_op1[21]_i_3_n_0 ;
  wire \reg_op1[22]_i_1_n_0 ;
  wire \reg_op1[22]_i_2_n_0 ;
  wire \reg_op1[22]_i_3_n_0 ;
  wire \reg_op1[23]_i_1_n_0 ;
  wire \reg_op1[23]_i_2_n_0 ;
  wire \reg_op1[23]_i_3_n_0 ;
  wire \reg_op1[24]_i_1_n_0 ;
  wire \reg_op1[24]_i_2_n_0 ;
  wire \reg_op1[24]_i_3_n_0 ;
  wire \reg_op1[25]_i_1_n_0 ;
  wire \reg_op1[25]_i_2_n_0 ;
  wire \reg_op1[25]_i_3_n_0 ;
  wire \reg_op1[26]_i_1_n_0 ;
  wire \reg_op1[26]_i_2_n_0 ;
  wire \reg_op1[26]_i_3_n_0 ;
  wire \reg_op1[27]_i_1_n_0 ;
  wire \reg_op1[27]_i_2_n_0 ;
  wire \reg_op1[27]_i_3_n_0 ;
  wire \reg_op1[27]_i_4_n_0 ;
  wire \reg_op1[27]_i_5_n_0 ;
  wire \reg_op1[27]_i_6_n_0 ;
  wire \reg_op1[28]_i_1_n_0 ;
  wire \reg_op1[28]_i_2_n_0 ;
  wire \reg_op1[28]_i_3_n_0 ;
  wire \reg_op1[29]_i_1_n_0 ;
  wire \reg_op1[29]_i_2_n_0 ;
  wire \reg_op1[29]_i_3_n_0 ;
  wire \reg_op1[2]_i_1_n_0 ;
  wire \reg_op1[2]_i_2_n_0 ;
  wire \reg_op1[2]_i_3_n_0 ;
  wire \reg_op1[30]_i_1_n_0 ;
  wire \reg_op1[30]_i_2_n_0 ;
  wire \reg_op1[30]_i_3_n_0 ;
  wire \reg_op1[30]_i_4_n_0 ;
  wire \reg_op1[31]_i_10_n_0 ;
  wire \reg_op1[31]_i_11_n_0 ;
  wire \reg_op1[31]_i_1_n_0 ;
  wire \reg_op1[31]_i_2_n_0 ;
  wire \reg_op1[31]_i_3_n_0 ;
  wire \reg_op1[31]_i_4_n_0 ;
  wire \reg_op1[31]_i_5_n_0 ;
  wire \reg_op1[31]_i_6_n_0 ;
  wire \reg_op1[31]_i_7_n_0 ;
  wire \reg_op1[31]_i_8_n_0 ;
  wire \reg_op1[31]_i_9_n_0 ;
  wire \reg_op1[3]_i_1_n_0 ;
  wire \reg_op1[3]_i_2_n_0 ;
  wire \reg_op1[3]_i_3_n_0 ;
  wire \reg_op1[4]_i_1_n_0 ;
  wire \reg_op1[4]_i_2_n_0 ;
  wire \reg_op1[4]_i_3_n_0 ;
  wire \reg_op1[5]_i_1_n_0 ;
  wire \reg_op1[5]_i_2_n_0 ;
  wire \reg_op1[5]_i_3_n_0 ;
  wire \reg_op1[6]_i_1_n_0 ;
  wire \reg_op1[6]_i_2_n_0 ;
  wire \reg_op1[6]_i_3_n_0 ;
  wire \reg_op1[7]_i_1_n_0 ;
  wire \reg_op1[7]_i_2_n_0 ;
  wire \reg_op1[7]_i_3_n_0 ;
  wire \reg_op1[8]_i_1_n_0 ;
  wire \reg_op1[8]_i_2_n_0 ;
  wire \reg_op1[8]_i_3_n_0 ;
  wire \reg_op1[9]_i_1_n_0 ;
  wire \reg_op1[9]_i_2_n_0 ;
  wire \reg_op1[9]_i_3_n_0 ;
  wire \reg_op1_reg_n_0_[0] ;
  wire \reg_op1_reg_n_0_[10] ;
  wire \reg_op1_reg_n_0_[11] ;
  wire \reg_op1_reg_n_0_[12] ;
  wire \reg_op1_reg_n_0_[13] ;
  wire \reg_op1_reg_n_0_[14] ;
  wire \reg_op1_reg_n_0_[15] ;
  wire \reg_op1_reg_n_0_[16] ;
  wire \reg_op1_reg_n_0_[17] ;
  wire \reg_op1_reg_n_0_[18] ;
  wire \reg_op1_reg_n_0_[19] ;
  wire \reg_op1_reg_n_0_[1] ;
  wire \reg_op1_reg_n_0_[20] ;
  wire \reg_op1_reg_n_0_[21] ;
  wire \reg_op1_reg_n_0_[22] ;
  wire \reg_op1_reg_n_0_[23] ;
  wire \reg_op1_reg_n_0_[24] ;
  wire \reg_op1_reg_n_0_[25] ;
  wire \reg_op1_reg_n_0_[26] ;
  wire \reg_op1_reg_n_0_[27] ;
  wire \reg_op1_reg_n_0_[28] ;
  wire \reg_op1_reg_n_0_[29] ;
  wire \reg_op1_reg_n_0_[2] ;
  wire \reg_op1_reg_n_0_[30] ;
  wire \reg_op1_reg_n_0_[31] ;
  wire \reg_op1_reg_n_0_[3] ;
  wire \reg_op1_reg_n_0_[4] ;
  wire \reg_op1_reg_n_0_[5] ;
  wire \reg_op1_reg_n_0_[6] ;
  wire \reg_op1_reg_n_0_[7] ;
  wire \reg_op1_reg_n_0_[8] ;
  wire \reg_op1_reg_n_0_[9] ;
  wire [31:0]reg_op2;
  wire \reg_op2[31]_i_1_n_0 ;
  wire \reg_op2[31]_i_3_n_0 ;
  wire \reg_op2_reg_n_0_[10] ;
  wire \reg_op2_reg_n_0_[11] ;
  wire \reg_op2_reg_n_0_[12] ;
  wire \reg_op2_reg_n_0_[13] ;
  wire \reg_op2_reg_n_0_[14] ;
  wire \reg_op2_reg_n_0_[15] ;
  wire \reg_op2_reg_n_0_[16] ;
  wire \reg_op2_reg_n_0_[17] ;
  wire \reg_op2_reg_n_0_[18] ;
  wire \reg_op2_reg_n_0_[19] ;
  wire \reg_op2_reg_n_0_[20] ;
  wire \reg_op2_reg_n_0_[21] ;
  wire \reg_op2_reg_n_0_[22] ;
  wire \reg_op2_reg_n_0_[23] ;
  wire \reg_op2_reg_n_0_[24] ;
  wire \reg_op2_reg_n_0_[25] ;
  wire \reg_op2_reg_n_0_[26] ;
  wire \reg_op2_reg_n_0_[27] ;
  wire \reg_op2_reg_n_0_[28] ;
  wire \reg_op2_reg_n_0_[29] ;
  wire \reg_op2_reg_n_0_[30] ;
  wire \reg_op2_reg_n_0_[31] ;
  wire \reg_op2_reg_n_0_[8] ;
  wire \reg_op2_reg_n_0_[9] ;
  wire reg_out0_carry__0_i_1_n_0;
  wire reg_out0_carry__0_i_2_n_0;
  wire reg_out0_carry__0_i_3_n_0;
  wire reg_out0_carry__0_i_4_n_0;
  wire reg_out0_carry__0_n_0;
  wire reg_out0_carry__0_n_1;
  wire reg_out0_carry__0_n_2;
  wire reg_out0_carry__0_n_3;
  wire reg_out0_carry__0_n_4;
  wire reg_out0_carry__0_n_5;
  wire reg_out0_carry__0_n_6;
  wire reg_out0_carry__0_n_7;
  wire reg_out0_carry__1_i_1_n_0;
  wire reg_out0_carry__1_i_2_n_0;
  wire reg_out0_carry__1_i_3_n_0;
  wire reg_out0_carry__1_i_4_n_0;
  wire reg_out0_carry__1_n_0;
  wire reg_out0_carry__1_n_1;
  wire reg_out0_carry__1_n_2;
  wire reg_out0_carry__1_n_3;
  wire reg_out0_carry__1_n_4;
  wire reg_out0_carry__1_n_5;
  wire reg_out0_carry__1_n_6;
  wire reg_out0_carry__1_n_7;
  wire reg_out0_carry__2_i_1_n_0;
  wire reg_out0_carry__2_i_2_n_0;
  wire reg_out0_carry__2_i_3_n_0;
  wire reg_out0_carry__2_i_4_n_0;
  wire reg_out0_carry__2_n_0;
  wire reg_out0_carry__2_n_1;
  wire reg_out0_carry__2_n_2;
  wire reg_out0_carry__2_n_3;
  wire reg_out0_carry__2_n_4;
  wire reg_out0_carry__2_n_5;
  wire reg_out0_carry__2_n_6;
  wire reg_out0_carry__2_n_7;
  wire reg_out0_carry__3_i_1_n_0;
  wire reg_out0_carry__3_i_2_n_0;
  wire reg_out0_carry__3_i_3_n_0;
  wire reg_out0_carry__3_i_4_n_0;
  wire reg_out0_carry__3_n_0;
  wire reg_out0_carry__3_n_1;
  wire reg_out0_carry__3_n_2;
  wire reg_out0_carry__3_n_3;
  wire reg_out0_carry__3_n_4;
  wire reg_out0_carry__3_n_5;
  wire reg_out0_carry__3_n_6;
  wire reg_out0_carry__3_n_7;
  wire reg_out0_carry__4_i_1_n_0;
  wire reg_out0_carry__4_i_2_n_0;
  wire reg_out0_carry__4_i_3_n_0;
  wire reg_out0_carry__4_i_4_n_0;
  wire reg_out0_carry__4_n_0;
  wire reg_out0_carry__4_n_1;
  wire reg_out0_carry__4_n_2;
  wire reg_out0_carry__4_n_3;
  wire reg_out0_carry__4_n_4;
  wire reg_out0_carry__4_n_5;
  wire reg_out0_carry__4_n_6;
  wire reg_out0_carry__4_n_7;
  wire reg_out0_carry__5_i_1_n_0;
  wire reg_out0_carry__5_i_2_n_0;
  wire reg_out0_carry__5_i_3_n_0;
  wire reg_out0_carry__5_i_4_n_0;
  wire reg_out0_carry__5_n_0;
  wire reg_out0_carry__5_n_1;
  wire reg_out0_carry__5_n_2;
  wire reg_out0_carry__5_n_3;
  wire reg_out0_carry__5_n_4;
  wire reg_out0_carry__5_n_5;
  wire reg_out0_carry__5_n_6;
  wire reg_out0_carry__5_n_7;
  wire reg_out0_carry__6_i_1_n_0;
  wire reg_out0_carry__6_i_2_n_0;
  wire reg_out0_carry__6_i_3_n_0;
  wire reg_out0_carry__6_n_2;
  wire reg_out0_carry__6_n_3;
  wire reg_out0_carry__6_n_5;
  wire reg_out0_carry__6_n_6;
  wire reg_out0_carry__6_n_7;
  wire reg_out0_carry_i_1_n_0;
  wire reg_out0_carry_i_2_n_0;
  wire reg_out0_carry_i_3_n_0;
  wire reg_out0_carry_i_4_n_0;
  wire reg_out0_carry_n_0;
  wire reg_out0_carry_n_1;
  wire reg_out0_carry_n_2;
  wire reg_out0_carry_n_3;
  wire reg_out0_carry_n_4;
  wire reg_out0_carry_n_5;
  wire reg_out0_carry_n_6;
  wire [31:0]reg_out1;
  wire \reg_out[0]_i_1_n_0 ;
  wire \reg_out[0]_i_2_n_0 ;
  wire \reg_out[0]_i_3_n_0 ;
  wire \reg_out[0]_i_4_n_0 ;
  wire \reg_out[0]_i_5_n_0 ;
  wire \reg_out[0]_i_6_n_0 ;
  wire \reg_out[0]_i_7_n_0 ;
  wire \reg_out[0]_i_8_n_0 ;
  wire \reg_out[0]_i_9_n_0 ;
  wire \reg_out[10]_i_1_n_0 ;
  wire \reg_out[10]_i_2_n_0 ;
  wire \reg_out[10]_i_3_n_0 ;
  wire \reg_out[10]_i_4_n_0 ;
  wire \reg_out[11]_i_1_n_0 ;
  wire \reg_out[11]_i_2_n_0 ;
  wire \reg_out[11]_i_3_n_0 ;
  wire \reg_out[11]_i_4_n_0 ;
  wire \reg_out[12]_i_1_n_0 ;
  wire \reg_out[12]_i_2_n_0 ;
  wire \reg_out[12]_i_3_n_0 ;
  wire \reg_out[12]_i_4_n_0 ;
  wire \reg_out[13]_i_1_n_0 ;
  wire \reg_out[13]_i_2_n_0 ;
  wire \reg_out[13]_i_3_n_0 ;
  wire \reg_out[13]_i_4_n_0 ;
  wire \reg_out[14]_i_1_n_0 ;
  wire \reg_out[14]_i_2_n_0 ;
  wire \reg_out[14]_i_3_n_0 ;
  wire \reg_out[14]_i_4_n_0 ;
  wire \reg_out[14]_i_5_n_0 ;
  wire \reg_out[14]_i_6_n_0 ;
  wire \reg_out[15]_i_1_n_0 ;
  wire \reg_out[15]_i_2_n_0 ;
  wire \reg_out[15]_i_3_n_0 ;
  wire \reg_out[15]_i_4_n_0 ;
  wire \reg_out[15]_i_5_n_0 ;
  wire \reg_out[15]_i_6_n_0 ;
  wire \reg_out[15]_i_7_n_0 ;
  wire \reg_out[16]_i_1_n_0 ;
  wire \reg_out[16]_i_2_n_0 ;
  wire \reg_out[16]_i_3_n_0 ;
  wire \reg_out[16]_i_4_n_0 ;
  wire \reg_out[17]_i_1_n_0 ;
  wire \reg_out[17]_i_2_n_0 ;
  wire \reg_out[17]_i_3_n_0 ;
  wire \reg_out[17]_i_4_n_0 ;
  wire \reg_out[18]_i_1_n_0 ;
  wire \reg_out[18]_i_2_n_0 ;
  wire \reg_out[18]_i_3_n_0 ;
  wire \reg_out[18]_i_4_n_0 ;
  wire \reg_out[19]_i_1_n_0 ;
  wire \reg_out[19]_i_2_n_0 ;
  wire \reg_out[19]_i_3_n_0 ;
  wire \reg_out[19]_i_4_n_0 ;
  wire \reg_out[1]_i_1_n_0 ;
  wire \reg_out[1]_i_2_n_0 ;
  wire \reg_out[1]_i_3_n_0 ;
  wire \reg_out[1]_i_4_n_0 ;
  wire \reg_out[1]_i_5_n_0 ;
  wire \reg_out[1]_i_6_n_0 ;
  wire \reg_out[1]_i_7_n_0 ;
  wire \reg_out[1]_i_8_n_0 ;
  wire \reg_out[20]_i_1_n_0 ;
  wire \reg_out[20]_i_2_n_0 ;
  wire \reg_out[20]_i_3_n_0 ;
  wire \reg_out[20]_i_4_n_0 ;
  wire \reg_out[21]_i_1_n_0 ;
  wire \reg_out[21]_i_2_n_0 ;
  wire \reg_out[21]_i_3_n_0 ;
  wire \reg_out[21]_i_4_n_0 ;
  wire \reg_out[22]_i_1_n_0 ;
  wire \reg_out[22]_i_2_n_0 ;
  wire \reg_out[22]_i_3_n_0 ;
  wire \reg_out[22]_i_4_n_0 ;
  wire \reg_out[23]_i_1_n_0 ;
  wire \reg_out[23]_i_2_n_0 ;
  wire \reg_out[23]_i_3_n_0 ;
  wire \reg_out[23]_i_4_n_0 ;
  wire \reg_out[24]_i_1_n_0 ;
  wire \reg_out[24]_i_2_n_0 ;
  wire \reg_out[24]_i_3_n_0 ;
  wire \reg_out[24]_i_4_n_0 ;
  wire \reg_out[25]_i_1_n_0 ;
  wire \reg_out[25]_i_2_n_0 ;
  wire \reg_out[25]_i_3_n_0 ;
  wire \reg_out[25]_i_4_n_0 ;
  wire \reg_out[26]_i_1_n_0 ;
  wire \reg_out[26]_i_2_n_0 ;
  wire \reg_out[26]_i_3_n_0 ;
  wire \reg_out[26]_i_4_n_0 ;
  wire \reg_out[27]_i_1_n_0 ;
  wire \reg_out[27]_i_2_n_0 ;
  wire \reg_out[27]_i_3_n_0 ;
  wire \reg_out[27]_i_4_n_0 ;
  wire \reg_out[28]_i_1_n_0 ;
  wire \reg_out[28]_i_2_n_0 ;
  wire \reg_out[28]_i_3_n_0 ;
  wire \reg_out[28]_i_4_n_0 ;
  wire \reg_out[29]_i_1_n_0 ;
  wire \reg_out[29]_i_2_n_0 ;
  wire \reg_out[29]_i_3_n_0 ;
  wire \reg_out[29]_i_4_n_0 ;
  wire \reg_out[2]_i_1_n_0 ;
  wire \reg_out[2]_i_2_n_0 ;
  wire \reg_out[2]_i_3_n_0 ;
  wire \reg_out[2]_i_4_n_0 ;
  wire \reg_out[2]_i_5_n_0 ;
  wire \reg_out[2]_i_6_n_0 ;
  wire \reg_out[2]_i_7_n_0 ;
  wire \reg_out[2]_i_8_n_0 ;
  wire \reg_out[30]_i_1_n_0 ;
  wire \reg_out[30]_i_2_n_0 ;
  wire \reg_out[30]_i_3_n_0 ;
  wire \reg_out[30]_i_4_n_0 ;
  wire \reg_out[31]_i_10_n_0 ;
  wire \reg_out[31]_i_11_n_0 ;
  wire \reg_out[31]_i_1_n_0 ;
  wire \reg_out[31]_i_2_n_0 ;
  wire \reg_out[31]_i_3_n_0 ;
  wire \reg_out[31]_i_4_n_0 ;
  wire \reg_out[31]_i_5_n_0 ;
  wire \reg_out[31]_i_6_n_0 ;
  wire \reg_out[31]_i_7_n_0 ;
  wire \reg_out[31]_i_8_n_0 ;
  wire \reg_out[31]_i_9_n_0 ;
  wire \reg_out[3]_i_1_n_0 ;
  wire \reg_out[3]_i_2_n_0 ;
  wire \reg_out[3]_i_3_n_0 ;
  wire \reg_out[3]_i_4_n_0 ;
  wire \reg_out[3]_i_5_n_0 ;
  wire \reg_out[3]_i_6_n_0 ;
  wire \reg_out[3]_i_7_n_0 ;
  wire \reg_out[3]_i_8_n_0 ;
  wire \reg_out[4]_i_1_n_0 ;
  wire \reg_out[4]_i_2_n_0 ;
  wire \reg_out[4]_i_3_n_0 ;
  wire \reg_out[4]_i_4_n_0 ;
  wire \reg_out[4]_i_5_n_0 ;
  wire \reg_out[4]_i_6_n_0 ;
  wire \reg_out[4]_i_7_n_0 ;
  wire \reg_out[4]_i_8_n_0 ;
  wire \reg_out[5]_i_1_n_0 ;
  wire \reg_out[5]_i_2_n_0 ;
  wire \reg_out[5]_i_3_n_0 ;
  wire \reg_out[5]_i_4_n_0 ;
  wire \reg_out[5]_i_5_n_0 ;
  wire \reg_out[5]_i_6_n_0 ;
  wire \reg_out[5]_i_7_n_0 ;
  wire \reg_out[5]_i_8_n_0 ;
  wire \reg_out[6]_i_1_n_0 ;
  wire \reg_out[6]_i_2_n_0 ;
  wire \reg_out[6]_i_3_n_0 ;
  wire \reg_out[6]_i_4_n_0 ;
  wire \reg_out[6]_i_5_n_0 ;
  wire \reg_out[6]_i_6_n_0 ;
  wire \reg_out[6]_i_7_n_0 ;
  wire \reg_out[6]_i_8_n_0 ;
  wire \reg_out[6]_i_9_n_0 ;
  wire \reg_out[7]_i_1_n_0 ;
  wire \reg_out[7]_i_2_n_0 ;
  wire \reg_out[7]_i_3_n_0 ;
  wire \reg_out[7]_i_4_n_0 ;
  wire \reg_out[7]_i_5_n_0 ;
  wire \reg_out[7]_i_6_n_0 ;
  wire \reg_out[7]_i_7_n_0 ;
  wire \reg_out[8]_i_1_n_0 ;
  wire \reg_out[8]_i_2_n_0 ;
  wire \reg_out[8]_i_3_n_0 ;
  wire \reg_out[8]_i_4_n_0 ;
  wire \reg_out[9]_i_1_n_0 ;
  wire \reg_out[9]_i_2_n_0 ;
  wire \reg_out[9]_i_3_n_0 ;
  wire \reg_out[9]_i_4_n_0 ;
  wire \reg_out_reg_n_0_[0] ;
  wire \reg_out_reg_n_0_[10] ;
  wire \reg_out_reg_n_0_[11] ;
  wire \reg_out_reg_n_0_[12] ;
  wire \reg_out_reg_n_0_[13] ;
  wire \reg_out_reg_n_0_[14] ;
  wire \reg_out_reg_n_0_[15] ;
  wire \reg_out_reg_n_0_[16] ;
  wire \reg_out_reg_n_0_[17] ;
  wire \reg_out_reg_n_0_[18] ;
  wire \reg_out_reg_n_0_[19] ;
  wire \reg_out_reg_n_0_[1] ;
  wire \reg_out_reg_n_0_[20] ;
  wire \reg_out_reg_n_0_[21] ;
  wire \reg_out_reg_n_0_[22] ;
  wire \reg_out_reg_n_0_[23] ;
  wire \reg_out_reg_n_0_[24] ;
  wire \reg_out_reg_n_0_[25] ;
  wire \reg_out_reg_n_0_[26] ;
  wire \reg_out_reg_n_0_[27] ;
  wire \reg_out_reg_n_0_[28] ;
  wire \reg_out_reg_n_0_[29] ;
  wire \reg_out_reg_n_0_[2] ;
  wire \reg_out_reg_n_0_[30] ;
  wire \reg_out_reg_n_0_[31] ;
  wire \reg_out_reg_n_0_[3] ;
  wire \reg_out_reg_n_0_[4] ;
  wire \reg_out_reg_n_0_[5] ;
  wire \reg_out_reg_n_0_[6] ;
  wire \reg_out_reg_n_0_[7] ;
  wire \reg_out_reg_n_0_[8] ;
  wire \reg_out_reg_n_0_[9] ;
  wire \reg_pc_reg_n_0_[10] ;
  wire \reg_pc_reg_n_0_[11] ;
  wire \reg_pc_reg_n_0_[12] ;
  wire \reg_pc_reg_n_0_[13] ;
  wire \reg_pc_reg_n_0_[14] ;
  wire \reg_pc_reg_n_0_[15] ;
  wire \reg_pc_reg_n_0_[16] ;
  wire \reg_pc_reg_n_0_[17] ;
  wire \reg_pc_reg_n_0_[18] ;
  wire \reg_pc_reg_n_0_[19] ;
  wire \reg_pc_reg_n_0_[1] ;
  wire \reg_pc_reg_n_0_[20] ;
  wire \reg_pc_reg_n_0_[21] ;
  wire \reg_pc_reg_n_0_[22] ;
  wire \reg_pc_reg_n_0_[23] ;
  wire \reg_pc_reg_n_0_[24] ;
  wire \reg_pc_reg_n_0_[25] ;
  wire \reg_pc_reg_n_0_[26] ;
  wire \reg_pc_reg_n_0_[27] ;
  wire \reg_pc_reg_n_0_[28] ;
  wire \reg_pc_reg_n_0_[29] ;
  wire \reg_pc_reg_n_0_[2] ;
  wire \reg_pc_reg_n_0_[30] ;
  wire \reg_pc_reg_n_0_[31] ;
  wire \reg_pc_reg_n_0_[3] ;
  wire \reg_pc_reg_n_0_[4] ;
  wire \reg_pc_reg_n_0_[5] ;
  wire \reg_pc_reg_n_0_[6] ;
  wire \reg_pc_reg_n_0_[7] ;
  wire \reg_pc_reg_n_0_[8] ;
  wire \reg_pc_reg_n_0_[9] ;
  wire [31:0]reg_sh1;
  wire \reg_sh[0]_i_1_n_0 ;
  wire \reg_sh[0]_i_2_n_0 ;
  wire \reg_sh[1]_i_1_n_0 ;
  wire \reg_sh[1]_i_2_n_0 ;
  wire \reg_sh[2]_i_1_n_0 ;
  wire \reg_sh[2]_i_2_n_0 ;
  wire \reg_sh[3]_i_1_n_0 ;
  wire \reg_sh[3]_i_2_n_0 ;
  wire \reg_sh[3]_i_3_n_0 ;
  wire \reg_sh[3]_i_4_n_0 ;
  wire \reg_sh[4]_i_1_n_0 ;
  wire \reg_sh[4]_i_2_n_0 ;
  wire \reg_sh[4]_i_3_n_0 ;
  wire \reg_sh_reg_n_0_[0] ;
  wire \reg_sh_reg_n_0_[1] ;
  wire \reg_sh_reg_n_0_[2] ;
  wire \reg_sh_reg_n_0_[3] ;
  wire \reg_sh_reg_n_0_[4] ;
  wire resetn_IBUF;
  wire trap_OBUF;
  wire [3:3]\NLW_alu_out0_inferred__2/i___29_carry__6_CO_UNCONNECTED ;
  wire [3:0]NLW_alu_out_00_carry_O_UNCONNECTED;
  wire [3:0]NLW_alu_out_00_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_alu_out_00_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_alu_out_00_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_alu_out_01_carry_O_UNCONNECTED;
  wire [3:0]NLW_alu_out_01_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_alu_out_01_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_alu_out_01_carry__2_O_UNCONNECTED;
  wire [3:0]\NLW_alu_out_01_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_alu_out_01_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_alu_out_01_inferred__0/i__carry__1_O_UNCONNECTED ;
  wire [3:0]\NLW_alu_out_01_inferred__0/i__carry__2_O_UNCONNECTED ;
  wire [3:3]\NLW_count_cycle_reg[60]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_count_instr_reg[60]_i_1_CO_UNCONNECTED ;
  wire [1:0]NLW_cpuregs_reg_r1_0_31_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r1_0_31_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r1_0_31_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r1_0_31_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r1_0_31_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r1_0_31_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r1_0_31_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r1_0_31_6_11_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r2_0_31_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r2_0_31_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r2_0_31_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r2_0_31_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r2_0_31_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r2_0_31_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r2_0_31_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_cpuregs_reg_r2_0_31_6_11_DOD_UNCONNECTED;
  wire [0:0]NLW_p_1_out_carry_O_UNCONNECTED;
  wire [3:3]NLW_p_1_out_carry__6_CO_UNCONNECTED;
  wire [0:0]NLW_reg_next_pc0_carry_O_UNCONNECTED;
  wire [3:2]NLW_reg_next_pc0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_reg_next_pc0_carry__6_O_UNCONNECTED;
  wire [3:3]\NLW_reg_op10_inferred__0/i__carry__6_CO_UNCONNECTED ;
  wire [0:0]NLW_reg_out0_carry_O_UNCONNECTED;
  wire [3:2]NLW_reg_out0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_reg_out0_carry__6_O_UNCONNECTED;

  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out0_inferred__2/i___29_carry 
       (.CI(1'b0),
        .CO({\alu_out0_inferred__2/i___29_carry_n_0 ,\alu_out0_inferred__2/i___29_carry_n_1 ,\alu_out0_inferred__2/i___29_carry_n_2 ,\alu_out0_inferred__2/i___29_carry_n_3 }),
        .CYINIT(\reg_op1_reg_n_0_[0] ),
        .DI({\reg_op1_reg_n_0_[3] ,\reg_op1_reg_n_0_[2] ,\reg_op1_reg_n_0_[1] ,instr_sub}),
        .O({\alu_out0_inferred__2/i___29_carry_n_4 ,\alu_out0_inferred__2/i___29_carry_n_5 ,\alu_out0_inferred__2/i___29_carry_n_6 ,\alu_out0_inferred__2/i___29_carry_n_7 }),
        .S({i___29_carry_i_1_n_0,i___29_carry_i_2_n_0,i___29_carry_i_3_n_0,i___29_carry_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out0_inferred__2/i___29_carry__0 
       (.CI(\alu_out0_inferred__2/i___29_carry_n_0 ),
        .CO({\alu_out0_inferred__2/i___29_carry__0_n_0 ,\alu_out0_inferred__2/i___29_carry__0_n_1 ,\alu_out0_inferred__2/i___29_carry__0_n_2 ,\alu_out0_inferred__2/i___29_carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[7] ,\reg_op1_reg_n_0_[6] ,\reg_op1_reg_n_0_[5] ,\reg_op1_reg_n_0_[4] }),
        .O({\alu_out0_inferred__2/i___29_carry__0_n_4 ,\alu_out0_inferred__2/i___29_carry__0_n_5 ,\alu_out0_inferred__2/i___29_carry__0_n_6 ,\alu_out0_inferred__2/i___29_carry__0_n_7 }),
        .S({i___29_carry__0_i_1_n_0,i___29_carry__0_i_2_n_0,i___29_carry__0_i_3_n_0,i___29_carry__0_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out0_inferred__2/i___29_carry__1 
       (.CI(\alu_out0_inferred__2/i___29_carry__0_n_0 ),
        .CO({\alu_out0_inferred__2/i___29_carry__1_n_0 ,\alu_out0_inferred__2/i___29_carry__1_n_1 ,\alu_out0_inferred__2/i___29_carry__1_n_2 ,\alu_out0_inferred__2/i___29_carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[11] ,\reg_op1_reg_n_0_[10] ,\reg_op1_reg_n_0_[9] ,\reg_op1_reg_n_0_[8] }),
        .O({\alu_out0_inferred__2/i___29_carry__1_n_4 ,\alu_out0_inferred__2/i___29_carry__1_n_5 ,\alu_out0_inferred__2/i___29_carry__1_n_6 ,\alu_out0_inferred__2/i___29_carry__1_n_7 }),
        .S({i___29_carry__1_i_1_n_0,i___29_carry__1_i_2_n_0,i___29_carry__1_i_3_n_0,i___29_carry__1_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out0_inferred__2/i___29_carry__2 
       (.CI(\alu_out0_inferred__2/i___29_carry__1_n_0 ),
        .CO({\alu_out0_inferred__2/i___29_carry__2_n_0 ,\alu_out0_inferred__2/i___29_carry__2_n_1 ,\alu_out0_inferred__2/i___29_carry__2_n_2 ,\alu_out0_inferred__2/i___29_carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[15] ,\reg_op1_reg_n_0_[14] ,\reg_op1_reg_n_0_[13] ,\reg_op1_reg_n_0_[12] }),
        .O({\alu_out0_inferred__2/i___29_carry__2_n_4 ,\alu_out0_inferred__2/i___29_carry__2_n_5 ,\alu_out0_inferred__2/i___29_carry__2_n_6 ,\alu_out0_inferred__2/i___29_carry__2_n_7 }),
        .S({i___29_carry__2_i_1_n_0,i___29_carry__2_i_2_n_0,i___29_carry__2_i_3_n_0,i___29_carry__2_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out0_inferred__2/i___29_carry__3 
       (.CI(\alu_out0_inferred__2/i___29_carry__2_n_0 ),
        .CO({\alu_out0_inferred__2/i___29_carry__3_n_0 ,\alu_out0_inferred__2/i___29_carry__3_n_1 ,\alu_out0_inferred__2/i___29_carry__3_n_2 ,\alu_out0_inferred__2/i___29_carry__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[19] ,\reg_op1_reg_n_0_[18] ,\reg_op1_reg_n_0_[17] ,\reg_op1_reg_n_0_[16] }),
        .O({\alu_out0_inferred__2/i___29_carry__3_n_4 ,\alu_out0_inferred__2/i___29_carry__3_n_5 ,\alu_out0_inferred__2/i___29_carry__3_n_6 ,\alu_out0_inferred__2/i___29_carry__3_n_7 }),
        .S({i___29_carry__3_i_1_n_0,i___29_carry__3_i_2_n_0,i___29_carry__3_i_3_n_0,i___29_carry__3_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out0_inferred__2/i___29_carry__4 
       (.CI(\alu_out0_inferred__2/i___29_carry__3_n_0 ),
        .CO({\alu_out0_inferred__2/i___29_carry__4_n_0 ,\alu_out0_inferred__2/i___29_carry__4_n_1 ,\alu_out0_inferred__2/i___29_carry__4_n_2 ,\alu_out0_inferred__2/i___29_carry__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[23] ,\reg_op1_reg_n_0_[22] ,\reg_op1_reg_n_0_[21] ,\reg_op1_reg_n_0_[20] }),
        .O({\alu_out0_inferred__2/i___29_carry__4_n_4 ,\alu_out0_inferred__2/i___29_carry__4_n_5 ,\alu_out0_inferred__2/i___29_carry__4_n_6 ,\alu_out0_inferred__2/i___29_carry__4_n_7 }),
        .S({i___29_carry__4_i_1_n_0,i___29_carry__4_i_2_n_0,i___29_carry__4_i_3_n_0,i___29_carry__4_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out0_inferred__2/i___29_carry__5 
       (.CI(\alu_out0_inferred__2/i___29_carry__4_n_0 ),
        .CO({\alu_out0_inferred__2/i___29_carry__5_n_0 ,\alu_out0_inferred__2/i___29_carry__5_n_1 ,\alu_out0_inferred__2/i___29_carry__5_n_2 ,\alu_out0_inferred__2/i___29_carry__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[27] ,\reg_op1_reg_n_0_[26] ,\reg_op1_reg_n_0_[25] ,\reg_op1_reg_n_0_[24] }),
        .O({\alu_out0_inferred__2/i___29_carry__5_n_4 ,\alu_out0_inferred__2/i___29_carry__5_n_5 ,\alu_out0_inferred__2/i___29_carry__5_n_6 ,\alu_out0_inferred__2/i___29_carry__5_n_7 }),
        .S({i___29_carry__5_i_1_n_0,i___29_carry__5_i_2_n_0,i___29_carry__5_i_3_n_0,i___29_carry__5_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out0_inferred__2/i___29_carry__6 
       (.CI(\alu_out0_inferred__2/i___29_carry__5_n_0 ),
        .CO({\NLW_alu_out0_inferred__2/i___29_carry__6_CO_UNCONNECTED [3],\alu_out0_inferred__2/i___29_carry__6_n_1 ,\alu_out0_inferred__2/i___29_carry__6_n_2 ,\alu_out0_inferred__2/i___29_carry__6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\reg_op1_reg_n_0_[30] ,\reg_op1_reg_n_0_[29] ,\reg_op1_reg_n_0_[28] }),
        .O({\alu_out0_inferred__2/i___29_carry__6_n_4 ,\alu_out0_inferred__2/i___29_carry__6_n_5 ,\alu_out0_inferred__2/i___29_carry__6_n_6 ,\alu_out0_inferred__2/i___29_carry__6_n_7 }),
        .S({i___29_carry__6_i_1_n_0,i___29_carry__6_i_2_n_0,i___29_carry__6_i_3_n_0,i___29_carry__6_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 alu_out_00_carry
       (.CI(1'b0),
        .CO({alu_out_00_carry_n_0,alu_out_00_carry_n_1,alu_out_00_carry_n_2,alu_out_00_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_alu_out_00_carry_O_UNCONNECTED[3:0]),
        .S({alu_out_00_carry_i_1_n_0,alu_out_00_carry_i_2_n_0,alu_out_00_carry_i_3_n_0,alu_out_00_carry_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 alu_out_00_carry__0
       (.CI(alu_out_00_carry_n_0),
        .CO({alu_out_00_carry__0_n_0,alu_out_00_carry__0_n_1,alu_out_00_carry__0_n_2,alu_out_00_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_alu_out_00_carry__0_O_UNCONNECTED[3:0]),
        .S({alu_out_00_carry__0_i_1_n_0,alu_out_00_carry__0_i_2_n_0,alu_out_00_carry__0_i_3_n_0,alu_out_00_carry__0_i_4_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry__0_i_1
       (.I0(\reg_op1_reg_n_0_[21] ),
        .I1(\reg_op2_reg_n_0_[21] ),
        .I2(\reg_op1_reg_n_0_[23] ),
        .I3(\reg_op2_reg_n_0_[23] ),
        .I4(\reg_op2_reg_n_0_[22] ),
        .I5(\reg_op1_reg_n_0_[22] ),
        .O(alu_out_00_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry__0_i_2
       (.I0(\reg_op1_reg_n_0_[19] ),
        .I1(\reg_op2_reg_n_0_[19] ),
        .I2(\reg_op1_reg_n_0_[20] ),
        .I3(\reg_op2_reg_n_0_[20] ),
        .I4(\reg_op2_reg_n_0_[18] ),
        .I5(\reg_op1_reg_n_0_[18] ),
        .O(alu_out_00_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry__0_i_3
       (.I0(\reg_op1_reg_n_0_[15] ),
        .I1(\reg_op2_reg_n_0_[15] ),
        .I2(\reg_op1_reg_n_0_[17] ),
        .I3(\reg_op2_reg_n_0_[17] ),
        .I4(\reg_op2_reg_n_0_[16] ),
        .I5(\reg_op1_reg_n_0_[16] ),
        .O(alu_out_00_carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry__0_i_4
       (.I0(\reg_op1_reg_n_0_[13] ),
        .I1(\reg_op2_reg_n_0_[13] ),
        .I2(\reg_op1_reg_n_0_[14] ),
        .I3(\reg_op2_reg_n_0_[14] ),
        .I4(\reg_op2_reg_n_0_[12] ),
        .I5(\reg_op1_reg_n_0_[12] ),
        .O(alu_out_00_carry__0_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 alu_out_00_carry__1
       (.CI(alu_out_00_carry__0_n_0),
        .CO({NLW_alu_out_00_carry__1_CO_UNCONNECTED[3],alu_out_00_carry__1_n_1,alu_out_00_carry__1_n_2,alu_out_00_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_alu_out_00_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,alu_out_00_carry__1_i_1_n_0,alu_out_00_carry__1_i_2_n_0,alu_out_00_carry__1_i_3_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_00_carry__1_i_1
       (.I0(\reg_op1_reg_n_0_[31] ),
        .I1(\reg_op2_reg_n_0_[31] ),
        .I2(\reg_op1_reg_n_0_[30] ),
        .I3(\reg_op2_reg_n_0_[30] ),
        .O(alu_out_00_carry__1_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry__1_i_2
       (.I0(\reg_op1_reg_n_0_[27] ),
        .I1(\reg_op2_reg_n_0_[27] ),
        .I2(\reg_op1_reg_n_0_[29] ),
        .I3(\reg_op2_reg_n_0_[29] ),
        .I4(\reg_op2_reg_n_0_[28] ),
        .I5(\reg_op1_reg_n_0_[28] ),
        .O(alu_out_00_carry__1_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry__1_i_3
       (.I0(\reg_op1_reg_n_0_[25] ),
        .I1(\reg_op2_reg_n_0_[25] ),
        .I2(\reg_op1_reg_n_0_[26] ),
        .I3(\reg_op2_reg_n_0_[26] ),
        .I4(\reg_op2_reg_n_0_[24] ),
        .I5(\reg_op1_reg_n_0_[24] ),
        .O(alu_out_00_carry__1_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry_i_1
       (.I0(\reg_op1_reg_n_0_[9] ),
        .I1(\reg_op2_reg_n_0_[9] ),
        .I2(\reg_op1_reg_n_0_[11] ),
        .I3(\reg_op2_reg_n_0_[11] ),
        .I4(\reg_op2_reg_n_0_[10] ),
        .I5(\reg_op1_reg_n_0_[10] ),
        .O(alu_out_00_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry_i_2
       (.I0(\reg_op1_reg_n_0_[7] ),
        .I1(Q[7]),
        .I2(\reg_op1_reg_n_0_[8] ),
        .I3(\reg_op2_reg_n_0_[8] ),
        .I4(Q[6]),
        .I5(\reg_op1_reg_n_0_[6] ),
        .O(alu_out_00_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry_i_3
       (.I0(\reg_op1_reg_n_0_[3] ),
        .I1(Q[3]),
        .I2(\reg_op1_reg_n_0_[5] ),
        .I3(Q[5]),
        .I4(Q[4]),
        .I5(\reg_op1_reg_n_0_[4] ),
        .O(alu_out_00_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    alu_out_00_carry_i_4
       (.I0(\reg_op1_reg_n_0_[1] ),
        .I1(Q[1]),
        .I2(\reg_op1_reg_n_0_[2] ),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(\reg_op1_reg_n_0_[0] ),
        .O(alu_out_00_carry_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 alu_out_01_carry
       (.CI(1'b0),
        .CO({alu_out_01_carry_n_0,alu_out_01_carry_n_1,alu_out_01_carry_n_2,alu_out_01_carry_n_3}),
        .CYINIT(1'b0),
        .DI({alu_out_01_carry_i_1_n_0,alu_out_01_carry_i_2_n_0,alu_out_01_carry_i_3_n_0,alu_out_01_carry_i_4_n_0}),
        .O(NLW_alu_out_01_carry_O_UNCONNECTED[3:0]),
        .S({alu_out_01_carry_i_5_n_0,alu_out_01_carry_i_6_n_0,alu_out_01_carry_i_7_n_0,alu_out_01_carry_i_8_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 alu_out_01_carry__0
       (.CI(alu_out_01_carry_n_0),
        .CO({alu_out_01_carry__0_n_0,alu_out_01_carry__0_n_1,alu_out_01_carry__0_n_2,alu_out_01_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({alu_out_01_carry__0_i_1_n_0,alu_out_01_carry__0_i_2_n_0,alu_out_01_carry__0_i_3_n_0,alu_out_01_carry__0_i_4_n_0}),
        .O(NLW_alu_out_01_carry__0_O_UNCONNECTED[3:0]),
        .S({alu_out_01_carry__0_i_5_n_0,alu_out_01_carry__0_i_6_n_0,alu_out_01_carry__0_i_7_n_0,alu_out_01_carry__0_i_8_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry__0_i_1
       (.I0(\reg_op2_reg_n_0_[14] ),
        .I1(\reg_op1_reg_n_0_[14] ),
        .I2(\reg_op1_reg_n_0_[15] ),
        .I3(\reg_op2_reg_n_0_[15] ),
        .O(alu_out_01_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry__0_i_2
       (.I0(\reg_op2_reg_n_0_[12] ),
        .I1(\reg_op1_reg_n_0_[12] ),
        .I2(\reg_op1_reg_n_0_[13] ),
        .I3(\reg_op2_reg_n_0_[13] ),
        .O(alu_out_01_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    alu_out_01_carry__0_i_3
       (.I0(\reg_op2_reg_n_0_[11] ),
        .I1(\reg_op1_reg_n_0_[11] ),
        .I2(\reg_op2_reg_n_0_[10] ),
        .I3(\reg_op1_reg_n_0_[10] ),
        .O(alu_out_01_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry__0_i_4
       (.I0(\reg_op2_reg_n_0_[8] ),
        .I1(\reg_op1_reg_n_0_[8] ),
        .I2(\reg_op1_reg_n_0_[9] ),
        .I3(\reg_op2_reg_n_0_[9] ),
        .O(alu_out_01_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__0_i_5
       (.I0(\reg_op2_reg_n_0_[14] ),
        .I1(\reg_op1_reg_n_0_[14] ),
        .I2(\reg_op2_reg_n_0_[15] ),
        .I3(\reg_op1_reg_n_0_[15] ),
        .O(alu_out_01_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__0_i_6
       (.I0(\reg_op2_reg_n_0_[12] ),
        .I1(\reg_op1_reg_n_0_[12] ),
        .I2(\reg_op2_reg_n_0_[13] ),
        .I3(\reg_op1_reg_n_0_[13] ),
        .O(alu_out_01_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__0_i_7
       (.I0(\reg_op2_reg_n_0_[10] ),
        .I1(\reg_op1_reg_n_0_[10] ),
        .I2(\reg_op2_reg_n_0_[11] ),
        .I3(\reg_op1_reg_n_0_[11] ),
        .O(alu_out_01_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__0_i_8
       (.I0(\reg_op2_reg_n_0_[8] ),
        .I1(\reg_op1_reg_n_0_[8] ),
        .I2(\reg_op2_reg_n_0_[9] ),
        .I3(\reg_op1_reg_n_0_[9] ),
        .O(alu_out_01_carry__0_i_8_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 alu_out_01_carry__1
       (.CI(alu_out_01_carry__0_n_0),
        .CO({alu_out_01_carry__1_n_0,alu_out_01_carry__1_n_1,alu_out_01_carry__1_n_2,alu_out_01_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({alu_out_01_carry__1_i_1_n_0,alu_out_01_carry__1_i_2_n_0,alu_out_01_carry__1_i_3_n_0,alu_out_01_carry__1_i_4_n_0}),
        .O(NLW_alu_out_01_carry__1_O_UNCONNECTED[3:0]),
        .S({alu_out_01_carry__1_i_5_n_0,alu_out_01_carry__1_i_6_n_0,alu_out_01_carry__1_i_7_n_0,alu_out_01_carry__1_i_8_n_0}));
  LUT4 #(
    .INIT(16'h22B2)) 
    alu_out_01_carry__1_i_1
       (.I0(\reg_op2_reg_n_0_[23] ),
        .I1(\reg_op1_reg_n_0_[23] ),
        .I2(\reg_op2_reg_n_0_[22] ),
        .I3(\reg_op1_reg_n_0_[22] ),
        .O(alu_out_01_carry__1_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry__1_i_2
       (.I0(\reg_op2_reg_n_0_[20] ),
        .I1(\reg_op1_reg_n_0_[20] ),
        .I2(\reg_op1_reg_n_0_[21] ),
        .I3(\reg_op2_reg_n_0_[21] ),
        .O(alu_out_01_carry__1_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry__1_i_3
       (.I0(\reg_op2_reg_n_0_[18] ),
        .I1(\reg_op1_reg_n_0_[18] ),
        .I2(\reg_op1_reg_n_0_[19] ),
        .I3(\reg_op2_reg_n_0_[19] ),
        .O(alu_out_01_carry__1_i_3_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    alu_out_01_carry__1_i_4
       (.I0(\reg_op2_reg_n_0_[17] ),
        .I1(\reg_op1_reg_n_0_[17] ),
        .I2(\reg_op2_reg_n_0_[16] ),
        .I3(\reg_op1_reg_n_0_[16] ),
        .O(alu_out_01_carry__1_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__1_i_5
       (.I0(\reg_op2_reg_n_0_[22] ),
        .I1(\reg_op1_reg_n_0_[22] ),
        .I2(\reg_op2_reg_n_0_[23] ),
        .I3(\reg_op1_reg_n_0_[23] ),
        .O(alu_out_01_carry__1_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__1_i_6
       (.I0(\reg_op2_reg_n_0_[20] ),
        .I1(\reg_op1_reg_n_0_[20] ),
        .I2(\reg_op2_reg_n_0_[21] ),
        .I3(\reg_op1_reg_n_0_[21] ),
        .O(alu_out_01_carry__1_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__1_i_7
       (.I0(\reg_op2_reg_n_0_[18] ),
        .I1(\reg_op1_reg_n_0_[18] ),
        .I2(\reg_op2_reg_n_0_[19] ),
        .I3(\reg_op1_reg_n_0_[19] ),
        .O(alu_out_01_carry__1_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__1_i_8
       (.I0(\reg_op2_reg_n_0_[16] ),
        .I1(\reg_op1_reg_n_0_[16] ),
        .I2(\reg_op2_reg_n_0_[17] ),
        .I3(\reg_op1_reg_n_0_[17] ),
        .O(alu_out_01_carry__1_i_8_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 alu_out_01_carry__2
       (.CI(alu_out_01_carry__1_n_0),
        .CO({data4,alu_out_01_carry__2_n_1,alu_out_01_carry__2_n_2,alu_out_01_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({alu_out_01_carry__2_i_1_n_0,alu_out_01_carry__2_i_2_n_0,alu_out_01_carry__2_i_3_n_0,alu_out_01_carry__2_i_4_n_0}),
        .O(NLW_alu_out_01_carry__2_O_UNCONNECTED[3:0]),
        .S({alu_out_01_carry__2_i_5_n_0,alu_out_01_carry__2_i_6_n_0,alu_out_01_carry__2_i_7_n_0,alu_out_01_carry__2_i_8_n_0}));
  LUT4 #(
    .INIT(16'h20F2)) 
    alu_out_01_carry__2_i_1
       (.I0(\reg_op2_reg_n_0_[30] ),
        .I1(\reg_op1_reg_n_0_[30] ),
        .I2(\reg_op1_reg_n_0_[31] ),
        .I3(\reg_op2_reg_n_0_[31] ),
        .O(alu_out_01_carry__2_i_1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    alu_out_01_carry__2_i_2
       (.I0(\reg_op2_reg_n_0_[29] ),
        .I1(\reg_op1_reg_n_0_[29] ),
        .I2(\reg_op2_reg_n_0_[28] ),
        .I3(\reg_op1_reg_n_0_[28] ),
        .O(alu_out_01_carry__2_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry__2_i_3
       (.I0(\reg_op2_reg_n_0_[26] ),
        .I1(\reg_op1_reg_n_0_[26] ),
        .I2(\reg_op1_reg_n_0_[27] ),
        .I3(\reg_op2_reg_n_0_[27] ),
        .O(alu_out_01_carry__2_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry__2_i_4
       (.I0(\reg_op2_reg_n_0_[24] ),
        .I1(\reg_op1_reg_n_0_[24] ),
        .I2(\reg_op1_reg_n_0_[25] ),
        .I3(\reg_op2_reg_n_0_[25] ),
        .O(alu_out_01_carry__2_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__2_i_5
       (.I0(\reg_op1_reg_n_0_[31] ),
        .I1(\reg_op2_reg_n_0_[31] ),
        .I2(\reg_op1_reg_n_0_[30] ),
        .I3(\reg_op2_reg_n_0_[30] ),
        .O(alu_out_01_carry__2_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__2_i_6
       (.I0(\reg_op2_reg_n_0_[28] ),
        .I1(\reg_op1_reg_n_0_[28] ),
        .I2(\reg_op2_reg_n_0_[29] ),
        .I3(\reg_op1_reg_n_0_[29] ),
        .O(alu_out_01_carry__2_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__2_i_7
       (.I0(\reg_op2_reg_n_0_[26] ),
        .I1(\reg_op1_reg_n_0_[26] ),
        .I2(\reg_op2_reg_n_0_[27] ),
        .I3(\reg_op1_reg_n_0_[27] ),
        .O(alu_out_01_carry__2_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry__2_i_8
       (.I0(\reg_op2_reg_n_0_[24] ),
        .I1(\reg_op1_reg_n_0_[24] ),
        .I2(\reg_op2_reg_n_0_[25] ),
        .I3(\reg_op1_reg_n_0_[25] ),
        .O(alu_out_01_carry__2_i_8_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry_i_1
       (.I0(Q[6]),
        .I1(\reg_op1_reg_n_0_[6] ),
        .I2(\reg_op1_reg_n_0_[7] ),
        .I3(Q[7]),
        .O(alu_out_01_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    alu_out_01_carry_i_2
       (.I0(Q[5]),
        .I1(\reg_op1_reg_n_0_[5] ),
        .I2(Q[4]),
        .I3(\reg_op1_reg_n_0_[4] ),
        .O(alu_out_01_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry_i_3
       (.I0(Q[2]),
        .I1(\reg_op1_reg_n_0_[2] ),
        .I2(\reg_op1_reg_n_0_[3] ),
        .I3(Q[3]),
        .O(alu_out_01_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    alu_out_01_carry_i_4
       (.I0(Q[0]),
        .I1(\reg_op1_reg_n_0_[0] ),
        .I2(\reg_op1_reg_n_0_[1] ),
        .I3(Q[1]),
        .O(alu_out_01_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry_i_5
       (.I0(Q[6]),
        .I1(\reg_op1_reg_n_0_[6] ),
        .I2(Q[7]),
        .I3(\reg_op1_reg_n_0_[7] ),
        .O(alu_out_01_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry_i_6
       (.I0(Q[4]),
        .I1(\reg_op1_reg_n_0_[4] ),
        .I2(Q[5]),
        .I3(\reg_op1_reg_n_0_[5] ),
        .O(alu_out_01_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry_i_7
       (.I0(Q[2]),
        .I1(\reg_op1_reg_n_0_[2] ),
        .I2(Q[3]),
        .I3(\reg_op1_reg_n_0_[3] ),
        .O(alu_out_01_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    alu_out_01_carry_i_8
       (.I0(Q[0]),
        .I1(\reg_op1_reg_n_0_[0] ),
        .I2(Q[1]),
        .I3(\reg_op1_reg_n_0_[1] ),
        .O(alu_out_01_carry_i_8_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out_01_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\alu_out_01_inferred__0/i__carry_n_0 ,\alu_out_01_inferred__0/i__carry_n_1 ,\alu_out_01_inferred__0/i__carry_n_2 ,\alu_out_01_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}),
        .O(\NLW_alu_out_01_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out_01_inferred__0/i__carry__0 
       (.CI(\alu_out_01_inferred__0/i__carry_n_0 ),
        .CO({\alu_out_01_inferred__0/i__carry__0_n_0 ,\alu_out_01_inferred__0/i__carry__0_n_1 ,\alu_out_01_inferred__0/i__carry__0_n_2 ,\alu_out_01_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__0_i_1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0}),
        .O(\NLW_alu_out_01_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_5_n_0,i__carry__0_i_6_n_0,i__carry__0_i_7_n_0,i__carry__0_i_8_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out_01_inferred__0/i__carry__1 
       (.CI(\alu_out_01_inferred__0/i__carry__0_n_0 ),
        .CO({\alu_out_01_inferred__0/i__carry__1_n_0 ,\alu_out_01_inferred__0/i__carry__1_n_1 ,\alu_out_01_inferred__0/i__carry__1_n_2 ,\alu_out_01_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__1_i_1_n_0,i__carry__1_i_2_n_0,i__carry__1_i_3_n_0,i__carry__1_i_4_n_0}),
        .O(\NLW_alu_out_01_inferred__0/i__carry__1_O_UNCONNECTED [3:0]),
        .S({i__carry__1_i_5_n_0,i__carry__1_i_6_n_0,i__carry__1_i_7_n_0,i__carry__1_i_8_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \alu_out_01_inferred__0/i__carry__2 
       (.CI(\alu_out_01_inferred__0/i__carry__1_n_0 ),
        .CO({data5,\alu_out_01_inferred__0/i__carry__2_n_1 ,\alu_out_01_inferred__0/i__carry__2_n_2 ,\alu_out_01_inferred__0/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__2_i_1_n_0,i__carry__2_i_2_n_0,i__carry__2_i_3_n_0,i__carry__2_i_4_n_0}),
        .O(\NLW_alu_out_01_inferred__0/i__carry__2_O_UNCONNECTED [3:0]),
        .S({i__carry__2_i_5_n_0,i__carry__2_i_6_n_0,i__carry__2_i_7_n_0,i__carry__2_i_8_n_0}));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFAAEA)) 
    \alu_out_q[0]_i_1 
       (.I0(\alu_out_q[0]_i_2_n_0 ),
        .I1(\alu_out_q[0]_i_3_n_0 ),
        .I2(is_compare),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out_q[0]_i_4_n_0 ),
        .I5(\alu_out_q[0]_i_5_n_0 ),
        .O(alu_out[0]));
  LUT6 #(
    .INIT(64'h0000000000004440)) 
    \alu_out_q[0]_i_2 
       (.I0(Q[0]),
        .I1(\reg_op1_reg_n_0_[0] ),
        .I2(instr_xor),
        .I3(instr_xori),
        .I4(is_compare),
        .I5(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAA55AA30)) 
    \alu_out_q[0]_i_3 
       (.I0(alu_out_00_carry__1_n_1),
        .I1(data4),
        .I2(instr_bge),
        .I3(instr_beq),
        .I4(instr_bne),
        .I5(decoder_trigger_i_4_n_0),
        .O(\alu_out_q[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \alu_out_q[0]_i_4 
       (.I0(is_lui_auipc_jal_jalr_addi_add_sub),
        .I1(\alu_out0_inferred__2/i___29_carry_n_7 ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\alu_out_q[30]_i_4_n_0 ),
        .O(\alu_out_q[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hF010F010F010B010)) 
    \alu_out_q[0]_i_5 
       (.I0(\reg_op1_reg_n_0_[0] ),
        .I1(\alu_out_q[30]_i_2_n_0 ),
        .I2(Q[0]),
        .I3(\alu_out_q[31]_i_2_n_0 ),
        .I4(instr_ori),
        .I5(instr_or),
        .O(\alu_out_q[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[10]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[10]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[10] ),
        .I4(\reg_op1_reg_n_0_[10] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[10]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[10]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__1_n_5 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[11]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[11]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[11] ),
        .I4(\reg_op1_reg_n_0_[11] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[11]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[11]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__1_n_4 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[12]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[12]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[12] ),
        .I4(\reg_op1_reg_n_0_[12] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[12]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[12]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__2_n_7 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[13]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[13] ),
        .I3(\reg_op2_reg_n_0_[13] ),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[13]_i_2_n_0 ),
        .O(alu_out[13]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[13]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[13] ),
        .I2(\reg_op2_reg_n_0_[13] ),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry__2_n_6 ),
        .O(\alu_out_q[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[14]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[14]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[14] ),
        .I4(\reg_op1_reg_n_0_[14] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[14]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[14]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__2_n_5 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[15]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[15] ),
        .I3(\reg_op2_reg_n_0_[15] ),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[15]_i_2_n_0 ),
        .O(alu_out[15]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[15]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[15] ),
        .I2(\reg_op2_reg_n_0_[15] ),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry__2_n_4 ),
        .O(\alu_out_q[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[16]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[16]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[16] ),
        .I4(\reg_op1_reg_n_0_[16] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[16]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[16]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__3_n_7 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[17]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[17]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[17] ),
        .I4(\reg_op1_reg_n_0_[17] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[17]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[17]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__3_n_6 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[18]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[18]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[18] ),
        .I4(\reg_op1_reg_n_0_[18] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[18]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[18]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__3_n_5 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[19]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[19] ),
        .I3(\reg_op2_reg_n_0_[19] ),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[19]_i_2_n_0 ),
        .O(alu_out[19]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[19]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[19] ),
        .I2(\reg_op2_reg_n_0_[19] ),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry__3_n_4 ),
        .O(\alu_out_q[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[1]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[1] ),
        .I3(Q[1]),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[1]_i_2_n_0 ),
        .O(alu_out[1]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[1]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(Q[1]),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry_n_6 ),
        .O(\alu_out_q[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[20]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[20]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[20] ),
        .I4(\reg_op1_reg_n_0_[20] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[20]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[20]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__4_n_7 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[21]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[21] ),
        .I3(\reg_op2_reg_n_0_[21] ),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[21]_i_2_n_0 ),
        .O(alu_out[21]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[21]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[21] ),
        .I2(\reg_op2_reg_n_0_[21] ),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry__4_n_6 ),
        .O(\alu_out_q[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[22]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[22]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[22] ),
        .I4(\reg_op1_reg_n_0_[22] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[22]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[22]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__4_n_5 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[23]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[23]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[23] ),
        .I4(\reg_op1_reg_n_0_[23] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[23]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[23]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__4_n_4 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[24]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[24]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[24] ),
        .I4(\reg_op1_reg_n_0_[24] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[24]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[24]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__5_n_7 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[25]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[25] ),
        .I3(\reg_op2_reg_n_0_[25] ),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[25]_i_2_n_0 ),
        .O(alu_out[25]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[25]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[25] ),
        .I2(\reg_op2_reg_n_0_[25] ),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry__5_n_6 ),
        .O(\alu_out_q[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[26]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[26]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[26] ),
        .I4(\reg_op1_reg_n_0_[26] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[26]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[26]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__5_n_5 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[27]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[27] ),
        .I3(\reg_op2_reg_n_0_[27] ),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[27]_i_2_n_0 ),
        .O(alu_out[27]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[27]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[27] ),
        .I2(\reg_op2_reg_n_0_[27] ),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry__5_n_4 ),
        .O(\alu_out_q[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[28]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[28]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[28] ),
        .I4(\reg_op1_reg_n_0_[28] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[28]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[28]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__6_n_7 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[29]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[29]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[29] ),
        .I4(\reg_op1_reg_n_0_[29] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[29]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[29]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__6_n_6 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[2]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[2]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(Q[2]),
        .I4(\reg_op1_reg_n_0_[2] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[2]));
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[2]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry_n_5 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[30]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[30]_i_3_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[30] ),
        .I4(\reg_op1_reg_n_0_[30] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[30]));
  LUT4 #(
    .INIT(16'hEEEF)) 
    \alu_out_q[30]_i_2 
       (.I0(is_lui_auipc_jal_jalr_addi_add_sub),
        .I1(is_compare),
        .I2(instr_xori),
        .I3(instr_xor),
        .O(\alu_out_q[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[30]_i_3 
       (.I0(\alu_out0_inferred__2/i___29_carry__6_n_5 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[30]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hA8)) 
    \alu_out_q[30]_i_4 
       (.I0(\alu_out_q[31]_i_2_n_0 ),
        .I1(instr_ori),
        .I2(instr_or),
        .O(\alu_out_q[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFEEE80000)) 
    \alu_out_q[31]_i_1 
       (.I0(\reg_op2_reg_n_0_[31] ),
        .I1(\reg_op1_reg_n_0_[31] ),
        .I2(instr_or),
        .I3(instr_ori),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[31]_i_3_n_0 ),
        .O(alu_out[31]));
  LUT6 #(
    .INIT(64'h0001000100010000)) 
    \alu_out_q[31]_i_2 
       (.I0(instr_xor),
        .I1(instr_xori),
        .I2(is_compare),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(instr_and),
        .I5(\alu_out_q[31]_i_4_n_0 ),
        .O(\alu_out_q[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[31]_i_3 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op2_reg_n_0_[31] ),
        .I2(\reg_op1_reg_n_0_[31] ),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry__6_n_4 ),
        .O(\alu_out_q[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \alu_out_q[31]_i_4 
       (.I0(instr_andi),
        .I1(instr_ori),
        .I2(instr_or),
        .O(\alu_out_q[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[3]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[3] ),
        .I3(Q[3]),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[3]_i_2_n_0 ),
        .O(alu_out[3]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[3]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[3] ),
        .I2(Q[3]),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry_n_4 ),
        .O(\alu_out_q[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[4]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[4]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(Q[4]),
        .I4(\reg_op1_reg_n_0_[4] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[4]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__0_n_7 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[5]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[5]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(Q[5]),
        .I4(\reg_op1_reg_n_0_[5] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[5]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__0_n_6 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[6]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[6]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(Q[6]),
        .I4(\reg_op1_reg_n_0_[6] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[6]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__0_n_5 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[7]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[7] ),
        .I3(Q[7]),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[7]_i_2_n_0 ),
        .O(alu_out[7]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[7]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[7] ),
        .I2(Q[7]),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry__0_n_4 ),
        .O(\alu_out_q[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFDCCFCFDFDCC)) 
    \alu_out_q[8]_i_1 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\alu_out_q[8]_i_2_n_0 ),
        .I2(\alu_out_q[30]_i_4_n_0 ),
        .I3(\reg_op2_reg_n_0_[8] ),
        .I4(\reg_op1_reg_n_0_[8] ),
        .I5(\alu_out_q[31]_i_2_n_0 ),
        .O(alu_out[8]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alu_out_q[8]_i_2 
       (.I0(\alu_out0_inferred__2/i___29_carry__1_n_7 ),
        .I1(is_lui_auipc_jal_jalr_addi_add_sub),
        .O(\alu_out_q[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEE00000)) 
    \alu_out_q[9]_i_1 
       (.I0(instr_or),
        .I1(instr_ori),
        .I2(\reg_op1_reg_n_0_[9] ),
        .I3(\reg_op2_reg_n_0_[9] ),
        .I4(\alu_out_q[31]_i_2_n_0 ),
        .I5(\alu_out_q[9]_i_2_n_0 ),
        .O(alu_out[9]));
  LUT5 #(
    .INIT(32'hFF141414)) 
    \alu_out_q[9]_i_2 
       (.I0(\alu_out_q[30]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[9] ),
        .I2(\reg_op2_reg_n_0_[9] ),
        .I3(is_lui_auipc_jal_jalr_addi_add_sub),
        .I4(\alu_out0_inferred__2/i___29_carry__1_n_6 ),
        .O(\alu_out_q[9]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[0]),
        .Q(alu_out_q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[10]),
        .Q(alu_out_q[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[11]),
        .Q(alu_out_q[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[12]),
        .Q(alu_out_q[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[13]),
        .Q(alu_out_q[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[14]),
        .Q(alu_out_q[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[15]),
        .Q(alu_out_q[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[16]),
        .Q(alu_out_q[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[17]),
        .Q(alu_out_q[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[18]),
        .Q(alu_out_q[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[19]),
        .Q(alu_out_q[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[1]),
        .Q(alu_out_q[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[20]),
        .Q(alu_out_q[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[21]),
        .Q(alu_out_q[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[22]),
        .Q(alu_out_q[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[23]),
        .Q(alu_out_q[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[24]),
        .Q(alu_out_q[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[25]),
        .Q(alu_out_q[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[26]),
        .Q(alu_out_q[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[27]),
        .Q(alu_out_q[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[28]),
        .Q(alu_out_q[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[29]),
        .Q(alu_out_q[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[2]),
        .Q(alu_out_q[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[30]),
        .Q(alu_out_q[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[31]),
        .Q(alu_out_q[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[3]),
        .Q(alu_out_q[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[4]),
        .Q(alu_out_q[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[5]),
        .Q(alu_out_q[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[6]),
        .Q(alu_out_q[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[7]),
        .Q(alu_out_q[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[8]),
        .Q(alu_out_q[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \alu_out_q_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(alu_out[9]),
        .Q(alu_out_q[9]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \count_cycle[0]_i_2 
       (.I0(count_cycle_reg[0]),
        .O(\count_cycle[0]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[0]_i_1_n_7 ),
        .Q(count_cycle_reg[0]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\count_cycle_reg[0]_i_1_n_0 ,\count_cycle_reg[0]_i_1_n_1 ,\count_cycle_reg[0]_i_1_n_2 ,\count_cycle_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\count_cycle_reg[0]_i_1_n_4 ,\count_cycle_reg[0]_i_1_n_5 ,\count_cycle_reg[0]_i_1_n_6 ,\count_cycle_reg[0]_i_1_n_7 }),
        .S({count_cycle_reg[3:1],\count_cycle[0]_i_2_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[8]_i_1_n_5 ),
        .Q(count_cycle_reg[10]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[8]_i_1_n_4 ),
        .Q(count_cycle_reg[11]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[12]_i_1_n_7 ),
        .Q(count_cycle_reg[12]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[12]_i_1 
       (.CI(\count_cycle_reg[8]_i_1_n_0 ),
        .CO({\count_cycle_reg[12]_i_1_n_0 ,\count_cycle_reg[12]_i_1_n_1 ,\count_cycle_reg[12]_i_1_n_2 ,\count_cycle_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[12]_i_1_n_4 ,\count_cycle_reg[12]_i_1_n_5 ,\count_cycle_reg[12]_i_1_n_6 ,\count_cycle_reg[12]_i_1_n_7 }),
        .S(count_cycle_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[12]_i_1_n_6 ),
        .Q(count_cycle_reg[13]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[12]_i_1_n_5 ),
        .Q(count_cycle_reg[14]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[12]_i_1_n_4 ),
        .Q(count_cycle_reg[15]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[16]_i_1_n_7 ),
        .Q(count_cycle_reg[16]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[16]_i_1 
       (.CI(\count_cycle_reg[12]_i_1_n_0 ),
        .CO({\count_cycle_reg[16]_i_1_n_0 ,\count_cycle_reg[16]_i_1_n_1 ,\count_cycle_reg[16]_i_1_n_2 ,\count_cycle_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[16]_i_1_n_4 ,\count_cycle_reg[16]_i_1_n_5 ,\count_cycle_reg[16]_i_1_n_6 ,\count_cycle_reg[16]_i_1_n_7 }),
        .S(count_cycle_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[16]_i_1_n_6 ),
        .Q(count_cycle_reg[17]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[16]_i_1_n_5 ),
        .Q(count_cycle_reg[18]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[16]_i_1_n_4 ),
        .Q(count_cycle_reg[19]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[0]_i_1_n_6 ),
        .Q(count_cycle_reg[1]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[20]_i_1_n_7 ),
        .Q(count_cycle_reg[20]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[20]_i_1 
       (.CI(\count_cycle_reg[16]_i_1_n_0 ),
        .CO({\count_cycle_reg[20]_i_1_n_0 ,\count_cycle_reg[20]_i_1_n_1 ,\count_cycle_reg[20]_i_1_n_2 ,\count_cycle_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[20]_i_1_n_4 ,\count_cycle_reg[20]_i_1_n_5 ,\count_cycle_reg[20]_i_1_n_6 ,\count_cycle_reg[20]_i_1_n_7 }),
        .S(count_cycle_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[20]_i_1_n_6 ),
        .Q(count_cycle_reg[21]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[20]_i_1_n_5 ),
        .Q(count_cycle_reg[22]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[20]_i_1_n_4 ),
        .Q(count_cycle_reg[23]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[24]_i_1_n_7 ),
        .Q(count_cycle_reg[24]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[24]_i_1 
       (.CI(\count_cycle_reg[20]_i_1_n_0 ),
        .CO({\count_cycle_reg[24]_i_1_n_0 ,\count_cycle_reg[24]_i_1_n_1 ,\count_cycle_reg[24]_i_1_n_2 ,\count_cycle_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[24]_i_1_n_4 ,\count_cycle_reg[24]_i_1_n_5 ,\count_cycle_reg[24]_i_1_n_6 ,\count_cycle_reg[24]_i_1_n_7 }),
        .S(count_cycle_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[24]_i_1_n_6 ),
        .Q(count_cycle_reg[25]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[24]_i_1_n_5 ),
        .Q(count_cycle_reg[26]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[24]_i_1_n_4 ),
        .Q(count_cycle_reg[27]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[28]_i_1_n_7 ),
        .Q(count_cycle_reg[28]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[28]_i_1 
       (.CI(\count_cycle_reg[24]_i_1_n_0 ),
        .CO({\count_cycle_reg[28]_i_1_n_0 ,\count_cycle_reg[28]_i_1_n_1 ,\count_cycle_reg[28]_i_1_n_2 ,\count_cycle_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[28]_i_1_n_4 ,\count_cycle_reg[28]_i_1_n_5 ,\count_cycle_reg[28]_i_1_n_6 ,\count_cycle_reg[28]_i_1_n_7 }),
        .S(count_cycle_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[28]_i_1_n_6 ),
        .Q(count_cycle_reg[29]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[0]_i_1_n_5 ),
        .Q(count_cycle_reg[2]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[28]_i_1_n_5 ),
        .Q(count_cycle_reg[30]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[28]_i_1_n_4 ),
        .Q(count_cycle_reg[31]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[32] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[32]_i_1_n_7 ),
        .Q(count_cycle_reg[32]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[32]_i_1 
       (.CI(\count_cycle_reg[28]_i_1_n_0 ),
        .CO({\count_cycle_reg[32]_i_1_n_0 ,\count_cycle_reg[32]_i_1_n_1 ,\count_cycle_reg[32]_i_1_n_2 ,\count_cycle_reg[32]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[32]_i_1_n_4 ,\count_cycle_reg[32]_i_1_n_5 ,\count_cycle_reg[32]_i_1_n_6 ,\count_cycle_reg[32]_i_1_n_7 }),
        .S(count_cycle_reg[35:32]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[33] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[32]_i_1_n_6 ),
        .Q(count_cycle_reg[33]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[34] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[32]_i_1_n_5 ),
        .Q(count_cycle_reg[34]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[35] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[32]_i_1_n_4 ),
        .Q(count_cycle_reg[35]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[36] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[36]_i_1_n_7 ),
        .Q(count_cycle_reg[36]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[36]_i_1 
       (.CI(\count_cycle_reg[32]_i_1_n_0 ),
        .CO({\count_cycle_reg[36]_i_1_n_0 ,\count_cycle_reg[36]_i_1_n_1 ,\count_cycle_reg[36]_i_1_n_2 ,\count_cycle_reg[36]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[36]_i_1_n_4 ,\count_cycle_reg[36]_i_1_n_5 ,\count_cycle_reg[36]_i_1_n_6 ,\count_cycle_reg[36]_i_1_n_7 }),
        .S(count_cycle_reg[39:36]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[37] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[36]_i_1_n_6 ),
        .Q(count_cycle_reg[37]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[38] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[36]_i_1_n_5 ),
        .Q(count_cycle_reg[38]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[39] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[36]_i_1_n_4 ),
        .Q(count_cycle_reg[39]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[0]_i_1_n_4 ),
        .Q(count_cycle_reg[3]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[40] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[40]_i_1_n_7 ),
        .Q(count_cycle_reg[40]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[40]_i_1 
       (.CI(\count_cycle_reg[36]_i_1_n_0 ),
        .CO({\count_cycle_reg[40]_i_1_n_0 ,\count_cycle_reg[40]_i_1_n_1 ,\count_cycle_reg[40]_i_1_n_2 ,\count_cycle_reg[40]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[40]_i_1_n_4 ,\count_cycle_reg[40]_i_1_n_5 ,\count_cycle_reg[40]_i_1_n_6 ,\count_cycle_reg[40]_i_1_n_7 }),
        .S(count_cycle_reg[43:40]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[41] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[40]_i_1_n_6 ),
        .Q(count_cycle_reg[41]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[42] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[40]_i_1_n_5 ),
        .Q(count_cycle_reg[42]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[43] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[40]_i_1_n_4 ),
        .Q(count_cycle_reg[43]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[44] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[44]_i_1_n_7 ),
        .Q(count_cycle_reg[44]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[44]_i_1 
       (.CI(\count_cycle_reg[40]_i_1_n_0 ),
        .CO({\count_cycle_reg[44]_i_1_n_0 ,\count_cycle_reg[44]_i_1_n_1 ,\count_cycle_reg[44]_i_1_n_2 ,\count_cycle_reg[44]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[44]_i_1_n_4 ,\count_cycle_reg[44]_i_1_n_5 ,\count_cycle_reg[44]_i_1_n_6 ,\count_cycle_reg[44]_i_1_n_7 }),
        .S(count_cycle_reg[47:44]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[45] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[44]_i_1_n_6 ),
        .Q(count_cycle_reg[45]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[46] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[44]_i_1_n_5 ),
        .Q(count_cycle_reg[46]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[47] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[44]_i_1_n_4 ),
        .Q(count_cycle_reg[47]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[48] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[48]_i_1_n_7 ),
        .Q(count_cycle_reg[48]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[48]_i_1 
       (.CI(\count_cycle_reg[44]_i_1_n_0 ),
        .CO({\count_cycle_reg[48]_i_1_n_0 ,\count_cycle_reg[48]_i_1_n_1 ,\count_cycle_reg[48]_i_1_n_2 ,\count_cycle_reg[48]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[48]_i_1_n_4 ,\count_cycle_reg[48]_i_1_n_5 ,\count_cycle_reg[48]_i_1_n_6 ,\count_cycle_reg[48]_i_1_n_7 }),
        .S(count_cycle_reg[51:48]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[49] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[48]_i_1_n_6 ),
        .Q(count_cycle_reg[49]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[4]_i_1_n_7 ),
        .Q(count_cycle_reg[4]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[4]_i_1 
       (.CI(\count_cycle_reg[0]_i_1_n_0 ),
        .CO({\count_cycle_reg[4]_i_1_n_0 ,\count_cycle_reg[4]_i_1_n_1 ,\count_cycle_reg[4]_i_1_n_2 ,\count_cycle_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[4]_i_1_n_4 ,\count_cycle_reg[4]_i_1_n_5 ,\count_cycle_reg[4]_i_1_n_6 ,\count_cycle_reg[4]_i_1_n_7 }),
        .S(count_cycle_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[50] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[48]_i_1_n_5 ),
        .Q(count_cycle_reg[50]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[51] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[48]_i_1_n_4 ),
        .Q(count_cycle_reg[51]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[52] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[52]_i_1_n_7 ),
        .Q(count_cycle_reg[52]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[52]_i_1 
       (.CI(\count_cycle_reg[48]_i_1_n_0 ),
        .CO({\count_cycle_reg[52]_i_1_n_0 ,\count_cycle_reg[52]_i_1_n_1 ,\count_cycle_reg[52]_i_1_n_2 ,\count_cycle_reg[52]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[52]_i_1_n_4 ,\count_cycle_reg[52]_i_1_n_5 ,\count_cycle_reg[52]_i_1_n_6 ,\count_cycle_reg[52]_i_1_n_7 }),
        .S(count_cycle_reg[55:52]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[53] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[52]_i_1_n_6 ),
        .Q(count_cycle_reg[53]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[54] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[52]_i_1_n_5 ),
        .Q(count_cycle_reg[54]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[55] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[52]_i_1_n_4 ),
        .Q(count_cycle_reg[55]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[56] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[56]_i_1_n_7 ),
        .Q(count_cycle_reg[56]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[56]_i_1 
       (.CI(\count_cycle_reg[52]_i_1_n_0 ),
        .CO({\count_cycle_reg[56]_i_1_n_0 ,\count_cycle_reg[56]_i_1_n_1 ,\count_cycle_reg[56]_i_1_n_2 ,\count_cycle_reg[56]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[56]_i_1_n_4 ,\count_cycle_reg[56]_i_1_n_5 ,\count_cycle_reg[56]_i_1_n_6 ,\count_cycle_reg[56]_i_1_n_7 }),
        .S(count_cycle_reg[59:56]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[57] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[56]_i_1_n_6 ),
        .Q(count_cycle_reg[57]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[58] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[56]_i_1_n_5 ),
        .Q(count_cycle_reg[58]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[59] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[56]_i_1_n_4 ),
        .Q(count_cycle_reg[59]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[4]_i_1_n_6 ),
        .Q(count_cycle_reg[5]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[60] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[60]_i_1_n_7 ),
        .Q(count_cycle_reg[60]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[60]_i_1 
       (.CI(\count_cycle_reg[56]_i_1_n_0 ),
        .CO({\NLW_count_cycle_reg[60]_i_1_CO_UNCONNECTED [3],\count_cycle_reg[60]_i_1_n_1 ,\count_cycle_reg[60]_i_1_n_2 ,\count_cycle_reg[60]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[60]_i_1_n_4 ,\count_cycle_reg[60]_i_1_n_5 ,\count_cycle_reg[60]_i_1_n_6 ,\count_cycle_reg[60]_i_1_n_7 }),
        .S(count_cycle_reg[63:60]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[61] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[60]_i_1_n_6 ),
        .Q(count_cycle_reg[61]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[62] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[60]_i_1_n_5 ),
        .Q(count_cycle_reg[62]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[63] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[60]_i_1_n_4 ),
        .Q(count_cycle_reg[63]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[4]_i_1_n_5 ),
        .Q(count_cycle_reg[6]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[4]_i_1_n_4 ),
        .Q(count_cycle_reg[7]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[8]_i_1_n_7 ),
        .Q(count_cycle_reg[8]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_cycle_reg[8]_i_1 
       (.CI(\count_cycle_reg[4]_i_1_n_0 ),
        .CO({\count_cycle_reg[8]_i_1_n_0 ,\count_cycle_reg[8]_i_1_n_1 ,\count_cycle_reg[8]_i_1_n_2 ,\count_cycle_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_cycle_reg[8]_i_1_n_4 ,\count_cycle_reg[8]_i_1_n_5 ,\count_cycle_reg[8]_i_1_n_6 ,\count_cycle_reg[8]_i_1_n_7 }),
        .S(count_cycle_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \count_cycle_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_cycle_reg[8]_i_1_n_6 ),
        .Q(count_cycle_reg[9]),
        .R(instr_and_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    \count_instr[0]_i_1 
       (.I0(decoder_trigger_reg_n_0),
        .I1(reg_next_pc),
        .O(count_instr));
  LUT1 #(
    .INIT(2'h1)) 
    \count_instr[0]_i_3 
       (.I0(\count_instr_reg_n_0_[0] ),
        .O(\count_instr[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[0]_i_2_n_7 ),
        .Q(\count_instr_reg_n_0_[0] ),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\count_instr_reg[0]_i_2_n_0 ,\count_instr_reg[0]_i_2_n_1 ,\count_instr_reg[0]_i_2_n_2 ,\count_instr_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\count_instr_reg[0]_i_2_n_4 ,\count_instr_reg[0]_i_2_n_5 ,\count_instr_reg[0]_i_2_n_6 ,\count_instr_reg[0]_i_2_n_7 }),
        .S({\count_instr_reg_n_0_[3] ,\count_instr_reg_n_0_[2] ,\count_instr_reg_n_0_[1] ,\count_instr[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[8]_i_1_n_5 ),
        .Q(\count_instr_reg_n_0_[10] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[8]_i_1_n_4 ),
        .Q(\count_instr_reg_n_0_[11] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[12]_i_1_n_7 ),
        .Q(\count_instr_reg_n_0_[12] ),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[12]_i_1 
       (.CI(\count_instr_reg[8]_i_1_n_0 ),
        .CO({\count_instr_reg[12]_i_1_n_0 ,\count_instr_reg[12]_i_1_n_1 ,\count_instr_reg[12]_i_1_n_2 ,\count_instr_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[12]_i_1_n_4 ,\count_instr_reg[12]_i_1_n_5 ,\count_instr_reg[12]_i_1_n_6 ,\count_instr_reg[12]_i_1_n_7 }),
        .S({\count_instr_reg_n_0_[15] ,\count_instr_reg_n_0_[14] ,\count_instr_reg_n_0_[13] ,\count_instr_reg_n_0_[12] }));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[12]_i_1_n_6 ),
        .Q(\count_instr_reg_n_0_[13] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[12]_i_1_n_5 ),
        .Q(\count_instr_reg_n_0_[14] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[12]_i_1_n_4 ),
        .Q(\count_instr_reg_n_0_[15] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[16]_i_1_n_7 ),
        .Q(\count_instr_reg_n_0_[16] ),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[16]_i_1 
       (.CI(\count_instr_reg[12]_i_1_n_0 ),
        .CO({\count_instr_reg[16]_i_1_n_0 ,\count_instr_reg[16]_i_1_n_1 ,\count_instr_reg[16]_i_1_n_2 ,\count_instr_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[16]_i_1_n_4 ,\count_instr_reg[16]_i_1_n_5 ,\count_instr_reg[16]_i_1_n_6 ,\count_instr_reg[16]_i_1_n_7 }),
        .S({\count_instr_reg_n_0_[19] ,\count_instr_reg_n_0_[18] ,\count_instr_reg_n_0_[17] ,\count_instr_reg_n_0_[16] }));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[16]_i_1_n_6 ),
        .Q(\count_instr_reg_n_0_[17] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[16]_i_1_n_5 ),
        .Q(\count_instr_reg_n_0_[18] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[16]_i_1_n_4 ),
        .Q(\count_instr_reg_n_0_[19] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[0]_i_2_n_6 ),
        .Q(\count_instr_reg_n_0_[1] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[20]_i_1_n_7 ),
        .Q(\count_instr_reg_n_0_[20] ),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[20]_i_1 
       (.CI(\count_instr_reg[16]_i_1_n_0 ),
        .CO({\count_instr_reg[20]_i_1_n_0 ,\count_instr_reg[20]_i_1_n_1 ,\count_instr_reg[20]_i_1_n_2 ,\count_instr_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[20]_i_1_n_4 ,\count_instr_reg[20]_i_1_n_5 ,\count_instr_reg[20]_i_1_n_6 ,\count_instr_reg[20]_i_1_n_7 }),
        .S({\count_instr_reg_n_0_[23] ,\count_instr_reg_n_0_[22] ,\count_instr_reg_n_0_[21] ,\count_instr_reg_n_0_[20] }));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[20]_i_1_n_6 ),
        .Q(\count_instr_reg_n_0_[21] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[20]_i_1_n_5 ),
        .Q(\count_instr_reg_n_0_[22] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[20]_i_1_n_4 ),
        .Q(\count_instr_reg_n_0_[23] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[24]_i_1_n_7 ),
        .Q(\count_instr_reg_n_0_[24] ),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[24]_i_1 
       (.CI(\count_instr_reg[20]_i_1_n_0 ),
        .CO({\count_instr_reg[24]_i_1_n_0 ,\count_instr_reg[24]_i_1_n_1 ,\count_instr_reg[24]_i_1_n_2 ,\count_instr_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[24]_i_1_n_4 ,\count_instr_reg[24]_i_1_n_5 ,\count_instr_reg[24]_i_1_n_6 ,\count_instr_reg[24]_i_1_n_7 }),
        .S({\count_instr_reg_n_0_[27] ,\count_instr_reg_n_0_[26] ,\count_instr_reg_n_0_[25] ,\count_instr_reg_n_0_[24] }));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[24]_i_1_n_6 ),
        .Q(\count_instr_reg_n_0_[25] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[24]_i_1_n_5 ),
        .Q(\count_instr_reg_n_0_[26] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[24]_i_1_n_4 ),
        .Q(\count_instr_reg_n_0_[27] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[28]_i_1_n_7 ),
        .Q(\count_instr_reg_n_0_[28] ),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[28]_i_1 
       (.CI(\count_instr_reg[24]_i_1_n_0 ),
        .CO({\count_instr_reg[28]_i_1_n_0 ,\count_instr_reg[28]_i_1_n_1 ,\count_instr_reg[28]_i_1_n_2 ,\count_instr_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[28]_i_1_n_4 ,\count_instr_reg[28]_i_1_n_5 ,\count_instr_reg[28]_i_1_n_6 ,\count_instr_reg[28]_i_1_n_7 }),
        .S({\count_instr_reg_n_0_[31] ,\count_instr_reg_n_0_[30] ,\count_instr_reg_n_0_[29] ,\count_instr_reg_n_0_[28] }));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[28]_i_1_n_6 ),
        .Q(\count_instr_reg_n_0_[29] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[0]_i_2_n_5 ),
        .Q(\count_instr_reg_n_0_[2] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[28]_i_1_n_5 ),
        .Q(\count_instr_reg_n_0_[30] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[28]_i_1_n_4 ),
        .Q(\count_instr_reg_n_0_[31] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[32] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[32]_i_1_n_7 ),
        .Q(data3[0]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[32]_i_1 
       (.CI(\count_instr_reg[28]_i_1_n_0 ),
        .CO({\count_instr_reg[32]_i_1_n_0 ,\count_instr_reg[32]_i_1_n_1 ,\count_instr_reg[32]_i_1_n_2 ,\count_instr_reg[32]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[32]_i_1_n_4 ,\count_instr_reg[32]_i_1_n_5 ,\count_instr_reg[32]_i_1_n_6 ,\count_instr_reg[32]_i_1_n_7 }),
        .S(data3[3:0]));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[33] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[32]_i_1_n_6 ),
        .Q(data3[1]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[34] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[32]_i_1_n_5 ),
        .Q(data3[2]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[35] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[32]_i_1_n_4 ),
        .Q(data3[3]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[36] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[36]_i_1_n_7 ),
        .Q(data3[4]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[36]_i_1 
       (.CI(\count_instr_reg[32]_i_1_n_0 ),
        .CO({\count_instr_reg[36]_i_1_n_0 ,\count_instr_reg[36]_i_1_n_1 ,\count_instr_reg[36]_i_1_n_2 ,\count_instr_reg[36]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[36]_i_1_n_4 ,\count_instr_reg[36]_i_1_n_5 ,\count_instr_reg[36]_i_1_n_6 ,\count_instr_reg[36]_i_1_n_7 }),
        .S(data3[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[37] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[36]_i_1_n_6 ),
        .Q(data3[5]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[38] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[36]_i_1_n_5 ),
        .Q(data3[6]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[39] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[36]_i_1_n_4 ),
        .Q(data3[7]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[0]_i_2_n_4 ),
        .Q(\count_instr_reg_n_0_[3] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[40] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[40]_i_1_n_7 ),
        .Q(data3[8]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[40]_i_1 
       (.CI(\count_instr_reg[36]_i_1_n_0 ),
        .CO({\count_instr_reg[40]_i_1_n_0 ,\count_instr_reg[40]_i_1_n_1 ,\count_instr_reg[40]_i_1_n_2 ,\count_instr_reg[40]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[40]_i_1_n_4 ,\count_instr_reg[40]_i_1_n_5 ,\count_instr_reg[40]_i_1_n_6 ,\count_instr_reg[40]_i_1_n_7 }),
        .S(data3[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[41] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[40]_i_1_n_6 ),
        .Q(data3[9]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[42] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[40]_i_1_n_5 ),
        .Q(data3[10]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[43] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[40]_i_1_n_4 ),
        .Q(data3[11]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[44] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[44]_i_1_n_7 ),
        .Q(data3[12]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[44]_i_1 
       (.CI(\count_instr_reg[40]_i_1_n_0 ),
        .CO({\count_instr_reg[44]_i_1_n_0 ,\count_instr_reg[44]_i_1_n_1 ,\count_instr_reg[44]_i_1_n_2 ,\count_instr_reg[44]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[44]_i_1_n_4 ,\count_instr_reg[44]_i_1_n_5 ,\count_instr_reg[44]_i_1_n_6 ,\count_instr_reg[44]_i_1_n_7 }),
        .S(data3[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[45] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[44]_i_1_n_6 ),
        .Q(data3[13]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[46] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[44]_i_1_n_5 ),
        .Q(data3[14]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[47] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[44]_i_1_n_4 ),
        .Q(data3[15]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[48] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[48]_i_1_n_7 ),
        .Q(data3[16]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[48]_i_1 
       (.CI(\count_instr_reg[44]_i_1_n_0 ),
        .CO({\count_instr_reg[48]_i_1_n_0 ,\count_instr_reg[48]_i_1_n_1 ,\count_instr_reg[48]_i_1_n_2 ,\count_instr_reg[48]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[48]_i_1_n_4 ,\count_instr_reg[48]_i_1_n_5 ,\count_instr_reg[48]_i_1_n_6 ,\count_instr_reg[48]_i_1_n_7 }),
        .S(data3[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[49] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[48]_i_1_n_6 ),
        .Q(data3[17]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[4]_i_1_n_7 ),
        .Q(\count_instr_reg_n_0_[4] ),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[4]_i_1 
       (.CI(\count_instr_reg[0]_i_2_n_0 ),
        .CO({\count_instr_reg[4]_i_1_n_0 ,\count_instr_reg[4]_i_1_n_1 ,\count_instr_reg[4]_i_1_n_2 ,\count_instr_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[4]_i_1_n_4 ,\count_instr_reg[4]_i_1_n_5 ,\count_instr_reg[4]_i_1_n_6 ,\count_instr_reg[4]_i_1_n_7 }),
        .S({\count_instr_reg_n_0_[7] ,\count_instr_reg_n_0_[6] ,\count_instr_reg_n_0_[5] ,\count_instr_reg_n_0_[4] }));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[50] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[48]_i_1_n_5 ),
        .Q(data3[18]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[51] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[48]_i_1_n_4 ),
        .Q(data3[19]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[52] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[52]_i_1_n_7 ),
        .Q(data3[20]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[52]_i_1 
       (.CI(\count_instr_reg[48]_i_1_n_0 ),
        .CO({\count_instr_reg[52]_i_1_n_0 ,\count_instr_reg[52]_i_1_n_1 ,\count_instr_reg[52]_i_1_n_2 ,\count_instr_reg[52]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[52]_i_1_n_4 ,\count_instr_reg[52]_i_1_n_5 ,\count_instr_reg[52]_i_1_n_6 ,\count_instr_reg[52]_i_1_n_7 }),
        .S(data3[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[53] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[52]_i_1_n_6 ),
        .Q(data3[21]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[54] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[52]_i_1_n_5 ),
        .Q(data3[22]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[55] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[52]_i_1_n_4 ),
        .Q(data3[23]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[56] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[56]_i_1_n_7 ),
        .Q(data3[24]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[56]_i_1 
       (.CI(\count_instr_reg[52]_i_1_n_0 ),
        .CO({\count_instr_reg[56]_i_1_n_0 ,\count_instr_reg[56]_i_1_n_1 ,\count_instr_reg[56]_i_1_n_2 ,\count_instr_reg[56]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[56]_i_1_n_4 ,\count_instr_reg[56]_i_1_n_5 ,\count_instr_reg[56]_i_1_n_6 ,\count_instr_reg[56]_i_1_n_7 }),
        .S(data3[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[57] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[56]_i_1_n_6 ),
        .Q(data3[25]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[58] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[56]_i_1_n_5 ),
        .Q(data3[26]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[59] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[56]_i_1_n_4 ),
        .Q(data3[27]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[4]_i_1_n_6 ),
        .Q(\count_instr_reg_n_0_[5] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[60] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[60]_i_1_n_7 ),
        .Q(data3[28]),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[60]_i_1 
       (.CI(\count_instr_reg[56]_i_1_n_0 ),
        .CO({\NLW_count_instr_reg[60]_i_1_CO_UNCONNECTED [3],\count_instr_reg[60]_i_1_n_1 ,\count_instr_reg[60]_i_1_n_2 ,\count_instr_reg[60]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[60]_i_1_n_4 ,\count_instr_reg[60]_i_1_n_5 ,\count_instr_reg[60]_i_1_n_6 ,\count_instr_reg[60]_i_1_n_7 }),
        .S(data3[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[61] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[60]_i_1_n_6 ),
        .Q(data3[29]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[62] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[60]_i_1_n_5 ),
        .Q(data3[30]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[63] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[60]_i_1_n_4 ),
        .Q(data3[31]),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[4]_i_1_n_5 ),
        .Q(\count_instr_reg_n_0_[6] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[4]_i_1_n_4 ),
        .Q(\count_instr_reg_n_0_[7] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[8]_i_1_n_7 ),
        .Q(\count_instr_reg_n_0_[8] ),
        .R(instr_and_i_1_n_0));
  CARRY4 \count_instr_reg[8]_i_1 
       (.CI(\count_instr_reg[4]_i_1_n_0 ),
        .CO({\count_instr_reg[8]_i_1_n_0 ,\count_instr_reg[8]_i_1_n_1 ,\count_instr_reg[8]_i_1_n_2 ,\count_instr_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_instr_reg[8]_i_1_n_4 ,\count_instr_reg[8]_i_1_n_5 ,\count_instr_reg[8]_i_1_n_6 ,\count_instr_reg[8]_i_1_n_7 }),
        .S({\count_instr_reg_n_0_[11] ,\count_instr_reg_n_0_[10] ,\count_instr_reg_n_0_[9] ,\count_instr_reg_n_0_[8] }));
  FDRE #(
    .INIT(1'b0)) 
    \count_instr_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(count_instr),
        .D(\count_instr_reg[8]_i_1_n_6 ),
        .Q(\count_instr_reg_n_0_[9] ),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \cpu_state[0]_i_1 
       (.I0(is_lb_lh_lw_lbu_lhu),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(resetn_IBUF),
        .I3(\cpu_state[7]_i_8_n_0 ),
        .O(cpu_state0_out[0]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \cpu_state[1]_i_1 
       (.I0(is_sb_sh_sw),
        .I1(is_lb_lh_lw_lbu_lhu),
        .I2(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I3(is_lui_auipc_jal),
        .I4(\cpu_state[3]_i_2_n_0 ),
        .O(cpu_state0_out[1]));
  LUT6 #(
    .INIT(64'hFFFF008000000000)) 
    \cpu_state[2]_i_1 
       (.I0(\cpu_state[2]_i_2_n_0 ),
        .I1(\cpu_state[3]_i_3_n_0 ),
        .I2(is_sll_srl_sra),
        .I3(is_lb_lh_lw_lbu_lhu),
        .I4(is_slli_srli_srai),
        .I5(\reg_op2[31]_i_1_n_0 ),
        .O(cpu_state0_out[2]));
  LUT6 #(
    .INIT(64'h00000000FFFFFFBF)) 
    \cpu_state[2]_i_2 
       (.I0(\cpu_state[7]_i_12_n_0 ),
        .I1(\cpu_state[7]_i_11_n_0 ),
        .I2(\cpu_state[7]_i_10_n_0 ),
        .I3(is_lui_auipc_jal_i_1_n_0),
        .I4(\cpu_state[7]_i_9_n_0 ),
        .I5(\cpu_state[6]_i_3_n_0 ),
        .O(\cpu_state[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0002FFFF00020002)) 
    \cpu_state[3]_i_1 
       (.I0(\cpu_state[3]_i_2_n_0 ),
        .I1(is_lb_lh_lw_lbu_lhu),
        .I2(is_sb_sh_sw),
        .I3(is_sll_srl_sra),
        .I4(\cpu_state[3]_i_3_n_0 ),
        .I5(\reg_op2[31]_i_1_n_0 ),
        .O(cpu_state0_out[3]));
  LUT6 #(
    .INIT(64'h0000455500000000)) 
    \cpu_state[3]_i_2 
       (.I0(is_slli_srli_srai),
        .I1(\cpu_state[7]_i_12_n_0 ),
        .I2(\cpu_state[3]_i_4_n_0 ),
        .I3(\cpu_state[3]_i_5_n_0 ),
        .I4(\cpu_state[6]_i_3_n_0 ),
        .I5(\reg_op2[31]_i_1_n_0 ),
        .O(\cpu_state[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \cpu_state[3]_i_3 
       (.I0(is_lui_auipc_jal),
        .I1(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .O(\cpu_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    \cpu_state[3]_i_4 
       (.I0(\cpu_state[7]_i_13_n_0 ),
        .I1(\cpu_state[3]_i_6_n_0 ),
        .I2(\cpu_state[3]_i_7_n_0 ),
        .I3(instr_srl),
        .I4(instr_sra),
        .I5(instr_and),
        .O(\cpu_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \cpu_state[3]_i_5 
       (.I0(instr_jal),
        .I1(instr_lui),
        .I2(instr_auipc),
        .I3(instr_bge),
        .I4(instr_beq),
        .I5(instr_bne),
        .O(\cpu_state[3]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \cpu_state[3]_i_6 
       (.I0(instr_slt),
        .I1(instr_jalr),
        .I2(instr_sltiu),
        .I3(instr_sltu),
        .O(\cpu_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \cpu_state[3]_i_7 
       (.I0(instr_lbu),
        .I1(instr_lhu),
        .I2(instr_sb),
        .I3(instr_add),
        .O(\cpu_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cpu_state[5]_i_1 
       (.I0(resetn_IBUF),
        .I1(reg_next_pc),
        .O(cpu_state0_out[5]));
  LUT6 #(
    .INIT(64'hFFFDFFFFFFFDFFFD)) 
    \cpu_state[6]_i_1 
       (.I0(resetn_IBUF),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\cpu_state_reg_n_0_[3] ),
        .I3(\cpu_state[6]_i_2_n_0 ),
        .I4(reg_next_pc),
        .I5(\cpu_state[6]_i_3_n_0 ),
        .O(cpu_state0_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \cpu_state[6]_i_2 
       (.I0(\cpu_state_reg_n_0_[0] ),
        .I1(\cpu_state_reg_n_0_[1] ),
        .O(\cpu_state[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cpu_state[6]_i_3 
       (.I0(instr_rdcycle),
        .I1(instr_rdinstrh),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .O(\cpu_state[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFF00A800A800A800)) 
    \cpu_state[7]_i_1 
       (.I0(\cpu_state[7]_i_4_n_0 ),
        .I1(mem_do_rdata),
        .I2(mem_do_wdata),
        .I3(resetn_IBUF),
        .I4(\reg_pc_reg_n_0_[1] ),
        .I5(mem_do_rinst_reg_n_0),
        .O(\cpu_state[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \cpu_state[7]_i_10 
       (.I0(instr_sltu),
        .I1(instr_sltiu),
        .I2(instr_jalr),
        .I3(instr_slt),
        .I4(\cpu_state[7]_i_13_n_0 ),
        .O(\cpu_state[7]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \cpu_state[7]_i_11 
       (.I0(instr_and),
        .I1(instr_sra),
        .I2(instr_srl),
        .I3(instr_add),
        .I4(instr_sb),
        .I5(\cpu_state[7]_i_14_n_0 ),
        .O(\cpu_state[7]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \cpu_state[7]_i_12 
       (.I0(\cpu_state[7]_i_15_n_0 ),
        .I1(instr_slti),
        .I2(instr_sw),
        .I3(instr_sh),
        .I4(instr_sub),
        .I5(\cpu_state[7]_i_16_n_0 ),
        .O(\cpu_state[7]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \cpu_state[7]_i_13 
       (.I0(instr_blt),
        .I1(instr_bltu),
        .I2(instr_bgeu),
        .I3(instr_lw),
        .O(\cpu_state[7]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \cpu_state[7]_i_14 
       (.I0(instr_lhu),
        .I1(instr_lbu),
        .O(\cpu_state[7]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \cpu_state[7]_i_15 
       (.I0(instr_srli),
        .I1(instr_srai),
        .I2(instr_sll),
        .I3(instr_slli),
        .I4(instr_xor),
        .I5(instr_xori),
        .O(\cpu_state[7]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \cpu_state[7]_i_16 
       (.I0(instr_addi),
        .I1(instr_lh),
        .I2(instr_andi),
        .I3(instr_ori),
        .I4(instr_or),
        .I5(instr_lb),
        .O(\cpu_state[7]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hFDFCFFFFFDFCFFFC)) 
    \cpu_state[7]_i_2 
       (.I0(is_beq_bne_blt_bge_bltu_bgeu),
        .I1(\cpu_state[7]_i_5_n_0 ),
        .I2(\cpu_state[7]_i_6_n_0 ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(instr_jal_i_3_n_0),
        .I5(\cpu_state[7]_i_7_n_0 ),
        .O(\cpu_state[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \cpu_state[7]_i_3 
       (.I0(resetn_IBUF),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(\cpu_state[7]_i_8_n_0 ),
        .O(cpu_state0_out[7]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h2322)) 
    \cpu_state[7]_i_4 
       (.I0(\reg_op1_reg_n_0_[0] ),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .O(\cpu_state[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \cpu_state[7]_i_5 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_sh_reg_n_0_[0] ),
        .I2(\reg_sh_reg_n_0_[1] ),
        .I3(\reg_sh_reg_n_0_[2] ),
        .I4(\reg_sh_reg_n_0_[3] ),
        .I5(\reg_sh_reg_n_0_[4] ),
        .O(\cpu_state[7]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hDFDDDDDD)) 
    \cpu_state[7]_i_6 
       (.I0(resetn_IBUF),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(instr_jal),
        .I3(decoder_trigger_reg_n_0),
        .I4(reg_next_pc),
        .O(\cpu_state[7]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    \cpu_state[7]_i_7 
       (.I0(\cpu_state_reg_n_0_[1] ),
        .I1(\cpu_state_reg_n_0_[0] ),
        .I2(mem_do_prefetch_reg_n_0),
        .O(\cpu_state[7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    \cpu_state[7]_i_8 
       (.I0(\cpu_state[7]_i_9_n_0 ),
        .I1(is_lui_auipc_jal_i_1_n_0),
        .I2(\cpu_state[6]_i_3_n_0 ),
        .I3(\cpu_state[7]_i_10_n_0 ),
        .I4(\cpu_state[7]_i_11_n_0 ),
        .I5(\cpu_state[7]_i_12_n_0 ),
        .O(\cpu_state[7]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \cpu_state[7]_i_9 
       (.I0(instr_bne),
        .I1(instr_beq),
        .I2(instr_bge),
        .O(\cpu_state[7]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cpu_state_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\cpu_state[7]_i_2_n_0 ),
        .D(cpu_state0_out[0]),
        .Q(\cpu_state_reg_n_0_[0] ),
        .R(\cpu_state[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cpu_state_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\cpu_state[7]_i_2_n_0 ),
        .D(cpu_state0_out[1]),
        .Q(\cpu_state_reg_n_0_[1] ),
        .R(\cpu_state[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cpu_state_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\cpu_state[7]_i_2_n_0 ),
        .D(cpu_state0_out[2]),
        .Q(\cpu_state_reg_n_0_[2] ),
        .R(\cpu_state[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cpu_state_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\cpu_state[7]_i_2_n_0 ),
        .D(cpu_state0_out[3]),
        .Q(\cpu_state_reg_n_0_[3] ),
        .R(\cpu_state[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cpu_state_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\cpu_state[7]_i_2_n_0 ),
        .D(cpu_state0_out[5]),
        .Q(\cpu_state_reg_n_0_[5] ),
        .R(\cpu_state[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cpu_state_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\cpu_state[7]_i_2_n_0 ),
        .D(cpu_state0_out[6]),
        .Q(reg_next_pc),
        .R(\cpu_state[7]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \cpu_state_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\cpu_state[7]_i_2_n_0 ),
        .D(cpu_state0_out[7]),
        .Q(\cpu_state_reg_n_0_[7] ),
        .S(\cpu_state[7]_i_1_n_0 ));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M_UNIQ_BASE_ cpuregs_reg_r1_0_31_0_5
       (.ADDRA(decoded_rs2),
        .ADDRB(decoded_rs2),
        .ADDRC(decoded_rs2),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_0_5_i_2_n_0,cpuregs_reg_r1_0_31_0_5_i_3_n_0}),
        .DIB({cpuregs_reg_r1_0_31_0_5_i_4_n_0,cpuregs_reg_r1_0_31_0_5_i_5_n_0}),
        .DIC({cpuregs_reg_r1_0_31_0_5_i_6_n_0,cpuregs_reg_r1_0_31_0_5_i_7_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_sh1[1:0]),
        .DOB(reg_sh1[3:2]),
        .DOC(reg_sh1[5:4]),
        .DOD(NLW_cpuregs_reg_r1_0_31_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF0000FFFE)) 
    cpuregs_reg_r1_0_31_0_5_i_1
       (.I0(latched_rd[1]),
        .I1(latched_rd[2]),
        .I2(latched_rd[0]),
        .I3(latched_rd[3]),
        .I4(cpuregs_reg_r1_0_31_0_5_i_8_n_0),
        .I5(latched_rd[4]),
        .O(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_0_5_i_2
       (.I0(p_1_out_carry_n_6),
        .I1(\reg_out_reg_n_0_[1] ),
        .I2(alu_out_q[1]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_0_5_i_2_n_0));
  LUT5 #(
    .INIT(32'h00A000C0)) 
    cpuregs_reg_r1_0_31_0_5_i_3
       (.I0(alu_out_q[0]),
        .I1(\reg_out_reg_n_0_[0] ),
        .I2(latched_store_reg_n_0),
        .I3(latched_branch_reg_n_0),
        .I4(latched_stalu_reg_n_0),
        .O(cpuregs_reg_r1_0_31_0_5_i_3_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_0_5_i_4
       (.I0(p_1_out_carry_n_4),
        .I1(\reg_out_reg_n_0_[3] ),
        .I2(alu_out_q[3]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_0_5_i_4_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_0_5_i_5
       (.I0(p_1_out_carry_n_5),
        .I1(\reg_out_reg_n_0_[2] ),
        .I2(alu_out_q[2]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_0_5_i_5_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_0_5_i_6
       (.I0(p_1_out_carry__0_n_6),
        .I1(\reg_out_reg_n_0_[5] ),
        .I2(alu_out_q[5]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_0_5_i_6_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_0_5_i_7
       (.I0(p_1_out_carry__0_n_7),
        .I1(\reg_out_reg_n_0_[4] ),
        .I2(alu_out_q[4]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_0_5_i_7_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFEFFFEFFFF)) 
    cpuregs_reg_r1_0_31_0_5_i_8
       (.I0(cpuregs_reg_r1_0_31_0_5_i_9_n_0),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(\cpu_state[6]_i_2_n_0 ),
        .I3(mem_do_prefetch_i_2_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_store_reg_n_0),
        .O(cpuregs_reg_r1_0_31_0_5_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    cpuregs_reg_r1_0_31_0_5_i_9
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(\cpu_state_reg_n_0_[7] ),
        .O(cpuregs_reg_r1_0_31_0_5_i_9_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M_HD23 cpuregs_reg_r1_0_31_12_17
       (.ADDRA(decoded_rs2),
        .ADDRB(decoded_rs2),
        .ADDRC(decoded_rs2),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_12_17_i_1_n_0,cpuregs_reg_r1_0_31_12_17_i_2_n_0}),
        .DIB({cpuregs_reg_r1_0_31_12_17_i_3_n_0,cpuregs_reg_r1_0_31_12_17_i_4_n_0}),
        .DIC({cpuregs_reg_r1_0_31_12_17_i_5_n_0,cpuregs_reg_r1_0_31_12_17_i_6_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_sh1[13:12]),
        .DOB(reg_sh1[15:14]),
        .DOC(reg_sh1[17:16]),
        .DOD(NLW_cpuregs_reg_r1_0_31_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_12_17_i_1
       (.I0(p_1_out_carry__2_n_6),
        .I1(\reg_out_reg_n_0_[13] ),
        .I2(alu_out_q[13]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_12_17_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_12_17_i_2
       (.I0(p_1_out_carry__2_n_7),
        .I1(\reg_out_reg_n_0_[12] ),
        .I2(alu_out_q[12]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_12_17_i_2_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_12_17_i_3
       (.I0(p_1_out_carry__2_n_4),
        .I1(\reg_out_reg_n_0_[15] ),
        .I2(alu_out_q[15]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_12_17_i_3_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_12_17_i_4
       (.I0(p_1_out_carry__2_n_5),
        .I1(\reg_out_reg_n_0_[14] ),
        .I2(alu_out_q[14]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_12_17_i_4_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_12_17_i_5
       (.I0(p_1_out_carry__3_n_6),
        .I1(\reg_out_reg_n_0_[17] ),
        .I2(alu_out_q[17]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_12_17_i_5_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_12_17_i_6
       (.I0(p_1_out_carry__3_n_7),
        .I1(\reg_out_reg_n_0_[16] ),
        .I2(alu_out_q[16]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_12_17_i_6_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M_HD24 cpuregs_reg_r1_0_31_18_23
       (.ADDRA(decoded_rs2),
        .ADDRB(decoded_rs2),
        .ADDRC(decoded_rs2),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_18_23_i_1_n_0,cpuregs_reg_r1_0_31_18_23_i_2_n_0}),
        .DIB({cpuregs_reg_r1_0_31_18_23_i_3_n_0,cpuregs_reg_r1_0_31_18_23_i_4_n_0}),
        .DIC({cpuregs_reg_r1_0_31_18_23_i_5_n_0,cpuregs_reg_r1_0_31_18_23_i_6_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_sh1[19:18]),
        .DOB(reg_sh1[21:20]),
        .DOC(reg_sh1[23:22]),
        .DOD(NLW_cpuregs_reg_r1_0_31_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_18_23_i_1
       (.I0(p_1_out_carry__3_n_4),
        .I1(\reg_out_reg_n_0_[19] ),
        .I2(alu_out_q[19]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_18_23_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_18_23_i_2
       (.I0(p_1_out_carry__3_n_5),
        .I1(\reg_out_reg_n_0_[18] ),
        .I2(alu_out_q[18]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_18_23_i_2_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_18_23_i_3
       (.I0(p_1_out_carry__4_n_6),
        .I1(\reg_out_reg_n_0_[21] ),
        .I2(alu_out_q[21]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_18_23_i_3_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_18_23_i_4
       (.I0(p_1_out_carry__4_n_7),
        .I1(\reg_out_reg_n_0_[20] ),
        .I2(alu_out_q[20]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_18_23_i_4_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_18_23_i_5
       (.I0(p_1_out_carry__4_n_4),
        .I1(\reg_out_reg_n_0_[23] ),
        .I2(alu_out_q[23]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_18_23_i_5_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_18_23_i_6
       (.I0(p_1_out_carry__4_n_5),
        .I1(\reg_out_reg_n_0_[22] ),
        .I2(alu_out_q[22]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_18_23_i_6_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M_HD25 cpuregs_reg_r1_0_31_24_29
       (.ADDRA(decoded_rs2),
        .ADDRB(decoded_rs2),
        .ADDRC(decoded_rs2),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_24_29_i_1_n_0,cpuregs_reg_r1_0_31_24_29_i_2_n_0}),
        .DIB({cpuregs_reg_r1_0_31_24_29_i_3_n_0,cpuregs_reg_r1_0_31_24_29_i_4_n_0}),
        .DIC({cpuregs_reg_r1_0_31_24_29_i_5_n_0,cpuregs_reg_r1_0_31_24_29_i_6_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_sh1[25:24]),
        .DOB(reg_sh1[27:26]),
        .DOC(reg_sh1[29:28]),
        .DOD(NLW_cpuregs_reg_r1_0_31_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_24_29_i_1
       (.I0(p_1_out_carry__5_n_6),
        .I1(\reg_out_reg_n_0_[25] ),
        .I2(alu_out_q[25]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_24_29_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_24_29_i_2
       (.I0(p_1_out_carry__5_n_7),
        .I1(\reg_out_reg_n_0_[24] ),
        .I2(alu_out_q[24]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_24_29_i_2_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_24_29_i_3
       (.I0(p_1_out_carry__5_n_4),
        .I1(\reg_out_reg_n_0_[27] ),
        .I2(alu_out_q[27]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_24_29_i_3_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_24_29_i_4
       (.I0(p_1_out_carry__5_n_5),
        .I1(\reg_out_reg_n_0_[26] ),
        .I2(alu_out_q[26]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_24_29_i_4_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_24_29_i_5
       (.I0(p_1_out_carry__6_n_6),
        .I1(\reg_out_reg_n_0_[29] ),
        .I2(alu_out_q[29]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_24_29_i_5_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_24_29_i_6
       (.I0(p_1_out_carry__6_n_7),
        .I1(\reg_out_reg_n_0_[28] ),
        .I2(alu_out_q[28]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_24_29_i_6_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M_HD26 cpuregs_reg_r1_0_31_30_31
       (.ADDRA(decoded_rs2),
        .ADDRB(decoded_rs2),
        .ADDRC(decoded_rs2),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_30_31_i_1_n_0,cpuregs_reg_r1_0_31_30_31_i_2_n_0}),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_sh1[31:30]),
        .DOB(NLW_cpuregs_reg_r1_0_31_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_cpuregs_reg_r1_0_31_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_cpuregs_reg_r1_0_31_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_30_31_i_1
       (.I0(p_1_out_carry__6_n_4),
        .I1(\reg_out_reg_n_0_[31] ),
        .I2(alu_out_q[31]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_30_31_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_30_31_i_2
       (.I0(p_1_out_carry__6_n_5),
        .I1(\reg_out_reg_n_0_[30] ),
        .I2(alu_out_q[30]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_30_31_i_2_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M_HD27 cpuregs_reg_r1_0_31_6_11
       (.ADDRA(decoded_rs2),
        .ADDRB(decoded_rs2),
        .ADDRC(decoded_rs2),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_6_11_i_1_n_0,cpuregs_reg_r1_0_31_6_11_i_2_n_0}),
        .DIB({cpuregs_reg_r1_0_31_6_11_i_3_n_0,cpuregs_reg_r1_0_31_6_11_i_4_n_0}),
        .DIC({cpuregs_reg_r1_0_31_6_11_i_5_n_0,cpuregs_reg_r1_0_31_6_11_i_6_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_sh1[7:6]),
        .DOB(reg_sh1[9:8]),
        .DOC(reg_sh1[11:10]),
        .DOD(NLW_cpuregs_reg_r1_0_31_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_6_11_i_1
       (.I0(p_1_out_carry__0_n_4),
        .I1(\reg_out_reg_n_0_[7] ),
        .I2(alu_out_q[7]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_6_11_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_6_11_i_2
       (.I0(p_1_out_carry__0_n_5),
        .I1(\reg_out_reg_n_0_[6] ),
        .I2(alu_out_q[6]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_6_11_i_2_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_6_11_i_3
       (.I0(p_1_out_carry__1_n_6),
        .I1(\reg_out_reg_n_0_[9] ),
        .I2(alu_out_q[9]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_6_11_i_3_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_6_11_i_4
       (.I0(p_1_out_carry__1_n_7),
        .I1(\reg_out_reg_n_0_[8] ),
        .I2(alu_out_q[8]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_6_11_i_4_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_6_11_i_5
       (.I0(p_1_out_carry__1_n_4),
        .I1(\reg_out_reg_n_0_[11] ),
        .I2(alu_out_q[11]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_6_11_i_5_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAF0CC0000)) 
    cpuregs_reg_r1_0_31_6_11_i_6
       (.I0(p_1_out_carry__1_n_5),
        .I1(\reg_out_reg_n_0_[10] ),
        .I2(alu_out_q[10]),
        .I3(latched_stalu_reg_n_0),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(cpuregs_reg_r1_0_31_6_11_i_6_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M_HD28 cpuregs_reg_r2_0_31_0_5
       (.ADDRA(decoded_rs1),
        .ADDRB(decoded_rs1),
        .ADDRC(decoded_rs1),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_0_5_i_2_n_0,cpuregs_reg_r1_0_31_0_5_i_3_n_0}),
        .DIB({cpuregs_reg_r1_0_31_0_5_i_4_n_0,cpuregs_reg_r1_0_31_0_5_i_5_n_0}),
        .DIC({cpuregs_reg_r1_0_31_0_5_i_6_n_0,cpuregs_reg_r1_0_31_0_5_i_7_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_out1[1:0]),
        .DOB(reg_out1[3:2]),
        .DOC(reg_out1[5:4]),
        .DOD(NLW_cpuregs_reg_r2_0_31_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M_HD29 cpuregs_reg_r2_0_31_12_17
       (.ADDRA(decoded_rs1),
        .ADDRB(decoded_rs1),
        .ADDRC(decoded_rs1),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_12_17_i_1_n_0,cpuregs_reg_r1_0_31_12_17_i_2_n_0}),
        .DIB({cpuregs_reg_r1_0_31_12_17_i_3_n_0,cpuregs_reg_r1_0_31_12_17_i_4_n_0}),
        .DIC({cpuregs_reg_r1_0_31_12_17_i_5_n_0,cpuregs_reg_r1_0_31_12_17_i_6_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_out1[13:12]),
        .DOB(reg_out1[15:14]),
        .DOC(reg_out1[17:16]),
        .DOD(NLW_cpuregs_reg_r2_0_31_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M_HD30 cpuregs_reg_r2_0_31_18_23
       (.ADDRA(decoded_rs1),
        .ADDRB(decoded_rs1),
        .ADDRC(decoded_rs1),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_18_23_i_1_n_0,cpuregs_reg_r1_0_31_18_23_i_2_n_0}),
        .DIB({cpuregs_reg_r1_0_31_18_23_i_3_n_0,cpuregs_reg_r1_0_31_18_23_i_4_n_0}),
        .DIC({cpuregs_reg_r1_0_31_18_23_i_5_n_0,cpuregs_reg_r1_0_31_18_23_i_6_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_out1[19:18]),
        .DOB(reg_out1[21:20]),
        .DOC(reg_out1[23:22]),
        .DOD(NLW_cpuregs_reg_r2_0_31_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M_HD31 cpuregs_reg_r2_0_31_24_29
       (.ADDRA(decoded_rs1),
        .ADDRB(decoded_rs1),
        .ADDRC(decoded_rs1),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_24_29_i_1_n_0,cpuregs_reg_r1_0_31_24_29_i_2_n_0}),
        .DIB({cpuregs_reg_r1_0_31_24_29_i_3_n_0,cpuregs_reg_r1_0_31_24_29_i_4_n_0}),
        .DIC({cpuregs_reg_r1_0_31_24_29_i_5_n_0,cpuregs_reg_r1_0_31_24_29_i_6_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_out1[25:24]),
        .DOB(reg_out1[27:26]),
        .DOC(reg_out1[29:28]),
        .DOD(NLW_cpuregs_reg_r2_0_31_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M_HD32 cpuregs_reg_r2_0_31_30_31
       (.ADDRA(decoded_rs1),
        .ADDRB(decoded_rs1),
        .ADDRC(decoded_rs1),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_30_31_i_1_n_0,cpuregs_reg_r1_0_31_30_31_i_2_n_0}),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_out1[31:30]),
        .DOB(NLW_cpuregs_reg_r2_0_31_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_cpuregs_reg_r2_0_31_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_cpuregs_reg_r2_0_31_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "picorv32_core/cpuregs" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M_HD33 cpuregs_reg_r2_0_31_6_11
       (.ADDRA(decoded_rs1),
        .ADDRB(decoded_rs1),
        .ADDRC(decoded_rs1),
        .ADDRD(latched_rd),
        .DIA({cpuregs_reg_r1_0_31_6_11_i_1_n_0,cpuregs_reg_r1_0_31_6_11_i_2_n_0}),
        .DIB({cpuregs_reg_r1_0_31_6_11_i_3_n_0,cpuregs_reg_r1_0_31_6_11_i_4_n_0}),
        .DIC({cpuregs_reg_r1_0_31_6_11_i_5_n_0,cpuregs_reg_r1_0_31_6_11_i_6_n_0}),
        .DID({1'b0,1'b0}),
        .DOA(reg_out1[7:6]),
        .DOB(reg_out1[9:8]),
        .DOC(reg_out1[11:10]),
        .DOD(NLW_cpuregs_reg_r2_0_31_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(clk_IBUF_BUFG),
        .WE(cpuregs_reg_r1_0_31_0_5_i_1_n_0));
  LUT6 #(
    .INIT(64'h000000008888B888)) 
    \decoded_imm[0]_i_1 
       (.I0(\mem_rdata_q_reg_n_0_[20] ),
        .I1(\decoded_imm[11]_i_2_n_0 ),
        .I2(is_sb_sh_sw),
        .I3(\mem_rdata_q_reg_n_0_[7] ),
        .I4(is_beq_bne_blt_bge_bltu_bgeu),
        .I5(is_lui_auipc_jal_i_1_n_0),
        .O(decoded_imm[0]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'hF444)) 
    \decoded_imm[10]_i_1 
       (.I0(\decoded_imm[11]_i_3_n_0 ),
        .I1(\mem_rdata_q_reg_n_0_[30] ),
        .I2(decoded_imm_uj[10]),
        .I3(instr_jal),
        .O(decoded_imm[10]));
  LUT6 #(
    .INIT(64'hFFFFFFFF0E020F00)) 
    \decoded_imm[11]_i_1 
       (.I0(\mem_rdata_q_reg_n_0_[7] ),
        .I1(\decoded_imm[11]_i_2_n_0 ),
        .I2(\decoded_imm[11]_i_3_n_0 ),
        .I3(\mem_rdata_q_reg_n_0_[31] ),
        .I4(is_beq_bne_blt_bge_bltu_bgeu),
        .I5(\decoded_imm[11]_i_4_n_0 ),
        .O(decoded_imm[11]));
  LUT3 #(
    .INIT(8'hFE)) 
    \decoded_imm[11]_i_2 
       (.I0(is_lb_lh_lw_lbu_lhu),
        .I1(instr_jalr),
        .I2(is_alu_reg_imm),
        .O(\decoded_imm[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000001)) 
    \decoded_imm[11]_i_3 
       (.I0(is_lb_lh_lw_lbu_lhu),
        .I1(instr_jalr),
        .I2(is_alu_reg_imm),
        .I3(is_sb_sh_sw),
        .I4(is_beq_bne_blt_bge_bltu_bgeu),
        .I5(is_lui_auipc_jal_i_1_n_0),
        .O(\decoded_imm[11]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \decoded_imm[11]_i_4 
       (.I0(decoded_imm_uj[11]),
        .I1(instr_jal),
        .O(\decoded_imm[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFAFAFAFAEEEEEEAA)) 
    \decoded_imm[12]_i_1 
       (.I0(\decoded_imm[19]_i_2_n_0 ),
        .I1(p_0_in_1[0]),
        .I2(decoded_imm_uj[12]),
        .I3(instr_lui),
        .I4(instr_auipc),
        .I5(instr_jal),
        .O(decoded_imm[12]));
  LUT6 #(
    .INIT(64'hFFFFFEAAAAAAFEAA)) 
    \decoded_imm[13]_i_1 
       (.I0(\decoded_imm[19]_i_2_n_0 ),
        .I1(instr_lui),
        .I2(instr_auipc),
        .I3(p_0_in_1[1]),
        .I4(instr_jal),
        .I5(decoded_imm_uj[13]),
        .O(decoded_imm[13]));
  LUT6 #(
    .INIT(64'hFFFFFEAAAAAAFEAA)) 
    \decoded_imm[14]_i_1 
       (.I0(\decoded_imm[19]_i_2_n_0 ),
        .I1(instr_lui),
        .I2(instr_auipc),
        .I3(p_0_in_1[2]),
        .I4(instr_jal),
        .I5(decoded_imm_uj[14]),
        .O(decoded_imm[14]));
  LUT6 #(
    .INIT(64'hFFFFFEAAAAAAFEAA)) 
    \decoded_imm[15]_i_1 
       (.I0(\decoded_imm[19]_i_2_n_0 ),
        .I1(instr_lui),
        .I2(instr_auipc),
        .I3(\mem_rdata_q_reg_n_0_[15] ),
        .I4(instr_jal),
        .I5(decoded_rs1_0[0]),
        .O(decoded_imm[15]));
  LUT6 #(
    .INIT(64'hFAFAFAFAEEEEEEAA)) 
    \decoded_imm[16]_i_1 
       (.I0(\decoded_imm[19]_i_2_n_0 ),
        .I1(\mem_rdata_q_reg_n_0_[16] ),
        .I2(decoded_rs1_0[1]),
        .I3(instr_lui),
        .I4(instr_auipc),
        .I5(instr_jal),
        .O(decoded_imm[16]));
  LUT6 #(
    .INIT(64'hFFFFFEAAAAAAFEAA)) 
    \decoded_imm[17]_i_1 
       (.I0(\decoded_imm[19]_i_2_n_0 ),
        .I1(instr_lui),
        .I2(instr_auipc),
        .I3(\mem_rdata_q_reg_n_0_[17] ),
        .I4(instr_jal),
        .I5(decoded_rs1_0[2]),
        .O(decoded_imm[17]));
  LUT6 #(
    .INIT(64'hFAFAFAFAEEEEEEAA)) 
    \decoded_imm[18]_i_1 
       (.I0(\decoded_imm[19]_i_2_n_0 ),
        .I1(\mem_rdata_q_reg_n_0_[18] ),
        .I2(decoded_rs1_0[3]),
        .I3(instr_lui),
        .I4(instr_auipc),
        .I5(instr_jal),
        .O(decoded_imm[18]));
  LUT6 #(
    .INIT(64'hFAFAFAFAEEEEEEAA)) 
    \decoded_imm[19]_i_1 
       (.I0(\decoded_imm[19]_i_2_n_0 ),
        .I1(\mem_rdata_q_reg_n_0_[19] ),
        .I2(decoded_rs1_0[4]),
        .I3(instr_lui),
        .I4(instr_auipc),
        .I5(instr_jal),
        .O(decoded_imm[19]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \decoded_imm[19]_i_2 
       (.I0(\mem_rdata_q_reg_n_0_[31] ),
        .I1(\decoded_imm[11]_i_3_n_0 ),
        .O(\decoded_imm[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888FF88F8F8)) 
    \decoded_imm[1]_i_1 
       (.I0(decoded_imm_uj[1]),
        .I1(instr_jal),
        .I2(\mem_rdata_q_reg_n_0_[8] ),
        .I3(\mem_rdata_q_reg_n_0_[21] ),
        .I4(\decoded_imm[11]_i_2_n_0 ),
        .I5(\decoded_imm[11]_i_3_n_0 ),
        .O(decoded_imm[1]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[20]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[20] ),
        .O(\decoded_imm[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[21]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[21] ),
        .O(\decoded_imm[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[22]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[22] ),
        .O(\decoded_imm[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[23]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[23] ),
        .O(\decoded_imm[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[24]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[24] ),
        .O(\decoded_imm[24]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[25]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[25] ),
        .O(\decoded_imm[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[26]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[26] ),
        .O(\decoded_imm[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[27]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[27] ),
        .O(\decoded_imm[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[28]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[28] ),
        .O(\decoded_imm[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[29]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[29] ),
        .O(\decoded_imm[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h88888888FF88F8F8)) 
    \decoded_imm[2]_i_1 
       (.I0(decoded_imm_uj[2]),
        .I1(instr_jal),
        .I2(\mem_rdata_q_reg_n_0_[9] ),
        .I3(\mem_rdata_q_reg_n_0_[22] ),
        .I4(\decoded_imm[11]_i_2_n_0 ),
        .I5(\decoded_imm[11]_i_3_n_0 ),
        .O(decoded_imm[2]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[30]_i_1 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[30] ),
        .O(\decoded_imm[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EAAA0000)) 
    \decoded_imm[31]_i_1 
       (.I0(\decoded_imm[19]_i_2_n_0 ),
        .I1(instr_jal),
        .I2(decoded_imm_uj[31]),
        .I3(is_lui_auipc_jal_i_1_n_0),
        .I4(decoder_trigger_reg_n_0),
        .I5(decoder_pseudo_trigger_reg_n_0),
        .O(\decoded_imm[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \decoded_imm[31]_i_2 
       (.I0(instr_jal),
        .I1(instr_auipc),
        .I2(instr_lui),
        .I3(\mem_rdata_q_reg_n_0_[31] ),
        .O(\decoded_imm[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888FF88F8F8)) 
    \decoded_imm[3]_i_1 
       (.I0(decoded_imm_uj[3]),
        .I1(instr_jal),
        .I2(\mem_rdata_q_reg_n_0_[10] ),
        .I3(\mem_rdata_q_reg_n_0_[23] ),
        .I4(\decoded_imm[11]_i_2_n_0 ),
        .I5(\decoded_imm[11]_i_3_n_0 ),
        .O(decoded_imm[3]));
  LUT6 #(
    .INIT(64'h88888888FF88F8F8)) 
    \decoded_imm[4]_i_1 
       (.I0(decoded_imm_uj[4]),
        .I1(instr_jal),
        .I2(\mem_rdata_q_reg_n_0_[11] ),
        .I3(\mem_rdata_q_reg_n_0_[24] ),
        .I4(\decoded_imm[11]_i_2_n_0 ),
        .I5(\decoded_imm[11]_i_3_n_0 ),
        .O(decoded_imm[4]));
  LUT4 #(
    .INIT(16'hF444)) 
    \decoded_imm[5]_i_1 
       (.I0(\decoded_imm[11]_i_3_n_0 ),
        .I1(\mem_rdata_q_reg_n_0_[25] ),
        .I2(decoded_imm_uj[5]),
        .I3(instr_jal),
        .O(decoded_imm[5]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'hF444)) 
    \decoded_imm[6]_i_1 
       (.I0(\decoded_imm[11]_i_3_n_0 ),
        .I1(\mem_rdata_q_reg_n_0_[26] ),
        .I2(decoded_imm_uj[6]),
        .I3(instr_jal),
        .O(decoded_imm[6]));
  LUT4 #(
    .INIT(16'hF444)) 
    \decoded_imm[7]_i_1 
       (.I0(\decoded_imm[11]_i_3_n_0 ),
        .I1(\mem_rdata_q_reg_n_0_[27] ),
        .I2(decoded_imm_uj[7]),
        .I3(instr_jal),
        .O(decoded_imm[7]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'hF444)) 
    \decoded_imm[8]_i_1 
       (.I0(\decoded_imm[11]_i_3_n_0 ),
        .I1(\mem_rdata_q_reg_n_0_[28] ),
        .I2(decoded_imm_uj[8]),
        .I3(instr_jal),
        .O(decoded_imm[8]));
  LUT4 #(
    .INIT(16'hF444)) 
    \decoded_imm[9]_i_1 
       (.I0(\decoded_imm[11]_i_3_n_0 ),
        .I1(\mem_rdata_q_reg_n_0_[29] ),
        .I2(decoded_imm_uj[9]),
        .I3(instr_jal),
        .O(decoded_imm[9]));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[0]),
        .Q(\decoded_imm_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[10]),
        .Q(\decoded_imm_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[11]),
        .Q(\decoded_imm_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[12]),
        .Q(\decoded_imm_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[13]),
        .Q(\decoded_imm_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[14]),
        .Q(\decoded_imm_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[15]),
        .Q(\decoded_imm_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[16]),
        .Q(\decoded_imm_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[17]),
        .Q(\decoded_imm_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[18]),
        .Q(\decoded_imm_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[19]),
        .Q(\decoded_imm_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[1]),
        .Q(\decoded_imm_reg_n_0_[1] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[20]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[20] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[21]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[21] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[22]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[22] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[23]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[23] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[24]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[24] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[25]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[25] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[26]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[26] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[27]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[27] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[28]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[28] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[29]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[29] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[2]),
        .Q(\decoded_imm_reg_n_0_[2] ),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[30]_i_1_n_0 ),
        .Q(\decoded_imm_reg_n_0_[30] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \decoded_imm_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(\decoded_imm[31]_i_2_n_0 ),
        .Q(\decoded_imm_reg_n_0_[31] ),
        .S(\decoded_imm[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[3]),
        .Q(\decoded_imm_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[4]),
        .Q(\decoded_imm_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[5]),
        .Q(\decoded_imm_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[6]),
        .Q(\decoded_imm_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[7]),
        .Q(\decoded_imm_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[8]),
        .Q(\decoded_imm_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(decoded_imm[9]),
        .Q(\decoded_imm_reg_n_0_[9] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[10]_i_1 
       (.I0(mem_rdata[30]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[30] ),
        .O(\decoded_imm_uj[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[12]_i_1 
       (.I0(mem_rdata[12]),
        .I1(mem_valid_reg_n_0),
        .I2(p_0_in_1[0]),
        .O(\decoded_imm_uj[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[13]_i_1 
       (.I0(mem_rdata[13]),
        .I1(mem_valid_reg_n_0),
        .I2(p_0_in_1[1]),
        .O(\decoded_imm_uj[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[14]_i_1 
       (.I0(mem_rdata[14]),
        .I1(mem_valid_reg_n_0),
        .I2(p_0_in_1[2]),
        .O(\decoded_imm_uj[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[31]_i_1 
       (.I0(mem_rdata[31]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[31] ),
        .O(p_0_in0));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[5]_i_1 
       (.I0(mem_rdata[25]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[25] ),
        .O(\decoded_imm_uj[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[6]_i_1 
       (.I0(mem_rdata[26]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[26] ),
        .O(\decoded_imm_uj[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[7]_i_1 
       (.I0(mem_rdata[27]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[27] ),
        .O(\decoded_imm_uj[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[8]_i_1 
       (.I0(mem_rdata[28]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[28] ),
        .O(\decoded_imm_uj[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_imm_uj[9]_i_1 
       (.I0(mem_rdata[29]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[29] ),
        .O(\decoded_imm_uj[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_imm_uj[10]_i_1_n_0 ),
        .Q(decoded_imm_uj[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[0]),
        .Q(decoded_imm_uj[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_imm_uj[12]_i_1_n_0 ),
        .Q(decoded_imm_uj[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_imm_uj[13]_i_1_n_0 ),
        .Q(decoded_imm_uj[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_imm_uj[14]_i_1_n_0 ),
        .Q(decoded_imm_uj[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[1]),
        .Q(decoded_imm_uj[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[2]),
        .Q(decoded_imm_uj[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_0_in0),
        .Q(decoded_imm_uj[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[3]),
        .Q(decoded_imm_uj[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[4]),
        .Q(decoded_imm_uj[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_imm_uj[5]_i_1_n_0 ),
        .Q(decoded_imm_uj[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_imm_uj[6]_i_1_n_0 ),
        .Q(decoded_imm_uj[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_imm_uj[7]_i_1_n_0 ),
        .Q(decoded_imm_uj[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_imm_uj[8]_i_1_n_0 ),
        .Q(decoded_imm_uj[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_imm_uj_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_imm_uj[9]_i_1_n_0 ),
        .Q(decoded_imm_uj[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rd[0]_i_1 
       (.I0(mem_rdata[7]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[7] ),
        .O(\decoded_rd[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rd[1]_i_1 
       (.I0(mem_rdata[8]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[8] ),
        .O(\decoded_rd[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rd[2]_i_1 
       (.I0(mem_rdata[9]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[9] ),
        .O(\decoded_rd[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rd[3]_i_1 
       (.I0(mem_rdata[10]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[10] ),
        .O(\decoded_rd[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rd[4]_i_1 
       (.I0(mem_rdata[11]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[11] ),
        .O(\decoded_rd[4]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rd_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rd[0]_i_1_n_0 ),
        .Q(decoded_rd[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rd_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rd[1]_i_1_n_0 ),
        .Q(decoded_rd[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rd_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rd[2]_i_1_n_0 ),
        .Q(decoded_rd[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rd_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rd[3]_i_1_n_0 ),
        .Q(decoded_rd[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rd_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rd[4]_i_1_n_0 ),
        .Q(decoded_rd[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[0]_i_1_n_0 ),
        .Q(decoded_rs1_0[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[1]_i_1_n_0 ),
        .Q(decoded_rs1_0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[2]_i_1_n_0 ),
        .Q(decoded_rs1_0[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[3]_i_1_n_0 ),
        .Q(decoded_rs1_0[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[4]_i_1_n_0 ),
        .Q(decoded_rs1_0[4]),
        .R(1'b0));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg_rep[0] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[0]_i_1_n_0 ),
        .Q(decoded_rs1[0]),
        .R(1'b0));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg_rep[1] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[1]_i_1_n_0 ),
        .Q(decoded_rs1[1]),
        .R(1'b0));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg_rep[2] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[2]_i_1_n_0 ),
        .Q(decoded_rs1[2]),
        .R(1'b0));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg_rep[3] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[3]_i_1_n_0 ),
        .Q(decoded_rs1[3]),
        .R(1'b0));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs1_reg_rep[4] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(\decoded_rs1_rep[4]_i_1_n_0 ),
        .Q(decoded_rs1[4]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs1_rep[0]_i_1 
       (.I0(mem_rdata[15]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[15] ),
        .O(\decoded_rs1_rep[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs1_rep[1]_i_1 
       (.I0(mem_rdata[16]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[16] ),
        .O(\decoded_rs1_rep[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs1_rep[2]_i_1 
       (.I0(mem_rdata[17]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[17] ),
        .O(\decoded_rs1_rep[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs1_rep[3]_i_1 
       (.I0(mem_rdata[18]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[18] ),
        .O(\decoded_rs1_rep[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs1_rep[4]_i_1 
       (.I0(mem_rdata[19]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[19] ),
        .O(\decoded_rs1_rep[4]_i_1_n_0 ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs2_reg_rep[0] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[0]),
        .Q(decoded_rs2[0]),
        .R(1'b0));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs2_reg_rep[1] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[1]),
        .Q(decoded_rs2[1]),
        .R(1'b0));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs2_reg_rep[2] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[2]),
        .Q(decoded_rs2[2]),
        .R(1'b0));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs2_reg_rep[3] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[3]),
        .Q(decoded_rs2[3]),
        .R(1'b0));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \decoded_rs2_reg_rep[4] 
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(p_1_in[4]),
        .Q(decoded_rs2[4]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs2_rep[0]_i_1 
       (.I0(mem_rdata[20]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[20] ),
        .O(p_1_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs2_rep[1]_i_1 
       (.I0(mem_rdata[21]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[21] ),
        .O(p_1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs2_rep[2]_i_1 
       (.I0(mem_rdata[22]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[22] ),
        .O(p_1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs2_rep[3]_i_1 
       (.I0(mem_rdata[23]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[23] ),
        .O(p_1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \decoded_rs2_rep[4]_i_1 
       (.I0(mem_rdata[24]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[24] ),
        .O(p_1_in[4]));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    decoder_pseudo_trigger_i_1
       (.I0(decoder_pseudo_trigger_i_2_n_0),
        .I1(\cpu_state_reg_n_0_[7] ),
        .I2(\cpu_state_reg_n_0_[3] ),
        .I3(reg_next_pc),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(decoder_trigger_i_2_n_0),
        .O(decoder_pseudo_trigger));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'hE)) 
    decoder_pseudo_trigger_i_2
       (.I0(\cpu_state_reg_n_0_[5] ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .O(decoder_pseudo_trigger_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    decoder_pseudo_trigger_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(decoder_pseudo_trigger),
        .Q(decoder_pseudo_trigger_reg_n_0),
        .R(instr_and_i_1_n_0));
  LUT6 #(
    .INIT(64'hD0D0D0C0D0D0D000)) 
    decoder_trigger_i_1
       (.I0(mem_do_prefetch_reg_n_0),
        .I1(mem_do_rinst_reg_n_0),
        .I2(decoder_trigger_i_2_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\cpu_state_reg_n_0_[1] ),
        .I5(decoder_trigger_i_3_n_0),
        .O(decoder_trigger_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT2 #(
    .INIT(4'h2)) 
    decoder_trigger_i_2
       (.I0(resetn_IBUF),
        .I1(instr_jal_i_3_n_0),
        .O(decoder_trigger_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'h777F)) 
    decoder_trigger_i_3
       (.I0(\cpu_state_reg_n_0_[3] ),
        .I1(is_beq_bne_blt_bge_bltu_bgeu),
        .I2(decoder_trigger_i_4_n_0),
        .I3(decoder_trigger_i_5_n_0),
        .O(decoder_trigger_i_3_n_0));
  LUT6 #(
    .INIT(64'h000055550000F088)) 
    decoder_trigger_i_4
       (.I0(data5),
        .I1(is_sltiu_bltu_sltu),
        .I2(data4),
        .I3(is_slti_blt_slt),
        .I4(\cpu_state[7]_i_9_n_0 ),
        .I5(instr_bgeu),
        .O(decoder_trigger_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hCCDC2232)) 
    decoder_trigger_i_5
       (.I0(instr_bne),
        .I1(instr_beq),
        .I2(instr_bge),
        .I3(data4),
        .I4(alu_out_00_carry__1_n_1),
        .O(decoder_trigger_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    decoder_trigger_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(decoder_trigger_i_1_n_0),
        .Q(decoder_trigger_reg_n_0),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__0_i_1
       (.I0(Q[7]),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[7] ),
        .O(i___29_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__0_i_2
       (.I0(Q[6]),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[6] ),
        .O(i___29_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__0_i_3
       (.I0(Q[5]),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[5] ),
        .O(i___29_carry__0_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__0_i_4
       (.I0(Q[4]),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[4] ),
        .O(i___29_carry__0_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__1_i_1
       (.I0(\reg_op2_reg_n_0_[11] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[11] ),
        .O(i___29_carry__1_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__1_i_2
       (.I0(\reg_op2_reg_n_0_[10] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[10] ),
        .O(i___29_carry__1_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__1_i_3
       (.I0(\reg_op2_reg_n_0_[9] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[9] ),
        .O(i___29_carry__1_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__1_i_4
       (.I0(\reg_op2_reg_n_0_[8] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[8] ),
        .O(i___29_carry__1_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__2_i_1
       (.I0(\reg_op2_reg_n_0_[15] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[15] ),
        .O(i___29_carry__2_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__2_i_2
       (.I0(\reg_op2_reg_n_0_[14] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[14] ),
        .O(i___29_carry__2_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__2_i_3
       (.I0(\reg_op2_reg_n_0_[13] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[13] ),
        .O(i___29_carry__2_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__2_i_4
       (.I0(\reg_op2_reg_n_0_[12] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[12] ),
        .O(i___29_carry__2_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__3_i_1
       (.I0(\reg_op2_reg_n_0_[19] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[19] ),
        .O(i___29_carry__3_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__3_i_2
       (.I0(\reg_op2_reg_n_0_[18] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[18] ),
        .O(i___29_carry__3_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__3_i_3
       (.I0(\reg_op2_reg_n_0_[17] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[17] ),
        .O(i___29_carry__3_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__3_i_4
       (.I0(\reg_op2_reg_n_0_[16] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[16] ),
        .O(i___29_carry__3_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__4_i_1
       (.I0(\reg_op2_reg_n_0_[23] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[23] ),
        .O(i___29_carry__4_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__4_i_2
       (.I0(\reg_op2_reg_n_0_[22] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[22] ),
        .O(i___29_carry__4_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__4_i_3
       (.I0(\reg_op2_reg_n_0_[21] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[21] ),
        .O(i___29_carry__4_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__4_i_4
       (.I0(\reg_op2_reg_n_0_[20] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[20] ),
        .O(i___29_carry__4_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__5_i_1
       (.I0(\reg_op2_reg_n_0_[27] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[27] ),
        .O(i___29_carry__5_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__5_i_2
       (.I0(\reg_op2_reg_n_0_[26] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[26] ),
        .O(i___29_carry__5_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__5_i_3
       (.I0(\reg_op2_reg_n_0_[25] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[25] ),
        .O(i___29_carry__5_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__5_i_4
       (.I0(\reg_op2_reg_n_0_[24] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[24] ),
        .O(i___29_carry__5_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__6_i_1
       (.I0(instr_sub),
        .I1(\reg_op1_reg_n_0_[31] ),
        .I2(\reg_op2_reg_n_0_[31] ),
        .O(i___29_carry__6_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__6_i_2
       (.I0(instr_sub),
        .I1(\reg_op2_reg_n_0_[30] ),
        .I2(\reg_op1_reg_n_0_[30] ),
        .O(i___29_carry__6_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__6_i_3
       (.I0(\reg_op2_reg_n_0_[29] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[29] ),
        .O(i___29_carry__6_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry__6_i_4
       (.I0(\reg_op2_reg_n_0_[28] ),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[28] ),
        .O(i___29_carry__6_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry_i_1
       (.I0(Q[3]),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[3] ),
        .O(i___29_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry_i_2
       (.I0(Q[2]),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[2] ),
        .O(i___29_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___29_carry_i_3
       (.I0(Q[1]),
        .I1(instr_sub),
        .I2(\reg_op1_reg_n_0_[1] ),
        .O(i___29_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    i___29_carry_i_4
       (.I0(Q[0]),
        .O(i___29_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_1
       (.I0(\reg_op2_reg_n_0_[14] ),
        .I1(\reg_op1_reg_n_0_[14] ),
        .I2(\reg_op1_reg_n_0_[15] ),
        .I3(\reg_op2_reg_n_0_[15] ),
        .O(i__carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_1__0
       (.I0(\reg_op1_reg_n_0_[7] ),
        .I1(\decoded_imm_reg_n_0_[7] ),
        .O(i__carry__0_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_2
       (.I0(\reg_op2_reg_n_0_[12] ),
        .I1(\reg_op1_reg_n_0_[12] ),
        .I2(\reg_op1_reg_n_0_[13] ),
        .I3(\reg_op2_reg_n_0_[13] ),
        .O(i__carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_2__0
       (.I0(\reg_op1_reg_n_0_[6] ),
        .I1(\decoded_imm_reg_n_0_[6] ),
        .O(i__carry__0_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3
       (.I0(\reg_op2_reg_n_0_[11] ),
        .I1(\reg_op1_reg_n_0_[11] ),
        .I2(\reg_op2_reg_n_0_[10] ),
        .I3(\reg_op1_reg_n_0_[10] ),
        .O(i__carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_3__0
       (.I0(\reg_op1_reg_n_0_[5] ),
        .I1(\decoded_imm_reg_n_0_[5] ),
        .O(i__carry__0_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_4
       (.I0(\reg_op2_reg_n_0_[8] ),
        .I1(\reg_op1_reg_n_0_[8] ),
        .I2(\reg_op1_reg_n_0_[9] ),
        .I3(\reg_op2_reg_n_0_[9] ),
        .O(i__carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_4__0
       (.I0(\reg_op1_reg_n_0_[4] ),
        .I1(\decoded_imm_reg_n_0_[4] ),
        .O(i__carry__0_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5
       (.I0(\reg_op2_reg_n_0_[14] ),
        .I1(\reg_op1_reg_n_0_[14] ),
        .I2(\reg_op2_reg_n_0_[15] ),
        .I3(\reg_op1_reg_n_0_[15] ),
        .O(i__carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6
       (.I0(\reg_op2_reg_n_0_[12] ),
        .I1(\reg_op1_reg_n_0_[12] ),
        .I2(\reg_op2_reg_n_0_[13] ),
        .I3(\reg_op1_reg_n_0_[13] ),
        .O(i__carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_7
       (.I0(\reg_op2_reg_n_0_[10] ),
        .I1(\reg_op1_reg_n_0_[10] ),
        .I2(\reg_op2_reg_n_0_[11] ),
        .I3(\reg_op1_reg_n_0_[11] ),
        .O(i__carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_8
       (.I0(\reg_op2_reg_n_0_[8] ),
        .I1(\reg_op1_reg_n_0_[8] ),
        .I2(\reg_op2_reg_n_0_[9] ),
        .I3(\reg_op1_reg_n_0_[9] ),
        .O(i__carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__1_i_1
       (.I0(\reg_op2_reg_n_0_[23] ),
        .I1(\reg_op1_reg_n_0_[23] ),
        .I2(\reg_op2_reg_n_0_[22] ),
        .I3(\reg_op1_reg_n_0_[22] ),
        .O(i__carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__1_i_1__0
       (.I0(\reg_op1_reg_n_0_[11] ),
        .I1(\decoded_imm_reg_n_0_[11] ),
        .O(i__carry__1_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__1_i_2
       (.I0(\reg_op2_reg_n_0_[20] ),
        .I1(\reg_op1_reg_n_0_[20] ),
        .I2(\reg_op1_reg_n_0_[21] ),
        .I3(\reg_op2_reg_n_0_[21] ),
        .O(i__carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__1_i_2__0
       (.I0(\reg_op1_reg_n_0_[10] ),
        .I1(\decoded_imm_reg_n_0_[10] ),
        .O(i__carry__1_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__1_i_3
       (.I0(\reg_op2_reg_n_0_[18] ),
        .I1(\reg_op1_reg_n_0_[18] ),
        .I2(\reg_op1_reg_n_0_[19] ),
        .I3(\reg_op2_reg_n_0_[19] ),
        .O(i__carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__1_i_3__0
       (.I0(\reg_op1_reg_n_0_[9] ),
        .I1(\decoded_imm_reg_n_0_[9] ),
        .O(i__carry__1_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__1_i_4
       (.I0(\reg_op2_reg_n_0_[17] ),
        .I1(\reg_op1_reg_n_0_[17] ),
        .I2(\reg_op2_reg_n_0_[16] ),
        .I3(\reg_op1_reg_n_0_[16] ),
        .O(i__carry__1_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__1_i_4__0
       (.I0(\reg_op1_reg_n_0_[8] ),
        .I1(\decoded_imm_reg_n_0_[8] ),
        .O(i__carry__1_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__1_i_5
       (.I0(\reg_op2_reg_n_0_[22] ),
        .I1(\reg_op1_reg_n_0_[22] ),
        .I2(\reg_op2_reg_n_0_[23] ),
        .I3(\reg_op1_reg_n_0_[23] ),
        .O(i__carry__1_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__1_i_6
       (.I0(\reg_op2_reg_n_0_[20] ),
        .I1(\reg_op1_reg_n_0_[20] ),
        .I2(\reg_op2_reg_n_0_[21] ),
        .I3(\reg_op1_reg_n_0_[21] ),
        .O(i__carry__1_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__1_i_7
       (.I0(\reg_op2_reg_n_0_[18] ),
        .I1(\reg_op1_reg_n_0_[18] ),
        .I2(\reg_op2_reg_n_0_[19] ),
        .I3(\reg_op1_reg_n_0_[19] ),
        .O(i__carry__1_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__1_i_8
       (.I0(\reg_op2_reg_n_0_[16] ),
        .I1(\reg_op1_reg_n_0_[16] ),
        .I2(\reg_op2_reg_n_0_[17] ),
        .I3(\reg_op1_reg_n_0_[17] ),
        .O(i__carry__1_i_8_n_0));
  LUT4 #(
    .INIT(16'h20F2)) 
    i__carry__2_i_1
       (.I0(\reg_op2_reg_n_0_[30] ),
        .I1(\reg_op1_reg_n_0_[30] ),
        .I2(\reg_op2_reg_n_0_[31] ),
        .I3(\reg_op1_reg_n_0_[31] ),
        .O(i__carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__2_i_1__0
       (.I0(\reg_op1_reg_n_0_[15] ),
        .I1(\decoded_imm_reg_n_0_[15] ),
        .O(i__carry__2_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__2_i_2
       (.I0(\reg_op2_reg_n_0_[29] ),
        .I1(\reg_op1_reg_n_0_[29] ),
        .I2(\reg_op2_reg_n_0_[28] ),
        .I3(\reg_op1_reg_n_0_[28] ),
        .O(i__carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__2_i_2__0
       (.I0(\reg_op1_reg_n_0_[14] ),
        .I1(\decoded_imm_reg_n_0_[14] ),
        .O(i__carry__2_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__2_i_3
       (.I0(\reg_op2_reg_n_0_[26] ),
        .I1(\reg_op1_reg_n_0_[26] ),
        .I2(\reg_op1_reg_n_0_[27] ),
        .I3(\reg_op2_reg_n_0_[27] ),
        .O(i__carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__2_i_3__0
       (.I0(\reg_op1_reg_n_0_[13] ),
        .I1(\decoded_imm_reg_n_0_[13] ),
        .O(i__carry__2_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__2_i_4
       (.I0(\reg_op2_reg_n_0_[24] ),
        .I1(\reg_op1_reg_n_0_[24] ),
        .I2(\reg_op1_reg_n_0_[25] ),
        .I3(\reg_op2_reg_n_0_[25] ),
        .O(i__carry__2_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__2_i_4__0
       (.I0(\reg_op1_reg_n_0_[12] ),
        .I1(\decoded_imm_reg_n_0_[12] ),
        .O(i__carry__2_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__2_i_5
       (.I0(\reg_op1_reg_n_0_[31] ),
        .I1(\reg_op2_reg_n_0_[31] ),
        .I2(\reg_op1_reg_n_0_[30] ),
        .I3(\reg_op2_reg_n_0_[30] ),
        .O(i__carry__2_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__2_i_6
       (.I0(\reg_op2_reg_n_0_[28] ),
        .I1(\reg_op1_reg_n_0_[28] ),
        .I2(\reg_op2_reg_n_0_[29] ),
        .I3(\reg_op1_reg_n_0_[29] ),
        .O(i__carry__2_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__2_i_7
       (.I0(\reg_op2_reg_n_0_[26] ),
        .I1(\reg_op1_reg_n_0_[26] ),
        .I2(\reg_op2_reg_n_0_[27] ),
        .I3(\reg_op1_reg_n_0_[27] ),
        .O(i__carry__2_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__2_i_8
       (.I0(\reg_op2_reg_n_0_[24] ),
        .I1(\reg_op1_reg_n_0_[24] ),
        .I2(\reg_op2_reg_n_0_[25] ),
        .I3(\reg_op1_reg_n_0_[25] ),
        .O(i__carry__2_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__3_i_1
       (.I0(\reg_op1_reg_n_0_[19] ),
        .I1(\decoded_imm_reg_n_0_[19] ),
        .O(i__carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__3_i_2
       (.I0(\reg_op1_reg_n_0_[18] ),
        .I1(\decoded_imm_reg_n_0_[18] ),
        .O(i__carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__3_i_3
       (.I0(\reg_op1_reg_n_0_[17] ),
        .I1(\decoded_imm_reg_n_0_[17] ),
        .O(i__carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__3_i_4
       (.I0(\reg_op1_reg_n_0_[16] ),
        .I1(\decoded_imm_reg_n_0_[16] ),
        .O(i__carry__3_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__4_i_1
       (.I0(\reg_op1_reg_n_0_[23] ),
        .I1(\decoded_imm_reg_n_0_[23] ),
        .O(i__carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__4_i_2
       (.I0(\reg_op1_reg_n_0_[22] ),
        .I1(\decoded_imm_reg_n_0_[22] ),
        .O(i__carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__4_i_3
       (.I0(\reg_op1_reg_n_0_[21] ),
        .I1(\decoded_imm_reg_n_0_[21] ),
        .O(i__carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__4_i_4
       (.I0(\reg_op1_reg_n_0_[20] ),
        .I1(\decoded_imm_reg_n_0_[20] ),
        .O(i__carry__4_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__5_i_1
       (.I0(\reg_op1_reg_n_0_[27] ),
        .I1(\decoded_imm_reg_n_0_[27] ),
        .O(i__carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__5_i_2
       (.I0(\reg_op1_reg_n_0_[26] ),
        .I1(\decoded_imm_reg_n_0_[26] ),
        .O(i__carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__5_i_3
       (.I0(\reg_op1_reg_n_0_[25] ),
        .I1(\decoded_imm_reg_n_0_[25] ),
        .O(i__carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__5_i_4
       (.I0(\reg_op1_reg_n_0_[24] ),
        .I1(\decoded_imm_reg_n_0_[24] ),
        .O(i__carry__5_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__6_i_1
       (.I0(\decoded_imm_reg_n_0_[31] ),
        .I1(\reg_op1_reg_n_0_[31] ),
        .O(i__carry__6_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__6_i_2
       (.I0(\reg_op1_reg_n_0_[30] ),
        .I1(\decoded_imm_reg_n_0_[30] ),
        .O(i__carry__6_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__6_i_3
       (.I0(\reg_op1_reg_n_0_[29] ),
        .I1(\decoded_imm_reg_n_0_[29] ),
        .O(i__carry__6_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__6_i_4
       (.I0(\reg_op1_reg_n_0_[28] ),
        .I1(\decoded_imm_reg_n_0_[28] ),
        .O(i__carry__6_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_1
       (.I0(Q[6]),
        .I1(\reg_op1_reg_n_0_[6] ),
        .I2(\reg_op1_reg_n_0_[7] ),
        .I3(Q[7]),
        .O(i__carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_1__0
       (.I0(\reg_op1_reg_n_0_[3] ),
        .I1(\decoded_imm_reg_n_0_[3] ),
        .O(i__carry_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2
       (.I0(Q[5]),
        .I1(\reg_op1_reg_n_0_[5] ),
        .I2(Q[4]),
        .I3(\reg_op1_reg_n_0_[4] ),
        .O(i__carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_2__0
       (.I0(\reg_op1_reg_n_0_[2] ),
        .I1(\decoded_imm_reg_n_0_[2] ),
        .O(i__carry_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_3
       (.I0(Q[2]),
        .I1(\reg_op1_reg_n_0_[2] ),
        .I2(\reg_op1_reg_n_0_[3] ),
        .I3(Q[3]),
        .O(i__carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_3__0
       (.I0(\reg_op1_reg_n_0_[1] ),
        .I1(\decoded_imm_reg_n_0_[1] ),
        .O(i__carry_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_4
       (.I0(Q[0]),
        .I1(\reg_op1_reg_n_0_[0] ),
        .I2(\reg_op1_reg_n_0_[1] ),
        .I3(Q[1]),
        .O(i__carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_4__0
       (.I0(\reg_op1_reg_n_0_[0] ),
        .I1(\decoded_imm_reg_n_0_[0] ),
        .O(i__carry_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5
       (.I0(Q[6]),
        .I1(\reg_op1_reg_n_0_[6] ),
        .I2(Q[7]),
        .I3(\reg_op1_reg_n_0_[7] ),
        .O(i__carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(Q[4]),
        .I1(\reg_op1_reg_n_0_[4] ),
        .I2(Q[5]),
        .I3(\reg_op1_reg_n_0_[5] ),
        .O(i__carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(Q[2]),
        .I1(\reg_op1_reg_n_0_[2] ),
        .I2(Q[3]),
        .I3(\reg_op1_reg_n_0_[3] ),
        .O(i__carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8
       (.I0(Q[0]),
        .I1(\reg_op1_reg_n_0_[0] ),
        .I2(Q[1]),
        .I3(\reg_op1_reg_n_0_[1] ),
        .O(i__carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    instr_add_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(instr_and_i_3_n_0),
        .O(instr_add0));
  FDRE #(
    .INIT(1'b0)) 
    instr_add_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_add0),
        .Q(instr_add),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    instr_addi_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(is_alu_reg_imm),
        .O(instr_addi0));
  FDRE #(
    .INIT(1'b0)) 
    instr_addi_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_addi0),
        .Q(instr_addi),
        .R(instr_and_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    instr_and_i_1
       (.I0(resetn_IBUF),
        .O(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    instr_and_i_2
       (.I0(p_0_in_1[2]),
        .I1(p_0_in_1[1]),
        .I2(p_0_in_1[0]),
        .I3(instr_and_i_3_n_0),
        .O(instr_and0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    instr_and_i_3
       (.I0(is_alu_reg_reg),
        .I1(instr_and_i_4_n_0),
        .I2(\mem_rdata_q_reg_n_0_[27] ),
        .I3(\mem_rdata_q_reg_n_0_[30] ),
        .I4(\mem_rdata_q_reg_n_0_[29] ),
        .I5(\mem_rdata_q_reg_n_0_[28] ),
        .O(instr_and_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    instr_and_i_4
       (.I0(\mem_rdata_q_reg_n_0_[26] ),
        .I1(\mem_rdata_q_reg_n_0_[25] ),
        .I2(\mem_rdata_q_reg_n_0_[31] ),
        .O(instr_and_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_and_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_and0),
        .Q(instr_and),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    instr_andi_i_1
       (.I0(p_0_in_1[2]),
        .I1(is_alu_reg_imm),
        .I2(p_0_in_1[0]),
        .I3(p_0_in_1[1]),
        .O(instr_andi0));
  FDRE #(
    .INIT(1'b0)) 
    instr_andi_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_andi0),
        .Q(instr_andi),
        .R(instr_and_i_1_n_0));
  LUT6 #(
    .INIT(64'h0800088800000000)) 
    instr_auipc_i_1
       (.I0(\mem_rdata_q[2]_i_1_n_0 ),
        .I1(is_alu_reg_imm_i_1_n_0),
        .I2(mem_rdata[6]),
        .I3(mem_valid_reg_n_0),
        .I4(\mem_rdata_q_reg_n_0_[6] ),
        .I5(instr_auipc_i_2_n_0),
        .O(instr_auipc_i_1_n_0));
  LUT6 #(
    .INIT(64'h00000000B8308800)) 
    instr_auipc_i_2
       (.I0(mem_rdata[1]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[1] ),
        .I3(mem_rdata[0]),
        .I4(\mem_rdata_q_reg_n_0_[0] ),
        .I5(\mem_rdata_q[3]_i_1_n_0 ),
        .O(instr_auipc_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_auipc_reg
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(instr_auipc_i_1_n_0),
        .Q(instr_auipc),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    instr_beq_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(is_beq_bne_blt_bge_bltu_bgeu),
        .O(instr_beq0));
  FDRE #(
    .INIT(1'b0)) 
    instr_beq_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_beq0),
        .Q(instr_beq),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    instr_bge_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[0]),
        .I2(p_0_in_1[2]),
        .I3(is_beq_bne_blt_bge_bltu_bgeu),
        .O(instr_bge0));
  FDRE #(
    .INIT(1'b0)) 
    instr_bge_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_bge0),
        .Q(instr_bge),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    instr_bgeu_i_1
       (.I0(is_beq_bne_blt_bge_bltu_bgeu),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(p_0_in_1[1]),
        .O(instr_bgeu0));
  FDRE #(
    .INIT(1'b0)) 
    instr_bgeu_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_bgeu0),
        .Q(instr_bgeu),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    instr_blt_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(is_beq_bne_blt_bge_bltu_bgeu),
        .O(instr_blt0));
  FDRE #(
    .INIT(1'b0)) 
    instr_blt_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_blt0),
        .Q(instr_blt),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    instr_bltu_i_1
       (.I0(is_beq_bne_blt_bge_bltu_bgeu),
        .I1(p_0_in_1[1]),
        .I2(p_0_in_1[0]),
        .I3(p_0_in_1[2]),
        .O(instr_bltu0));
  FDRE #(
    .INIT(1'b0)) 
    instr_bltu_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_bltu0),
        .Q(instr_bltu),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    instr_bne_i_1
       (.I0(p_0_in_1[2]),
        .I1(p_0_in_1[1]),
        .I2(p_0_in_1[0]),
        .I3(is_beq_bne_blt_bge_bltu_bgeu),
        .O(instr_bne0));
  FDRE #(
    .INIT(1'b0)) 
    instr_bne_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_bne0),
        .Q(instr_bne),
        .R(instr_and_i_1_n_0));
  LUT3 #(
    .INIT(8'h40)) 
    instr_jal_i_1
       (.I0(instr_jal_i_3_n_0),
        .I1(resetn_IBUF),
        .I2(mem_do_rinst_reg_n_0),
        .O(instr_lui0));
  LUT5 #(
    .INIT(32'h80000000)) 
    instr_jal_i_2
       (.I0(instr_jal_i_4_n_0),
        .I1(is_sb_sh_sw_i_2_n_0),
        .I2(\mem_rdata_q[2]_i_1_n_0 ),
        .I3(\mem_rdata_q[3]_i_1_n_0 ),
        .I4(\mem_rdata_q[6]_i_1_n_0 ),
        .O(instr_jal_i_2_n_0));
  LUT6 #(
    .INIT(64'h001F0F1F0F1FFFFF)) 
    instr_jal_i_3
       (.I0(mem_do_rdata),
        .I1(mem_do_wdata),
        .I2(mem_valid_reg_n_0),
        .I3(mem_do_rinst_reg_n_0),
        .I4(\mem_state_reg_n_0_[0] ),
        .I5(\mem_state_reg_n_0_[1] ),
        .O(instr_jal_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hCCA000A0)) 
    instr_jal_i_4
       (.I0(\mem_rdata_q_reg_n_0_[0] ),
        .I1(mem_rdata[0]),
        .I2(\mem_rdata_q_reg_n_0_[1] ),
        .I3(mem_valid_reg_n_0),
        .I4(mem_rdata[1]),
        .O(instr_jal_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_jal_reg
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(instr_jal_i_2_n_0),
        .Q(instr_jal),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h80000000)) 
    instr_jalr_i_1
       (.I0(instr_auipc_i_2_n_0),
        .I1(\mem_rdata_q[6]_i_1_n_0 ),
        .I2(\mem_rdata_q[2]_i_1_n_0 ),
        .I3(is_sb_sh_sw_i_2_n_0),
        .I4(instr_jalr_i_2_n_0),
        .O(instr_jalr0));
  LUT5 #(
    .INIT(32'h01FF0100)) 
    instr_jalr_i_2
       (.I0(mem_rdata[13]),
        .I1(mem_rdata[14]),
        .I2(mem_rdata[12]),
        .I3(mem_valid_reg_n_0),
        .I4(instr_lb_i_1_n_0),
        .O(instr_jalr_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_jalr_reg
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(instr_jalr0),
        .Q(instr_jalr),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h01)) 
    instr_lb_i_1
       (.I0(p_0_in_1[0]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[1]),
        .O(instr_lb_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_lb_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_lb_i_1_n_0),
        .Q(instr_lb),
        .R(instr_lhu_i_1_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    instr_lbu_i_1
       (.I0(p_0_in_1[0]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[1]),
        .O(instr_lbu_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_lbu_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_lbu_i_1_n_0),
        .Q(instr_lbu),
        .R(instr_lhu_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'h02)) 
    instr_lh_i_1
       (.I0(p_0_in_1[0]),
        .I1(p_0_in_1[1]),
        .I2(p_0_in_1[2]),
        .O(instr_lh_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_lh_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_lh_i_1_n_0),
        .Q(instr_lh),
        .R(instr_lhu_i_1_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    instr_lhu_i_1
       (.I0(is_lb_lh_lw_lbu_lhu),
        .I1(decoder_trigger_reg_n_0),
        .I2(decoder_pseudo_trigger_reg_n_0),
        .O(instr_lhu_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'h08)) 
    instr_lhu_i_2
       (.I0(p_0_in_1[2]),
        .I1(p_0_in_1[0]),
        .I2(p_0_in_1[1]),
        .O(instr_lhu_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_lhu_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_lhu_i_2_n_0),
        .Q(instr_lhu),
        .R(instr_lhu_i_1_n_0));
  LUT6 #(
    .INIT(64'h0800088800000000)) 
    instr_lui_i_1
       (.I0(\mem_rdata_q[2]_i_1_n_0 ),
        .I1(is_alu_reg_reg_i_1_n_0),
        .I2(mem_rdata[6]),
        .I3(mem_valid_reg_n_0),
        .I4(\mem_rdata_q_reg_n_0_[6] ),
        .I5(instr_auipc_i_2_n_0),
        .O(instr_lui_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_lui_reg
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(instr_lui_i_1_n_0),
        .Q(instr_lui),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h10)) 
    instr_lw_i_1
       (.I0(p_0_in_1[0]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[1]),
        .O(instr_lw_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_lw_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_lw_i_1_n_0),
        .Q(instr_lw),
        .R(instr_lhu_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    instr_or_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(instr_and_i_3_n_0),
        .O(instr_or0));
  FDRE #(
    .INIT(1'b0)) 
    instr_or_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_or0),
        .Q(instr_or),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    instr_ori_i_1
       (.I0(p_0_in_1[1]),
        .I1(is_alu_reg_imm),
        .I2(p_0_in_1[0]),
        .I3(p_0_in_1[2]),
        .O(instr_ori0));
  FDRE #(
    .INIT(1'b0)) 
    instr_ori_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_ori0),
        .Q(instr_ori),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h00000004)) 
    instr_rdcycle_i_1
       (.I0(\mem_rdata_q_reg_n_0_[27] ),
        .I1(\mem_rdata_q_reg_n_0_[30] ),
        .I2(\mem_rdata_q_reg_n_0_[29] ),
        .I3(\mem_rdata_q_reg_n_0_[28] ),
        .I4(\mem_rdata_q_reg_n_0_[21] ),
        .O(instr_rdcycle_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_rdcycle_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_rdcycle_i_1_n_0),
        .Q(instr_rdcycle),
        .R(instr_rdinstrh_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h00040000)) 
    instr_rdcycleh_i_1
       (.I0(\mem_rdata_q_reg_n_0_[21] ),
        .I1(\mem_rdata_q_reg_n_0_[27] ),
        .I2(\mem_rdata_q_reg_n_0_[28] ),
        .I3(\mem_rdata_q_reg_n_0_[29] ),
        .I4(\mem_rdata_q_reg_n_0_[30] ),
        .O(instr_rdcycleh_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_rdcycleh_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_rdcycleh_i_1_n_0),
        .Q(instr_rdcycleh),
        .R(instr_rdinstrh_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000400)) 
    instr_rdinstr_i_1
       (.I0(\mem_rdata_q_reg_n_0_[20] ),
        .I1(\mem_rdata_q_reg_n_0_[21] ),
        .I2(\mem_rdata_q_reg_n_0_[27] ),
        .I3(\mem_rdata_q_reg_n_0_[30] ),
        .I4(\mem_rdata_q_reg_n_0_[29] ),
        .I5(\mem_rdata_q_reg_n_0_[28] ),
        .O(instr_rdinstr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_rdinstr_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_rdinstr_i_1_n_0),
        .Q(instr_rdinstr),
        .R(instr_rdinstrh_i_1_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFBFFFF)) 
    instr_rdinstrh_i_1
       (.I0(\mem_rdata_q_reg_n_0_[23] ),
        .I1(\mem_rdata_q_reg_n_0_[31] ),
        .I2(\mem_rdata_q_reg_n_0_[22] ),
        .I3(\mem_rdata_q_reg_n_0_[15] ),
        .I4(instr_rdinstrh_i_3_n_0),
        .I5(instr_rdinstrh_i_4_n_0),
        .O(instr_rdinstrh_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000004000000000)) 
    instr_rdinstrh_i_2
       (.I0(\mem_rdata_q_reg_n_0_[20] ),
        .I1(\mem_rdata_q_reg_n_0_[21] ),
        .I2(\mem_rdata_q_reg_n_0_[27] ),
        .I3(\mem_rdata_q_reg_n_0_[28] ),
        .I4(\mem_rdata_q_reg_n_0_[29] ),
        .I5(\mem_rdata_q_reg_n_0_[30] ),
        .O(instr_rdinstrh_i_2_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    instr_rdinstrh_i_3
       (.I0(instr_rdinstrh_i_5_n_0),
        .I1(instr_rdinstrh_i_6_n_0),
        .I2(\mem_rdata_q_reg_n_0_[3] ),
        .I3(\mem_rdata_q_reg_n_0_[2] ),
        .I4(\mem_rdata_q_reg_n_0_[5] ),
        .I5(instr_lw_i_1_n_0),
        .O(instr_rdinstrh_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'hB)) 
    instr_rdinstrh_i_4
       (.I0(decoder_pseudo_trigger_reg_n_0),
        .I1(decoder_trigger_reg_n_0),
        .O(instr_rdinstrh_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    instr_rdinstrh_i_5
       (.I0(\mem_rdata_q_reg_n_0_[24] ),
        .I1(instr_rdinstrh_i_7_n_0),
        .I2(\mem_rdata_q_reg_n_0_[18] ),
        .I3(\mem_rdata_q_reg_n_0_[19] ),
        .I4(\mem_rdata_q_reg_n_0_[17] ),
        .I5(\mem_rdata_q_reg_n_0_[16] ),
        .O(instr_rdinstrh_i_5_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    instr_rdinstrh_i_6
       (.I0(\mem_rdata_q_reg_n_0_[0] ),
        .I1(\mem_rdata_q_reg_n_0_[6] ),
        .I2(\mem_rdata_q_reg_n_0_[4] ),
        .I3(\mem_rdata_q_reg_n_0_[1] ),
        .O(instr_rdinstrh_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'hE)) 
    instr_rdinstrh_i_7
       (.I0(\mem_rdata_q_reg_n_0_[25] ),
        .I1(\mem_rdata_q_reg_n_0_[26] ),
        .O(instr_rdinstrh_i_7_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_rdinstrh_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_rdinstrh_i_2_n_0),
        .Q(instr_rdinstrh),
        .R(instr_rdinstrh_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    instr_sb_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(is_sb_sh_sw),
        .O(instr_sb0));
  FDRE #(
    .INIT(1'b0)) 
    instr_sb_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_sb0),
        .Q(instr_sb),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    instr_sh_i_1
       (.I0(p_0_in_1[2]),
        .I1(p_0_in_1[1]),
        .I2(p_0_in_1[0]),
        .I3(is_sb_sh_sw),
        .O(instr_sh0));
  FDRE #(
    .INIT(1'b0)) 
    instr_sh_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_sh0),
        .Q(instr_sh),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    instr_sll_i_1
       (.I0(p_0_in_1[2]),
        .I1(p_0_in_1[1]),
        .I2(p_0_in_1[0]),
        .I3(instr_and_i_3_n_0),
        .O(instr_sll0));
  FDRE #(
    .INIT(1'b0)) 
    instr_sll_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_sll0),
        .Q(instr_sll),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    instr_slli_i_1
       (.I0(is_alu_reg_imm),
        .I1(p_0_in_1[0]),
        .I2(p_0_in_1[1]),
        .I3(p_0_in_1[2]),
        .I4(instr_srli_i_2_n_0),
        .O(instr_slli0));
  FDRE #(
    .INIT(1'b0)) 
    instr_slli_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_slli0),
        .Q(instr_slli),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    instr_slt_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(instr_and_i_3_n_0),
        .O(instr_slt0));
  FDRE #(
    .INIT(1'b0)) 
    instr_slt_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_slt0),
        .Q(instr_slt),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    instr_slti_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(is_alu_reg_imm),
        .O(instr_slti0));
  FDRE #(
    .INIT(1'b0)) 
    instr_slti_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_slti0),
        .Q(instr_slti),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    instr_sltiu_i_1
       (.I0(p_0_in_1[2]),
        .I1(is_alu_reg_imm),
        .I2(p_0_in_1[0]),
        .I3(p_0_in_1[1]),
        .O(instr_sltiu0));
  FDRE #(
    .INIT(1'b0)) 
    instr_sltiu_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_sltiu0),
        .Q(instr_sltiu),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    instr_sltu_i_1
       (.I0(p_0_in_1[2]),
        .I1(p_0_in_1[1]),
        .I2(p_0_in_1[0]),
        .I3(instr_and_i_3_n_0),
        .O(instr_sltu0));
  FDRE #(
    .INIT(1'b0)) 
    instr_sltu_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_sltu0),
        .Q(instr_sltu),
        .R(instr_and_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    instr_sra_i_1
       (.I0(instr_lhu_i_2_n_0),
        .I1(is_alu_reg_reg),
        .I2(\mem_rdata_q_reg_n_0_[31] ),
        .I3(\mem_rdata_q_reg_n_0_[25] ),
        .I4(\mem_rdata_q_reg_n_0_[26] ),
        .I5(instr_sra_i_2_n_0),
        .O(instr_sra0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    instr_sra_i_2
       (.I0(\mem_rdata_q_reg_n_0_[28] ),
        .I1(\mem_rdata_q_reg_n_0_[29] ),
        .I2(\mem_rdata_q_reg_n_0_[30] ),
        .I3(\mem_rdata_q_reg_n_0_[27] ),
        .O(instr_sra_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_sra_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_sra0),
        .Q(instr_sra),
        .R(instr_and_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    instr_srai_i_1
       (.I0(instr_lhu_i_2_n_0),
        .I1(is_alu_reg_imm),
        .I2(\mem_rdata_q_reg_n_0_[31] ),
        .I3(\mem_rdata_q_reg_n_0_[25] ),
        .I4(\mem_rdata_q_reg_n_0_[26] ),
        .I5(instr_sra_i_2_n_0),
        .O(instr_srai0));
  FDRE #(
    .INIT(1'b0)) 
    instr_srai_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_srai0),
        .Q(instr_srai),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    instr_srl_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[0]),
        .I2(p_0_in_1[2]),
        .I3(instr_and_i_3_n_0),
        .O(instr_srl0));
  FDRE #(
    .INIT(1'b0)) 
    instr_srl_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_srl0),
        .Q(instr_srl),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h00000080)) 
    instr_srli_i_1
       (.I0(is_alu_reg_imm),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(p_0_in_1[1]),
        .I4(instr_srli_i_2_n_0),
        .O(instr_srli0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    instr_srli_i_2
       (.I0(\mem_rdata_q_reg_n_0_[28] ),
        .I1(\mem_rdata_q_reg_n_0_[29] ),
        .I2(\mem_rdata_q_reg_n_0_[30] ),
        .I3(\mem_rdata_q_reg_n_0_[27] ),
        .I4(instr_and_i_4_n_0),
        .O(instr_srli_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    instr_srli_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_srli0),
        .Q(instr_srli),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    instr_sub_i_1
       (.I0(instr_lb_i_1_n_0),
        .I1(is_alu_reg_reg),
        .I2(\mem_rdata_q_reg_n_0_[31] ),
        .I3(\mem_rdata_q_reg_n_0_[25] ),
        .I4(\mem_rdata_q_reg_n_0_[26] ),
        .I5(instr_sra_i_2_n_0),
        .O(instr_sub0));
  FDRE #(
    .INIT(1'b0)) 
    instr_sub_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_sub0),
        .Q(instr_sub),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    instr_sw_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(is_sb_sh_sw),
        .O(instr_sw0));
  FDRE #(
    .INIT(1'b0)) 
    instr_sw_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_sw0),
        .Q(instr_sw),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    instr_xor_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(instr_and_i_3_n_0),
        .O(instr_xor0));
  FDRE #(
    .INIT(1'b0)) 
    instr_xor_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_xor0),
        .Q(instr_xor),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    instr_xori_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[2]),
        .I2(p_0_in_1[0]),
        .I3(is_alu_reg_imm),
        .O(instr_xori0));
  FDRE #(
    .INIT(1'b0)) 
    instr_xori_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(instr_xori0),
        .Q(instr_xori),
        .R(instr_and_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h000ACC0A)) 
    is_alu_reg_imm_i_1
       (.I0(\mem_rdata_q_reg_n_0_[4] ),
        .I1(mem_rdata[4]),
        .I2(\mem_rdata_q_reg_n_0_[5] ),
        .I3(mem_valid_reg_n_0),
        .I4(mem_rdata[5]),
        .O(is_alu_reg_imm_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_alu_reg_imm_reg
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(is_alu_reg_imm_i_1_n_0),
        .Q(is_alu_reg_imm),
        .R(is_sb_sh_sw_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hCCA000A0)) 
    is_alu_reg_reg_i_1
       (.I0(\mem_rdata_q_reg_n_0_[4] ),
        .I1(mem_rdata[4]),
        .I2(\mem_rdata_q_reg_n_0_[5] ),
        .I3(mem_valid_reg_n_0),
        .I4(mem_rdata[5]),
        .O(is_alu_reg_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_alu_reg_reg_reg
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(is_alu_reg_reg_i_1_n_0),
        .Q(is_alu_reg_reg),
        .R(is_sb_sh_sw_i_1_n_0));
  LUT6 #(
    .INIT(64'h0800FFFF08000000)) 
    is_beq_bne_blt_bge_bltu_bgeu_i_1
       (.I0(is_sb_sh_sw_i_2_n_0),
        .I1(\mem_rdata_q[6]_i_1_n_0 ),
        .I2(\mem_rdata_q[2]_i_1_n_0 ),
        .I3(instr_auipc_i_2_n_0),
        .I4(instr_lui0),
        .I5(is_beq_bne_blt_bge_bltu_bgeu),
        .O(is_beq_bne_blt_bge_bltu_bgeu_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_beq_bne_blt_bge_bltu_bgeu_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(is_beq_bne_blt_bge_bltu_bgeu_i_1_n_0),
        .Q(is_beq_bne_blt_bge_bltu_bgeu),
        .R(instr_and_i_1_n_0));
  LUT6 #(
    .INIT(64'h8888888888888880)) 
    is_compare_i_1
       (.I0(resetn_IBUF),
        .I1(instr_rdinstrh_i_4_n_0),
        .I2(instr_slt),
        .I3(instr_slti),
        .I4(is_compare_i_2_n_0),
        .I5(is_beq_bne_blt_bge_bltu_bgeu),
        .O(is_compare_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT2 #(
    .INIT(4'hE)) 
    is_compare_i_2
       (.I0(instr_sltu),
        .I1(instr_sltiu),
        .O(is_compare_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_compare_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(is_compare_i_1_n_0),
        .Q(is_compare),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT4 #(
    .INIT(16'hFF8A)) 
    is_jalr_addi_slti_sltiu_xori_ori_andi_i_1
       (.I0(is_alu_reg_imm),
        .I1(p_0_in_1[1]),
        .I2(p_0_in_1[0]),
        .I3(instr_jalr),
        .O(is_jalr_addi_slti_sltiu_xori_ori_andi0));
  FDRE #(
    .INIT(1'b0)) 
    is_jalr_addi_slti_sltiu_xori_ori_andi_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(is_jalr_addi_slti_sltiu_xori_ori_andi0),
        .Q(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h00053305)) 
    is_lb_lh_lw_lbu_lhu_i_1
       (.I0(\mem_rdata_q_reg_n_0_[4] ),
        .I1(mem_rdata[4]),
        .I2(\mem_rdata_q_reg_n_0_[5] ),
        .I3(mem_valid_reg_n_0),
        .I4(mem_rdata[5]),
        .O(is_lb_lh_lw_lbu_lhu_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_lb_lh_lw_lbu_lhu_reg
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(is_lb_lh_lw_lbu_lhu_i_1_n_0),
        .Q(is_lb_lh_lw_lbu_lhu),
        .R(is_sb_sh_sw_i_1_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    is_lbu_lhu_lw_i_1
       (.I0(instr_lw),
        .I1(instr_lbu),
        .I2(instr_lhu),
        .O(is_lbu_lhu_lw_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_lbu_lhu_lw_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(is_lbu_lhu_lw_i_1_n_0),
        .Q(is_lbu_lhu_lw),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    is_lui_auipc_jal_i_1
       (.I0(instr_auipc),
        .I1(instr_lui),
        .I2(instr_jal),
        .O(is_lui_auipc_jal_i_1_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFFFFFE)) 
    is_lui_auipc_jal_jalr_addi_add_sub_i_1
       (.I0(instr_jalr),
        .I1(instr_add),
        .I2(instr_addi),
        .I3(instr_sub),
        .I4(is_lui_auipc_jal_i_1_n_0),
        .I5(is_lui_auipc_jal_jalr_addi_add_sub0),
        .O(is_lui_auipc_jal_jalr_addi_add_sub_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_lui_auipc_jal_jalr_addi_add_sub_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(is_lui_auipc_jal_jalr_addi_add_sub_i_1_n_0),
        .Q(is_lui_auipc_jal_jalr_addi_add_sub),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    is_lui_auipc_jal_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(is_lui_auipc_jal_i_1_n_0),
        .Q(is_lui_auipc_jal),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAA8A8A8AAA8A)) 
    is_sb_sh_sw_i_1
       (.I0(instr_lui0),
        .I1(\mem_rdata_q[2]_i_1_n_0 ),
        .I2(instr_auipc_i_2_n_0),
        .I3(\mem_rdata_q_reg_n_0_[6] ),
        .I4(mem_valid_reg_n_0),
        .I5(mem_rdata[6]),
        .O(is_sb_sh_sw_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h000ACC0A)) 
    is_sb_sh_sw_i_2
       (.I0(\mem_rdata_q_reg_n_0_[5] ),
        .I1(mem_rdata[5]),
        .I2(\mem_rdata_q_reg_n_0_[4] ),
        .I3(mem_valid_reg_n_0),
        .I4(mem_rdata[4]),
        .O(is_sb_sh_sw_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_sb_sh_sw_reg
       (.C(clk_IBUF_BUFG),
        .CE(instr_lui0),
        .D(is_sb_sh_sw_i_2_n_0),
        .Q(is_sb_sh_sw),
        .R(is_sb_sh_sw_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    is_sll_srl_sra_i_1
       (.I0(decoder_trigger_reg_n_0),
        .I1(decoder_pseudo_trigger_reg_n_0),
        .O(is_lui_auipc_jal_jalr_addi_add_sub0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    is_sll_srl_sra_i_2
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[0]),
        .I2(is_alu_reg_reg),
        .I3(is_sll_srl_sra_i_3_n_0),
        .O(is_sll_srl_sra0));
  LUT6 #(
    .INIT(64'h00000020FFFFFFFF)) 
    is_sll_srl_sra_i_3
       (.I0(is_sll_srl_sra_i_4_n_0),
        .I1(\mem_rdata_q_reg_n_0_[31] ),
        .I2(\mem_rdata_q_reg_n_0_[30] ),
        .I3(\mem_rdata_q_reg_n_0_[29] ),
        .I4(\mem_rdata_q_reg_n_0_[28] ),
        .I5(instr_srli_i_2_n_0),
        .O(is_sll_srl_sra_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    is_sll_srl_sra_i_4
       (.I0(\mem_rdata_q_reg_n_0_[25] ),
        .I1(p_0_in_1[2]),
        .I2(\mem_rdata_q_reg_n_0_[27] ),
        .I3(\mem_rdata_q_reg_n_0_[26] ),
        .O(is_sll_srl_sra_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_sll_srl_sra_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(is_sll_srl_sra0),
        .Q(is_sll_srl_sra),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    is_slli_srli_srai_i_1
       (.I0(p_0_in_1[1]),
        .I1(p_0_in_1[0]),
        .I2(is_alu_reg_imm),
        .I3(is_sll_srl_sra_i_3_n_0),
        .O(is_slli_srli_srai0));
  FDRE #(
    .INIT(1'b0)) 
    is_slli_srli_srai_reg
       (.C(clk_IBUF_BUFG),
        .CE(is_lui_auipc_jal_jalr_addi_add_sub0),
        .D(is_slli_srli_srai0),
        .Q(is_slli_srli_srai),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hFE)) 
    is_slti_blt_slt_i_1
       (.I0(instr_slti),
        .I1(instr_slt),
        .I2(instr_blt),
        .O(is_slti_blt_slt_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_slti_blt_slt_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(is_slti_blt_slt_i_1_n_0),
        .Q(is_slti_blt_slt),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    is_sltiu_bltu_sltu_i_1
       (.I0(instr_sltiu),
        .I1(instr_sltu),
        .I2(instr_bltu),
        .O(is_sltiu_bltu_sltu_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_sltiu_bltu_sltu_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(is_sltiu_bltu_sltu_i_1_n_0),
        .Q(is_sltiu_bltu_sltu),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF5F5FDFFF5F5FD00)) 
    latched_branch_i_1
       (.I0(decoder_trigger_i_3_n_0),
        .I1(latched_branch_i_2_n_0),
        .I2(latched_branch_i_3_n_0),
        .I3(reg_next_pc),
        .I4(\cpu_state_reg_n_0_[3] ),
        .I5(latched_branch_reg_n_0),
        .O(latched_branch_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    latched_branch_i_2
       (.I0(decoder_trigger_reg_n_0),
        .I1(instr_jal),
        .O(latched_branch_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h40)) 
    latched_branch_i_3
       (.I0(is_beq_bne_blt_bge_bltu_bgeu),
        .I1(instr_jalr),
        .I2(\cpu_state_reg_n_0_[3] ),
        .O(latched_branch_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    latched_branch_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(latched_branch_i_1_n_0),
        .Q(latched_branch_reg_n_0),
        .R(instr_and_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F20)) 
    latched_is_lb_i_1
       (.I0(instr_lb),
        .I1(reg_next_pc),
        .I2(latched_is_lu),
        .I3(latched_is_lb_reg_n_0),
        .O(latched_is_lb_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAABBFBAAAA)) 
    latched_is_lb_i_2
       (.I0(reg_next_pc),
        .I1(mem_do_prefetch_reg_n_0),
        .I2(resetn_IBUF),
        .I3(instr_jal_i_3_n_0),
        .I4(\cpu_state_reg_n_0_[0] ),
        .I5(mem_do_rdata),
        .O(latched_is_lu));
  FDRE #(
    .INIT(1'b0)) 
    latched_is_lb_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(latched_is_lb_i_1_n_0),
        .Q(latched_is_lb_reg_n_0),
        .R(instr_and_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F20)) 
    latched_is_lh_i_1
       (.I0(instr_lh),
        .I1(reg_next_pc),
        .I2(latched_is_lu),
        .I3(latched_is_lh_reg_n_0),
        .O(latched_is_lh_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    latched_is_lh_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(latched_is_lh_i_1_n_0),
        .Q(latched_is_lh_reg_n_0),
        .R(instr_and_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F20)) 
    latched_is_lu_i_1
       (.I0(is_lbu_lhu_lw),
        .I1(reg_next_pc),
        .I2(latched_is_lu),
        .I3(latched_is_lu_reg_n_0),
        .O(latched_is_lu_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    latched_is_lu_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(latched_is_lu_i_1_n_0),
        .Q(latched_is_lu_reg_n_0),
        .R(instr_and_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    \latched_rd[4]_i_1 
       (.I0(resetn_IBUF),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(is_beq_bne_blt_bge_bltu_bgeu),
        .I3(reg_next_pc),
        .O(\latched_rd[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF080)) 
    \latched_rd[4]_i_2 
       (.I0(is_beq_bne_blt_bge_bltu_bgeu),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(resetn_IBUF),
        .I3(reg_next_pc),
        .O(\latched_rd[4]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \latched_rd_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\latched_rd[4]_i_2_n_0 ),
        .D(decoded_rd[0]),
        .Q(latched_rd[0]),
        .R(\latched_rd[4]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \latched_rd_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\latched_rd[4]_i_2_n_0 ),
        .D(decoded_rd[1]),
        .Q(latched_rd[1]),
        .R(\latched_rd[4]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \latched_rd_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\latched_rd[4]_i_2_n_0 ),
        .D(decoded_rd[2]),
        .Q(latched_rd[2]),
        .R(\latched_rd[4]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \latched_rd_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\latched_rd[4]_i_2_n_0 ),
        .D(decoded_rd[3]),
        .Q(latched_rd[3]),
        .R(\latched_rd[4]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \latched_rd_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\latched_rd[4]_i_2_n_0 ),
        .D(decoded_rd[4]),
        .Q(latched_rd[4]),
        .R(\latched_rd[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'h0F04)) 
    latched_stalu_i_1
       (.I0(is_beq_bne_blt_bge_bltu_bgeu),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_next_pc),
        .I3(latched_stalu_reg_n_0),
        .O(latched_stalu_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    latched_stalu_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(latched_stalu_i_1_n_0),
        .Q(latched_stalu_reg_n_0),
        .R(instr_and_i_1_n_0));
  LUT6 #(
    .INIT(64'hAFAEFFFFAFAE0000)) 
    latched_store_i_1
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(latched_store_i_2_n_0),
        .I2(reg_next_pc),
        .I3(\alu_out_q[0]_i_3_n_0 ),
        .I4(latched_store),
        .I5(latched_store_reg_n_0),
        .O(latched_store_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h7)) 
    latched_store_i_2
       (.I0(is_beq_bne_blt_bge_bltu_bgeu),
        .I1(\cpu_state_reg_n_0_[3] ),
        .O(latched_store_i_2_n_0));
  LUT6 #(
    .INIT(64'h00FF00FF00FF000B)) 
    latched_store_i_3
       (.I0(\cpu_state[6]_i_3_n_0 ),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(\cpu_state_reg_n_0_[7] ),
        .I3(\cpu_state_reg_n_0_[1] ),
        .I4(\cpu_state_reg_n_0_[3] ),
        .I5(\cpu_state_reg_n_0_[2] ),
        .O(latched_store));
  FDRE #(
    .INIT(1'b0)) 
    latched_store_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(latched_store_i_1_n_0),
        .Q(latched_store_reg_n_0),
        .R(instr_and_i_1_n_0));
  LUT6 #(
    .INIT(64'h00000000A8AAABAA)) 
    mem_do_prefetch_i_1
       (.I0(mem_do_prefetch_reg_n_0),
        .I1(mem_do_prefetch_i_2_n_0),
        .I2(instr_jal),
        .I3(decoder_trigger_reg_n_0),
        .I4(instr_jalr),
        .I5(mem_do_rinst0),
        .O(mem_do_prefetch_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h7)) 
    mem_do_prefetch_i_2
       (.I0(reg_next_pc),
        .I1(resetn_IBUF),
        .O(mem_do_prefetch_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h7)) 
    mem_do_prefetch_i_3
       (.I0(instr_jal_i_3_n_0),
        .I1(resetn_IBUF),
        .O(mem_do_rinst0));
  FDRE #(
    .INIT(1'b0)) 
    mem_do_prefetch_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(mem_do_prefetch_i_1_n_0),
        .Q(mem_do_prefetch_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h2F22AAAA2022AAAA)) 
    mem_do_rdata_i_1
       (.I0(mem_do_rdata_i_2_n_0),
        .I1(mem_do_prefetch_reg_n_0),
        .I2(mem_do_rdata_i_3_n_0),
        .I3(resetn_IBUF),
        .I4(instr_jal_i_3_n_0),
        .I5(mem_do_rdata),
        .O(mem_do_rdata_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    mem_do_rdata_i_2
       (.I0(reg_next_pc),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(mem_do_rdata),
        .I3(resetn_IBUF),
        .I4(\cpu_state_reg_n_0_[1] ),
        .I5(cpuregs_reg_r1_0_31_0_5_i_9_n_0),
        .O(mem_do_rdata_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    mem_do_rdata_i_3
       (.I0(reg_next_pc),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(mem_do_rdata),
        .I3(\cpu_state_reg_n_0_[1] ),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(cpuregs_reg_r1_0_31_0_5_i_9_n_0),
        .O(mem_do_rdata_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mem_do_rdata_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(mem_do_rdata_i_1_n_0),
        .Q(mem_do_rdata),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF500D5005500D500)) 
    mem_do_rinst_i_1
       (.I0(decoder_trigger_i_3_n_0),
        .I1(mem_do_rinst_reg_n_0),
        .I2(instr_jal_i_3_n_0),
        .I3(resetn_IBUF),
        .I4(mem_do_rinst5_out),
        .I5(mem_do_rinst_i_3_n_0),
        .O(mem_do_rinst_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'h80)) 
    mem_do_rinst_i_10
       (.I0(\cpu_state_reg_n_0_[5] ),
        .I1(is_lui_auipc_jal),
        .I2(mem_do_prefetch_reg_n_0),
        .O(mem_do_rinst_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'h40)) 
    mem_do_rinst_i_11
       (.I0(is_lui_auipc_jal),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(is_lb_lh_lw_lbu_lhu),
        .O(mem_do_rinst_i_11_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF8A8AFF8A)) 
    mem_do_rinst_i_2
       (.I0(\cpu_state[3]_i_2_n_0 ),
        .I1(is_sb_sh_sw),
        .I2(is_sll_srl_sra),
        .I3(mem_do_rinst_i_4_n_0),
        .I4(\cpu_state[7]_i_8_n_0 ),
        .I5(mem_do_rinst_i_5_n_0),
        .O(mem_do_rinst5_out));
  LUT6 #(
    .INIT(64'hFFFFEEEEFEEEEEEE)) 
    mem_do_rinst_i_3
       (.I0(mem_do_rinst_i_6_n_0),
        .I1(mem_do_rinst_i_7_n_0),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(mem_do_prefetch_reg_n_0),
        .I4(mem_do_rinst_i_8_n_0),
        .I5(mem_do_rinst_i_9_n_0),
        .O(mem_do_rinst_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h80)) 
    mem_do_rinst_i_4
       (.I0(resetn_IBUF),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(is_lb_lh_lw_lbu_lhu),
        .O(mem_do_rinst_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000EEEA0000)) 
    mem_do_rinst_i_5
       (.I0(reg_next_pc),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I3(is_lui_auipc_jal),
        .I4(resetn_IBUF),
        .I5(\cpu_state[7]_i_5_n_0 ),
        .O(mem_do_rinst_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFF8A8A8A)) 
    mem_do_rinst_i_6
       (.I0(reg_next_pc),
        .I1(instr_jal),
        .I2(decoder_trigger_reg_n_0),
        .I3(mem_do_prefetch_reg_n_0),
        .I4(\cpu_state_reg_n_0_[2] ),
        .O(mem_do_rinst_i_6_n_0));
  LUT6 #(
    .INIT(64'h0000CFFF00008AAA)) 
    mem_do_rinst_i_7
       (.I0(mem_do_rinst_i_10_n_0),
        .I1(\cpu_state[7]_i_12_n_0 ),
        .I2(\cpu_state[3]_i_4_n_0 ),
        .I3(\cpu_state[3]_i_5_n_0 ),
        .I4(\cpu_state[6]_i_3_n_0 ),
        .I5(mem_do_rinst_i_11_n_0),
        .O(mem_do_rinst_i_7_n_0));
  LUT6 #(
    .INIT(64'h0000000055555455)) 
    mem_do_rinst_i_8
       (.I0(\cpu_state[6]_i_3_n_0 ),
        .I1(\cpu_state[7]_i_9_n_0 ),
        .I2(is_lui_auipc_jal_i_1_n_0),
        .I3(\cpu_state[3]_i_4_n_0 ),
        .I4(\cpu_state[7]_i_12_n_0 ),
        .I5(is_slli_srli_srai),
        .O(mem_do_rinst_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    mem_do_rinst_i_9
       (.I0(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I1(is_sb_sh_sw),
        .I2(is_lui_auipc_jal),
        .I3(\cpu_state_reg_n_0_[5] ),
        .O(mem_do_rinst_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mem_do_rinst_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(mem_do_rinst_i_1_n_0),
        .Q(mem_do_rinst_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'hF4000C00)) 
    mem_do_wdata_i_1
       (.I0(mem_do_prefetch_reg_n_0),
        .I1(\cpu_state_reg_n_0_[1] ),
        .I2(mem_do_wdata),
        .I3(resetn_IBUF),
        .I4(instr_jal_i_3_n_0),
        .O(mem_do_wdata_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mem_do_wdata_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(mem_do_wdata_i_1_n_0),
        .Q(mem_do_wdata),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mem_rdata_q[0]_i_1 
       (.I0(mem_rdata[0]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[0] ),
        .O(\mem_rdata_q[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mem_rdata_q[1]_i_1 
       (.I0(mem_rdata[1]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[1] ),
        .O(\mem_rdata_q[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mem_rdata_q[2]_i_1 
       (.I0(mem_rdata[2]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[2] ),
        .O(\mem_rdata_q[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mem_rdata_q[3]_i_1 
       (.I0(mem_rdata[3]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[3] ),
        .O(\mem_rdata_q[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mem_rdata_q[4]_i_1 
       (.I0(mem_rdata[4]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[4] ),
        .O(\mem_rdata_q[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mem_rdata_q[5]_i_1 
       (.I0(mem_rdata[5]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[5] ),
        .O(\mem_rdata_q[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mem_rdata_q[6]_i_1 
       (.I0(mem_rdata[6]),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_rdata_q_reg_n_0_[6] ),
        .O(\mem_rdata_q[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_rdata_q[0]_i_1_n_0 ),
        .Q(\mem_rdata_q_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[10]),
        .Q(\mem_rdata_q_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[11]),
        .Q(\mem_rdata_q_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[12]),
        .Q(p_0_in_1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[13]),
        .Q(p_0_in_1[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[14]),
        .Q(p_0_in_1[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[15]),
        .Q(\mem_rdata_q_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[16]),
        .Q(\mem_rdata_q_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[17]),
        .Q(\mem_rdata_q_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[18]),
        .Q(\mem_rdata_q_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[19]),
        .Q(\mem_rdata_q_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_rdata_q[1]_i_1_n_0 ),
        .Q(\mem_rdata_q_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[20]),
        .Q(\mem_rdata_q_reg_n_0_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[21]),
        .Q(\mem_rdata_q_reg_n_0_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[22]),
        .Q(\mem_rdata_q_reg_n_0_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[23]),
        .Q(\mem_rdata_q_reg_n_0_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[24]),
        .Q(\mem_rdata_q_reg_n_0_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[25]),
        .Q(\mem_rdata_q_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[26]),
        .Q(\mem_rdata_q_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[27]),
        .Q(\mem_rdata_q_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[28]),
        .Q(\mem_rdata_q_reg_n_0_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[29]),
        .Q(\mem_rdata_q_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_rdata_q[2]_i_1_n_0 ),
        .Q(\mem_rdata_q_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[30]),
        .Q(\mem_rdata_q_reg_n_0_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[31]),
        .Q(\mem_rdata_q_reg_n_0_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_rdata_q[3]_i_1_n_0 ),
        .Q(\mem_rdata_q_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_rdata_q[4]_i_1_n_0 ),
        .Q(\mem_rdata_q_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_rdata_q[5]_i_1_n_0 ),
        .Q(\mem_rdata_q_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_rdata_q[6]_i_1_n_0 ),
        .Q(\mem_rdata_q_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[7]),
        .Q(\mem_rdata_q_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[8]),
        .Q(\mem_rdata_q_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_rdata_q_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(mem_valid_reg_n_0),
        .D(mem_rdata[9]),
        .Q(\mem_rdata_q_reg_n_0_[9] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0011FFFF000F0000)) 
    \mem_state[0]_i_1 
       (.I0(mem_do_rdata),
        .I1(mem_do_rinst_reg_n_0),
        .I2(mem_do_wdata),
        .I3(\mem_state_reg_n_0_[1] ),
        .I4(mem_state),
        .I5(\mem_state_reg_n_0_[0] ),
        .O(\mem_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF1F100000)) 
    \mem_state[1]_i_1 
       (.I0(mem_do_rdata),
        .I1(mem_do_rinst_reg_n_0),
        .I2(\mem_state_reg_n_0_[0] ),
        .I3(mem_do_wdata),
        .I4(mem_state),
        .I5(\mem_state_reg_n_0_[1] ),
        .O(\mem_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00008000)) 
    \mem_state[1]_i_2 
       (.I0(mem_do_rinst_reg_n_0),
        .I1(\mem_state_reg_n_0_[0] ),
        .I2(\mem_state_reg_n_0_[1] ),
        .I3(resetn_IBUF),
        .I4(trap_OBUF),
        .I5(mem_valid11_out),
        .O(mem_state));
  LUT6 #(
    .INIT(64'h4444444404400000)) 
    \mem_state[1]_i_3 
       (.I0(trap_OBUF),
        .I1(resetn_IBUF),
        .I2(\mem_state_reg_n_0_[0] ),
        .I3(\mem_state_reg_n_0_[1] ),
        .I4(mem_valid_reg_n_0),
        .I5(mem_valid_i_2_n_0),
        .O(mem_valid11_out));
  FDRE #(
    .INIT(1'b0)) 
    \mem_state_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_state[0]_i_1_n_0 ),
        .Q(\mem_state_reg_n_0_[0] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_state_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_state[1]_i_1_n_0 ),
        .Q(\mem_state_reg_n_0_[1] ),
        .R(instr_and_i_1_n_0));
  LUT6 #(
    .INIT(64'h00000000EAAE0000)) 
    mem_valid_i_1
       (.I0(mem_valid_i_2_n_0),
        .I1(mem_valid_reg_n_0),
        .I2(\mem_state_reg_n_0_[1] ),
        .I3(\mem_state_reg_n_0_[0] ),
        .I4(resetn_IBUF),
        .I5(trap_OBUF),
        .O(mem_valid_i_1_n_0));
  LUT6 #(
    .INIT(64'h000000000000FFFE)) 
    mem_valid_i_2
       (.I0(mem_do_rinst_reg_n_0),
        .I1(mem_do_prefetch_reg_n_0),
        .I2(mem_do_wdata),
        .I3(mem_do_rdata),
        .I4(\mem_state_reg_n_0_[0] ),
        .I5(\mem_state_reg_n_0_[1] ),
        .O(mem_valid_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mem_valid_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(mem_valid_i_1_n_0),
        .Q(mem_valid_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mem_wordsize[0]_i_1 
       (.I0(mem_wordsize[0]),
        .I1(\mem_wordsize[1]_i_3_n_0 ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .O(\mem_wordsize[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFF04444)) 
    \mem_wordsize[0]_i_2 
       (.I0(reg_next_pc),
        .I1(instr_sh),
        .I2(instr_lh),
        .I3(instr_lhu),
        .I4(\cpu_state_reg_n_0_[0] ),
        .O(mem_wordsize[0]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \mem_wordsize[1]_i_1 
       (.I0(mem_wordsize[1]),
        .I1(\mem_wordsize[1]_i_3_n_0 ),
        .I2(\mem_wordsize_reg_n_0_[1] ),
        .O(\mem_wordsize[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFF04444)) 
    \mem_wordsize[1]_i_2 
       (.I0(reg_next_pc),
        .I1(instr_sb),
        .I2(instr_lb),
        .I3(instr_lbu),
        .I4(\cpu_state_reg_n_0_[0] ),
        .O(mem_wordsize[1]));
  LUT6 #(
    .INIT(64'h75557555FF557555)) 
    \mem_wordsize[1]_i_3 
       (.I0(mem_do_prefetch_i_2_n_0),
        .I1(mem_do_wdata),
        .I2(\cpu_state_reg_n_0_[1] ),
        .I3(\reg_op1[31]_i_3_n_0 ),
        .I4(\cpu_state_reg_n_0_[0] ),
        .I5(mem_do_rdata),
        .O(\mem_wordsize[1]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mem_wordsize_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_wordsize[0]_i_1_n_0 ),
        .Q(\mem_wordsize_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \mem_wordsize_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\mem_wordsize[1]_i_1_n_0 ),
        .Q(\mem_wordsize_reg_n_0_[1] ),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h40)) 
    memory_reg_0_i_1
       (.I0(memory_reg_0_i_15_n_0),
        .I1(memory_reg_0_i_16_n_0),
        .I2(memory_reg_0_i_17_n_0),
        .O(out_byte_en01_out));
  LUT6 #(
    .INIT(64'hE2E2E2FFE2E2E200)) 
    memory_reg_0_i_10
       (.I0(\reg_next_pc_reg_n_0_[5] ),
        .I1(memory_reg_0_i_18_n_0),
        .I2(\reg_out_reg_n_0_[5] ),
        .I3(mem_do_prefetch_reg_n_0),
        .I4(mem_do_rinst_reg_n_0),
        .I5(\reg_op1_reg_n_0_[5] ),
        .O(ADDRARDADDR[3]));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_11
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[4] ),
        .I2(\reg_out_reg_n_0_[4] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[4] ),
        .O(ADDRARDADDR[2]));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_12
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[3] ),
        .I2(\reg_out_reg_n_0_[3] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[3] ),
        .O(ADDRARDADDR[1]));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_13
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[2] ),
        .I2(\reg_out_reg_n_0_[2] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[2] ),
        .O(ADDRARDADDR[0]));
  LUT4 #(
    .INIT(16'h1D1F)) 
    memory_reg_0_i_14
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(\mem_wordsize_reg_n_0_[0] ),
        .I2(\reg_op1_reg_n_0_[1] ),
        .I3(\reg_op1_reg_n_0_[0] ),
        .O(p_0_in[0]));
  LUT6 #(
    .INIT(64'hEEEEEEFFEEEEEEFE)) 
    memory_reg_0_i_15
       (.I0(memory_reg_0_i_19_n_0),
        .I1(memory_reg_0_i_20_n_0),
        .I2(\reg_op1_reg_n_0_[14] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[15] ),
        .O(memory_reg_0_i_15_n_0));
  LUT6 #(
    .INIT(64'h1B1B1B1B1B1B00FF)) 
    memory_reg_0_i_16
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[28] ),
        .I2(\reg_out_reg_n_0_[28] ),
        .I3(\reg_op1_reg_n_0_[28] ),
        .I4(mem_do_rinst_reg_n_0),
        .I5(mem_do_prefetch_reg_n_0),
        .O(memory_reg_0_i_16_n_0));
  LUT4 #(
    .INIT(16'hAFAE)) 
    memory_reg_0_i_17
       (.I0(memory_reg_0_i_21_n_0),
        .I1(memory_reg_0_i_22_n_0),
        .I2(memory_reg_0_i_23_n_0),
        .I3(memory_reg_0_i_24_n_0),
        .O(memory_reg_0_i_17_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    memory_reg_0_i_18
       (.I0(latched_branch_reg_n_0),
        .I1(latched_store_reg_n_0),
        .O(memory_reg_0_i_18_n_0));
  LUT6 #(
    .INIT(64'h0E0F0E0F0E0F0E00)) 
    memory_reg_0_i_19
       (.I0(\reg_out_reg_n_0_[15] ),
        .I1(\reg_out_reg_n_0_[14] ),
        .I2(memory_reg_0_i_25_n_0),
        .I3(memory_reg_0_i_18_n_0),
        .I4(\reg_next_pc_reg_n_0_[14] ),
        .I5(\reg_next_pc_reg_n_0_[15] ),
        .O(memory_reg_0_i_19_n_0));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_2
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[13] ),
        .I2(\reg_out_reg_n_0_[13] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[13] ),
        .O(ADDRARDADDR[11]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    memory_reg_0_i_20
       (.I0(mem_do_wdata),
        .I1(resetn_IBUF),
        .I2(\mem_state_reg_n_0_[0] ),
        .I3(\mem_state_reg_n_0_[1] ),
        .O(memory_reg_0_i_20_n_0));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    memory_reg_0_i_21
       (.I0(\reg_op1_reg_n_0_[18] ),
        .I1(\reg_op1_reg_n_0_[17] ),
        .I2(\reg_op1_reg_n_0_[16] ),
        .I3(memory_reg_0_i_26_n_0),
        .I4(memory_reg_0_i_27_n_0),
        .I5(memory_reg_0_i_28_n_0),
        .O(memory_reg_0_i_21_n_0));
  LUT5 #(
    .INIT(32'h00020000)) 
    memory_reg_0_i_22
       (.I0(memory_reg_0_i_29_n_0),
        .I1(\reg_out_reg_n_0_[18] ),
        .I2(\reg_out_reg_n_0_[17] ),
        .I3(\reg_out_reg_n_0_[16] ),
        .I4(memory_reg_0_i_30_n_0),
        .O(memory_reg_0_i_22_n_0));
  LUT6 #(
    .INIT(64'hFFFFAFAFFFFEAFAE)) 
    memory_reg_0_i_23
       (.I0(memory_reg_0_i_31_n_0),
        .I1(\reg_next_pc_reg_n_0_[24] ),
        .I2(memory_reg_0_i_18_n_0),
        .I3(\reg_next_pc_reg_n_0_[26] ),
        .I4(\reg_out_reg_n_0_[27] ),
        .I5(\reg_next_pc_reg_n_0_[27] ),
        .O(memory_reg_0_i_23_n_0));
  LUT5 #(
    .INIT(32'h00020000)) 
    memory_reg_0_i_24
       (.I0(memory_reg_0_i_32_n_0),
        .I1(\reg_next_pc_reg_n_0_[18] ),
        .I2(\reg_next_pc_reg_n_0_[17] ),
        .I3(\reg_next_pc_reg_n_0_[16] ),
        .I4(memory_reg_0_i_33_n_0),
        .O(memory_reg_0_i_24_n_0));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h1)) 
    memory_reg_0_i_25
       (.I0(mem_do_rinst_reg_n_0),
        .I1(mem_do_prefetch_reg_n_0),
        .O(memory_reg_0_i_25_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    memory_reg_0_i_26
       (.I0(\reg_op1_reg_n_0_[22] ),
        .I1(\reg_op1_reg_n_0_[21] ),
        .I2(\reg_op1_reg_n_0_[20] ),
        .I3(\reg_op1_reg_n_0_[19] ),
        .O(memory_reg_0_i_26_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    memory_reg_0_i_27
       (.I0(\reg_op1_reg_n_0_[26] ),
        .I1(\reg_op1_reg_n_0_[25] ),
        .I2(\reg_op1_reg_n_0_[24] ),
        .I3(\reg_op1_reg_n_0_[23] ),
        .O(memory_reg_0_i_27_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    memory_reg_0_i_28
       (.I0(\reg_op1_reg_n_0_[27] ),
        .I1(\reg_op1_reg_n_0_[29] ),
        .I2(\reg_op1_reg_n_0_[30] ),
        .I3(\reg_op1_reg_n_0_[31] ),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(mem_do_rinst_reg_n_0),
        .O(memory_reg_0_i_28_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    memory_reg_0_i_29
       (.I0(\reg_out_reg_n_0_[22] ),
        .I1(\reg_out_reg_n_0_[21] ),
        .I2(\reg_out_reg_n_0_[20] ),
        .I3(\reg_out_reg_n_0_[19] ),
        .O(memory_reg_0_i_29_n_0));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_3
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[12] ),
        .I2(\reg_out_reg_n_0_[12] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[12] ),
        .O(ADDRARDADDR[10]));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    memory_reg_0_i_30
       (.I0(\reg_out_reg_n_0_[23] ),
        .I1(\reg_out_reg_n_0_[29] ),
        .I2(\reg_out_reg_n_0_[30] ),
        .I3(\reg_out_reg_n_0_[31] ),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(memory_reg_0_i_30_n_0));
  LUT6 #(
    .INIT(64'hFFFFFCFCFFFAFCFC)) 
    memory_reg_0_i_31
       (.I0(\reg_out_reg_n_0_[24] ),
        .I1(\reg_next_pc_reg_n_0_[25] ),
        .I2(memory_reg_0_i_25_n_0),
        .I3(\reg_out_reg_n_0_[26] ),
        .I4(memory_reg_0_i_18_n_0),
        .I5(\reg_out_reg_n_0_[25] ),
        .O(memory_reg_0_i_31_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    memory_reg_0_i_32
       (.I0(\reg_next_pc_reg_n_0_[22] ),
        .I1(\reg_next_pc_reg_n_0_[21] ),
        .I2(\reg_next_pc_reg_n_0_[20] ),
        .I3(\reg_next_pc_reg_n_0_[19] ),
        .O(memory_reg_0_i_32_n_0));
  LUT6 #(
    .INIT(64'h0000000100010001)) 
    memory_reg_0_i_33
       (.I0(\reg_next_pc_reg_n_0_[23] ),
        .I1(\reg_next_pc_reg_n_0_[29] ),
        .I2(\reg_next_pc_reg_n_0_[30] ),
        .I3(\reg_next_pc_reg_n_0_[31] ),
        .I4(latched_store_reg_n_0),
        .I5(latched_branch_reg_n_0),
        .O(memory_reg_0_i_33_n_0));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_4
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[11] ),
        .I2(\reg_out_reg_n_0_[11] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[11] ),
        .O(ADDRARDADDR[9]));
  LUT6 #(
    .INIT(64'hFE10FE10FEFE1010)) 
    memory_reg_0_i_5
       (.I0(mem_do_prefetch_reg_n_0),
        .I1(mem_do_rinst_reg_n_0),
        .I2(\reg_op1_reg_n_0_[10] ),
        .I3(\reg_out_reg_n_0_[10] ),
        .I4(\reg_next_pc_reg_n_0_[10] ),
        .I5(memory_reg_0_i_18_n_0),
        .O(ADDRARDADDR[8]));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_6
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[9] ),
        .I2(\reg_out_reg_n_0_[9] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[9] ),
        .O(ADDRARDADDR[7]));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_7
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[8] ),
        .I2(\reg_out_reg_n_0_[8] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[8] ),
        .O(ADDRARDADDR[6]));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_8
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[7] ),
        .I2(\reg_out_reg_n_0_[7] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[7] ),
        .O(ADDRARDADDR[5]));
  LUT6 #(
    .INIT(64'hE4E4E4FFE4E4E400)) 
    memory_reg_0_i_9
       (.I0(memory_reg_0_i_18_n_0),
        .I1(\reg_next_pc_reg_n_0_[6] ),
        .I2(\reg_out_reg_n_0_[6] ),
        .I3(mem_do_rinst_reg_n_0),
        .I4(mem_do_prefetch_reg_n_0),
        .I5(\reg_op1_reg_n_0_[6] ),
        .O(ADDRARDADDR[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    memory_reg_1_i_1
       (.I0(Q[7]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\reg_op2_reg_n_0_[15] ),
        .O(p_2_in[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    memory_reg_1_i_2
       (.I0(Q[6]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\reg_op2_reg_n_0_[14] ),
        .O(p_2_in[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    memory_reg_1_i_3
       (.I0(Q[5]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\reg_op2_reg_n_0_[13] ),
        .O(p_2_in[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    memory_reg_1_i_4
       (.I0(Q[4]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\reg_op2_reg_n_0_[12] ),
        .O(p_2_in[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    memory_reg_1_i_5
       (.I0(Q[3]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\reg_op2_reg_n_0_[11] ),
        .O(p_2_in[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    memory_reg_1_i_6
       (.I0(Q[2]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\reg_op2_reg_n_0_[10] ),
        .O(p_2_in[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    memory_reg_1_i_7
       (.I0(Q[1]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\reg_op2_reg_n_0_[9] ),
        .O(p_2_in[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    memory_reg_1_i_8
       (.I0(Q[0]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\reg_op2_reg_n_0_[8] ),
        .O(p_2_in[0]));
  LUT4 #(
    .INIT(16'h1F1D)) 
    memory_reg_1_i_9
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(\mem_wordsize_reg_n_0_[0] ),
        .I2(\reg_op1_reg_n_0_[1] ),
        .I3(\reg_op1_reg_n_0_[0] ),
        .O(p_0_in[1]));
  LUT4 #(
    .INIT(16'hABA8)) 
    memory_reg_2_i_1
       (.I0(Q[7]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op2_reg_n_0_[23] ),
        .O(p_2_in[15]));
  LUT4 #(
    .INIT(16'hABA8)) 
    memory_reg_2_i_2
       (.I0(Q[6]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op2_reg_n_0_[22] ),
        .O(p_2_in[14]));
  LUT4 #(
    .INIT(16'hABA8)) 
    memory_reg_2_i_3
       (.I0(Q[5]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op2_reg_n_0_[21] ),
        .O(p_2_in[13]));
  LUT4 #(
    .INIT(16'hABA8)) 
    memory_reg_2_i_4
       (.I0(Q[4]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op2_reg_n_0_[20] ),
        .O(p_2_in[12]));
  LUT4 #(
    .INIT(16'hABA8)) 
    memory_reg_2_i_5
       (.I0(Q[3]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op2_reg_n_0_[19] ),
        .O(p_2_in[11]));
  LUT4 #(
    .INIT(16'hABA8)) 
    memory_reg_2_i_6
       (.I0(Q[2]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op2_reg_n_0_[18] ),
        .O(p_2_in[10]));
  LUT4 #(
    .INIT(16'hABA8)) 
    memory_reg_2_i_7
       (.I0(Q[1]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op2_reg_n_0_[17] ),
        .O(p_2_in[9]));
  LUT4 #(
    .INIT(16'hABA8)) 
    memory_reg_2_i_8
       (.I0(Q[0]),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op2_reg_n_0_[16] ),
        .O(p_2_in[8]));
  LUT4 #(
    .INIT(16'hDF11)) 
    memory_reg_2_i_9
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(\mem_wordsize_reg_n_0_[0] ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'h0000CCF00000CCAA)) 
    memory_reg_3_i_1
       (.I0(\reg_op2_reg_n_0_[31] ),
        .I1(Q[7]),
        .I2(\reg_op2_reg_n_0_[15] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(memory_reg_3_i_10_n_0),
        .I5(\mem_wordsize_reg_n_0_[0] ),
        .O(p_2_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h04FC)) 
    memory_reg_3_i_10
       (.I0(\reg_op1_reg_n_0_[0] ),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .O(memory_reg_3_i_10_n_0));
  LUT6 #(
    .INIT(64'h0000CCF00000CCAA)) 
    memory_reg_3_i_2
       (.I0(\reg_op2_reg_n_0_[30] ),
        .I1(Q[6]),
        .I2(\reg_op2_reg_n_0_[14] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(memory_reg_3_i_10_n_0),
        .I5(\mem_wordsize_reg_n_0_[0] ),
        .O(p_2_in[22]));
  LUT6 #(
    .INIT(64'h0000CCF00000CCAA)) 
    memory_reg_3_i_3
       (.I0(\reg_op2_reg_n_0_[29] ),
        .I1(Q[5]),
        .I2(\reg_op2_reg_n_0_[13] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(memory_reg_3_i_10_n_0),
        .I5(\mem_wordsize_reg_n_0_[0] ),
        .O(p_2_in[21]));
  LUT6 #(
    .INIT(64'h0000CCF00000CCAA)) 
    memory_reg_3_i_4
       (.I0(\reg_op2_reg_n_0_[28] ),
        .I1(Q[4]),
        .I2(\reg_op2_reg_n_0_[12] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(memory_reg_3_i_10_n_0),
        .I5(\mem_wordsize_reg_n_0_[0] ),
        .O(p_2_in[20]));
  LUT6 #(
    .INIT(64'h0000CCF00000CCAA)) 
    memory_reg_3_i_5
       (.I0(\reg_op2_reg_n_0_[27] ),
        .I1(Q[3]),
        .I2(\reg_op2_reg_n_0_[11] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(memory_reg_3_i_10_n_0),
        .I5(\mem_wordsize_reg_n_0_[0] ),
        .O(p_2_in[19]));
  LUT6 #(
    .INIT(64'h0000CCF00000CCAA)) 
    memory_reg_3_i_6
       (.I0(\reg_op2_reg_n_0_[26] ),
        .I1(Q[2]),
        .I2(\reg_op2_reg_n_0_[10] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(memory_reg_3_i_10_n_0),
        .I5(\mem_wordsize_reg_n_0_[0] ),
        .O(p_2_in[18]));
  LUT6 #(
    .INIT(64'h0000CCF00000CCAA)) 
    memory_reg_3_i_7
       (.I0(\reg_op2_reg_n_0_[25] ),
        .I1(Q[1]),
        .I2(\reg_op2_reg_n_0_[9] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(memory_reg_3_i_10_n_0),
        .I5(\mem_wordsize_reg_n_0_[0] ),
        .O(p_2_in[17]));
  LUT6 #(
    .INIT(64'h0000CCF00000CCAA)) 
    memory_reg_3_i_8
       (.I0(\reg_op2_reg_n_0_[24] ),
        .I1(Q[0]),
        .I2(\reg_op2_reg_n_0_[8] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(memory_reg_3_i_10_n_0),
        .I5(\mem_wordsize_reg_n_0_[0] ),
        .O(p_2_in[16]));
  LUT4 #(
    .INIT(16'hAB8B)) 
    memory_reg_3_i_9
       (.I0(\reg_op1_reg_n_0_[1] ),
        .I1(\mem_wordsize_reg_n_0_[0] ),
        .I2(\mem_wordsize_reg_n_0_[1] ),
        .I3(\reg_op1_reg_n_0_[0] ),
        .O(p_0_in[3]));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    \out_byte[7]_i_1 
       (.I0(\out_byte[7]_i_2_n_0 ),
        .I1(ADDRARDADDR[0]),
        .I2(ADDRARDADDR[11]),
        .I3(ADDRARDADDR[10]),
        .I4(\out_byte[7]_i_3_n_0 ),
        .I5(memory_reg_0_i_17_n_0),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \out_byte[7]_i_2 
       (.I0(memory_reg_0_i_15_n_0),
        .I1(\out_byte[7]_i_4_n_0 ),
        .I2(ADDRARDADDR[5]),
        .I3(ADDRARDADDR[7]),
        .I4(ADDRARDADDR[6]),
        .I5(ADDRARDADDR[2]),
        .O(\out_byte[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \out_byte[7]_i_3 
       (.I0(ADDRARDADDR[9]),
        .I1(memory_reg_0_i_16_n_0),
        .I2(ADDRARDADDR[1]),
        .I3(ADDRARDADDR[4]),
        .O(\out_byte[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h2727227700000000)) 
    \out_byte[7]_i_4 
       (.I0(memory_reg_0_i_25_n_0),
        .I1(\reg_op1_reg_n_0_[10] ),
        .I2(\reg_out_reg_n_0_[10] ),
        .I3(\reg_next_pc_reg_n_0_[10] ),
        .I4(memory_reg_0_i_18_n_0),
        .I5(\out_byte[7]_i_5_n_0 ),
        .O(\out_byte[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h01FD010101FDFDFD)) 
    \out_byte[7]_i_5 
       (.I0(\reg_op1_reg_n_0_[5] ),
        .I1(mem_do_rinst_reg_n_0),
        .I2(mem_do_prefetch_reg_n_0),
        .I3(\reg_out_reg_n_0_[5] ),
        .I4(memory_reg_0_i_18_n_0),
        .I5(\reg_next_pc_reg_n_0_[5] ),
        .O(\out_byte[7]_i_5_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_1_out_carry
       (.CI(1'b0),
        .CO({p_1_out_carry_n_0,p_1_out_carry_n_1,p_1_out_carry_n_2,p_1_out_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,\reg_pc_reg_n_0_[2] ,\reg_pc_reg_n_0_[1] ,1'b0}),
        .O({p_1_out_carry_n_4,p_1_out_carry_n_5,p_1_out_carry_n_6,NLW_p_1_out_carry_O_UNCONNECTED[0]}),
        .S({\reg_pc_reg_n_0_[3] ,p_1_out_carry_i_1_n_0,\reg_pc_reg_n_0_[1] ,1'b0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_1_out_carry__0
       (.CI(p_1_out_carry_n_0),
        .CO({p_1_out_carry__0_n_0,p_1_out_carry__0_n_1,p_1_out_carry__0_n_2,p_1_out_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({p_1_out_carry__0_n_4,p_1_out_carry__0_n_5,p_1_out_carry__0_n_6,p_1_out_carry__0_n_7}),
        .S({\reg_pc_reg_n_0_[7] ,\reg_pc_reg_n_0_[6] ,\reg_pc_reg_n_0_[5] ,\reg_pc_reg_n_0_[4] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_1_out_carry__1
       (.CI(p_1_out_carry__0_n_0),
        .CO({p_1_out_carry__1_n_0,p_1_out_carry__1_n_1,p_1_out_carry__1_n_2,p_1_out_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({p_1_out_carry__1_n_4,p_1_out_carry__1_n_5,p_1_out_carry__1_n_6,p_1_out_carry__1_n_7}),
        .S({\reg_pc_reg_n_0_[11] ,\reg_pc_reg_n_0_[10] ,\reg_pc_reg_n_0_[9] ,\reg_pc_reg_n_0_[8] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_1_out_carry__2
       (.CI(p_1_out_carry__1_n_0),
        .CO({p_1_out_carry__2_n_0,p_1_out_carry__2_n_1,p_1_out_carry__2_n_2,p_1_out_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({p_1_out_carry__2_n_4,p_1_out_carry__2_n_5,p_1_out_carry__2_n_6,p_1_out_carry__2_n_7}),
        .S({\reg_pc_reg_n_0_[15] ,\reg_pc_reg_n_0_[14] ,\reg_pc_reg_n_0_[13] ,\reg_pc_reg_n_0_[12] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_1_out_carry__3
       (.CI(p_1_out_carry__2_n_0),
        .CO({p_1_out_carry__3_n_0,p_1_out_carry__3_n_1,p_1_out_carry__3_n_2,p_1_out_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({p_1_out_carry__3_n_4,p_1_out_carry__3_n_5,p_1_out_carry__3_n_6,p_1_out_carry__3_n_7}),
        .S({\reg_pc_reg_n_0_[19] ,\reg_pc_reg_n_0_[18] ,\reg_pc_reg_n_0_[17] ,\reg_pc_reg_n_0_[16] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_1_out_carry__4
       (.CI(p_1_out_carry__3_n_0),
        .CO({p_1_out_carry__4_n_0,p_1_out_carry__4_n_1,p_1_out_carry__4_n_2,p_1_out_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({p_1_out_carry__4_n_4,p_1_out_carry__4_n_5,p_1_out_carry__4_n_6,p_1_out_carry__4_n_7}),
        .S({\reg_pc_reg_n_0_[23] ,\reg_pc_reg_n_0_[22] ,\reg_pc_reg_n_0_[21] ,\reg_pc_reg_n_0_[20] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_1_out_carry__5
       (.CI(p_1_out_carry__4_n_0),
        .CO({p_1_out_carry__5_n_0,p_1_out_carry__5_n_1,p_1_out_carry__5_n_2,p_1_out_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({p_1_out_carry__5_n_4,p_1_out_carry__5_n_5,p_1_out_carry__5_n_6,p_1_out_carry__5_n_7}),
        .S({\reg_pc_reg_n_0_[27] ,\reg_pc_reg_n_0_[26] ,\reg_pc_reg_n_0_[25] ,\reg_pc_reg_n_0_[24] }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 p_1_out_carry__6
       (.CI(p_1_out_carry__5_n_0),
        .CO({NLW_p_1_out_carry__6_CO_UNCONNECTED[3],p_1_out_carry__6_n_1,p_1_out_carry__6_n_2,p_1_out_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({p_1_out_carry__6_n_4,p_1_out_carry__6_n_5,p_1_out_carry__6_n_6,p_1_out_carry__6_n_7}),
        .S({\reg_pc_reg_n_0_[31] ,\reg_pc_reg_n_0_[30] ,\reg_pc_reg_n_0_[29] ,\reg_pc_reg_n_0_[28] }));
  LUT1 #(
    .INIT(2'h1)) 
    p_1_out_carry_i_1
       (.I0(\reg_pc_reg_n_0_[2] ),
        .O(p_1_out_carry_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 reg_next_pc0_carry
       (.CI(1'b0),
        .CO({reg_next_pc0_carry_n_0,reg_next_pc0_carry_n_1,reg_next_pc0_carry_n_2,reg_next_pc0_carry_n_3}),
        .CYINIT(1'b0),
        .DI(current_pc[4:1]),
        .O({reg_next_pc1_in[4:2],NLW_reg_next_pc0_carry_O_UNCONNECTED[0]}),
        .S({reg_next_pc0_carry_i_1_n_0,reg_next_pc0_carry_i_2_n_0,reg_next_pc0_carry_i_3_n_0,reg_next_pc0_carry_i_4_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 reg_next_pc0_carry__0
       (.CI(reg_next_pc0_carry_n_0),
        .CO({reg_next_pc0_carry__0_n_0,reg_next_pc0_carry__0_n_1,reg_next_pc0_carry__0_n_2,reg_next_pc0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(current_pc[8:5]),
        .O(reg_next_pc1_in[8:5]),
        .S({reg_next_pc0_carry__0_i_1_n_0,reg_next_pc0_carry__0_i_2_n_0,reg_next_pc0_carry__0_i_3_n_0,reg_next_pc0_carry__0_i_4_n_0}));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__0_i_1
       (.I0(current_pc[8]),
        .I1(decoded_imm_uj[8]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__0_i_2
       (.I0(current_pc[7]),
        .I1(decoded_imm_uj[7]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__0_i_3
       (.I0(current_pc[6]),
        .I1(decoded_imm_uj[6]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__0_i_4
       (.I0(current_pc[5]),
        .I1(decoded_imm_uj[5]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__0_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 reg_next_pc0_carry__1
       (.CI(reg_next_pc0_carry__0_n_0),
        .CO({reg_next_pc0_carry__1_n_0,reg_next_pc0_carry__1_n_1,reg_next_pc0_carry__1_n_2,reg_next_pc0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(current_pc[12:9]),
        .O(reg_next_pc1_in[12:9]),
        .S({reg_next_pc0_carry__1_i_1_n_0,reg_next_pc0_carry__1_i_2_n_0,reg_next_pc0_carry__1_i_3_n_0,reg_next_pc0_carry__1_i_4_n_0}));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__1_i_1
       (.I0(current_pc[12]),
        .I1(decoded_imm_uj[12]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__1_i_1_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__1_i_2
       (.I0(current_pc[11]),
        .I1(decoded_imm_uj[11]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__1_i_2_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__1_i_3
       (.I0(current_pc[10]),
        .I1(decoded_imm_uj[10]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__1_i_3_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__1_i_4
       (.I0(current_pc[9]),
        .I1(decoded_imm_uj[9]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__1_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 reg_next_pc0_carry__2
       (.CI(reg_next_pc0_carry__1_n_0),
        .CO({reg_next_pc0_carry__2_n_0,reg_next_pc0_carry__2_n_1,reg_next_pc0_carry__2_n_2,reg_next_pc0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(current_pc[16:13]),
        .O(reg_next_pc1_in[16:13]),
        .S({reg_next_pc0_carry__2_i_1_n_0,reg_next_pc0_carry__2_i_2_n_0,reg_next_pc0_carry__2_i_3_n_0,reg_next_pc0_carry__2_i_4_n_0}));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__2_i_1
       (.I0(current_pc[16]),
        .I1(decoded_rs1_0[1]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__2_i_1_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__2_i_2
       (.I0(current_pc[15]),
        .I1(decoded_rs1_0[0]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__2_i_2_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__2_i_3
       (.I0(current_pc[14]),
        .I1(decoded_imm_uj[14]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__2_i_3_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__2_i_4
       (.I0(current_pc[13]),
        .I1(decoded_imm_uj[13]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__2_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 reg_next_pc0_carry__3
       (.CI(reg_next_pc0_carry__2_n_0),
        .CO({reg_next_pc0_carry__3_n_0,reg_next_pc0_carry__3_n_1,reg_next_pc0_carry__3_n_2,reg_next_pc0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({reg_next_pc0_carry__3_i_1_n_0,current_pc[19:17]}),
        .O(reg_next_pc1_in[20:17]),
        .S({reg_next_pc0_carry__3_i_2_n_0,reg_next_pc0_carry__3_i_3_n_0,reg_next_pc0_carry__3_i_4_n_0,reg_next_pc0_carry__3_i_5_n_0}));
  LUT3 #(
    .INIT(8'h80)) 
    reg_next_pc0_carry__3_i_1
       (.I0(instr_jal),
        .I1(decoder_trigger_reg_n_0),
        .I2(decoded_imm_uj[31]),
        .O(reg_next_pc0_carry__3_i_1_n_0));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__3_i_2
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[20]),
        .I3(\reg_out_reg_n_0_[20] ),
        .I4(\reg_next_pc_reg_n_0_[20] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__3_i_2_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__3_i_3
       (.I0(current_pc[19]),
        .I1(decoded_rs1_0[4]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__3_i_3_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__3_i_4
       (.I0(current_pc[18]),
        .I1(decoded_rs1_0[3]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__3_i_4_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry__3_i_5
       (.I0(current_pc[17]),
        .I1(decoded_rs1_0[2]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry__3_i_5_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 reg_next_pc0_carry__4
       (.CI(reg_next_pc0_carry__3_n_0),
        .CO({reg_next_pc0_carry__4_n_0,reg_next_pc0_carry__4_n_1,reg_next_pc0_carry__4_n_2,reg_next_pc0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({reg_next_pc0_carry__3_i_1_n_0,reg_next_pc0_carry__3_i_1_n_0,reg_next_pc0_carry__3_i_1_n_0,reg_next_pc0_carry__3_i_1_n_0}),
        .O(reg_next_pc1_in[24:21]),
        .S({reg_next_pc0_carry__4_i_1_n_0,reg_next_pc0_carry__4_i_2_n_0,reg_next_pc0_carry__4_i_3_n_0,reg_next_pc0_carry__4_i_4_n_0}));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__4_i_1
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[24]),
        .I3(\reg_out_reg_n_0_[24] ),
        .I4(\reg_next_pc_reg_n_0_[24] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__4_i_1_n_0));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__4_i_2
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[23]),
        .I3(\reg_out_reg_n_0_[23] ),
        .I4(\reg_next_pc_reg_n_0_[23] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__4_i_2_n_0));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__4_i_3
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[22]),
        .I3(\reg_out_reg_n_0_[22] ),
        .I4(\reg_next_pc_reg_n_0_[22] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__4_i_3_n_0));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__4_i_4
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[21]),
        .I3(\reg_out_reg_n_0_[21] ),
        .I4(\reg_next_pc_reg_n_0_[21] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__4_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 reg_next_pc0_carry__5
       (.CI(reg_next_pc0_carry__4_n_0),
        .CO({reg_next_pc0_carry__5_n_0,reg_next_pc0_carry__5_n_1,reg_next_pc0_carry__5_n_2,reg_next_pc0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({reg_next_pc0_carry__3_i_1_n_0,reg_next_pc0_carry__3_i_1_n_0,reg_next_pc0_carry__3_i_1_n_0,reg_next_pc0_carry__3_i_1_n_0}),
        .O(reg_next_pc1_in[28:25]),
        .S({reg_next_pc0_carry__5_i_1_n_0,reg_next_pc0_carry__5_i_2_n_0,reg_next_pc0_carry__5_i_3_n_0,reg_next_pc0_carry__5_i_4_n_0}));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__5_i_1
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[28]),
        .I3(\reg_out_reg_n_0_[28] ),
        .I4(\reg_next_pc_reg_n_0_[28] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__5_i_1_n_0));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__5_i_2
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[27]),
        .I3(\reg_out_reg_n_0_[27] ),
        .I4(\reg_next_pc_reg_n_0_[27] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__5_i_2_n_0));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__5_i_3
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[26]),
        .I3(\reg_out_reg_n_0_[26] ),
        .I4(\reg_next_pc_reg_n_0_[26] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__5_i_3_n_0));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__5_i_4
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[25]),
        .I3(\reg_out_reg_n_0_[25] ),
        .I4(\reg_next_pc_reg_n_0_[25] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__5_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 reg_next_pc0_carry__6
       (.CI(reg_next_pc0_carry__5_n_0),
        .CO({NLW_reg_next_pc0_carry__6_CO_UNCONNECTED[3:2],reg_next_pc0_carry__6_n_2,reg_next_pc0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,reg_next_pc0_carry__3_i_1_n_0,reg_next_pc0_carry__3_i_1_n_0}),
        .O({NLW_reg_next_pc0_carry__6_O_UNCONNECTED[3],reg_next_pc1_in[31:29]}),
        .S({1'b0,reg_next_pc0_carry__6_i_1_n_0,reg_next_pc0_carry__6_i_2_n_0,reg_next_pc0_carry__6_i_3_n_0}));
  LUT6 #(
    .INIT(64'h66555A5566AA5AAA)) 
    reg_next_pc0_carry__6_i_1
       (.I0(reg_next_pc0_carry__3_i_1_n_0),
        .I1(alu_out_q[31]),
        .I2(\reg_out_reg_n_0_[31] ),
        .I3(memory_reg_0_i_18_n_0),
        .I4(latched_stalu_reg_n_0),
        .I5(\reg_next_pc_reg_n_0_[31] ),
        .O(reg_next_pc0_carry__6_i_1_n_0));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__6_i_2
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[30]),
        .I3(\reg_out_reg_n_0_[30] ),
        .I4(\reg_next_pc_reg_n_0_[30] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__6_i_2_n_0));
  LUT6 #(
    .INIT(64'h084C3B7FF7B3C480)) 
    reg_next_pc0_carry__6_i_3
       (.I0(latched_stalu_reg_n_0),
        .I1(memory_reg_0_i_18_n_0),
        .I2(alu_out_q[29]),
        .I3(\reg_out_reg_n_0_[29] ),
        .I4(\reg_next_pc_reg_n_0_[29] ),
        .I5(reg_next_pc0_carry__3_i_1_n_0),
        .O(reg_next_pc0_carry__6_i_3_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry_i_1
       (.I0(current_pc[4]),
        .I1(decoded_imm_uj[4]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry_i_2
       (.I0(current_pc[3]),
        .I1(decoded_imm_uj[3]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h5A9A)) 
    reg_next_pc0_carry_i_3
       (.I0(current_pc[2]),
        .I1(instr_jal),
        .I2(decoder_trigger_reg_n_0),
        .I3(decoded_imm_uj[2]),
        .O(reg_next_pc0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h6AAA)) 
    reg_next_pc0_carry_i_4
       (.I0(current_pc[1]),
        .I1(decoded_imm_uj[1]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc0_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \reg_next_pc[1]_i_1 
       (.I0(current_pc[1]),
        .I1(decoded_imm_uj[1]),
        .I2(decoder_trigger_reg_n_0),
        .I3(instr_jal),
        .O(reg_next_pc1_in[1]));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[10]),
        .Q(\reg_next_pc_reg_n_0_[10] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[11]),
        .Q(\reg_next_pc_reg_n_0_[11] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[12]),
        .Q(\reg_next_pc_reg_n_0_[12] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[13]),
        .Q(\reg_next_pc_reg_n_0_[13] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[14]),
        .Q(\reg_next_pc_reg_n_0_[14] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[15]),
        .Q(\reg_next_pc_reg_n_0_[15] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[16]),
        .Q(\reg_next_pc_reg_n_0_[16] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[17]),
        .Q(\reg_next_pc_reg_n_0_[17] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[18]),
        .Q(\reg_next_pc_reg_n_0_[18] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[19]),
        .Q(\reg_next_pc_reg_n_0_[19] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[1]),
        .Q(\reg_next_pc_reg_n_0_[1] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[20]),
        .Q(\reg_next_pc_reg_n_0_[20] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[21]),
        .Q(\reg_next_pc_reg_n_0_[21] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[22]),
        .Q(\reg_next_pc_reg_n_0_[22] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[23]),
        .Q(\reg_next_pc_reg_n_0_[23] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[24]),
        .Q(\reg_next_pc_reg_n_0_[24] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[25]),
        .Q(\reg_next_pc_reg_n_0_[25] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[26]),
        .Q(\reg_next_pc_reg_n_0_[26] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[27]),
        .Q(\reg_next_pc_reg_n_0_[27] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[28]),
        .Q(\reg_next_pc_reg_n_0_[28] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[29]),
        .Q(\reg_next_pc_reg_n_0_[29] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[2]),
        .Q(\reg_next_pc_reg_n_0_[2] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[30]),
        .Q(\reg_next_pc_reg_n_0_[30] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[31]),
        .Q(\reg_next_pc_reg_n_0_[31] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[3]),
        .Q(\reg_next_pc_reg_n_0_[3] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[4]),
        .Q(\reg_next_pc_reg_n_0_[4] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[5]),
        .Q(\reg_next_pc_reg_n_0_[5] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[6]),
        .Q(\reg_next_pc_reg_n_0_[6] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[7]),
        .Q(\reg_next_pc_reg_n_0_[7] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[8]),
        .Q(\reg_next_pc_reg_n_0_[8] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_next_pc_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(reg_next_pc1_in[9]),
        .Q(\reg_next_pc_reg_n_0_[9] ),
        .R(instr_and_i_1_n_0));
  CARRY4 \reg_op10_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\reg_op10_inferred__0/i__carry_n_0 ,\reg_op10_inferred__0/i__carry_n_1 ,\reg_op10_inferred__0/i__carry_n_2 ,\reg_op10_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[3] ,\reg_op1_reg_n_0_[2] ,\reg_op1_reg_n_0_[1] ,\reg_op1_reg_n_0_[0] }),
        .O({\reg_op10_inferred__0/i__carry_n_4 ,\reg_op10_inferred__0/i__carry_n_5 ,\reg_op10_inferred__0/i__carry_n_6 ,\reg_op10_inferred__0/i__carry_n_7 }),
        .S({i__carry_i_1__0_n_0,i__carry_i_2__0_n_0,i__carry_i_3__0_n_0,i__carry_i_4__0_n_0}));
  CARRY4 \reg_op10_inferred__0/i__carry__0 
       (.CI(\reg_op10_inferred__0/i__carry_n_0 ),
        .CO({\reg_op10_inferred__0/i__carry__0_n_0 ,\reg_op10_inferred__0/i__carry__0_n_1 ,\reg_op10_inferred__0/i__carry__0_n_2 ,\reg_op10_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[7] ,\reg_op1_reg_n_0_[6] ,\reg_op1_reg_n_0_[5] ,\reg_op1_reg_n_0_[4] }),
        .O({\reg_op10_inferred__0/i__carry__0_n_4 ,\reg_op10_inferred__0/i__carry__0_n_5 ,\reg_op10_inferred__0/i__carry__0_n_6 ,\reg_op10_inferred__0/i__carry__0_n_7 }),
        .S({i__carry__0_i_1__0_n_0,i__carry__0_i_2__0_n_0,i__carry__0_i_3__0_n_0,i__carry__0_i_4__0_n_0}));
  CARRY4 \reg_op10_inferred__0/i__carry__1 
       (.CI(\reg_op10_inferred__0/i__carry__0_n_0 ),
        .CO({\reg_op10_inferred__0/i__carry__1_n_0 ,\reg_op10_inferred__0/i__carry__1_n_1 ,\reg_op10_inferred__0/i__carry__1_n_2 ,\reg_op10_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[11] ,\reg_op1_reg_n_0_[10] ,\reg_op1_reg_n_0_[9] ,\reg_op1_reg_n_0_[8] }),
        .O({\reg_op10_inferred__0/i__carry__1_n_4 ,\reg_op10_inferred__0/i__carry__1_n_5 ,\reg_op10_inferred__0/i__carry__1_n_6 ,\reg_op10_inferred__0/i__carry__1_n_7 }),
        .S({i__carry__1_i_1__0_n_0,i__carry__1_i_2__0_n_0,i__carry__1_i_3__0_n_0,i__carry__1_i_4__0_n_0}));
  CARRY4 \reg_op10_inferred__0/i__carry__2 
       (.CI(\reg_op10_inferred__0/i__carry__1_n_0 ),
        .CO({\reg_op10_inferred__0/i__carry__2_n_0 ,\reg_op10_inferred__0/i__carry__2_n_1 ,\reg_op10_inferred__0/i__carry__2_n_2 ,\reg_op10_inferred__0/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[15] ,\reg_op1_reg_n_0_[14] ,\reg_op1_reg_n_0_[13] ,\reg_op1_reg_n_0_[12] }),
        .O({\reg_op10_inferred__0/i__carry__2_n_4 ,\reg_op10_inferred__0/i__carry__2_n_5 ,\reg_op10_inferred__0/i__carry__2_n_6 ,\reg_op10_inferred__0/i__carry__2_n_7 }),
        .S({i__carry__2_i_1__0_n_0,i__carry__2_i_2__0_n_0,i__carry__2_i_3__0_n_0,i__carry__2_i_4__0_n_0}));
  CARRY4 \reg_op10_inferred__0/i__carry__3 
       (.CI(\reg_op10_inferred__0/i__carry__2_n_0 ),
        .CO({\reg_op10_inferred__0/i__carry__3_n_0 ,\reg_op10_inferred__0/i__carry__3_n_1 ,\reg_op10_inferred__0/i__carry__3_n_2 ,\reg_op10_inferred__0/i__carry__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[19] ,\reg_op1_reg_n_0_[18] ,\reg_op1_reg_n_0_[17] ,\reg_op1_reg_n_0_[16] }),
        .O({\reg_op10_inferred__0/i__carry__3_n_4 ,\reg_op10_inferred__0/i__carry__3_n_5 ,\reg_op10_inferred__0/i__carry__3_n_6 ,\reg_op10_inferred__0/i__carry__3_n_7 }),
        .S({i__carry__3_i_1_n_0,i__carry__3_i_2_n_0,i__carry__3_i_3_n_0,i__carry__3_i_4_n_0}));
  CARRY4 \reg_op10_inferred__0/i__carry__4 
       (.CI(\reg_op10_inferred__0/i__carry__3_n_0 ),
        .CO({\reg_op10_inferred__0/i__carry__4_n_0 ,\reg_op10_inferred__0/i__carry__4_n_1 ,\reg_op10_inferred__0/i__carry__4_n_2 ,\reg_op10_inferred__0/i__carry__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[23] ,\reg_op1_reg_n_0_[22] ,\reg_op1_reg_n_0_[21] ,\reg_op1_reg_n_0_[20] }),
        .O({\reg_op10_inferred__0/i__carry__4_n_4 ,\reg_op10_inferred__0/i__carry__4_n_5 ,\reg_op10_inferred__0/i__carry__4_n_6 ,\reg_op10_inferred__0/i__carry__4_n_7 }),
        .S({i__carry__4_i_1_n_0,i__carry__4_i_2_n_0,i__carry__4_i_3_n_0,i__carry__4_i_4_n_0}));
  CARRY4 \reg_op10_inferred__0/i__carry__5 
       (.CI(\reg_op10_inferred__0/i__carry__4_n_0 ),
        .CO({\reg_op10_inferred__0/i__carry__5_n_0 ,\reg_op10_inferred__0/i__carry__5_n_1 ,\reg_op10_inferred__0/i__carry__5_n_2 ,\reg_op10_inferred__0/i__carry__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\reg_op1_reg_n_0_[27] ,\reg_op1_reg_n_0_[26] ,\reg_op1_reg_n_0_[25] ,\reg_op1_reg_n_0_[24] }),
        .O({\reg_op10_inferred__0/i__carry__5_n_4 ,\reg_op10_inferred__0/i__carry__5_n_5 ,\reg_op10_inferred__0/i__carry__5_n_6 ,\reg_op10_inferred__0/i__carry__5_n_7 }),
        .S({i__carry__5_i_1_n_0,i__carry__5_i_2_n_0,i__carry__5_i_3_n_0,i__carry__5_i_4_n_0}));
  CARRY4 \reg_op10_inferred__0/i__carry__6 
       (.CI(\reg_op10_inferred__0/i__carry__5_n_0 ),
        .CO({\NLW_reg_op10_inferred__0/i__carry__6_CO_UNCONNECTED [3],\reg_op10_inferred__0/i__carry__6_n_1 ,\reg_op10_inferred__0/i__carry__6_n_2 ,\reg_op10_inferred__0/i__carry__6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\reg_op1_reg_n_0_[30] ,\reg_op1_reg_n_0_[29] ,\reg_op1_reg_n_0_[28] }),
        .O({\reg_op10_inferred__0/i__carry__6_n_4 ,\reg_op10_inferred__0/i__carry__6_n_5 ,\reg_op10_inferred__0/i__carry__6_n_6 ,\reg_op10_inferred__0/i__carry__6_n_7 }),
        .S({i__carry__6_i_1_n_0,i__carry__6_i_2_n_0,i__carry__6_i_3_n_0,i__carry__6_i_4_n_0}));
  LUT5 #(
    .INIT(32'hFFFFF888)) 
    \reg_op1[0]_i_1 
       (.I0(\reg_op1[0]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[4] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .I4(\reg_op1[0]_i_3_n_0 ),
        .O(\reg_op1[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAAAAA8)) 
    \reg_op1[0]_i_2 
       (.I0(\reg_op1[0]_i_4_n_0 ),
        .I1(instr_sra),
        .I2(instr_srai),
        .I3(instr_srl),
        .I4(instr_srli),
        .O(\reg_op1[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h400040FF40004000)) 
    \reg_op1[0]_i_3 
       (.I0(is_lui_auipc_jal),
        .I1(reg_out1[0]),
        .I2(\reg_op1[0]_i_5_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(\cpu_state_reg_n_0_[2] ),
        .I5(\reg_op10_inferred__0/i__carry_n_7 ),
        .O(\reg_op1[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h44444440)) 
    \reg_op1[0]_i_4 
       (.I0(\cpu_state_reg_n_0_[5] ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_sh_reg_n_0_[4] ),
        .I3(\reg_sh_reg_n_0_[3] ),
        .I4(\reg_sh_reg_n_0_[2] ),
        .O(\reg_op1[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg_op1[0]_i_5 
       (.I0(decoded_rs1_0[0]),
        .I1(decoded_rs1_0[3]),
        .I2(decoded_rs1_0[4]),
        .I3(decoded_rs1_0[2]),
        .I4(decoded_rs1_0[1]),
        .O(\reg_op1[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[10]_i_1 
       (.I0(\reg_op1[10]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[11] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[9] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[10]_i_3_n_0 ),
        .O(\reg_op1[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[10]_i_2 
       (.I0(\reg_op1_reg_n_0_[6] ),
        .I1(\reg_op1[31]_i_7_n_0 ),
        .I2(\reg_op1_reg_n_0_[14] ),
        .I3(\reg_op1[0]_i_2_n_0 ),
        .O(\reg_op1[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[10]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__1_n_5 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[10] ),
        .I4(reg_out1[10]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[11]_i_1 
       (.I0(\reg_op1[11]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[12] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[10] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[11]_i_3_n_0 ),
        .O(\reg_op1[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[11]_i_2 
       (.I0(\reg_op1_reg_n_0_[7] ),
        .I1(\reg_op1[31]_i_7_n_0 ),
        .I2(\reg_op1_reg_n_0_[15] ),
        .I3(\reg_op1[0]_i_2_n_0 ),
        .O(\reg_op1[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[11]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__1_n_4 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[11] ),
        .I4(reg_out1[11]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[12]_i_1 
       (.I0(\reg_op1[12]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[13] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[11] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[12]_i_3_n_0 ),
        .O(\reg_op1[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[12]_i_2 
       (.I0(\reg_op1_reg_n_0_[8] ),
        .I1(\reg_op1[31]_i_7_n_0 ),
        .I2(\reg_op1_reg_n_0_[16] ),
        .I3(\reg_op1[0]_i_2_n_0 ),
        .O(\reg_op1[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[12]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__2_n_7 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[12] ),
        .I4(reg_out1[12]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[13]_i_1 
       (.I0(\reg_op1[13]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[12] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[14] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[13]_i_3_n_0 ),
        .O(\reg_op1[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[13]_i_2 
       (.I0(\reg_op1_reg_n_0_[17] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[9] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[13]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__2_n_6 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[13] ),
        .I4(reg_out1[13]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[14]_i_1 
       (.I0(\reg_op1[14]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[13] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[15] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[14]_i_3_n_0 ),
        .O(\reg_op1[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[14]_i_2 
       (.I0(\reg_op1_reg_n_0_[18] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[10] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[14]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__2_n_5 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[14] ),
        .I4(reg_out1[14]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[15]_i_1 
       (.I0(\reg_op1[15]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[14] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[16] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[15]_i_3_n_0 ),
        .O(\reg_op1[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[15]_i_2 
       (.I0(\reg_op1_reg_n_0_[19] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[11] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[15]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__2_n_4 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[15] ),
        .I4(reg_out1[15]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[16]_i_1 
       (.I0(\reg_op1[16]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[17] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[15] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[16]_i_3_n_0 ),
        .O(\reg_op1[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[16]_i_2 
       (.I0(\reg_op1_reg_n_0_[12] ),
        .I1(\reg_op1[31]_i_7_n_0 ),
        .I2(\reg_op1_reg_n_0_[20] ),
        .I3(\reg_op1[0]_i_2_n_0 ),
        .O(\reg_op1[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[16]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__3_n_7 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[16] ),
        .I4(reg_out1[16]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[17]_i_1 
       (.I0(\reg_op1[17]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[18] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[16] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[17]_i_3_n_0 ),
        .O(\reg_op1[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[17]_i_2 
       (.I0(\reg_op1_reg_n_0_[13] ),
        .I1(\reg_op1[31]_i_7_n_0 ),
        .I2(\reg_op1_reg_n_0_[21] ),
        .I3(\reg_op1[0]_i_2_n_0 ),
        .O(\reg_op1[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[17]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__3_n_6 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[17] ),
        .I4(reg_out1[17]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[18]_i_1 
       (.I0(\reg_op1[18]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[19] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[17] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[18]_i_3_n_0 ),
        .O(\reg_op1[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[18]_i_2 
       (.I0(\reg_op1_reg_n_0_[14] ),
        .I1(\reg_op1[31]_i_7_n_0 ),
        .I2(\reg_op1_reg_n_0_[22] ),
        .I3(\reg_op1[0]_i_2_n_0 ),
        .O(\reg_op1[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[18]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__3_n_5 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[18] ),
        .I4(reg_out1[18]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[19]_i_1 
       (.I0(\reg_op1[19]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[18] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[20] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[19]_i_3_n_0 ),
        .O(\reg_op1[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[19]_i_2 
       (.I0(\reg_op1_reg_n_0_[23] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[15] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[19]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__3_n_4 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[19] ),
        .I4(reg_out1[19]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[19]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFEA)) 
    \reg_op1[1]_i_1 
       (.I0(\reg_op1[1]_i_2_n_0 ),
        .I1(\reg_op1[27]_i_3_n_0 ),
        .I2(\reg_op1_reg_n_0_[2] ),
        .I3(\reg_op1[1]_i_3_n_0 ),
        .O(\reg_op1[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \reg_op1[1]_i_2 
       (.I0(\reg_op1[0]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[5] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[0] ),
        .I4(reg_out1[1]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h080008FF08000800)) 
    \reg_op1[1]_i_3 
       (.I0(\reg_pc_reg_n_0_[1] ),
        .I1(is_lui_auipc_jal),
        .I2(instr_lui),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(\cpu_state_reg_n_0_[2] ),
        .I5(\reg_op10_inferred__0/i__carry_n_6 ),
        .O(\reg_op1[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[20]_i_1 
       (.I0(\reg_op1[20]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[19] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[21] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[20]_i_3_n_0 ),
        .O(\reg_op1[20]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[20]_i_2 
       (.I0(\reg_op1_reg_n_0_[24] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[16] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[20]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__4_n_7 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[20] ),
        .I4(reg_out1[20]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[21]_i_1 
       (.I0(\reg_op1[21]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[20] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[22] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[21]_i_3_n_0 ),
        .O(\reg_op1[21]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[21]_i_2 
       (.I0(\reg_op1_reg_n_0_[25] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[17] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[21]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__4_n_6 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[21] ),
        .I4(reg_out1[21]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[22]_i_1 
       (.I0(\reg_op1[22]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[23] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[21] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[22]_i_3_n_0 ),
        .O(\reg_op1[22]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[22]_i_2 
       (.I0(\reg_op1_reg_n_0_[18] ),
        .I1(\reg_op1[31]_i_7_n_0 ),
        .I2(\reg_op1_reg_n_0_[26] ),
        .I3(\reg_op1[0]_i_2_n_0 ),
        .O(\reg_op1[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[22]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__4_n_5 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[22] ),
        .I4(reg_out1[22]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[23]_i_1 
       (.I0(\reg_op1[23]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[24] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[22] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[23]_i_3_n_0 ),
        .O(\reg_op1[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[23]_i_2 
       (.I0(\reg_op1_reg_n_0_[19] ),
        .I1(\reg_op1[31]_i_7_n_0 ),
        .I2(\reg_op1_reg_n_0_[27] ),
        .I3(\reg_op1[0]_i_2_n_0 ),
        .O(\reg_op1[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[23]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__4_n_4 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[23] ),
        .I4(reg_out1[23]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[24]_i_1 
       (.I0(\reg_op1[24]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[25] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[23] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[24]_i_3_n_0 ),
        .O(\reg_op1[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[24]_i_2 
       (.I0(\reg_op1_reg_n_0_[20] ),
        .I1(\reg_op1[31]_i_7_n_0 ),
        .I2(\reg_op1_reg_n_0_[28] ),
        .I3(\reg_op1[0]_i_2_n_0 ),
        .O(\reg_op1[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[24]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__5_n_7 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[24] ),
        .I4(reg_out1[24]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[25]_i_1 
       (.I0(\reg_op1[25]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[24] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[26] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[25]_i_3_n_0 ),
        .O(\reg_op1[25]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[25]_i_2 
       (.I0(\reg_op1_reg_n_0_[29] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[21] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[25]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__5_n_6 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[25] ),
        .I4(reg_out1[25]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[26]_i_1 
       (.I0(\reg_op1[26]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[25] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[27] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[26]_i_3_n_0 ),
        .O(\reg_op1[26]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[26]_i_2 
       (.I0(\reg_op1_reg_n_0_[30] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[22] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[26]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__5_n_5 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[26] ),
        .I4(reg_out1[26]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[27]_i_1 
       (.I0(\reg_op1[27]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[26] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[28] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[27]_i_4_n_0 ),
        .O(\reg_op1[27]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[27]_i_2 
       (.I0(\reg_op1_reg_n_0_[31] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[23] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAAAAAA8)) 
    \reg_op1[27]_i_3 
       (.I0(\reg_op1[27]_i_5_n_0 ),
        .I1(instr_sra),
        .I2(instr_srai),
        .I3(instr_srl),
        .I4(instr_srli),
        .O(\reg_op1[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[27]_i_4 
       (.I0(\reg_op10_inferred__0/i__carry__5_n_4 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[27] ),
        .I4(reg_out1[27]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[27]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h00000004)) 
    \reg_op1[27]_i_5 
       (.I0(\cpu_state_reg_n_0_[5] ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_sh_reg_n_0_[4] ),
        .I3(\reg_sh_reg_n_0_[3] ),
        .I4(\reg_sh_reg_n_0_[2] ),
        .O(\reg_op1[27]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \reg_op1[27]_i_6 
       (.I0(\cpu_state_reg_n_0_[5] ),
        .I1(is_lui_auipc_jal),
        .I2(instr_lui),
        .O(\reg_op1[27]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_op1[28]_i_1 
       (.I0(\reg_op1[28]_i_2_n_0 ),
        .I1(\reg_op1[31]_i_6_n_0 ),
        .I2(\reg_op1_reg_n_0_[27] ),
        .I3(\reg_op1[28]_i_3_n_0 ),
        .I4(\reg_op1[30]_i_4_n_0 ),
        .O(\reg_op1[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \reg_op1[28]_i_2 
       (.I0(\reg_op1[31]_i_7_n_0 ),
        .I1(\reg_op1_reg_n_0_[24] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[29] ),
        .I4(reg_out1[28]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h080008FF08000800)) 
    \reg_op1[28]_i_3 
       (.I0(\reg_pc_reg_n_0_[28] ),
        .I1(is_lui_auipc_jal),
        .I2(instr_lui),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(\cpu_state_reg_n_0_[2] ),
        .I5(\reg_op10_inferred__0/i__carry__6_n_7 ),
        .O(\reg_op1[28]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_op1[29]_i_1 
       (.I0(\reg_op1[29]_i_2_n_0 ),
        .I1(\reg_op1[31]_i_6_n_0 ),
        .I2(\reg_op1_reg_n_0_[28] ),
        .I3(\reg_op1[29]_i_3_n_0 ),
        .I4(\reg_op1[30]_i_4_n_0 ),
        .O(\reg_op1[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \reg_op1[29]_i_2 
       (.I0(\reg_op1[31]_i_7_n_0 ),
        .I1(\reg_op1_reg_n_0_[25] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[30] ),
        .I4(reg_out1[29]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h080008FF08000800)) 
    \reg_op1[29]_i_3 
       (.I0(\reg_pc_reg_n_0_[29] ),
        .I1(is_lui_auipc_jal),
        .I2(instr_lui),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(\cpu_state_reg_n_0_[2] ),
        .I5(\reg_op10_inferred__0/i__carry__6_n_6 ),
        .O(\reg_op1[29]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFEA)) 
    \reg_op1[2]_i_1 
       (.I0(\reg_op1[2]_i_2_n_0 ),
        .I1(\reg_op1[27]_i_3_n_0 ),
        .I2(\reg_op1_reg_n_0_[3] ),
        .I3(\reg_op1[2]_i_3_n_0 ),
        .O(\reg_op1[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \reg_op1[2]_i_2 
       (.I0(\reg_op1[0]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[6] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .I4(reg_out1[2]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h080008FF08000800)) 
    \reg_op1[2]_i_3 
       (.I0(\reg_pc_reg_n_0_[2] ),
        .I1(is_lui_auipc_jal),
        .I2(instr_lui),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(\cpu_state_reg_n_0_[2] ),
        .I5(\reg_op10_inferred__0/i__carry_n_5 ),
        .O(\reg_op1[2]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_op1[30]_i_1 
       (.I0(\reg_op1[30]_i_2_n_0 ),
        .I1(\reg_op1[31]_i_6_n_0 ),
        .I2(\reg_op1_reg_n_0_[29] ),
        .I3(\reg_op1[30]_i_3_n_0 ),
        .I4(\reg_op1[30]_i_4_n_0 ),
        .O(\reg_op1[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \reg_op1[30]_i_2 
       (.I0(\reg_op1[31]_i_7_n_0 ),
        .I1(\reg_op1_reg_n_0_[26] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[31] ),
        .I4(reg_out1[30]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h080008FF08000800)) 
    \reg_op1[30]_i_3 
       (.I0(\reg_pc_reg_n_0_[30] ),
        .I1(is_lui_auipc_jal),
        .I2(instr_lui),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(\cpu_state_reg_n_0_[2] ),
        .I5(\reg_op10_inferred__0/i__carry__6_n_5 ),
        .O(\reg_op1[30]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000A80000)) 
    \reg_op1[30]_i_4 
       (.I0(\reg_op1_reg_n_0_[31] ),
        .I1(instr_sra),
        .I2(instr_srai),
        .I3(\reg_op1[31]_i_10_n_0 ),
        .I4(\cpu_state_reg_n_0_[2] ),
        .I5(\cpu_state_reg_n_0_[5] ),
        .O(\reg_op1[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF0400AE00)) 
    \reg_op1[31]_i_1 
       (.I0(\cpu_state_reg_n_0_[0] ),
        .I1(\cpu_state_reg_n_0_[1] ),
        .I2(mem_do_wdata),
        .I3(\reg_op1[31]_i_3_n_0 ),
        .I4(mem_do_rdata),
        .I5(\reg_op1[31]_i_4_n_0 ),
        .O(\reg_op1[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \reg_op1[31]_i_10 
       (.I0(\reg_sh_reg_n_0_[2] ),
        .I1(\reg_sh_reg_n_0_[3] ),
        .I2(\reg_sh_reg_n_0_[4] ),
        .O(\reg_op1[31]_i_10_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \reg_op1[31]_i_11 
       (.I0(\cpu_state_reg_n_0_[5] ),
        .I1(\reg_op1[0]_i_5_n_0 ),
        .I2(is_lui_auipc_jal),
        .O(\reg_op1[31]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[31]_i_2 
       (.I0(\reg_op1[31]_i_5_n_0 ),
        .I1(\reg_op1_reg_n_0_[30] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[27] ),
        .I4(\reg_op1[31]_i_7_n_0 ),
        .I5(\reg_op1[31]_i_8_n_0 ),
        .O(\reg_op1[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h70)) 
    \reg_op1[31]_i_3 
       (.I0(instr_jal_i_3_n_0),
        .I1(mem_do_prefetch_reg_n_0),
        .I2(resetn_IBUF),
        .O(\reg_op1[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0200020003000000)) 
    \reg_op1[31]_i_4 
       (.I0(\reg_op1[31]_i_9_n_0 ),
        .I1(\cpu_state_reg_n_0_[0] ),
        .I2(\cpu_state_reg_n_0_[1] ),
        .I3(resetn_IBUF),
        .I4(\cpu_state_reg_n_0_[5] ),
        .I5(\cpu_state_reg_n_0_[2] ),
        .O(\reg_op1[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E000FF00E00000)) 
    \reg_op1[31]_i_5 
       (.I0(instr_srai),
        .I1(instr_sra),
        .I2(\reg_op1_reg_n_0_[31] ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(\cpu_state_reg_n_0_[2] ),
        .I5(\reg_op10_inferred__0/i__carry__6_n_4 ),
        .O(\reg_op1[31]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h08080800)) 
    \reg_op1[31]_i_6 
       (.I0(\reg_op1[31]_i_10_n_0 ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_slli),
        .I4(instr_sll),
        .O(\reg_op1[31]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h04040400)) 
    \reg_op1[31]_i_7 
       (.I0(\reg_op1[31]_i_10_n_0 ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_slli),
        .I4(instr_sll),
        .O(\reg_op1[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF008000800080)) 
    \reg_op1[31]_i_8 
       (.I0(\reg_pc_reg_n_0_[31] ),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(is_lui_auipc_jal),
        .I3(instr_lui),
        .I4(reg_out1[31]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[31]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg_op1[31]_i_9 
       (.I0(\reg_sh_reg_n_0_[4] ),
        .I1(\reg_sh_reg_n_0_[3] ),
        .I2(\reg_sh_reg_n_0_[2] ),
        .I3(\reg_sh_reg_n_0_[1] ),
        .I4(\reg_sh_reg_n_0_[0] ),
        .O(\reg_op1[31]_i_9_n_0 ));
  LUT4 #(
    .INIT(16'hFFEA)) 
    \reg_op1[3]_i_1 
       (.I0(\reg_op1[3]_i_2_n_0 ),
        .I1(\reg_op1[27]_i_3_n_0 ),
        .I2(\reg_op1_reg_n_0_[4] ),
        .I3(\reg_op1[3]_i_3_n_0 ),
        .O(\reg_op1[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \reg_op1[3]_i_2 
       (.I0(\reg_op1[0]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[7] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[2] ),
        .I4(reg_out1[3]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h080008FF08000800)) 
    \reg_op1[3]_i_3 
       (.I0(\reg_pc_reg_n_0_[3] ),
        .I1(is_lui_auipc_jal),
        .I2(instr_lui),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(\cpu_state_reg_n_0_[2] ),
        .I5(\reg_op10_inferred__0/i__carry_n_4 ),
        .O(\reg_op1[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[4]_i_1 
       (.I0(\reg_op1[4]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[5] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[3] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[4]_i_3_n_0 ),
        .O(\reg_op1[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[4]_i_2 
       (.I0(\reg_op1_reg_n_0_[8] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[4]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__0_n_7 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[4] ),
        .I4(reg_out1[4]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[5]_i_1 
       (.I0(\reg_op1[5]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[6] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[4] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[5]_i_3_n_0 ),
        .O(\reg_op1[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[5]_i_2 
       (.I0(\reg_op1_reg_n_0_[9] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[1] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[5]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__0_n_6 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[5] ),
        .I4(reg_out1[5]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[6]_i_1 
       (.I0(\reg_op1[6]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[7] ),
        .I2(\reg_op1[27]_i_3_n_0 ),
        .I3(\reg_op1_reg_n_0_[5] ),
        .I4(\reg_op1[31]_i_6_n_0 ),
        .I5(\reg_op1[6]_i_3_n_0 ),
        .O(\reg_op1[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[6]_i_2 
       (.I0(\reg_op1_reg_n_0_[10] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[2] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[6]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__0_n_5 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[6] ),
        .I4(reg_out1[6]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[7]_i_1 
       (.I0(\reg_op1[7]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[6] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[8] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[7]_i_3_n_0 ),
        .O(\reg_op1[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[7]_i_2 
       (.I0(\reg_op1_reg_n_0_[11] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[3] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[7]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__0_n_4 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[7] ),
        .I4(reg_out1[7]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[8]_i_1 
       (.I0(\reg_op1[8]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[7] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[9] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[8]_i_3_n_0 ),
        .O(\reg_op1[8]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[8]_i_2 
       (.I0(\reg_op1_reg_n_0_[12] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[4] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[8]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__1_n_7 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[8] ),
        .I4(reg_out1[8]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_op1[9]_i_1 
       (.I0(\reg_op1[9]_i_2_n_0 ),
        .I1(\reg_op1_reg_n_0_[8] ),
        .I2(\reg_op1[31]_i_6_n_0 ),
        .I3(\reg_op1_reg_n_0_[10] ),
        .I4(\reg_op1[27]_i_3_n_0 ),
        .I5(\reg_op1[9]_i_3_n_0 ),
        .O(\reg_op1[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \reg_op1[9]_i_2 
       (.I0(\reg_op1_reg_n_0_[13] ),
        .I1(\reg_op1[0]_i_2_n_0 ),
        .I2(\reg_op1_reg_n_0_[5] ),
        .I3(\reg_op1[31]_i_7_n_0 ),
        .O(\reg_op1[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \reg_op1[9]_i_3 
       (.I0(\reg_op10_inferred__0/i__carry__1_n_6 ),
        .I1(decoder_pseudo_trigger_i_2_n_0),
        .I2(\reg_op1[27]_i_6_n_0 ),
        .I3(\reg_pc_reg_n_0_[9] ),
        .I4(reg_out1[9]),
        .I5(\reg_op1[31]_i_11_n_0 ),
        .O(\reg_op1[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[0]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[10]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[11]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[12]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[13]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[14]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[15]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[16]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[17]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[18]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[19]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[1]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[20]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[21]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[22]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[23]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[24]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[25]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[26]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[27]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[28]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[29]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[2]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[30]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[31]_i_2_n_0 ),
        .Q(\reg_op1_reg_n_0_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[3]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[4]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[5]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[6]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[7]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[8]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op1_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op1[31]_i_1_n_0 ),
        .D(\reg_op1[9]_i_1_n_0 ),
        .Q(\reg_op1_reg_n_0_[9] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[0]_i_1 
       (.I0(reg_sh1[0]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[0] ),
        .O(reg_op2[0]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[10]_i_1 
       (.I0(reg_sh1[10]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[10] ),
        .O(reg_op2[10]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[11]_i_1 
       (.I0(reg_sh1[11]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[11] ),
        .O(reg_op2[11]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[12]_i_1 
       (.I0(reg_sh1[12]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[12] ),
        .O(reg_op2[12]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[13]_i_1 
       (.I0(reg_sh1[13]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[13] ),
        .O(reg_op2[13]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[14]_i_1 
       (.I0(reg_sh1[14]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[14] ),
        .O(reg_op2[14]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[15]_i_1 
       (.I0(reg_sh1[15]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[15] ),
        .O(reg_op2[15]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[16]_i_1 
       (.I0(reg_sh1[16]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[16] ),
        .O(reg_op2[16]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[17]_i_1 
       (.I0(reg_sh1[17]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[17] ),
        .O(reg_op2[17]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[18]_i_1 
       (.I0(reg_sh1[18]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[18] ),
        .O(reg_op2[18]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[19]_i_1 
       (.I0(reg_sh1[19]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[19] ),
        .O(reg_op2[19]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[1]_i_1 
       (.I0(reg_sh1[1]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[1] ),
        .O(reg_op2[1]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[20]_i_1 
       (.I0(reg_sh1[20]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[20] ),
        .O(reg_op2[20]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[21]_i_1 
       (.I0(reg_sh1[21]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[21] ),
        .O(reg_op2[21]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[22]_i_1 
       (.I0(reg_sh1[22]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[22] ),
        .O(reg_op2[22]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[23]_i_1 
       (.I0(reg_sh1[23]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[23] ),
        .O(reg_op2[23]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[24]_i_1 
       (.I0(reg_sh1[24]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[24] ),
        .O(reg_op2[24]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[25]_i_1 
       (.I0(reg_sh1[25]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[25] ),
        .O(reg_op2[25]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[26]_i_1 
       (.I0(reg_sh1[26]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[26] ),
        .O(reg_op2[26]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[27]_i_1 
       (.I0(reg_sh1[27]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[27] ),
        .O(reg_op2[27]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[28]_i_1 
       (.I0(reg_sh1[28]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[28] ),
        .O(reg_op2[28]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[29]_i_1 
       (.I0(reg_sh1[29]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[29] ),
        .O(reg_op2[29]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[2]_i_1 
       (.I0(reg_sh1[2]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[2] ),
        .O(reg_op2[2]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[30]_i_1 
       (.I0(reg_sh1[30]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[30] ),
        .O(reg_op2[30]));
  LUT2 #(
    .INIT(4'h8)) 
    \reg_op2[31]_i_1 
       (.I0(\cpu_state_reg_n_0_[5] ),
        .I1(resetn_IBUF),
        .O(\reg_op2[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[31]_i_2 
       (.I0(reg_sh1[31]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[31] ),
        .O(reg_op2[31]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    \reg_op2[31]_i_3 
       (.I0(\cpu_state[3]_i_3_n_0 ),
        .I1(decoded_imm_uj[4]),
        .I2(decoded_imm_uj[11]),
        .I3(decoded_imm_uj[3]),
        .I4(decoded_imm_uj[2]),
        .I5(decoded_imm_uj[1]),
        .O(\reg_op2[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[3]_i_1 
       (.I0(reg_sh1[3]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[3] ),
        .O(reg_op2[3]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[4]_i_1 
       (.I0(reg_sh1[4]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[4] ),
        .O(reg_op2[4]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[5]_i_1 
       (.I0(reg_sh1[5]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[5] ),
        .O(reg_op2[5]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[6]_i_1 
       (.I0(reg_sh1[6]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[6] ),
        .O(reg_op2[6]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[7]_i_1 
       (.I0(reg_sh1[7]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[7] ),
        .O(reg_op2[7]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[8]_i_1 
       (.I0(reg_sh1[8]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[8] ),
        .O(reg_op2[8]));
  LUT5 #(
    .INIT(32'hFFF88888)) 
    \reg_op2[9]_i_1 
       (.I0(reg_sh1[9]),
        .I1(\reg_op2[31]_i_3_n_0 ),
        .I2(is_lui_auipc_jal),
        .I3(is_jalr_addi_slti_sltiu_xori_ori_andi),
        .I4(\decoded_imm_reg_n_0_[9] ),
        .O(reg_op2[9]));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[10]),
        .Q(\reg_op2_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[11]),
        .Q(\reg_op2_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[12]),
        .Q(\reg_op2_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[13]),
        .Q(\reg_op2_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[14]),
        .Q(\reg_op2_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[15]),
        .Q(\reg_op2_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[16]),
        .Q(\reg_op2_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[17]),
        .Q(\reg_op2_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[18]),
        .Q(\reg_op2_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[19]),
        .Q(\reg_op2_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[1]),
        .Q(Q[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[20]),
        .Q(\reg_op2_reg_n_0_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[21]),
        .Q(\reg_op2_reg_n_0_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[22]),
        .Q(\reg_op2_reg_n_0_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[23]),
        .Q(\reg_op2_reg_n_0_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[24]),
        .Q(\reg_op2_reg_n_0_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[25]),
        .Q(\reg_op2_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[26]),
        .Q(\reg_op2_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[27]),
        .Q(\reg_op2_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[28]),
        .Q(\reg_op2_reg_n_0_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[29]),
        .Q(\reg_op2_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[2]),
        .Q(Q[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[30]),
        .Q(\reg_op2_reg_n_0_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[31]),
        .Q(\reg_op2_reg_n_0_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[3]),
        .Q(Q[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[4]),
        .Q(Q[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[5]),
        .Q(Q[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[6]),
        .Q(Q[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[7]),
        .Q(Q[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[8]),
        .Q(\reg_op2_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_op2_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(\reg_op2[31]_i_1_n_0 ),
        .D(reg_op2[9]),
        .Q(\reg_op2_reg_n_0_[9] ),
        .R(1'b0));
  CARRY4 reg_out0_carry
       (.CI(1'b0),
        .CO({reg_out0_carry_n_0,reg_out0_carry_n_1,reg_out0_carry_n_2,reg_out0_carry_n_3}),
        .CYINIT(1'b0),
        .DI({\reg_pc_reg_n_0_[4] ,\reg_pc_reg_n_0_[3] ,\reg_pc_reg_n_0_[2] ,\reg_pc_reg_n_0_[1] }),
        .O({reg_out0_carry_n_4,reg_out0_carry_n_5,reg_out0_carry_n_6,NLW_reg_out0_carry_O_UNCONNECTED[0]}),
        .S({reg_out0_carry_i_1_n_0,reg_out0_carry_i_2_n_0,reg_out0_carry_i_3_n_0,reg_out0_carry_i_4_n_0}));
  CARRY4 reg_out0_carry__0
       (.CI(reg_out0_carry_n_0),
        .CO({reg_out0_carry__0_n_0,reg_out0_carry__0_n_1,reg_out0_carry__0_n_2,reg_out0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({\reg_pc_reg_n_0_[8] ,\reg_pc_reg_n_0_[7] ,\reg_pc_reg_n_0_[6] ,\reg_pc_reg_n_0_[5] }),
        .O({reg_out0_carry__0_n_4,reg_out0_carry__0_n_5,reg_out0_carry__0_n_6,reg_out0_carry__0_n_7}),
        .S({reg_out0_carry__0_i_1_n_0,reg_out0_carry__0_i_2_n_0,reg_out0_carry__0_i_3_n_0,reg_out0_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__0_i_1
       (.I0(\reg_pc_reg_n_0_[8] ),
        .I1(\decoded_imm_reg_n_0_[8] ),
        .O(reg_out0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__0_i_2
       (.I0(\reg_pc_reg_n_0_[7] ),
        .I1(\decoded_imm_reg_n_0_[7] ),
        .O(reg_out0_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__0_i_3
       (.I0(\reg_pc_reg_n_0_[6] ),
        .I1(\decoded_imm_reg_n_0_[6] ),
        .O(reg_out0_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__0_i_4
       (.I0(\reg_pc_reg_n_0_[5] ),
        .I1(\decoded_imm_reg_n_0_[5] ),
        .O(reg_out0_carry__0_i_4_n_0));
  CARRY4 reg_out0_carry__1
       (.CI(reg_out0_carry__0_n_0),
        .CO({reg_out0_carry__1_n_0,reg_out0_carry__1_n_1,reg_out0_carry__1_n_2,reg_out0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({\reg_pc_reg_n_0_[12] ,\reg_pc_reg_n_0_[11] ,\reg_pc_reg_n_0_[10] ,\reg_pc_reg_n_0_[9] }),
        .O({reg_out0_carry__1_n_4,reg_out0_carry__1_n_5,reg_out0_carry__1_n_6,reg_out0_carry__1_n_7}),
        .S({reg_out0_carry__1_i_1_n_0,reg_out0_carry__1_i_2_n_0,reg_out0_carry__1_i_3_n_0,reg_out0_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__1_i_1
       (.I0(\reg_pc_reg_n_0_[12] ),
        .I1(\decoded_imm_reg_n_0_[12] ),
        .O(reg_out0_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__1_i_2
       (.I0(\reg_pc_reg_n_0_[11] ),
        .I1(\decoded_imm_reg_n_0_[11] ),
        .O(reg_out0_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__1_i_3
       (.I0(\reg_pc_reg_n_0_[10] ),
        .I1(\decoded_imm_reg_n_0_[10] ),
        .O(reg_out0_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__1_i_4
       (.I0(\reg_pc_reg_n_0_[9] ),
        .I1(\decoded_imm_reg_n_0_[9] ),
        .O(reg_out0_carry__1_i_4_n_0));
  CARRY4 reg_out0_carry__2
       (.CI(reg_out0_carry__1_n_0),
        .CO({reg_out0_carry__2_n_0,reg_out0_carry__2_n_1,reg_out0_carry__2_n_2,reg_out0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({\reg_pc_reg_n_0_[16] ,\reg_pc_reg_n_0_[15] ,\reg_pc_reg_n_0_[14] ,\reg_pc_reg_n_0_[13] }),
        .O({reg_out0_carry__2_n_4,reg_out0_carry__2_n_5,reg_out0_carry__2_n_6,reg_out0_carry__2_n_7}),
        .S({reg_out0_carry__2_i_1_n_0,reg_out0_carry__2_i_2_n_0,reg_out0_carry__2_i_3_n_0,reg_out0_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__2_i_1
       (.I0(\reg_pc_reg_n_0_[16] ),
        .I1(\decoded_imm_reg_n_0_[16] ),
        .O(reg_out0_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__2_i_2
       (.I0(\reg_pc_reg_n_0_[15] ),
        .I1(\decoded_imm_reg_n_0_[15] ),
        .O(reg_out0_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__2_i_3
       (.I0(\reg_pc_reg_n_0_[14] ),
        .I1(\decoded_imm_reg_n_0_[14] ),
        .O(reg_out0_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__2_i_4
       (.I0(\reg_pc_reg_n_0_[13] ),
        .I1(\decoded_imm_reg_n_0_[13] ),
        .O(reg_out0_carry__2_i_4_n_0));
  CARRY4 reg_out0_carry__3
       (.CI(reg_out0_carry__2_n_0),
        .CO({reg_out0_carry__3_n_0,reg_out0_carry__3_n_1,reg_out0_carry__3_n_2,reg_out0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({\reg_pc_reg_n_0_[20] ,\reg_pc_reg_n_0_[19] ,\reg_pc_reg_n_0_[18] ,\reg_pc_reg_n_0_[17] }),
        .O({reg_out0_carry__3_n_4,reg_out0_carry__3_n_5,reg_out0_carry__3_n_6,reg_out0_carry__3_n_7}),
        .S({reg_out0_carry__3_i_1_n_0,reg_out0_carry__3_i_2_n_0,reg_out0_carry__3_i_3_n_0,reg_out0_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__3_i_1
       (.I0(\reg_pc_reg_n_0_[20] ),
        .I1(\decoded_imm_reg_n_0_[20] ),
        .O(reg_out0_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__3_i_2
       (.I0(\reg_pc_reg_n_0_[19] ),
        .I1(\decoded_imm_reg_n_0_[19] ),
        .O(reg_out0_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__3_i_3
       (.I0(\reg_pc_reg_n_0_[18] ),
        .I1(\decoded_imm_reg_n_0_[18] ),
        .O(reg_out0_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__3_i_4
       (.I0(\reg_pc_reg_n_0_[17] ),
        .I1(\decoded_imm_reg_n_0_[17] ),
        .O(reg_out0_carry__3_i_4_n_0));
  CARRY4 reg_out0_carry__4
       (.CI(reg_out0_carry__3_n_0),
        .CO({reg_out0_carry__4_n_0,reg_out0_carry__4_n_1,reg_out0_carry__4_n_2,reg_out0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({\reg_pc_reg_n_0_[24] ,\reg_pc_reg_n_0_[23] ,\reg_pc_reg_n_0_[22] ,\reg_pc_reg_n_0_[21] }),
        .O({reg_out0_carry__4_n_4,reg_out0_carry__4_n_5,reg_out0_carry__4_n_6,reg_out0_carry__4_n_7}),
        .S({reg_out0_carry__4_i_1_n_0,reg_out0_carry__4_i_2_n_0,reg_out0_carry__4_i_3_n_0,reg_out0_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__4_i_1
       (.I0(\reg_pc_reg_n_0_[24] ),
        .I1(\decoded_imm_reg_n_0_[24] ),
        .O(reg_out0_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__4_i_2
       (.I0(\reg_pc_reg_n_0_[23] ),
        .I1(\decoded_imm_reg_n_0_[23] ),
        .O(reg_out0_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__4_i_3
       (.I0(\reg_pc_reg_n_0_[22] ),
        .I1(\decoded_imm_reg_n_0_[22] ),
        .O(reg_out0_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__4_i_4
       (.I0(\reg_pc_reg_n_0_[21] ),
        .I1(\decoded_imm_reg_n_0_[21] ),
        .O(reg_out0_carry__4_i_4_n_0));
  CARRY4 reg_out0_carry__5
       (.CI(reg_out0_carry__4_n_0),
        .CO({reg_out0_carry__5_n_0,reg_out0_carry__5_n_1,reg_out0_carry__5_n_2,reg_out0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({\reg_pc_reg_n_0_[28] ,\reg_pc_reg_n_0_[27] ,\reg_pc_reg_n_0_[26] ,\reg_pc_reg_n_0_[25] }),
        .O({reg_out0_carry__5_n_4,reg_out0_carry__5_n_5,reg_out0_carry__5_n_6,reg_out0_carry__5_n_7}),
        .S({reg_out0_carry__5_i_1_n_0,reg_out0_carry__5_i_2_n_0,reg_out0_carry__5_i_3_n_0,reg_out0_carry__5_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__5_i_1
       (.I0(\reg_pc_reg_n_0_[28] ),
        .I1(\decoded_imm_reg_n_0_[28] ),
        .O(reg_out0_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__5_i_2
       (.I0(\reg_pc_reg_n_0_[27] ),
        .I1(\decoded_imm_reg_n_0_[27] ),
        .O(reg_out0_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__5_i_3
       (.I0(\reg_pc_reg_n_0_[26] ),
        .I1(\decoded_imm_reg_n_0_[26] ),
        .O(reg_out0_carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__5_i_4
       (.I0(\reg_pc_reg_n_0_[25] ),
        .I1(\decoded_imm_reg_n_0_[25] ),
        .O(reg_out0_carry__5_i_4_n_0));
  CARRY4 reg_out0_carry__6
       (.CI(reg_out0_carry__5_n_0),
        .CO({NLW_reg_out0_carry__6_CO_UNCONNECTED[3:2],reg_out0_carry__6_n_2,reg_out0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,\reg_pc_reg_n_0_[30] ,\reg_pc_reg_n_0_[29] }),
        .O({NLW_reg_out0_carry__6_O_UNCONNECTED[3],reg_out0_carry__6_n_5,reg_out0_carry__6_n_6,reg_out0_carry__6_n_7}),
        .S({1'b0,reg_out0_carry__6_i_1_n_0,reg_out0_carry__6_i_2_n_0,reg_out0_carry__6_i_3_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__6_i_1
       (.I0(\decoded_imm_reg_n_0_[31] ),
        .I1(\reg_pc_reg_n_0_[31] ),
        .O(reg_out0_carry__6_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__6_i_2
       (.I0(\reg_pc_reg_n_0_[30] ),
        .I1(\decoded_imm_reg_n_0_[30] ),
        .O(reg_out0_carry__6_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry__6_i_3
       (.I0(\reg_pc_reg_n_0_[29] ),
        .I1(\decoded_imm_reg_n_0_[29] ),
        .O(reg_out0_carry__6_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry_i_1
       (.I0(\reg_pc_reg_n_0_[4] ),
        .I1(\decoded_imm_reg_n_0_[4] ),
        .O(reg_out0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry_i_2
       (.I0(\reg_pc_reg_n_0_[3] ),
        .I1(\decoded_imm_reg_n_0_[3] ),
        .O(reg_out0_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry_i_3
       (.I0(\reg_pc_reg_n_0_[2] ),
        .I1(\decoded_imm_reg_n_0_[2] ),
        .O(reg_out0_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    reg_out0_carry_i_4
       (.I0(\reg_pc_reg_n_0_[1] ),
        .I1(\decoded_imm_reg_n_0_[1] ),
        .O(reg_out0_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFCCFFC8)) 
    \reg_out[0]_i_1 
       (.I0(\reg_out[6]_i_4_n_0 ),
        .I1(\reg_out[0]_i_2_n_0 ),
        .I2(\reg_out[0]_i_3_n_0 ),
        .I3(\reg_out[0]_i_4_n_0 ),
        .I4(\reg_out[0]_i_5_n_0 ),
        .I5(\reg_out[0]_i_6_n_0 ),
        .O(\reg_out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hBEAEBAAA)) 
    \reg_out[0]_i_2 
       (.I0(\reg_out[0]_i_7_n_0 ),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(mem_rdata[8]),
        .I4(mem_rdata[16]),
        .O(\reg_out[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5554000000000000)) 
    \reg_out[0]_i_3 
       (.I0(\reg_out[7]_i_6_n_0 ),
        .I1(latched_is_lb_reg_n_0),
        .I2(latched_is_lh_reg_n_0),
        .I3(latched_is_lu_reg_n_0),
        .I4(\cpu_state_reg_n_0_[0] ),
        .I5(mem_rdata[0]),
        .O(\reg_out[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h888888888F888888)) 
    \reg_out[0]_i_4 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[0] ),
        .I2(\reg_out[0]_i_8_n_0 ),
        .I3(data3[0]),
        .I4(\cpu_state_reg_n_0_[5] ),
        .I5(instr_rdcycle),
        .O(\reg_out[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFE00000000000000)) 
    \reg_out[0]_i_5 
       (.I0(latched_is_lb_reg_n_0),
        .I1(latched_is_lh_reg_n_0),
        .I2(latched_is_lu_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\reg_out[7]_i_6_n_0 ),
        .I5(mem_rdata[16]),
        .O(\reg_out[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAEAEAEAEAEAEA)) 
    \reg_out[0]_i_6 
       (.I0(\reg_out[0]_i_9_n_0 ),
        .I1(\decoded_imm_reg_n_0_[0] ),
        .I2(\cpu_state_reg_n_0_[3] ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .I5(count_cycle_reg[0]),
        .O(\reg_out[0]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hF55D555D)) 
    \reg_out[0]_i_7 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(mem_rdata[0]),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .I4(mem_rdata[24]),
        .O(\reg_out[0]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \reg_out[0]_i_8 
       (.I0(instr_rdcycleh),
        .I1(instr_rdinstr),
        .I2(instr_rdinstrh),
        .O(\reg_out[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000ACA000000000)) 
    \reg_out[0]_i_9 
       (.I0(count_cycle_reg[32]),
        .I1(\count_instr_reg_n_0_[0] ),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdcycle),
        .I5(\cpu_state_reg_n_0_[5] ),
        .O(\reg_out[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_out[10]_i_1 
       (.I0(\reg_out[10]_i_2_n_0 ),
        .I1(\reg_out[14]_i_3_n_0 ),
        .I2(mem_rdata[26]),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__1_n_6),
        .I5(\reg_out[15]_i_4_n_0 ),
        .O(\reg_out[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF8888888)) 
    \reg_out[10]_i_2 
       (.I0(\reg_out[14]_i_4_n_0 ),
        .I1(mem_rdata[10]),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycle),
        .I4(count_cycle_reg[10]),
        .I5(\reg_out[10]_i_3_n_0 ),
        .O(\reg_out[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[10]_i_3 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[10] ),
        .I2(\reg_out[10]_i_4_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[10]_i_4 
       (.I0(\count_instr_reg_n_0_[10] ),
        .I1(count_cycle_reg[42]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[10]),
        .O(\reg_out[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_out[11]_i_1 
       (.I0(\reg_out[11]_i_2_n_0 ),
        .I1(\reg_out[14]_i_3_n_0 ),
        .I2(mem_rdata[27]),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__1_n_5),
        .I5(\reg_out[15]_i_4_n_0 ),
        .O(\reg_out[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF8888888)) 
    \reg_out[11]_i_2 
       (.I0(\reg_out[14]_i_4_n_0 ),
        .I1(mem_rdata[11]),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycle),
        .I4(count_cycle_reg[11]),
        .I5(\reg_out[11]_i_3_n_0 ),
        .O(\reg_out[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[11]_i_3 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[11] ),
        .I2(\reg_out[11]_i_4_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[11]_i_4 
       (.I0(\count_instr_reg_n_0_[11] ),
        .I1(count_cycle_reg[43]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[11]),
        .O(\reg_out[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_out[12]_i_1 
       (.I0(\reg_out[12]_i_2_n_0 ),
        .I1(\reg_out[14]_i_3_n_0 ),
        .I2(mem_rdata[28]),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[12] ),
        .I5(\reg_out[15]_i_4_n_0 ),
        .O(\reg_out[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF8888888)) 
    \reg_out[12]_i_2 
       (.I0(\reg_out[14]_i_4_n_0 ),
        .I1(mem_rdata[12]),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycle),
        .I4(count_cycle_reg[12]),
        .I5(\reg_out[12]_i_3_n_0 ),
        .O(\reg_out[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[12]_i_3 
       (.I0(\cpu_state_reg_n_0_[3] ),
        .I1(reg_out0_carry__1_n_4),
        .I2(\reg_out[12]_i_4_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[12]_i_4 
       (.I0(\count_instr_reg_n_0_[12] ),
        .I1(count_cycle_reg[44]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[12]),
        .O(\reg_out[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_out[13]_i_1 
       (.I0(\reg_out[13]_i_2_n_0 ),
        .I1(\reg_out[14]_i_3_n_0 ),
        .I2(mem_rdata[29]),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__2_n_7),
        .I5(\reg_out[15]_i_4_n_0 ),
        .O(\reg_out[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF8888888)) 
    \reg_out[13]_i_2 
       (.I0(\reg_out[14]_i_4_n_0 ),
        .I1(mem_rdata[13]),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycle),
        .I4(count_cycle_reg[13]),
        .I5(\reg_out[13]_i_3_n_0 ),
        .O(\reg_out[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[13]_i_3 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[13] ),
        .I2(\reg_out[13]_i_4_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[13]_i_4 
       (.I0(\count_instr_reg_n_0_[13] ),
        .I1(count_cycle_reg[45]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[13]),
        .O(\reg_out[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_out[14]_i_1 
       (.I0(\reg_out[14]_i_2_n_0 ),
        .I1(\reg_out[14]_i_3_n_0 ),
        .I2(mem_rdata[30]),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__2_n_6),
        .I5(\reg_out[15]_i_4_n_0 ),
        .O(\reg_out[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF8888888)) 
    \reg_out[14]_i_2 
       (.I0(\reg_out[14]_i_4_n_0 ),
        .I1(mem_rdata[14]),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycle),
        .I4(count_cycle_reg[14]),
        .I5(\reg_out[14]_i_5_n_0 ),
        .O(\reg_out[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5400000000000000)) 
    \reg_out[14]_i_3 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(latched_is_lu_reg_n_0),
        .I2(latched_is_lh_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\mem_wordsize_reg_n_0_[0] ),
        .I5(\reg_op1_reg_n_0_[1] ),
        .O(\reg_out[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000540054005400)) 
    \reg_out[14]_i_4 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(latched_is_lu_reg_n_0),
        .I2(latched_is_lh_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\mem_wordsize_reg_n_0_[0] ),
        .I5(\reg_op1_reg_n_0_[1] ),
        .O(\reg_out[14]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[14]_i_5 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[14] ),
        .I2(\reg_out[14]_i_6_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[14]_i_6 
       (.I0(\count_instr_reg_n_0_[14] ),
        .I1(count_cycle_reg[46]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[14]),
        .O(\reg_out[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \reg_out[15]_i_1 
       (.I0(\reg_out[15]_i_2_n_0 ),
        .I1(\reg_out[15]_i_3_n_0 ),
        .I2(\cpu_state_reg_n_0_[3] ),
        .I3(reg_out0_carry__2_n_5),
        .I4(\reg_out[15]_i_4_n_0 ),
        .I5(\reg_out[15]_i_5_n_0 ),
        .O(\reg_out[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF8F8F88888888888)) 
    \reg_out[15]_i_2 
       (.I0(\count_instr_reg_n_0_[15] ),
        .I1(\reg_out[31]_i_7_n_0 ),
        .I2(\reg_out[15]_i_6_n_0 ),
        .I3(latched_is_lu_reg_n_0),
        .I4(latched_is_lh_reg_n_0),
        .I5(\cpu_state_reg_n_0_[0] ),
        .O(\reg_out[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[15]_i_3 
       (.I0(count_cycle_reg[47]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[15]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[15]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAA80000)) 
    \reg_out[15]_i_4 
       (.I0(\cpu_state_reg_n_0_[0] ),
        .I1(\reg_out[31]_i_10_n_0 ),
        .I2(\reg_out[31]_i_9_n_0 ),
        .I3(\reg_out[31]_i_8_n_0 ),
        .I4(latched_is_lb_reg_n_0),
        .O(\reg_out[15]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[15]_i_5 
       (.I0(count_cycle_reg[15]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[15] ),
        .O(\reg_out[15]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h0000BF80)) 
    \reg_out[15]_i_6 
       (.I0(mem_rdata[31]),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(mem_rdata[15]),
        .I4(\mem_wordsize_reg_n_0_[1] ),
        .O(\reg_out[15]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h00000040)) 
    \reg_out[15]_i_7 
       (.I0(instr_rdcycle),
        .I1(\cpu_state_reg_n_0_[5] ),
        .I2(instr_rdinstrh),
        .I3(instr_rdinstr),
        .I4(instr_rdcycleh),
        .O(\reg_out[15]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[16]_i_1 
       (.I0(\reg_out[16]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_out0_carry__2_n_4),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[16]_i_3_n_0 ),
        .O(\reg_out[16]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[16]_i_2 
       (.I0(\reg_out[16]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[16]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[16] ),
        .O(\reg_out[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[16]_i_3 
       (.I0(count_cycle_reg[16]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[16] ),
        .O(\reg_out[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[16]_i_4 
       (.I0(count_cycle_reg[48]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[16]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[16]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[17]_i_1 
       (.I0(\reg_out[17]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_out0_carry__3_n_7),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[17]_i_3_n_0 ),
        .O(\reg_out[17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[17]_i_2 
       (.I0(\reg_out[17]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[17]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[17] ),
        .O(\reg_out[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[17]_i_3 
       (.I0(count_cycle_reg[17]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[17] ),
        .O(\reg_out[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[17]_i_4 
       (.I0(count_cycle_reg[49]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[17]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[17]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[18]_i_1 
       (.I0(\reg_out[18]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_out0_carry__3_n_6),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[18]_i_3_n_0 ),
        .O(\reg_out[18]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[18]_i_2 
       (.I0(\reg_out[18]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[18]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[18] ),
        .O(\reg_out[18]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[18]_i_3 
       (.I0(count_cycle_reg[18]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[18] ),
        .O(\reg_out[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[18]_i_4 
       (.I0(count_cycle_reg[50]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[18]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[18]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[19]_i_1 
       (.I0(\reg_out[19]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_op1_reg_n_0_[19] ),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[19]_i_3_n_0 ),
        .O(\reg_out[19]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[19]_i_2 
       (.I0(\reg_out[19]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[19]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[19] ),
        .O(\reg_out[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[19]_i_3 
       (.I0(count_cycle_reg[19]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__3_n_5),
        .O(\reg_out[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[19]_i_4 
       (.I0(count_cycle_reg[51]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[19]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEAAAA)) 
    \reg_out[1]_i_1 
       (.I0(\reg_out[1]_i_2_n_0 ),
        .I1(\reg_out[6]_i_4_n_0 ),
        .I2(\reg_out[1]_i_3_n_0 ),
        .I3(\reg_out[1]_i_4_n_0 ),
        .I4(\reg_out[1]_i_5_n_0 ),
        .I5(\reg_out[1]_i_6_n_0 ),
        .O(\reg_out[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8080FF80FF808080)) 
    \reg_out[1]_i_2 
       (.I0(count_cycle_reg[1]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(\reg_pc_reg_n_0_[1] ),
        .I5(\decoded_imm_reg_n_0_[1] ),
        .O(\reg_out[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5554000000000000)) 
    \reg_out[1]_i_3 
       (.I0(\reg_out[7]_i_6_n_0 ),
        .I1(latched_is_lb_reg_n_0),
        .I2(latched_is_lh_reg_n_0),
        .I3(latched_is_lu_reg_n_0),
        .I4(\cpu_state_reg_n_0_[0] ),
        .I5(mem_rdata[1]),
        .O(\reg_out[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFE00000000000000)) 
    \reg_out[1]_i_4 
       (.I0(latched_is_lb_reg_n_0),
        .I1(latched_is_lh_reg_n_0),
        .I2(latched_is_lu_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\reg_out[7]_i_6_n_0 ),
        .I5(mem_rdata[17]),
        .O(\reg_out[1]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hBEAEBAAA)) 
    \reg_out[1]_i_5 
       (.I0(\reg_out[1]_i_7_n_0 ),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(mem_rdata[9]),
        .I4(mem_rdata[17]),
        .O(\reg_out[1]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[1]_i_6 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\reg_out[1]_i_8_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[1]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hF55D555D)) 
    \reg_out[1]_i_7 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(mem_rdata[1]),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .I4(mem_rdata[25]),
        .O(\reg_out[1]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[1]_i_8 
       (.I0(\count_instr_reg_n_0_[1] ),
        .I1(count_cycle_reg[33]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[1]),
        .O(\reg_out[1]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[20]_i_1 
       (.I0(\reg_out[20]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_op1_reg_n_0_[20] ),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[20]_i_3_n_0 ),
        .O(\reg_out[20]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[20]_i_2 
       (.I0(\reg_out[20]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[20]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[20] ),
        .O(\reg_out[20]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[20]_i_3 
       (.I0(count_cycle_reg[20]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__3_n_4),
        .O(\reg_out[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[20]_i_4 
       (.I0(count_cycle_reg[52]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[20]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[20]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[21]_i_1 
       (.I0(\reg_out[21]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_out0_carry__4_n_7),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[21]_i_3_n_0 ),
        .O(\reg_out[21]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[21]_i_2 
       (.I0(\reg_out[21]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[21]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[21] ),
        .O(\reg_out[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[21]_i_3 
       (.I0(count_cycle_reg[21]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[21] ),
        .O(\reg_out[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[21]_i_4 
       (.I0(count_cycle_reg[53]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[21]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[21]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[22]_i_1 
       (.I0(\reg_out[22]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_op1_reg_n_0_[22] ),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[22]_i_3_n_0 ),
        .O(\reg_out[22]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[22]_i_2 
       (.I0(\reg_out[22]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[22]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[22] ),
        .O(\reg_out[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[22]_i_3 
       (.I0(count_cycle_reg[22]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__4_n_6),
        .O(\reg_out[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[22]_i_4 
       (.I0(count_cycle_reg[54]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[22]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[22]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[23]_i_1 
       (.I0(\reg_out[23]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_out0_carry__4_n_5),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[23]_i_3_n_0 ),
        .O(\reg_out[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[23]_i_2 
       (.I0(\reg_out[23]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[23]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[23] ),
        .O(\reg_out[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[23]_i_3 
       (.I0(count_cycle_reg[23]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[23] ),
        .O(\reg_out[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[23]_i_4 
       (.I0(count_cycle_reg[55]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[23]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[23]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[24]_i_1 
       (.I0(\reg_out[24]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_op1_reg_n_0_[24] ),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[24]_i_3_n_0 ),
        .O(\reg_out[24]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[24]_i_2 
       (.I0(\reg_out[24]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[24]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[24] ),
        .O(\reg_out[24]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[24]_i_3 
       (.I0(count_cycle_reg[24]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__4_n_4),
        .O(\reg_out[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[24]_i_4 
       (.I0(count_cycle_reg[56]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[24]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[24]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[25]_i_1 
       (.I0(\reg_out[25]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_out0_carry__5_n_7),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[25]_i_3_n_0 ),
        .O(\reg_out[25]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[25]_i_2 
       (.I0(\reg_out[25]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[25]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[25] ),
        .O(\reg_out[25]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[25]_i_3 
       (.I0(count_cycle_reg[25]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[25] ),
        .O(\reg_out[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[25]_i_4 
       (.I0(count_cycle_reg[57]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[25]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[25]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[26]_i_1 
       (.I0(\reg_out[26]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_op1_reg_n_0_[26] ),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[26]_i_3_n_0 ),
        .O(\reg_out[26]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[26]_i_2 
       (.I0(\reg_out[26]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[26]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[26] ),
        .O(\reg_out[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[26]_i_3 
       (.I0(count_cycle_reg[26]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__5_n_6),
        .O(\reg_out[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[26]_i_4 
       (.I0(count_cycle_reg[58]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[26]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[26]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[27]_i_1 
       (.I0(\reg_out[27]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_out0_carry__5_n_5),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[27]_i_3_n_0 ),
        .O(\reg_out[27]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[27]_i_2 
       (.I0(\reg_out[27]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[27]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[27] ),
        .O(\reg_out[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[27]_i_3 
       (.I0(count_cycle_reg[27]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[27] ),
        .O(\reg_out[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[27]_i_4 
       (.I0(count_cycle_reg[59]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[27]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[27]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[28]_i_1 
       (.I0(\reg_out[28]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_out0_carry__5_n_4),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[28]_i_3_n_0 ),
        .O(\reg_out[28]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[28]_i_2 
       (.I0(\reg_out[28]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[28]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[28] ),
        .O(\reg_out[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[28]_i_3 
       (.I0(count_cycle_reg[28]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[28] ),
        .O(\reg_out[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[28]_i_4 
       (.I0(count_cycle_reg[60]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[28]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[28]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[29]_i_1 
       (.I0(\reg_out[29]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[3] ),
        .I2(reg_out0_carry__6_n_7),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[29]_i_3_n_0 ),
        .O(\reg_out[29]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[29]_i_2 
       (.I0(\reg_out[29]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[29]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[29] ),
        .O(\reg_out[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[29]_i_3 
       (.I0(count_cycle_reg[29]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[29] ),
        .O(\reg_out[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[29]_i_4 
       (.I0(count_cycle_reg[61]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[29]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEAAAA)) 
    \reg_out[2]_i_1 
       (.I0(\reg_out[2]_i_2_n_0 ),
        .I1(\reg_out[6]_i_4_n_0 ),
        .I2(\reg_out[2]_i_3_n_0 ),
        .I3(\reg_out[2]_i_4_n_0 ),
        .I4(\reg_out[2]_i_5_n_0 ),
        .I5(\reg_out[2]_i_6_n_0 ),
        .O(\reg_out[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[2]_i_2 
       (.I0(count_cycle_reg[2]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry_n_6),
        .O(\reg_out[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5554000000000000)) 
    \reg_out[2]_i_3 
       (.I0(\reg_out[7]_i_6_n_0 ),
        .I1(latched_is_lb_reg_n_0),
        .I2(latched_is_lh_reg_n_0),
        .I3(latched_is_lu_reg_n_0),
        .I4(\cpu_state_reg_n_0_[0] ),
        .I5(mem_rdata[2]),
        .O(\reg_out[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFE00000000000000)) 
    \reg_out[2]_i_4 
       (.I0(latched_is_lb_reg_n_0),
        .I1(latched_is_lh_reg_n_0),
        .I2(latched_is_lu_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\reg_out[7]_i_6_n_0 ),
        .I5(mem_rdata[18]),
        .O(\reg_out[2]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hBEAEBAAA)) 
    \reg_out[2]_i_5 
       (.I0(\reg_out[2]_i_7_n_0 ),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(mem_rdata[10]),
        .I4(mem_rdata[18]),
        .O(\reg_out[2]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[2]_i_6 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[2] ),
        .I2(\reg_out[2]_i_8_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[2]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hF55D555D)) 
    \reg_out[2]_i_7 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(mem_rdata[2]),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .I4(mem_rdata[26]),
        .O(\reg_out[2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[2]_i_8 
       (.I0(\count_instr_reg_n_0_[2] ),
        .I1(count_cycle_reg[34]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[2]),
        .O(\reg_out[2]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[30]_i_1 
       (.I0(\reg_out[30]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_op1_reg_n_0_[30] ),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[30]_i_3_n_0 ),
        .O(\reg_out[30]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[30]_i_2 
       (.I0(\reg_out[30]_i_4_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[30]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[30] ),
        .O(\reg_out[30]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[30]_i_3 
       (.I0(count_cycle_reg[30]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__6_n_6),
        .O(\reg_out[30]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[30]_i_4 
       (.I0(count_cycle_reg[62]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[30]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[30]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \reg_out[31]_i_1 
       (.I0(\reg_out[31]_i_2_n_0 ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_op1_reg_n_0_[31] ),
        .I3(\reg_out[31]_i_3_n_0 ),
        .I4(\reg_out[31]_i_4_n_0 ),
        .O(\reg_out[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h0000BF80)) 
    \reg_out[31]_i_10 
       (.I0(mem_rdata[23]),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\mem_wordsize_reg_n_0_[0] ),
        .I3(mem_rdata[7]),
        .I4(\mem_wordsize_reg_n_0_[1] ),
        .O(\reg_out[31]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h2220202000202020)) 
    \reg_out[31]_i_11 
       (.I0(latched_is_lh_reg_n_0),
        .I1(\mem_wordsize_reg_n_0_[1] ),
        .I2(mem_rdata[15]),
        .I3(\mem_wordsize_reg_n_0_[0] ),
        .I4(\reg_op1_reg_n_0_[1] ),
        .I5(mem_rdata[31]),
        .O(\reg_out[31]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg_out[31]_i_2 
       (.I0(\reg_out[31]_i_5_n_0 ),
        .I1(\reg_out[31]_i_6_n_0 ),
        .I2(mem_rdata[31]),
        .I3(\reg_out[31]_i_7_n_0 ),
        .I4(\count_instr_reg_n_0_[31] ),
        .O(\reg_out[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFAAA800000000)) 
    \reg_out[31]_i_3 
       (.I0(latched_is_lb_reg_n_0),
        .I1(\reg_out[31]_i_8_n_0 ),
        .I2(\reg_out[31]_i_9_n_0 ),
        .I3(\reg_out[31]_i_10_n_0 ),
        .I4(\reg_out[31]_i_11_n_0 ),
        .I5(\cpu_state_reg_n_0_[0] ),
        .O(\reg_out[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[31]_i_4 
       (.I0(count_cycle_reg[31]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__6_n_5),
        .O(\reg_out[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg_out[31]_i_5 
       (.I0(count_cycle_reg[63]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycleh),
        .I4(data3[31]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[31]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \reg_out[31]_i_6 
       (.I0(\cpu_state_reg_n_0_[0] ),
        .I1(latched_is_lu_reg_n_0),
        .I2(\mem_wordsize_reg_n_0_[1] ),
        .I3(\mem_wordsize_reg_n_0_[0] ),
        .O(\reg_out[31]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \reg_out[31]_i_7 
       (.I0(instr_rdcycleh),
        .I1(instr_rdinstr),
        .I2(instr_rdcycle),
        .I3(\cpu_state_reg_n_0_[5] ),
        .O(\reg_out[31]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h8C008000)) 
    \reg_out[31]_i_8 
       (.I0(mem_rdata[31]),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(mem_rdata[23]),
        .O(\reg_out[31]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'h0B000800)) 
    \reg_out[31]_i_9 
       (.I0(mem_rdata[15]),
        .I1(\reg_op1_reg_n_0_[0] ),
        .I2(\reg_op1_reg_n_0_[1] ),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(mem_rdata[7]),
        .O(\reg_out[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEAAAA)) 
    \reg_out[3]_i_1 
       (.I0(\reg_out[3]_i_2_n_0 ),
        .I1(\reg_out[6]_i_4_n_0 ),
        .I2(\reg_out[3]_i_3_n_0 ),
        .I3(\reg_out[3]_i_4_n_0 ),
        .I4(\reg_out[3]_i_5_n_0 ),
        .I5(\reg_out[3]_i_6_n_0 ),
        .O(\reg_out[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[3]_i_2 
       (.I0(count_cycle_reg[3]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry_n_5),
        .O(\reg_out[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5554000000000000)) 
    \reg_out[3]_i_3 
       (.I0(\reg_out[7]_i_6_n_0 ),
        .I1(latched_is_lb_reg_n_0),
        .I2(latched_is_lh_reg_n_0),
        .I3(latched_is_lu_reg_n_0),
        .I4(\cpu_state_reg_n_0_[0] ),
        .I5(mem_rdata[3]),
        .O(\reg_out[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFE00000000000000)) 
    \reg_out[3]_i_4 
       (.I0(latched_is_lb_reg_n_0),
        .I1(latched_is_lh_reg_n_0),
        .I2(latched_is_lu_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\reg_out[7]_i_6_n_0 ),
        .I5(mem_rdata[19]),
        .O(\reg_out[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hBEAEBAAA)) 
    \reg_out[3]_i_5 
       (.I0(\reg_out[3]_i_7_n_0 ),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(mem_rdata[11]),
        .I4(mem_rdata[19]),
        .O(\reg_out[3]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[3]_i_6 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[3] ),
        .I2(\reg_out[3]_i_8_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[3]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hF55D555D)) 
    \reg_out[3]_i_7 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(mem_rdata[3]),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .I4(mem_rdata[27]),
        .O(\reg_out[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[3]_i_8 
       (.I0(\count_instr_reg_n_0_[3] ),
        .I1(count_cycle_reg[35]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[3]),
        .O(\reg_out[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEAAAA)) 
    \reg_out[4]_i_1 
       (.I0(\reg_out[4]_i_2_n_0 ),
        .I1(\reg_out[6]_i_4_n_0 ),
        .I2(\reg_out[4]_i_3_n_0 ),
        .I3(\reg_out[4]_i_4_n_0 ),
        .I4(\reg_out[4]_i_5_n_0 ),
        .I5(\reg_out[4]_i_6_n_0 ),
        .O(\reg_out[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[4]_i_2 
       (.I0(count_cycle_reg[4]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry_n_4),
        .O(\reg_out[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5554000000000000)) 
    \reg_out[4]_i_3 
       (.I0(\reg_out[7]_i_6_n_0 ),
        .I1(latched_is_lb_reg_n_0),
        .I2(latched_is_lh_reg_n_0),
        .I3(latched_is_lu_reg_n_0),
        .I4(\cpu_state_reg_n_0_[0] ),
        .I5(mem_rdata[4]),
        .O(\reg_out[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFE00000000000000)) 
    \reg_out[4]_i_4 
       (.I0(latched_is_lb_reg_n_0),
        .I1(latched_is_lh_reg_n_0),
        .I2(latched_is_lu_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\reg_out[7]_i_6_n_0 ),
        .I5(mem_rdata[20]),
        .O(\reg_out[4]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hBEAEBAAA)) 
    \reg_out[4]_i_5 
       (.I0(\reg_out[4]_i_7_n_0 ),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(mem_rdata[12]),
        .I4(mem_rdata[20]),
        .O(\reg_out[4]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[4]_i_6 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[4] ),
        .I2(\reg_out[4]_i_8_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[4]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hF55D555D)) 
    \reg_out[4]_i_7 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(mem_rdata[4]),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .I4(mem_rdata[28]),
        .O(\reg_out[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[4]_i_8 
       (.I0(\count_instr_reg_n_0_[4] ),
        .I1(count_cycle_reg[36]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[4]),
        .O(\reg_out[4]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEAAAA)) 
    \reg_out[5]_i_1 
       (.I0(\reg_out[5]_i_2_n_0 ),
        .I1(\reg_out[6]_i_4_n_0 ),
        .I2(\reg_out[5]_i_3_n_0 ),
        .I3(\reg_out[5]_i_4_n_0 ),
        .I4(\reg_out[5]_i_5_n_0 ),
        .I5(\reg_out[5]_i_6_n_0 ),
        .O(\reg_out[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[5]_i_2 
       (.I0(count_cycle_reg[5]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__0_n_7),
        .O(\reg_out[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5554000000000000)) 
    \reg_out[5]_i_3 
       (.I0(\reg_out[7]_i_6_n_0 ),
        .I1(latched_is_lb_reg_n_0),
        .I2(latched_is_lh_reg_n_0),
        .I3(latched_is_lu_reg_n_0),
        .I4(\cpu_state_reg_n_0_[0] ),
        .I5(mem_rdata[5]),
        .O(\reg_out[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFE00000000000000)) 
    \reg_out[5]_i_4 
       (.I0(latched_is_lb_reg_n_0),
        .I1(latched_is_lh_reg_n_0),
        .I2(latched_is_lu_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\reg_out[7]_i_6_n_0 ),
        .I5(mem_rdata[21]),
        .O(\reg_out[5]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hBEAEBAAA)) 
    \reg_out[5]_i_5 
       (.I0(\reg_out[5]_i_7_n_0 ),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(mem_rdata[13]),
        .I4(mem_rdata[21]),
        .O(\reg_out[5]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[5]_i_6 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[5] ),
        .I2(\reg_out[5]_i_8_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[5]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hF55D555D)) 
    \reg_out[5]_i_7 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(mem_rdata[5]),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .I4(mem_rdata[29]),
        .O(\reg_out[5]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[5]_i_8 
       (.I0(\count_instr_reg_n_0_[5] ),
        .I1(count_cycle_reg[37]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[5]),
        .O(\reg_out[5]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEAAAA)) 
    \reg_out[6]_i_1 
       (.I0(\reg_out[6]_i_2_n_0 ),
        .I1(\reg_out[6]_i_3_n_0 ),
        .I2(\reg_out[6]_i_4_n_0 ),
        .I3(\reg_out[6]_i_5_n_0 ),
        .I4(\reg_out[6]_i_6_n_0 ),
        .I5(\reg_out[6]_i_7_n_0 ),
        .O(\reg_out[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFF808080)) 
    \reg_out[6]_i_2 
       (.I0(count_cycle_reg[6]),
        .I1(instr_rdcycle),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__0_n_6),
        .O(\reg_out[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5554000000000000)) 
    \reg_out[6]_i_3 
       (.I0(\reg_out[7]_i_6_n_0 ),
        .I1(latched_is_lb_reg_n_0),
        .I2(latched_is_lh_reg_n_0),
        .I3(latched_is_lu_reg_n_0),
        .I4(\cpu_state_reg_n_0_[0] ),
        .I5(mem_rdata[6]),
        .O(\reg_out[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h88888880)) 
    \reg_out[6]_i_4 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(\cpu_state_reg_n_0_[0] ),
        .I2(latched_is_lu_reg_n_0),
        .I3(latched_is_lh_reg_n_0),
        .I4(latched_is_lb_reg_n_0),
        .O(\reg_out[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFE00000000000000)) 
    \reg_out[6]_i_5 
       (.I0(latched_is_lb_reg_n_0),
        .I1(latched_is_lh_reg_n_0),
        .I2(latched_is_lu_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .I4(\reg_out[7]_i_6_n_0 ),
        .I5(mem_rdata[22]),
        .O(\reg_out[6]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hBEAEBAAA)) 
    \reg_out[6]_i_6 
       (.I0(\reg_out[6]_i_8_n_0 ),
        .I1(\reg_op1_reg_n_0_[1] ),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(mem_rdata[14]),
        .I4(mem_rdata[22]),
        .O(\reg_out[6]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[6]_i_7 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[6] ),
        .I2(\reg_out[6]_i_9_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[6]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hF55D555D)) 
    \reg_out[6]_i_8 
       (.I0(\mem_wordsize_reg_n_0_[1] ),
        .I1(mem_rdata[6]),
        .I2(\reg_op1_reg_n_0_[0] ),
        .I3(\reg_op1_reg_n_0_[1] ),
        .I4(mem_rdata[30]),
        .O(\reg_out[6]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[6]_i_9 
       (.I0(\count_instr_reg_n_0_[6] ),
        .I1(count_cycle_reg[38]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[6]),
        .O(\reg_out[6]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF8F88)) 
    \reg_out[7]_i_1 
       (.I0(\cpu_state_reg_n_0_[3] ),
        .I1(reg_out0_carry__0_n_5),
        .I2(\reg_out[7]_i_2_n_0 ),
        .I3(\reg_out[7]_i_3_n_0 ),
        .I4(\reg_out[7]_i_4_n_0 ),
        .I5(\reg_out[7]_i_5_n_0 ),
        .O(\reg_out[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h01FF)) 
    \reg_out[7]_i_2 
       (.I0(latched_is_lb_reg_n_0),
        .I1(latched_is_lh_reg_n_0),
        .I2(latched_is_lu_reg_n_0),
        .I3(\cpu_state_reg_n_0_[0] ),
        .O(\reg_out[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF00B8)) 
    \reg_out[7]_i_3 
       (.I0(mem_rdata[23]),
        .I1(\reg_out[7]_i_6_n_0 ),
        .I2(mem_rdata[7]),
        .I3(\mem_wordsize_reg_n_0_[1] ),
        .I4(\reg_out[31]_i_9_n_0 ),
        .I5(\reg_out[31]_i_8_n_0 ),
        .O(\reg_out[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000ACA000000000)) 
    \reg_out[7]_i_4 
       (.I0(count_cycle_reg[39]),
        .I1(\count_instr_reg_n_0_[7] ),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdcycle),
        .I5(\cpu_state_reg_n_0_[5] ),
        .O(\reg_out[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \reg_out[7]_i_5 
       (.I0(\reg_op1_reg_n_0_[7] ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_out[7]_i_7_n_0 ),
        .I3(count_cycle_reg[7]),
        .I4(data3[7]),
        .I5(\reg_out[15]_i_7_n_0 ),
        .O(\reg_out[7]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg_out[7]_i_6 
       (.I0(\reg_op1_reg_n_0_[1] ),
        .I1(\mem_wordsize_reg_n_0_[0] ),
        .O(\reg_out[7]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg_out[7]_i_7 
       (.I0(instr_rdcycle),
        .I1(\cpu_state_reg_n_0_[5] ),
        .O(\reg_out[7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_out[8]_i_1 
       (.I0(\reg_out[8]_i_2_n_0 ),
        .I1(\reg_out[14]_i_3_n_0 ),
        .I2(mem_rdata[24]),
        .I3(\cpu_state_reg_n_0_[3] ),
        .I4(reg_out0_carry__0_n_4),
        .I5(\reg_out[15]_i_4_n_0 ),
        .O(\reg_out[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF8888888)) 
    \reg_out[8]_i_2 
       (.I0(\reg_out[14]_i_4_n_0 ),
        .I1(mem_rdata[8]),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycle),
        .I4(count_cycle_reg[8]),
        .I5(\reg_out[8]_i_3_n_0 ),
        .O(\reg_out[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[8]_i_3 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_op1_reg_n_0_[8] ),
        .I2(\reg_out[8]_i_4_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[8]_i_4 
       (.I0(\count_instr_reg_n_0_[8] ),
        .I1(count_cycle_reg[40]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[8]),
        .O(\reg_out[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    \reg_out[9]_i_1 
       (.I0(\reg_out[9]_i_2_n_0 ),
        .I1(\reg_out[14]_i_3_n_0 ),
        .I2(mem_rdata[25]),
        .I3(\cpu_state_reg_n_0_[2] ),
        .I4(\reg_op1_reg_n_0_[9] ),
        .I5(\reg_out[15]_i_4_n_0 ),
        .O(\reg_out[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF8888888)) 
    \reg_out[9]_i_2 
       (.I0(\reg_out[14]_i_4_n_0 ),
        .I1(mem_rdata[9]),
        .I2(\cpu_state_reg_n_0_[5] ),
        .I3(instr_rdcycle),
        .I4(count_cycle_reg[9]),
        .I5(\reg_out[9]_i_3_n_0 ),
        .O(\reg_out[9]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8888F888)) 
    \reg_out[9]_i_3 
       (.I0(\cpu_state_reg_n_0_[3] ),
        .I1(reg_out0_carry__1_n_7),
        .I2(\reg_out[9]_i_4_n_0 ),
        .I3(\cpu_state_reg_n_0_[5] ),
        .I4(instr_rdcycle),
        .O(\reg_out[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCACFCAC0CAC0CAC0)) 
    \reg_out[9]_i_4 
       (.I0(\count_instr_reg_n_0_[9] ),
        .I1(count_cycle_reg[41]),
        .I2(instr_rdcycleh),
        .I3(instr_rdinstr),
        .I4(instr_rdinstrh),
        .I5(data3[9]),
        .O(\reg_out[9]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[0]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[10]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[11]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[12]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[13]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[14]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[15]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[16]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[17]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[18]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[19]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[1]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[20]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[21]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[22]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[23]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[24]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[25]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[26]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[27]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[28]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[29]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[2]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[30]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[31]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[3]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[4]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[5]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[6]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[7]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[8]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_out_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_out[9]_i_1_n_0 ),
        .Q(\reg_out_reg_n_0_[9] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[10]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[10] ),
        .I1(\reg_out_reg_n_0_[10] ),
        .I2(alu_out_q[10]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[10]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[11]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[11] ),
        .I1(\reg_out_reg_n_0_[11] ),
        .I2(alu_out_q[11]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[11]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[12]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[12] ),
        .I1(\reg_out_reg_n_0_[12] ),
        .I2(alu_out_q[12]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[12]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[13]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[13] ),
        .I1(\reg_out_reg_n_0_[13] ),
        .I2(alu_out_q[13]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[13]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[14]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[14] ),
        .I1(\reg_out_reg_n_0_[14] ),
        .I2(alu_out_q[14]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[14]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[15]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[15] ),
        .I1(\reg_out_reg_n_0_[15] ),
        .I2(alu_out_q[15]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[15]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[16]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[16] ),
        .I1(\reg_out_reg_n_0_[16] ),
        .I2(alu_out_q[16]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[16]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[17]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[17] ),
        .I1(\reg_out_reg_n_0_[17] ),
        .I2(alu_out_q[17]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[17]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[18]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[18] ),
        .I1(\reg_out_reg_n_0_[18] ),
        .I2(alu_out_q[18]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[18]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[19]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[19] ),
        .I1(\reg_out_reg_n_0_[19] ),
        .I2(alu_out_q[19]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[19]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[1]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[1] ),
        .I1(\reg_out_reg_n_0_[1] ),
        .I2(alu_out_q[1]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[1]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[20]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[20] ),
        .I1(\reg_out_reg_n_0_[20] ),
        .I2(alu_out_q[20]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[20]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[21]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[21] ),
        .I1(\reg_out_reg_n_0_[21] ),
        .I2(alu_out_q[21]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[21]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[22]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[22] ),
        .I1(\reg_out_reg_n_0_[22] ),
        .I2(alu_out_q[22]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[22]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[23]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[23] ),
        .I1(\reg_out_reg_n_0_[23] ),
        .I2(alu_out_q[23]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[23]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[24]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[24] ),
        .I1(\reg_out_reg_n_0_[24] ),
        .I2(alu_out_q[24]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[24]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[25]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[25] ),
        .I1(\reg_out_reg_n_0_[25] ),
        .I2(alu_out_q[25]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[25]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[26]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[26] ),
        .I1(\reg_out_reg_n_0_[26] ),
        .I2(alu_out_q[26]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[26]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[27]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[27] ),
        .I1(\reg_out_reg_n_0_[27] ),
        .I2(alu_out_q[27]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[27]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[28]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[28] ),
        .I1(\reg_out_reg_n_0_[28] ),
        .I2(alu_out_q[28]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[28]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[29]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[29] ),
        .I1(\reg_out_reg_n_0_[29] ),
        .I2(alu_out_q[29]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[29]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[2]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[2] ),
        .I1(\reg_out_reg_n_0_[2] ),
        .I2(alu_out_q[2]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[2]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[30]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[30] ),
        .I1(\reg_out_reg_n_0_[30] ),
        .I2(alu_out_q[30]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[30]));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    \reg_pc[31]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[31] ),
        .I1(latched_stalu_reg_n_0),
        .I2(latched_branch_reg_n_0),
        .I3(latched_store_reg_n_0),
        .I4(\reg_out_reg_n_0_[31] ),
        .I5(alu_out_q[31]),
        .O(current_pc[31]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[3]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[3] ),
        .I1(\reg_out_reg_n_0_[3] ),
        .I2(alu_out_q[3]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[3]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[4]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[4] ),
        .I1(\reg_out_reg_n_0_[4] ),
        .I2(alu_out_q[4]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[4]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[5]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[5] ),
        .I1(\reg_out_reg_n_0_[5] ),
        .I2(alu_out_q[5]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[5]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[6]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[6] ),
        .I1(\reg_out_reg_n_0_[6] ),
        .I2(alu_out_q[6]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[6]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[7]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[7] ),
        .I1(\reg_out_reg_n_0_[7] ),
        .I2(alu_out_q[7]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[7]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[8]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[8] ),
        .I1(\reg_out_reg_n_0_[8] ),
        .I2(alu_out_q[8]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[8]));
  LUT6 #(
    .INIT(64'hF0AAAAAACCAAAAAA)) 
    \reg_pc[9]_i_1 
       (.I0(\reg_next_pc_reg_n_0_[9] ),
        .I1(\reg_out_reg_n_0_[9] ),
        .I2(alu_out_q[9]),
        .I3(latched_store_reg_n_0),
        .I4(latched_branch_reg_n_0),
        .I5(latched_stalu_reg_n_0),
        .O(current_pc[9]));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[10]),
        .Q(\reg_pc_reg_n_0_[10] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[11]),
        .Q(\reg_pc_reg_n_0_[11] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[12]),
        .Q(\reg_pc_reg_n_0_[12] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[13]),
        .Q(\reg_pc_reg_n_0_[13] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[14]),
        .Q(\reg_pc_reg_n_0_[14] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[15]),
        .Q(\reg_pc_reg_n_0_[15] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[16]),
        .Q(\reg_pc_reg_n_0_[16] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[17]),
        .Q(\reg_pc_reg_n_0_[17] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[18]),
        .Q(\reg_pc_reg_n_0_[18] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[19]),
        .Q(\reg_pc_reg_n_0_[19] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[1]),
        .Q(\reg_pc_reg_n_0_[1] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[20]),
        .Q(\reg_pc_reg_n_0_[20] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[21]),
        .Q(\reg_pc_reg_n_0_[21] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[22]),
        .Q(\reg_pc_reg_n_0_[22] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[23]),
        .Q(\reg_pc_reg_n_0_[23] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[24]),
        .Q(\reg_pc_reg_n_0_[24] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[25]),
        .Q(\reg_pc_reg_n_0_[25] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[26]),
        .Q(\reg_pc_reg_n_0_[26] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[27]),
        .Q(\reg_pc_reg_n_0_[27] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[28]),
        .Q(\reg_pc_reg_n_0_[28] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[29]),
        .Q(\reg_pc_reg_n_0_[29] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[2]),
        .Q(\reg_pc_reg_n_0_[2] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[30]),
        .Q(\reg_pc_reg_n_0_[30] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[31]),
        .Q(\reg_pc_reg_n_0_[31] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[3]),
        .Q(\reg_pc_reg_n_0_[3] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[4]),
        .Q(\reg_pc_reg_n_0_[4] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[5]),
        .Q(\reg_pc_reg_n_0_[5] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[6]),
        .Q(\reg_pc_reg_n_0_[6] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[7]),
        .Q(\reg_pc_reg_n_0_[7] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[8]),
        .Q(\reg_pc_reg_n_0_[8] ),
        .R(instr_and_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_pc_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(reg_next_pc),
        .D(current_pc[9]),
        .Q(\reg_pc_reg_n_0_[9] ),
        .R(instr_and_i_1_n_0));
  LUT6 #(
    .INIT(64'hFF0BFF0AFF01FF00)) 
    \reg_sh[0]_i_1 
       (.I0(is_slli_srli_srai),
        .I1(\reg_sh[4]_i_2_n_0 ),
        .I2(\cpu_state_reg_n_0_[2] ),
        .I3(\reg_sh[0]_i_2_n_0 ),
        .I4(reg_sh1[0]),
        .I5(decoded_imm_uj[11]),
        .O(\reg_sh[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h88888884)) 
    \reg_sh[0]_i_2 
       (.I0(\reg_sh_reg_n_0_[0] ),
        .I1(\cpu_state_reg_n_0_[2] ),
        .I2(\reg_sh_reg_n_0_[4] ),
        .I3(\reg_sh_reg_n_0_[3] ),
        .I4(\reg_sh_reg_n_0_[2] ),
        .O(\reg_sh[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF01FFFFFF00)) 
    \reg_sh[1]_i_1 
       (.I0(is_slli_srli_srai),
        .I1(\reg_sh[4]_i_2_n_0 ),
        .I2(\cpu_state_reg_n_0_[2] ),
        .I3(\reg_sh[1]_i_2_n_0 ),
        .I4(\cpu_state[7]_i_5_n_0 ),
        .I5(reg_sh1[1]),
        .O(\reg_sh[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF4040EA404040)) 
    \reg_sh[1]_i_2 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(is_slli_srli_srai),
        .I2(decoded_imm_uj[1]),
        .I3(\reg_sh_reg_n_0_[0] ),
        .I4(\reg_sh_reg_n_0_[1] ),
        .I5(\reg_sh[3]_i_4_n_0 ),
        .O(\reg_sh[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF01FFFFFF00)) 
    \reg_sh[2]_i_1 
       (.I0(is_slli_srli_srai),
        .I1(\reg_sh[4]_i_2_n_0 ),
        .I2(\cpu_state_reg_n_0_[2] ),
        .I3(\reg_sh[2]_i_2_n_0 ),
        .I4(\cpu_state[7]_i_5_n_0 ),
        .I5(reg_sh1[2]),
        .O(\reg_sh[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h54545454FF000000)) 
    \reg_sh[2]_i_2 
       (.I0(\reg_sh_reg_n_0_[2] ),
        .I1(\reg_sh_reg_n_0_[4] ),
        .I2(\reg_sh_reg_n_0_[3] ),
        .I3(decoded_imm_uj[2]),
        .I4(is_slli_srli_srai),
        .I5(\cpu_state_reg_n_0_[2] ),
        .O(\reg_sh[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAEAEAEAEAFFEA)) 
    \reg_sh[3]_i_1 
       (.I0(\reg_sh[3]_i_2_n_0 ),
        .I1(\reg_sh[3]_i_3_n_0 ),
        .I2(decoded_imm_uj[3]),
        .I3(\reg_sh[3]_i_4_n_0 ),
        .I4(\reg_sh_reg_n_0_[2] ),
        .I5(\reg_sh_reg_n_0_[3] ),
        .O(\reg_sh[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hFFFF0100)) 
    \reg_sh[3]_i_2 
       (.I0(is_slli_srli_srai),
        .I1(\reg_sh[4]_i_2_n_0 ),
        .I2(\cpu_state_reg_n_0_[2] ),
        .I3(reg_sh1[3]),
        .I4(\cpu_state[7]_i_5_n_0 ),
        .O(\reg_sh[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \reg_sh[3]_i_3 
       (.I0(is_slli_srli_srai),
        .I1(\cpu_state_reg_n_0_[2] ),
        .O(\reg_sh[3]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hAAA8)) 
    \reg_sh[3]_i_4 
       (.I0(\cpu_state_reg_n_0_[2] ),
        .I1(\reg_sh_reg_n_0_[4] ),
        .I2(\reg_sh_reg_n_0_[3] ),
        .I3(\reg_sh_reg_n_0_[2] ),
        .O(\reg_sh[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF01FFFFFF00)) 
    \reg_sh[4]_i_1 
       (.I0(is_slli_srli_srai),
        .I1(\reg_sh[4]_i_2_n_0 ),
        .I2(\cpu_state_reg_n_0_[2] ),
        .I3(\reg_sh[4]_i_3_n_0 ),
        .I4(\cpu_state[7]_i_5_n_0 ),
        .I5(reg_sh1[4]),
        .O(\reg_sh[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \reg_sh[4]_i_2 
       (.I0(decoded_imm_uj[1]),
        .I1(decoded_imm_uj[2]),
        .I2(decoded_imm_uj[3]),
        .I3(decoded_imm_uj[11]),
        .I4(decoded_imm_uj[4]),
        .O(\reg_sh[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hE0E0E0E0FF000000)) 
    \reg_sh[4]_i_3 
       (.I0(\reg_sh_reg_n_0_[3] ),
        .I1(\reg_sh_reg_n_0_[2] ),
        .I2(\reg_sh_reg_n_0_[4] ),
        .I3(decoded_imm_uj[4]),
        .I4(is_slli_srli_srai),
        .I5(\cpu_state_reg_n_0_[2] ),
        .O(\reg_sh[4]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \reg_sh_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_sh[0]_i_1_n_0 ),
        .Q(\reg_sh_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_sh_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_sh[1]_i_1_n_0 ),
        .Q(\reg_sh_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_sh_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_sh[2]_i_1_n_0 ),
        .Q(\reg_sh_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_sh_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_sh[3]_i_1_n_0 ),
        .Q(\reg_sh_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \reg_sh_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\reg_sh[4]_i_1_n_0 ),
        .Q(\reg_sh_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    trap_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\cpu_state_reg_n_0_[7] ),
        .Q(trap_OBUF),
        .R(instr_and_i_1_n_0));
endmodule

(* FAST_MEMORY = "1" *) (* MEM_SIZE = "4096" *) 
(* NotValidForBitStream *)
module system
   (clk,
    resetn,
    trap,
    out_byte,
    out_byte_en);
  input clk;
  input resetn;
  output trap;
  output [7:0]out_byte;
  output out_byte_en;

  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire [13:2]mem_la_addr;
  wire [23:8]mem_la_wdata;
  wire [31:0]mem_rdata;
  wire [7:0]out_byte;
  wire [7:0]out_byte_OBUF;
  wire out_byte_en;
  wire out_byte_en01_out;
  wire out_byte_en_OBUF;
  wire p_1_in;
  wire p_2_in;
  wire picorv32_core_n_1;
  wire picorv32_core_n_10;
  wire picorv32_core_n_2;
  wire picorv32_core_n_23;
  wire picorv32_core_n_26;
  wire picorv32_core_n_27;
  wire picorv32_core_n_28;
  wire picorv32_core_n_29;
  wire picorv32_core_n_3;
  wire picorv32_core_n_30;
  wire picorv32_core_n_31;
  wire picorv32_core_n_32;
  wire picorv32_core_n_33;
  wire picorv32_core_n_34;
  wire picorv32_core_n_4;
  wire picorv32_core_n_5;
  wire picorv32_core_n_6;
  wire picorv32_core_n_7;
  wire picorv32_core_n_8;
  wire resetn;
  wire resetn_IBUF;
  wire trap;
  wire trap_OBUF;
  wire NLW_memory_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_reg_0_DBITERR_UNCONNECTED;
  wire NLW_memory_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_reg_0_SBITERR_UNCONNECTED;
  wire [31:8]NLW_memory_reg_0_DIADI_UNCONNECTED;
  wire [31:8]NLW_memory_reg_0_DIBDI_UNCONNECTED;
  wire [3:1]NLW_memory_reg_0_DIPADIP_UNCONNECTED;
  wire [3:1]NLW_memory_reg_0_DIPBDIP_UNCONNECTED;
  wire [31:0]NLW_memory_reg_0_DOADO_UNCONNECTED;
  wire [31:8]NLW_memory_reg_0_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_memory_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_reg_1_DBITERR_UNCONNECTED;
  wire NLW_memory_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_reg_1_SBITERR_UNCONNECTED;
  wire [31:8]NLW_memory_reg_1_DIADI_UNCONNECTED;
  wire [31:8]NLW_memory_reg_1_DIBDI_UNCONNECTED;
  wire [3:1]NLW_memory_reg_1_DIPADIP_UNCONNECTED;
  wire [3:1]NLW_memory_reg_1_DIPBDIP_UNCONNECTED;
  wire [31:0]NLW_memory_reg_1_DOADO_UNCONNECTED;
  wire [31:8]NLW_memory_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_reg_1_RDADDRECC_UNCONNECTED;
  wire NLW_memory_reg_2_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_reg_2_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_reg_2_DBITERR_UNCONNECTED;
  wire NLW_memory_reg_2_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_reg_2_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_reg_2_SBITERR_UNCONNECTED;
  wire [31:8]NLW_memory_reg_2_DIADI_UNCONNECTED;
  wire [31:8]NLW_memory_reg_2_DIBDI_UNCONNECTED;
  wire [3:1]NLW_memory_reg_2_DIPADIP_UNCONNECTED;
  wire [3:1]NLW_memory_reg_2_DIPBDIP_UNCONNECTED;
  wire [31:0]NLW_memory_reg_2_DOADO_UNCONNECTED;
  wire [31:8]NLW_memory_reg_2_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_reg_2_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_reg_2_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_reg_2_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_reg_2_RDADDRECC_UNCONNECTED;
  wire NLW_memory_reg_3_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_reg_3_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_reg_3_DBITERR_UNCONNECTED;
  wire NLW_memory_reg_3_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_reg_3_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_reg_3_SBITERR_UNCONNECTED;
  wire [31:8]NLW_memory_reg_3_DIADI_UNCONNECTED;
  wire [31:8]NLW_memory_reg_3_DIBDI_UNCONNECTED;
  wire [3:1]NLW_memory_reg_3_DIPADIP_UNCONNECTED;
  wire [3:1]NLW_memory_reg_3_DIPBDIP_UNCONNECTED;
  wire [31:0]NLW_memory_reg_3_DOADO_UNCONNECTED;
  wire [31:8]NLW_memory_reg_3_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_reg_3_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_reg_3_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_reg_3_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_reg_3_RDADDRECC_UNCONNECTED;

initial begin
 $sdf_annotate("system_tb_time_synth.sdf",,,,"tool_control");
end
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "131072" *) 
  (* RTL_RAM_NAME = "memory" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "4095" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "4095" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000002E5520476FE313931323B7379373EF1337),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    memory_reg_0
       (.ADDRARDADDR({1'b1,mem_la_addr,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,mem_la_addr,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_IBUF_BUFG),
        .CLKBWRCLK(clk_IBUF_BUFG),
        .DBITERR(NLW_memory_reg_0_DBITERR_UNCONNECTED),
        .DIADI({NLW_memory_reg_0_DIADI_UNCONNECTED[31:8],picorv32_core_n_1,picorv32_core_n_2,picorv32_core_n_3,picorv32_core_n_4,picorv32_core_n_5,picorv32_core_n_6,picorv32_core_n_7,picorv32_core_n_8}),
        .DIBDI({NLW_memory_reg_0_DIBDI_UNCONNECTED[31:8],1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({NLW_memory_reg_0_DIPADIP_UNCONNECTED[3:1],1'b0}),
        .DIPBDIP({NLW_memory_reg_0_DIPBDIP_UNCONNECTED[3:1],1'b0}),
        .DOADO(NLW_memory_reg_0_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_reg_0_DOBDO_UNCONNECTED[31:8],mem_rdata[7:0]}),
        .DOPADOP(NLW_memory_reg_0_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(out_byte_en01_out),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_memory_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_reg_0_SBITERR_UNCONNECTED),
        .WEA({1'b0,1'b0,1'b0,picorv32_core_n_26}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "131072" *) 
  (* RTL_RAM_NAME = "memory" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "4095" *) 
  (* bram_slice_begin = "8" *) 
  (* bram_slice_end = "15" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "4095" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "15" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h00000000000000000000000000000032292843F01E0787872046060700000141),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    memory_reg_1
       (.ADDRARDADDR({1'b1,mem_la_addr,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,mem_la_addr,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_IBUF_BUFG),
        .CLKBWRCLK(clk_IBUF_BUFG),
        .DBITERR(NLW_memory_reg_1_DBITERR_UNCONNECTED),
        .DIADI({NLW_memory_reg_1_DIADI_UNCONNECTED[31:8],mem_la_wdata[15:8]}),
        .DIBDI({NLW_memory_reg_1_DIBDI_UNCONNECTED[31:8],1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({NLW_memory_reg_1_DIPADIP_UNCONNECTED[3:1],1'b0}),
        .DIPBDIP({NLW_memory_reg_1_DIPBDIP_UNCONNECTED[3:1],1'b0}),
        .DOADO(NLW_memory_reg_1_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_reg_1_DOBDO_UNCONNECTED[31:8],mem_rdata[15:8]}),
        .DOPADOP(NLW_memory_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(out_byte_en01_out),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_memory_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_reg_1_SBITERR_UNCONNECTED),
        .WEA({1'b0,1'b0,1'b0,p_1_in}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "131072" *) 
  (* RTL_RAM_NAME = "memory" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "4095" *) 
  (* bram_slice_begin = "16" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "4095" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "23" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000002E204743DF07F71706F60F000010800100),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    memory_reg_2
       (.ADDRARDADDR({1'b1,mem_la_addr,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,mem_la_addr,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_reg_2_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_reg_2_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_IBUF_BUFG),
        .CLKBWRCLK(clk_IBUF_BUFG),
        .DBITERR(NLW_memory_reg_2_DBITERR_UNCONNECTED),
        .DIADI({NLW_memory_reg_2_DIADI_UNCONNECTED[31:8],mem_la_wdata[23:16]}),
        .DIBDI({NLW_memory_reg_2_DIBDI_UNCONNECTED[31:8],1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({NLW_memory_reg_2_DIPADIP_UNCONNECTED[3:1],1'b0}),
        .DIPBDIP({NLW_memory_reg_2_DIPBDIP_UNCONNECTED[3:1],1'b0}),
        .DOADO(NLW_memory_reg_2_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_reg_2_DOBDO_UNCONNECTED[31:8],mem_rdata[23:16]}),
        .DOPADOP(NLW_memory_reg_2_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_reg_2_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_reg_2_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(out_byte_en01_out),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_memory_reg_2_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_reg_2_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_reg_2_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_reg_2_SBITERR_UNCONNECTED),
        .WEA({1'b0,1'b0,1'b0,p_2_in}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "131072" *) 
  (* RTL_RAM_NAME = "memory" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "4095" *) 
  (* bram_slice_begin = "24" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "4095" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h00000000000000000000000000000030384E3AFEFEFF00240000100000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    memory_reg_3
       (.ADDRARDADDR({1'b1,mem_la_addr,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,mem_la_addr,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_reg_3_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_reg_3_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_IBUF_BUFG),
        .CLKBWRCLK(clk_IBUF_BUFG),
        .DBITERR(NLW_memory_reg_3_DBITERR_UNCONNECTED),
        .DIADI({NLW_memory_reg_3_DIADI_UNCONNECTED[31:8],picorv32_core_n_27,picorv32_core_n_28,picorv32_core_n_29,picorv32_core_n_30,picorv32_core_n_31,picorv32_core_n_32,picorv32_core_n_33,picorv32_core_n_34}),
        .DIBDI({NLW_memory_reg_3_DIBDI_UNCONNECTED[31:8],1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({NLW_memory_reg_3_DIPADIP_UNCONNECTED[3:1],1'b0}),
        .DIPBDIP({NLW_memory_reg_3_DIPBDIP_UNCONNECTED[3:1],1'b0}),
        .DOADO(NLW_memory_reg_3_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_reg_3_DOBDO_UNCONNECTED[31:8],mem_rdata[31:24]}),
        .DOPADOP(NLW_memory_reg_3_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_reg_3_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_reg_3_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(out_byte_en01_out),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_memory_reg_3_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_reg_3_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_reg_3_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_reg_3_SBITERR_UNCONNECTED),
        .WEA({1'b0,1'b0,1'b0,picorv32_core_n_23}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  OBUF \out_byte_OBUF[0]_inst 
       (.I(out_byte_OBUF[0]),
        .O(out_byte[0]));
  OBUF \out_byte_OBUF[1]_inst 
       (.I(out_byte_OBUF[1]),
        .O(out_byte[1]));
  OBUF \out_byte_OBUF[2]_inst 
       (.I(out_byte_OBUF[2]),
        .O(out_byte[2]));
  OBUF \out_byte_OBUF[3]_inst 
       (.I(out_byte_OBUF[3]),
        .O(out_byte[3]));
  OBUF \out_byte_OBUF[4]_inst 
       (.I(out_byte_OBUF[4]),
        .O(out_byte[4]));
  OBUF \out_byte_OBUF[5]_inst 
       (.I(out_byte_OBUF[5]),
        .O(out_byte[5]));
  OBUF \out_byte_OBUF[6]_inst 
       (.I(out_byte_OBUF[6]),
        .O(out_byte[6]));
  OBUF \out_byte_OBUF[7]_inst 
       (.I(out_byte_OBUF[7]),
        .O(out_byte[7]));
  OBUF out_byte_en_OBUF_inst
       (.I(out_byte_en_OBUF),
        .O(out_byte_en));
  FDRE #(
    .INIT(1'b0)) 
    out_byte_en_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(picorv32_core_n_10),
        .Q(out_byte_en_OBUF),
        .R(out_byte_en01_out));
  FDRE #(
    .INIT(1'b0)) 
    \out_byte_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(picorv32_core_n_10),
        .D(picorv32_core_n_8),
        .Q(out_byte_OBUF[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_byte_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(picorv32_core_n_10),
        .D(picorv32_core_n_7),
        .Q(out_byte_OBUF[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_byte_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(picorv32_core_n_10),
        .D(picorv32_core_n_6),
        .Q(out_byte_OBUF[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_byte_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(picorv32_core_n_10),
        .D(picorv32_core_n_5),
        .Q(out_byte_OBUF[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_byte_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(picorv32_core_n_10),
        .D(picorv32_core_n_4),
        .Q(out_byte_OBUF[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_byte_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(picorv32_core_n_10),
        .D(picorv32_core_n_3),
        .Q(out_byte_OBUF[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_byte_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(picorv32_core_n_10),
        .D(picorv32_core_n_2),
        .Q(out_byte_OBUF[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_byte_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(picorv32_core_n_10),
        .D(picorv32_core_n_1),
        .Q(out_byte_OBUF[7]),
        .R(1'b0));
  picorv32 picorv32_core
       (.ADDRARDADDR(mem_la_addr),
        .E(picorv32_core_n_10),
        .Q({picorv32_core_n_1,picorv32_core_n_2,picorv32_core_n_3,picorv32_core_n_4,picorv32_core_n_5,picorv32_core_n_6,picorv32_core_n_7,picorv32_core_n_8}),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .mem_rdata(mem_rdata),
        .out_byte_en01_out(out_byte_en01_out),
        .p_0_in({picorv32_core_n_23,p_2_in,p_1_in,picorv32_core_n_26}),
        .p_2_in({picorv32_core_n_27,picorv32_core_n_28,picorv32_core_n_29,picorv32_core_n_30,picorv32_core_n_31,picorv32_core_n_32,picorv32_core_n_33,picorv32_core_n_34,mem_la_wdata}),
        .resetn_IBUF(resetn_IBUF),
        .trap_OBUF(trap_OBUF));
  IBUF resetn_IBUF_inst
       (.I(resetn),
        .O(resetn_IBUF));
  OBUF trap_OBUF_inst
       (.I(trap_OBUF),
        .O(trap));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
