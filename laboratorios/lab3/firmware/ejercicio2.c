#include <stdint.h>

static void putuintAddr(uint32_t i) {
	*((volatile uint32_t *)0x10000000) = i;
}
static void putuintColor(uint32_t i) {
	*((volatile uint32_t *)0x10000004) = i;
}
static void putuintCtrl(uint32_t i) {
	*((volatile uint32_t *)0x10000008) = i;
}

/*static uint32_t resultado() {
	return *((volatile uint32_t *)0x0FFFFFF8);
}*/


void main() {


	uint32_t ROW_SIZE = 8;		//Valores para ejercicio 1
	uint32_t COL_SIZE = 8;		//Valores para ejercicio 1
	uint32_t rojo     = 0xF00;	//Valores para ejercicio 1
	uint32_t verde    = 0x0F0;	//Valores para ejercicio 1
	uint32_t fucsia   = 0xF0F;	//Valores para ejercicio 1
	uint32_t azul     = 0x00F;	//Valores para ejercicio 1
	uint32_t negro    = 0x000;	//Valores para ejercicio 2
	uint32_t blanco   = 0xFFF;	//Valores para ejercicio 2
	uint32_t Addres   = 0;		//Valores para ejercicio 1
	uint32_t Fila     = 65536;	//Valores para ejercicio 1
	uint32_t Columna  = 1;		//Valores para ejercicio 1
	uint32_t contador = 0;		//Valores para ejercicio 1
	uint32_t contador_filas = 0;		//Valores para ejercicio 2
	/*
	0 rojo
	1 verde
	2 morado
	3 azul
	*/

	while (1) {
		putuintCtrl(0);
		putuintAddr(Addres);
/*//EJERCICIO1
		switch (contador)
		{
			case 0:
				putuintColor(verde);
			break;

			case 2:
				putuintColor(rojo);
			break;

			case 4:
				putuintColor(fucsia);
			break;

			case 6:
				putuintColor(azul);
			break;
		}*/
		//EJERCICIO 2
		if(contador_filas%2==0)
			putuintColor(negro);
		else
			putuintColor(blanco);
		if(Addres < 524288){				
			if((Addres&65535)!=COL_SIZE-1){
				Addres=Addres+Columna;
				contador_filas++;				//EJERCICIO2
			}
			else{
				Addres=Addres-(COL_SIZE-1);
				Addres=Addres+Fila;
				//contador++;				//EJERCICIO1
			}
			/*PONER ADRES EN 0 Y CONTADOR EN 0*/
			putuintCtrl(1);
		}
	}
}

 
