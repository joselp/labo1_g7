
module cuadro_color(
    input 		               clk, 	
    input 		               reset, 	
    input     [2:0]            columna_in,
    input     [2:0 ]           fila_in,
    input     [11:0]           color_in,
    input                      visible,
    input 		               up, 	
    input 		               down, 	
    input 		               left, 	
    input 		               rigth, 			
    output     reg   [11:0]    color_out
);
    wire [3:0] bus_botones;
    assign bus_botones={up,down,left,rigth};

    
    
    //Cuadro Rojo
    //contadores fila columna donde se ubica el cuadro rojo
    reg [2:0] fila;
    reg [2:0] columna;
    reg [11:0] color_predeterminado;
    reg cambio_hecho;
    always@(posedge clk)
    begin
        if(!reset)
        begin
            fila<=0;
            columna<=0;
            color_predeterminado<=12'hF12;
            color_out<=0;
            cambio_hecho<=0;
        end
        else
        begin
            if(bus_botones==0)
                cambio_hecho<=0;
            if (bus_botones[0] && !cambio_hecho) begin
                columna=columna+1;
                cambio_hecho<=1;
            end
            if (bus_botones[1] && !cambio_hecho) begin
                columna=columna-1;
                cambio_hecho<=1;
            end
            if (bus_botones[2] && !cambio_hecho) begin
                fila=fila+1;
                cambio_hecho<=1;
            end
            if (bus_botones[3] && !cambio_hecho) begin
                fila=fila-1;
                cambio_hecho<=1;
            end

            if((fila==fila_in) && (columna==columna_in))
                if(visible)
                    color_out<=color_predeterminado;
                else
                    color_out<=0;
            else
                if(visible)
                    color_out<=color_in;
                else
                    color_out<=0;
                
        end
    end
  
endmodule

