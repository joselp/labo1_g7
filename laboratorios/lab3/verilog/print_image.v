

module print_image (
    input      [15:0] column,
    input      [15:0] row,
    input      [11:0] data_mem,
    input             visible,
    output reg [5:0]  addr_mem,
    output reg [3:0]  red,
    output reg [3:0]  green,
    output reg [3:0]  blue
);
    parameter SIZE = 8;
    reg [2:0] column_out;
    reg [2:0] row_out;

    always @(*) begin
        column_out = column/80;
        row_out    = row/60;  
        addr_mem   = {row_out,column_out};

        if(!visible)
        begin
            red   =     0;
            green =     0;
            blue  =     0;
        end
        else
        begin
            red   = data_mem[11:8];
            green = data_mem[7:4];
            blue  = data_mem[3:0];
        end
    end    

endmodule