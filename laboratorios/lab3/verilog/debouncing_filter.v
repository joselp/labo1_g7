

module debouncing_filter(
    input      clk,
    input      reset,
    input      signal,
    output reg filtered_signal
);

    wire clk_div;
	clock_div #(
	   .DIV      (4)
	)clk_div_25M(
	   .clk      (clk),
	   .reset    (reset),
	   .clk_div  (clk_div)
	);

    reg ff0, ff1;
    always @(posedge clk_div) begin
        if (~reset) begin
            ff0             <= 0;
            ff1             <= 0;
            filtered_signal <= 0;
        end else begin
            ff0             <= signal;
            ff1             <= ff0;
            filtered_signal <= ff0 & ff1;
        end
    end
endmodule

