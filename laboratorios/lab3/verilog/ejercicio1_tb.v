`timescale 1 ns / 1 ps

module ejercicio1_tb;
	reg clk = 1;
	reg up = 0;
	reg down= 0;
	reg rigth = 0;
	reg left = 0;
	reg [2:0]columna_in= 1;
	reg [2:0] fila_in = 1;
	reg [11:0] color_in = 12'hFFF;
	always #5 clk = ~clk;
    
    always #30 columna_in=columna_in+1;
    always #240 fila_in=fila_in+1;
	reg resetn = 0;
	initial begin
		if ($test$plusargs("vcd")) begin
			$dumpfile("ejercicio1.vcd");
			$dumpvars(0, ejercicio1_tb);
		end
		repeat (100) @(posedge clk);
		resetn <= 1;
		repeat (100) @(posedge clk);
		resetn <= 0;
		repeat (100) @(posedge clk);
		resetn <= 1;

		
		repeat(500) #3 rigth=~rigth;
		repeat(500) #3 rigth=~rigth;
		
		repeat (600) @(posedge clk);

		repeat(500) #3 down=~down;
		repeat(500) #3 down=~down;
		
		repeat (600) @(posedge clk);

		repeat(500) #3 left=~left;
		repeat(500) #3 left=~left;
		
		repeat (600) @(posedge clk);

		repeat(500) #3 up=~up;
		repeat(500) #3 up=~up;
		
	end
   


	wire trap;
	wire [3:0] red, green, blue;
	wire hsync, vsync;
    wire [11:0] color_out;
	ejercicio1 uut (
		.clk        (clk        ),
		.resetn     (resetn     ),
		.trap       (trap       ),
		.red        (red        ),
	    .green      (green      ),
	    .blue       (blue       ),
	    .hsync      (hsync      ),
	    .vsync      (vsync      )		
	);



	always @(posedge clk) begin
		if (resetn && trap) begin
			$finish;
		end
	end
endmodule
