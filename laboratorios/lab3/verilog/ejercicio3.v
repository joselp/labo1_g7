module ejercicio3 (
	input            clk,
	input            resetn,
	input            up,
	input            down,
	input            right,
	input            left,
	input            center,
	input            kclk,
	input            kdata,
	output           trap,
	output reg prueba,
	output [3:0] red,
	output [3:0] green,
	output [3:0] blue,
	output hsync,
	output vsync
);
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 1;

	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 4096;
    
	wire mem_valid;
	wire mem_instr;
	reg  mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0]  mem_wstrb;
	reg  [31:0] mem_rdata;

	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0]  mem_la_wstrb;
	
    reg [6:0] LED_0;
    reg [6:0] LED_1;
    reg [6:0] LED_2;
    reg [7:0] active_leds;
    reg [7:0] res_Sec;
    
    
    reg  [31:0]  Addr;
	reg  [11:0]  Color;
	reg          Ctrl;
	wire [11:0]  data_mem;
    wire [5:0]   addr_mem;
    wire [15:0]  column, row;
    wire visible;
	
	picorv32 picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);

	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("/home/joselp/inicio-ie424/src/firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			mem_ready <= 1;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end else
			     if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
				    Addr <= mem_la_wdata;
			     end
			     if (mem_la_write && mem_la_addr == 32'h1000_0004) begin
				    Color <= mem_la_wdata;                                              
			     end
			     if (mem_la_write && mem_la_addr == 32'h1000_0008) begin
				    Ctrl <= mem_la_wdata;
			     end
			end    
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
					
					mem_ready <= 1;
				end
			endcase
			
		end
	end endgenerate
	
	video_memory #(12,6,8*8) videoMemory
	(
		.Clock         (clk),
		.iWriteEnable  (Ctrl),
		.iReadAddress  (addr_mem),
		.iWriteAddress ({Addr[18:16],Addr[2:0]}),
		.iDataIn       (Color),
		.oDataOut      (data_mem)
    );
	
	synchronizer_vga sync_0(
        .clk      (clk),
        .reset    (resetn),
        .visible  (visible),
        .vsync    (vsync),
        .hsync    (hsync),
        .column   (column),
        .row      (row) 
	);
	wire [3:0]red_i,green_i,blue_i;
	print_image print_image
	(
		.column   (column),
		.row      (row),
		.data_mem (data_mem),
		.visible  (visible),
		.addr_mem (addr_mem),
		.red      (red_i),
		.green    (green_i),
		.blue     (blue_i)
    );
    
    
    wire up_keyboard, down_keyboard, left_keyboard, right_keyboard;
    wire [31:0] keycode;
    cuadro_color cuadro_color(
    .clk 	            (clk),
    .reset          	(resetn),
    .columna_in         (addr_mem[2:0]),
    .fila_in            (addr_mem[5:3]),
    .color_in           ({red_i,green_i,blue_i}),
    .visible            (visible),
    .up 	            (up_keyboard),
    .down 	            (down_keyboard),
    .left 	            (left_keyboard),
    .rigth 	            (right_keyboard),
    .color_out          ({red,green,blue})
);
    
    wire clk_reducido;
    
    clock_div  #(
    .DIV  (128))clock_div(
    .clk(clk),
    .reset(resetn),
   .clk_div(clk_reducido)
);
    
    
    PS2Receiver PS2Receiver(
    .clk(clk_reducido),
    .kclk(kclk),
    .kdata(kdata),
    .keycodeout(keycode)
    );
    
    keyboard keyboard(
    .keycode(keycode),
    .up(up_keyboard),
    .down(down_keyboard),
    .left(left_keyboard),
    .right(right_keyboard)

);

    
	/*
	wire up_filtered, down_filtered, left_filtered, right_filtered;
	cuadro_color cuadro_color(
    .clk 	            (clk),
    .reset          	(resetn),
    .columna_in         (addr_mem[2:0]),
    .fila_in            (addr_mem[5:3]),
    .color_in           ({red_i,green_i,blue_i}),
    .visible            (visible),
    .up 	            (up_filtered),
    .down 	            (down_filtered),
    .left 	            (left_filtered),
    .rigth 	            (right_filtered),
    .color_out          ({red,green,blue})
);
*/

 /*
anti_rebote anti_rebote(
    .clk                (clk), 	
    .reset              (resetn), 	
    .up_in              (up),
    .down_in            (down),
    .left_in            (left),
    .right_in           (right),		
    .up_out             (up_filtered),		
    .down_out           (down_filtered),		
    .left_out           (left_filtered),		
    .right_out          (right_filtered)
);
*/

    always@(center)
        begin
        if(center)
            prueba<=1;
         else
            prueba<=0;
    end
endmodule
