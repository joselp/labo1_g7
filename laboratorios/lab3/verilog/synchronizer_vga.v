
module synchronizer_vga (
    input             clk,
    input             reset,
    output reg        visible,
    output reg        vsync,
    output reg        hsync,
    output reg [15:0] column,
    output reg [15:0] row
);

    wire clk_25M;
	clock_div #(
	   .DIV      (4)
	)clk_div_25M(
	   .clk      (clk),
	   .reset    (reset),
	   .clk_div  (clk_25M)
	);
	
	reg [31:0] hsync_counter;
	reg [31:0] vsync_counter;
	
	reg en_hsync, en_vsync;
	
	always @(*) begin
	   if(en_vsync && en_hsync) begin
	       visible = 1; 
	   end else begin
	       visible = 0;
	   end
	end
	
	always @(posedge clk_25M) begin
	   if(~reset) begin
	       hsync     <= 1;
	       vsync     <= 0;
	       hsync_counter <= 0;
	       vsync_counter <= 0;
	       en_hsync  <= 0;
	       en_vsync  <= 0;
	       column    <= 0;
	       row       <= 0;
	   end else begin
	   
	   
	       if(en_vsync && en_hsync) begin
	   	       if(column == 639) begin
	   	           en_hsync    <= 0;
	               column      <= 0;
	               if(row == 479) begin
	                   en_vsync  <= 0;
	                   en_hsync  <= 0;
	                   row       <= 0;
	               end else begin
	                   row <= row +1; 
	               end
	           end else begin 
	               column = column +1;
	           end 
	       end
	      
	       if(~en_hsync) begin
	           if(hsync_counter  <= 158) begin
	               hsync_counter <= hsync_counter + 1;
	               if(hsync_counter == 15) begin  // front porch
	                   hsync         <= 0;
	               end
	               if(hsync_counter == 111) begin // pulse width
	                   hsync         <= 1;
	               end 
	           end else begin                     // back porch                     
	               hsync_counter   <= 0;
	               en_hsync        <= 1;
	           end
	       end
	        
	       if(~en_vsync) begin
	       	   if(en_hsync)begin
	       	       hsync_counter <= hsync_counter + 1;
	       	       if(hsync_counter == 639) begin
	       	           hsync_counter <= 0;
	                   en_hsync      <= 0;
	       	       end
	           end
	           if(vsync_counter <= 32958) begin
	               vsync_counter <= vsync_counter + 1;
	               if(vsync_counter == 7999) begin  // front porch
	                   vsync         <= 0;
	               end
	               if(vsync_counter == 9599) begin  // pulse width
	                   vsync         <= 1;
	               end 
	           end else begin                       // back porch                     
	               vsync_counter   <= 0; 
	               en_vsync        <= 1;
	           end
	       end
	   end
	end
	
endmodule