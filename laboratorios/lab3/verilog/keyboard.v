
module keyboard(
    input 		       [31:0]        keycode,
    output reg                       up,
    output reg                       down,
    output reg                       left,
    output reg                       right

);
   
    always@(*)
    begin
        if(keycode[7:0]==8'h1D && keycode[15:8]!=8'hF0)
            up=1;
        else
            up=0;
        
        if(keycode[7:0]==8'h1B && keycode[15:8]!=8'hF0)
            down=1;
        else
            down=0;

        if(keycode[7:0]==8'h1C && keycode[15:8]!=8'hF0)
            left=1;
        else
            left=0;

        if(keycode[7:0]==8'h23 && keycode[15:8]!=8'hF0)
            right=1;
        else
            right=0;
    end
endmodule

